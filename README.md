# TSEC eVote

*TSEC eVote* or NaaVote is an eVoting solution developed by [Telangana State Election Commission (TSEC)](https://tsec.gov.in/home.do) to be piloted partnering with the [Emerging Technologies wing](@EmergingTechTS) of the [Telangana Information Technology (IT) Department](https://it.telangana.gov.in/) and receiving technical support from the [Centre for Development of Advanced Computing - CDAC](https://cdac.in/) for the project to be undertaken in Khammam district.

Professors from IIT Bombay, IIT Bhilai and IIT Delhi, and an expert committee that includes Professor Rajat Moona, director IIT Bhilai, have guided the development of the App. Moona is also the technical adviser to the Election Commission.


. 
## Archive

This project intends to archive all technical artefacts related to TSEC eVote technology solution to help researchers analyse the same.

* apk/ - This folder will contains archives all versions of application
* docs/ - All documents related to TSEC eVoting solution
* google_playstore_data/ - All metadata obtained from [Google Play API](https://gplayapi.herokuapp.com/)
* images/ - All images related to app / solution.
* src/ - This folder will contain the reversed source code of Android applications. [MobSF](https://github.com/MobSF/Mobile-Security-Framework-MobSF) is used to reverse the app and get the source code.

## Presentation

The analysis was presented at [OneVote Project](https://hasgeek.com/onevote)
- [Slides](https://docs.google.com/presentation/d/1pwx7bBVJaWkolU1zwgjSYOHdn3wGa6D1jv-_gtmAN3Q/edit?usp=sharing)
- [Youtube](https://www.youtube.com/watch?v=ynKKvli1QAc)

## Disclaimer

We do not take any liability for any vulnerabilities exposed in the artefacts stored. If you find any vulnerabilities, please contact TSEC via [Email](jd-tsec@telangana.gov.in) and notify the same.
