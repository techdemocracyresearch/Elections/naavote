package me.furtado.smsretriever;

import android.content.Context;
import android.content.pm.PackageManager;
import com.google.android.gms.common.GoogleApiAvailability;

final class GooglePlayServicesHelper {
    private static final int MINIMAL_VERSION = 10200000;
    static final String UNAVAILABLE_ERROR_MESSAGE = "Google Play Services is not available.";
    static final String UNAVAILABLE_ERROR_TYPE = "UNAVAILABLE_ERROR_TYPE";
    static final String UNSUPORTED_VERSION_ERROR_MESSAGE = "The device version of Google Play Services is not supported.";
    static final String UNSUPORTED_VERSION_ERROR_TYPE = "UNSUPORTED_VERSION_ERROR_TYPE";

    private GooglePlayServicesHelper() {
    }

    static boolean isAvailable(Context context) {
        int isGooglePlayServicesAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        return (isGooglePlayServicesAvailable == 1 || isGooglePlayServicesAvailable == 3 || isGooglePlayServicesAvailable == 9) ? false : true;
    }

    static boolean hasSupportedVersion(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode >= MINIMAL_VERSION) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}
