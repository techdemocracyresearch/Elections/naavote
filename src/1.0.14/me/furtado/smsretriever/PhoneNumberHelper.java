package me.furtado.smsretriever;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class PhoneNumberHelper {
    private static final String ACTIVITY_NULL_ERROR_MESSAGE = "Activity is null.";
    private static final String ACTIVITY_NULL_ERROR_TYPE = "ACTIVITY_NULL_ERROR_TYPE";
    private static final String ACTIVITY_RESULT_NOOK_ERROR_MESSAGE = "There was an error trying to get the phone number.";
    private static final String ACTIVITY_RESULT_NOOK_ERROR_TYPE = "ACTIVITY_RESULT_NOOK_ERROR_TYPE";
    private static final String CONNECTION_FAILED_ERROR_MESSAGE = "There was an error connecting the client to the service.";
    private static final String CONNECTION_FAILED_ERROR_TYPE = "CONNECTION_FAILED_ERROR_TYPE";
    private static final String CONNECTION_SUSPENDED_ERROR_TYPE = "CONNECTION_SUSPENDED_ERROR_TYPE";
    private static final String CONNECTION_SUSPENENDED_ERROR_MESSAGE = "Client is temporarily in a disconnected state.";
    private static final int REQUEST_PHONE_NUMBER_REQUEST_CODE = 1;
    private static final String SEND_INTENT_ERROR_MESSAGE = "There was an error trying to send intent.";
    private static final String SEND_INTENT_ERROR_TYPE = "SEND_INTENT_ERROR_TYPE";
    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        /* class me.furtado.smsretriever.PhoneNumberHelper.AnonymousClass3 */

        @Override // com.facebook.react.bridge.BaseActivityEventListener, com.facebook.react.bridge.ActivityEventListener
        public void onActivityResult(Activity activity, int i, int i2, Intent intent) {
            super.onActivityResult(activity, i, i2, intent);
            if (i == 1 && i2 == -1) {
                PhoneNumberHelper.this.promiseResolve(((Credential) intent.getParcelableExtra(Credential.EXTRA_KEY)).getId());
                PhoneNumberHelper.this.callAndResetListener();
                return;
            }
            PhoneNumberHelper.this.promiseReject(PhoneNumberHelper.ACTIVITY_RESULT_NOOK_ERROR_TYPE, PhoneNumberHelper.ACTIVITY_RESULT_NOOK_ERROR_MESSAGE);
            PhoneNumberHelper.this.callAndResetListener();
        }
    };
    private final GoogleApiClient.ConnectionCallbacks mApiClientConnectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
        /* class me.furtado.smsretriever.PhoneNumberHelper.AnonymousClass1 */

        @Override // com.google.android.gms.common.api.internal.ConnectionCallbacks
        public void onConnected(Bundle bundle) {
        }

        @Override // com.google.android.gms.common.api.internal.ConnectionCallbacks
        public void onConnectionSuspended(int i) {
            PhoneNumberHelper.this.promiseReject(PhoneNumberHelper.CONNECTION_SUSPENDED_ERROR_TYPE, PhoneNumberHelper.CONNECTION_SUSPENENDED_ERROR_MESSAGE);
            PhoneNumberHelper.this.callAndResetListener();
        }
    };
    private final GoogleApiClient.OnConnectionFailedListener mApiClientOnConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        /* class me.furtado.smsretriever.PhoneNumberHelper.AnonymousClass2 */

        @Override // com.google.android.gms.common.api.internal.OnConnectionFailedListener
        public void onConnectionFailed(ConnectionResult connectionResult) {
            PhoneNumberHelper.this.promiseReject(PhoneNumberHelper.CONNECTION_FAILED_ERROR_TYPE, PhoneNumberHelper.CONNECTION_FAILED_ERROR_MESSAGE);
            PhoneNumberHelper.this.callAndResetListener();
        }
    };
    private GoogleApiClient mGoogleApiClient;
    private Listener mListener;
    private Promise mPromise;

    public interface Listener {
        void phoneNumberResultReceived();
    }

    PhoneNumberHelper() {
    }

    /* access modifiers changed from: package-private */
    public ActivityEventListener getActivityEventListener() {
        return this.mActivityEventListener;
    }

    /* access modifiers changed from: package-private */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    /* access modifiers changed from: package-private */
    public void requestPhoneNumber(Context context, Activity activity, Promise promise) {
        if (promise == null) {
            callAndResetListener();
            return;
        }
        this.mPromise = promise;
        if (!GooglePlayServicesHelper.isAvailable(context)) {
            promiseReject("UNAVAILABLE_ERROR_TYPE", "Google Play Services is not available.");
            callAndResetListener();
        } else if (!GooglePlayServicesHelper.hasSupportedVersion(context)) {
            promiseReject("UNSUPORTED_VERSION_ERROR_TYPE", "The device version of Google Play Services is not supported.");
            callAndResetListener();
        } else if (activity == null) {
            promiseReject(ACTIVITY_NULL_ERROR_TYPE, ACTIVITY_NULL_ERROR_MESSAGE);
            callAndResetListener();
        } else {
            HintRequest build = new HintRequest.Builder().setPhoneNumberIdentifierSupported(true).build();
            try {
                activity.startIntentSenderForResult(Auth.CredentialsApi.getHintPickerIntent(getGoogleApiClient(context, activity), build).getIntentSender(), 1, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException unused) {
                promiseReject(SEND_INTENT_ERROR_TYPE, SEND_INTENT_ERROR_MESSAGE);
                callAndResetListener();
            }
        }
    }

    private GoogleApiClient getGoogleApiClient(Context context, Activity activity) {
        if (this.mGoogleApiClient == null) {
            GoogleApiClient.Builder addApi = new GoogleApiClient.Builder(context).addConnectionCallbacks(this.mApiClientConnectionCallbacks).addApi(Auth.CREDENTIALS_API);
            if (activity instanceof FragmentActivity) {
                addApi = addApi.enableAutoManage((FragmentActivity) activity, this.mApiClientOnConnectionFailedListener);
            }
            this.mGoogleApiClient = addApi.build();
        }
        return this.mGoogleApiClient;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void callAndResetListener() {
        Listener listener = this.mListener;
        if (listener != null) {
            listener.phoneNumberResultReceived();
            this.mListener = null;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void promiseResolve(Object obj) {
        Promise promise = this.mPromise;
        if (promise != null) {
            promise.resolve(obj);
            this.mPromise = null;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void promiseReject(String str, String str2) {
        Promise promise = this.mPromise;
        if (promise != null) {
            promise.reject(str, str2);
            this.mPromise = null;
        }
    }
}
