package com.sun.mail.pop3;

import com.sun.mail.util.LineInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.FolderNotFoundException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.MethodNotSupportedException;
import javax.mail.UIDFolder;

public class POP3Folder extends Folder {
    private boolean doneUidl = false;
    private boolean exists = false;
    private Vector message_cache;
    private String name;
    private boolean opened = false;
    private Protocol port;
    private int size;
    private int total;

    @Override // javax.mail.Folder
    public boolean create(int i) throws MessagingException {
        return false;
    }

    @Override // javax.mail.Folder
    public char getSeparator() {
        return 0;
    }

    @Override // javax.mail.Folder
    public int getType() {
        return 1;
    }

    @Override // javax.mail.Folder
    public boolean hasNewMessages() throws MessagingException {
        return false;
    }

    POP3Folder(POP3Store pOP3Store, String str) {
        super(pOP3Store);
        this.name = str;
        if (str.equalsIgnoreCase("INBOX")) {
            this.exists = true;
        }
    }

    @Override // javax.mail.Folder
    public String getName() {
        return this.name;
    }

    @Override // javax.mail.Folder
    public String getFullName() {
        return this.name;
    }

    @Override // javax.mail.Folder
    public Folder getParent() {
        return new DefaultFolder((POP3Store) this.store);
    }

    @Override // javax.mail.Folder
    public boolean exists() {
        return this.exists;
    }

    @Override // javax.mail.Folder
    public Folder[] list(String str) throws MessagingException {
        throw new MessagingException("not a directory");
    }

    @Override // javax.mail.Folder
    public Folder getFolder(String str) throws MessagingException {
        throw new MessagingException("not a directory");
    }

    @Override // javax.mail.Folder
    public boolean delete(boolean z) throws MessagingException {
        throw new MethodNotSupportedException("delete");
    }

    @Override // javax.mail.Folder
    public boolean renameTo(Folder folder) throws MessagingException {
        throw new MethodNotSupportedException("renameTo");
    }

    @Override // javax.mail.Folder
    public synchronized void open(int i) throws MessagingException {
        POP3Store pOP3Store;
        checkClosed();
        if (this.exists) {
            try {
                Protocol port2 = ((POP3Store) this.store).getPort(this);
                this.port = port2;
                Status stat = port2.stat();
                this.total = stat.total;
                this.size = stat.size;
                this.mode = i;
                this.opened = true;
                Vector vector = new Vector(this.total);
                this.message_cache = vector;
                vector.setSize(this.total);
                this.doneUidl = false;
                notifyConnectionListeners(1);
            } catch (IOException e) {
                try {
                    Protocol protocol = this.port;
                    if (protocol != null) {
                        protocol.quit();
                    }
                    this.port = null;
                    pOP3Store = (POP3Store) this.store;
                } catch (IOException unused) {
                    this.port = null;
                    pOP3Store = (POP3Store) this.store;
                } catch (Throwable th) {
                    this.port = null;
                    ((POP3Store) this.store).closePort(this);
                    throw th;
                }
                pOP3Store.closePort(this);
                throw new MessagingException("Open failed", e);
            }
        } else {
            throw new FolderNotFoundException(this, "folder is not INBOX");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003f, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0047, code lost:
        throw new javax.mail.MessagingException("Exception deleting messages during close", r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0061, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0062, code lost:
        r5.port = null;
        ((com.sun.mail.pop3.POP3Store) r5.store).closePort(r5);
        r5.message_cache = null;
        r5.opened = false;
        notifyConnectionListeners(3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0072, code lost:
        throw r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[ExcHandler: all (r6v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x0007] */
    @Override // javax.mail.Folder
    public synchronized void close(boolean z) throws MessagingException {
        checkOpen();
        try {
            if (((POP3Store) this.store).rsetBeforeQuit) {
                this.port.rset();
            }
            if (z && this.mode == 2) {
                for (int i = 0; i < this.message_cache.size(); i++) {
                    POP3Message pOP3Message = (POP3Message) this.message_cache.elementAt(i);
                    if (pOP3Message != null && pOP3Message.isSet(Flags.Flag.DELETED)) {
                        this.port.dele(i + 1);
                    }
                }
            }
            this.port.quit();
            this.port = null;
            ((POP3Store) this.store).closePort(this);
            this.message_cache = null;
            this.opened = false;
        } catch (IOException unused) {
            this.port = null;
            ((POP3Store) this.store).closePort(this);
            this.message_cache = null;
            this.opened = false;
        } catch (Throwable th) {
        }
        notifyConnectionListeners(3);
    }

    @Override // javax.mail.Folder
    public boolean isOpen() {
        if (!this.opened) {
            return false;
        }
        if (this.store.isConnected()) {
            return true;
        }
        try {
            close(false);
        } catch (MessagingException unused) {
        }
        return false;
    }

    @Override // javax.mail.Folder
    public Flags getPermanentFlags() {
        return new Flags();
    }

    @Override // javax.mail.Folder
    public synchronized int getMessageCount() throws MessagingException {
        if (!this.opened) {
            return -1;
        }
        checkReadable();
        return this.total;
    }

    @Override // javax.mail.Folder
    public synchronized Message getMessage(int i) throws MessagingException {
        POP3Message pOP3Message;
        checkOpen();
        int i2 = i - 1;
        pOP3Message = (POP3Message) this.message_cache.elementAt(i2);
        if (pOP3Message == null) {
            pOP3Message = createMessage(this, i);
            this.message_cache.setElementAt(pOP3Message, i2);
        }
        return pOP3Message;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0020  */
    public POP3Message createMessage(Folder folder, int i) throws MessagingException {
        POP3Message pOP3Message;
        Constructor constructor = ((POP3Store) this.store).messageConstructor;
        if (constructor != null) {
            try {
                pOP3Message = (POP3Message) constructor.newInstance(this, new Integer(i));
            } catch (Exception unused) {
            }
            return pOP3Message != null ? new POP3Message(this, i) : pOP3Message;
        }
        pOP3Message = null;
        if (pOP3Message != null) {
        }
    }

    @Override // javax.mail.Folder
    public void appendMessages(Message[] messageArr) throws MessagingException {
        throw new MethodNotSupportedException("Append not supported");
    }

    @Override // javax.mail.Folder
    public Message[] expunge() throws MessagingException {
        throw new MethodNotSupportedException("Expunge not supported");
    }

    @Override // javax.mail.Folder
    public synchronized void fetch(Message[] messageArr, FetchProfile fetchProfile) throws MessagingException {
        checkReadable();
        if (!this.doneUidl && fetchProfile.contains(UIDFolder.FetchProfileItem.UID)) {
            int size2 = this.message_cache.size();
            String[] strArr = new String[size2];
            try {
                if (this.port.uidl(strArr)) {
                    for (int i = 0; i < size2; i++) {
                        if (strArr[i] != null) {
                            ((POP3Message) getMessage(i + 1)).uid = strArr[i];
                        }
                    }
                    this.doneUidl = true;
                } else {
                    return;
                }
            } catch (EOFException e) {
                close(false);
                throw new FolderClosedException(this, e.toString());
            } catch (IOException e2) {
                throw new MessagingException("error getting UIDL", e2);
            }
        }
        if (fetchProfile.contains(FetchProfile.Item.ENVELOPE)) {
            for (int i2 = 0; i2 < messageArr.length; i2++) {
                try {
                    POP3Message pOP3Message = (POP3Message) messageArr[i2];
                    pOP3Message.getHeader("");
                    pOP3Message.getSize();
                } catch (MessageRemovedException unused) {
                }
            }
        }
    }

    public synchronized String getUID(Message message) throws MessagingException {
        POP3Message pOP3Message;
        checkOpen();
        pOP3Message = (POP3Message) message;
        try {
            if (pOP3Message.uid == "UNKNOWN") {
                pOP3Message.uid = this.port.uidl(pOP3Message.getMessageNumber());
            }
        } catch (EOFException e) {
            close(false);
            throw new FolderClosedException(this, e.toString());
        } catch (IOException e2) {
            throw new MessagingException("error getting UIDL", e2);
        }
        return pOP3Message.uid;
    }

    public synchronized int getSize() throws MessagingException {
        checkOpen();
        return this.size;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001f, code lost:
        if (r2 == null) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0063, code lost:
        if (r2 == null) goto L_0x0066;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0014 */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0050 A[SYNTHETIC, Splitter:B:31:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0057 A[SYNTHETIC, Splitter:B:35:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x005e A[SYNTHETIC, Splitter:B:43:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0014 A[LOOP:0: B:8:0x0014->B:53:0x0014, LOOP_START, SYNTHETIC, Splitter:B:8:0x0014] */
    public synchronized int[] getSizes() throws MessagingException {
        int[] iArr;
        InputStream inputStream;
        Throwable th;
        checkOpen();
        iArr = new int[this.total];
        LineInputStream lineInputStream = null;
        try {
            inputStream = this.port.list();
            try {
                LineInputStream lineInputStream2 = new LineInputStream(inputStream);
                while (true) {
                    try {
                        String readLine = lineInputStream2.readLine();
                        if (readLine == null) {
                            try {
                                break;
                            } catch (IOException unused) {
                            }
                        } else {
                            StringTokenizer stringTokenizer = new StringTokenizer(readLine);
                            int parseInt = Integer.parseInt(stringTokenizer.nextToken());
                            int parseInt2 = Integer.parseInt(stringTokenizer.nextToken());
                            if (parseInt > 0 && parseInt <= this.total) {
                                iArr[parseInt - 1] = parseInt2;
                            }
                        }
                    } catch (IOException unused2) {
                        lineInputStream = lineInputStream2;
                        if (lineInputStream != null) {
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        lineInputStream = lineInputStream2;
                        if (lineInputStream != null) {
                        }
                        if (inputStream != null) {
                        }
                        throw th;
                    }
                }
                lineInputStream2.close();
            } catch (IOException unused3) {
                if (lineInputStream != null) {
                    try {
                        lineInputStream.close();
                    } catch (IOException unused4) {
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                if (lineInputStream != null) {
                    try {
                        lineInputStream.close();
                    } catch (IOException unused5) {
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused6) {
                    }
                }
                throw th;
            }
        } catch (IOException unused7) {
            inputStream = null;
            if (lineInputStream != null) {
            }
        } catch (Throwable th4) {
            th = th4;
            inputStream = null;
            if (lineInputStream != null) {
            }
            if (inputStream != null) {
            }
            throw th;
        }
        return iArr;
        return iArr;
    }

    public synchronized InputStream listCommand() throws MessagingException, IOException {
        checkOpen();
        return this.port.list();
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Folder
    public void finalize() throws Throwable {
        super.finalize();
        close(false);
    }

    /* access modifiers changed from: package-private */
    public void checkOpen() throws IllegalStateException {
        if (!this.opened) {
            throw new IllegalStateException("Folder is not Open");
        }
    }

    /* access modifiers changed from: package-private */
    public void checkClosed() throws IllegalStateException {
        if (this.opened) {
            throw new IllegalStateException("Folder is Open");
        }
    }

    /* access modifiers changed from: package-private */
    public void checkReadable() throws IllegalStateException {
        if (!this.opened || !(this.mode == 1 || this.mode == 2)) {
            throw new IllegalStateException("Folder is not Readable");
        }
    }

    /* access modifiers changed from: package-private */
    public void checkWritable() throws IllegalStateException {
        if (!this.opened || this.mode != 2) {
            throw new IllegalStateException("Folder is not Writable");
        }
    }

    /* access modifiers changed from: package-private */
    public Protocol getProtocol() throws MessagingException {
        checkOpen();
        return this.port;
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Folder
    public void notifyMessageChangedListeners(int i, Message message) {
        super.notifyMessageChangedListeners(i, message);
    }
}
