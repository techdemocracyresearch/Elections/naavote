package com.sun.mail.smtp;

import com.sun.mail.util.CRLFOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class SMTPOutputStream extends CRLFOutputStream {
    @Override // java.io.OutputStream, java.io.FilterOutputStream, java.io.Flushable
    public void flush() {
    }

    public SMTPOutputStream(OutputStream outputStream) {
        super(outputStream);
    }

    @Override // com.sun.mail.util.CRLFOutputStream, java.io.OutputStream, java.io.FilterOutputStream
    public void write(int i) throws IOException {
        if ((this.lastb == 10 || this.lastb == 13 || this.lastb == -1) && i == 46) {
            this.out.write(46);
        }
        super.write(i);
    }

    @Override // com.sun.mail.util.CRLFOutputStream, java.io.OutputStream, java.io.FilterOutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3 = i2 + i;
        byte b = (this.lastb == -1 ? 10 : this.lastb) == 1 ? 1 : 0;
        int i4 = i;
        while (i < i3) {
            if ((b == 10 || b == 13) && bArr[i] == 46) {
                super.write(bArr, i4, i - i4);
                this.out.write(46);
                i4 = i;
            }
            b = bArr[i];
            i++;
        }
        int i5 = i3 - i4;
        if (i5 > 0) {
            super.write(bArr, i4, i5);
        }
    }

    public void ensureAtBOL() throws IOException {
        if (!this.atBOL) {
            super.writeln();
        }
    }
}
