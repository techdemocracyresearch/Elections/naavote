package com.google.firebase.ml.vision.document;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzkl;
import com.google.android.gms.internal.firebase_ml.zzkz;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqh;
import com.google.android.gms.internal.firebase_ml.zzrr;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionDocumentTextRecognizer extends zzrr<FirebaseVisionDocumentText> {
    private static final Map<zzqh<FirebaseVisionCloudDocumentRecognizerOptions>, FirebaseVisionDocumentTextRecognizer> zzbim = new HashMap();

    public static synchronized FirebaseVisionDocumentTextRecognizer zza(zzqf zzqf, FirebaseVisionCloudDocumentRecognizerOptions firebaseVisionCloudDocumentRecognizerOptions) {
        FirebaseVisionDocumentTextRecognizer firebaseVisionDocumentTextRecognizer;
        synchronized (FirebaseVisionDocumentTextRecognizer.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            Preconditions.checkNotNull(firebaseVisionCloudDocumentRecognizerOptions, "Options must not be null");
            zzqh<FirebaseVisionCloudDocumentRecognizerOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionCloudDocumentRecognizerOptions);
            Map<zzqh<FirebaseVisionCloudDocumentRecognizerOptions>, FirebaseVisionDocumentTextRecognizer> map = zzbim;
            firebaseVisionDocumentTextRecognizer = map.get(zzj);
            if (firebaseVisionDocumentTextRecognizer == null) {
                zzkz zzkz = new zzkz();
                zzkz.zzd(firebaseVisionCloudDocumentRecognizerOptions.getHintedLanguages());
                FirebaseVisionDocumentTextRecognizer firebaseVisionDocumentTextRecognizer2 = new FirebaseVisionDocumentTextRecognizer(zzqf, zzkz, firebaseVisionCloudDocumentRecognizerOptions.isEnforceCertFingerprintMatch());
                map.put(zzj, firebaseVisionDocumentTextRecognizer2);
                firebaseVisionDocumentTextRecognizer = firebaseVisionDocumentTextRecognizer2;
            }
        }
        return firebaseVisionDocumentTextRecognizer;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzqk() {
        return 1024;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzql() {
        return 768;
    }

    private FirebaseVisionDocumentTextRecognizer(zzqf zzqf, zzkz zzkz, boolean z) {
        super(zzqf, "DOCUMENT_TEXT_DETECTION", zzkz, z);
        zzqg.zza(zzqf, 1).zza(zzns.zzad.zzma(), zzod.CLOUD_DOCUMENT_TEXT_CREATE);
    }

    public Task<FirebaseVisionDocumentText> processImage(FirebaseVisionImage firebaseVisionImage) {
        zzqg.zza(this.zzbkb, 1).zza(zzns.zzad.zzma(), zzod.CLOUD_DOCUMENT_TEXT_DETECT);
        return super.zza(firebaseVisionImage);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final /* synthetic */ FirebaseVisionDocumentText zza(zzkl zzkl, float f) {
        return FirebaseVisionDocumentText.zza(zzkl.zzii(), 1.0f / f);
    }
}
