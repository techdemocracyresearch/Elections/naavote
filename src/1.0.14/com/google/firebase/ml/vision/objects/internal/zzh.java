package com.google.firebase.ml.vision.objects.internal;

import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzqo;
import com.google.android.gms.internal.firebase_ml.zzsf;
import java.util.List;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final /* synthetic */ class zzh implements zzqo {
    private final long zzboh;
    private final zze zzbun;
    private final List zzbuo;
    private final zzoc zzbup;
    private final zzsf zzbuq;

    zzh(zze zze, List list, long j, zzoc zzoc, zzsf zzsf) {
        this.zzbun = zze;
        this.zzbuo = list;
        this.zzboh = j;
        this.zzbup = zzoc;
        this.zzbuq = zzsf;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqo
    public final zzns.zzad.zza zzok() {
        return this.zzbun.zza(this.zzbuo, this.zzboh, this.zzbup, this.zzbuq);
    }
}
