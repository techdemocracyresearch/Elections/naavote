package com.google.firebase.ml.vision.label;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqh;
import com.google.android.gms.internal.firebase_ml.zzsj;
import com.google.android.gms.internal.firebase_ml.zzsl;
import com.google.android.gms.internal.firebase_ml.zzsp;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.io.Closeable;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionImageLabeler implements Closeable {
    public static final int CLOUD = 2;
    public static final int ON_DEVICE = 1;
    public static final int ON_DEVICE_AUTOML = 3;
    private static final Map<zzqh<FirebaseVisionOnDeviceImageLabelerOptions>, FirebaseVisionImageLabeler> zzbtr = new HashMap();
    private static final Map<zzqh<FirebaseVisionCloudImageLabelerOptions>, FirebaseVisionImageLabeler> zzbts = new HashMap();
    private static final Map<zzqh<FirebaseVisionOnDeviceAutoMLImageLabelerOptions>, FirebaseVisionImageLabeler> zzbtt = new HashMap();
    private final zzsj zzbtm;
    private final zzsl zzbtn;
    private final zzsp zzbto;
    private final FirebaseVisionCloudImageLabelerOptions zzbtp;
    private final int zzbtq;

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface ImageLabelerType {
    }

    public static synchronized FirebaseVisionImageLabeler zza(zzqf zzqf, FirebaseVisionOnDeviceImageLabelerOptions firebaseVisionOnDeviceImageLabelerOptions) {
        FirebaseVisionImageLabeler firebaseVisionImageLabeler;
        synchronized (FirebaseVisionImageLabeler.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            zzqh<FirebaseVisionOnDeviceImageLabelerOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionOnDeviceImageLabelerOptions);
            Map<zzqh<FirebaseVisionOnDeviceImageLabelerOptions>, FirebaseVisionImageLabeler> map = zzbtr;
            firebaseVisionImageLabeler = map.get(zzj);
            if (firebaseVisionImageLabeler == null) {
                FirebaseVisionImageLabeler firebaseVisionImageLabeler2 = new FirebaseVisionImageLabeler(new zzsl(zzqf, firebaseVisionOnDeviceImageLabelerOptions));
                map.put(zzj, firebaseVisionImageLabeler2);
                firebaseVisionImageLabeler = firebaseVisionImageLabeler2;
            }
        }
        return firebaseVisionImageLabeler;
    }

    public static synchronized FirebaseVisionImageLabeler zza(zzqf zzqf, FirebaseVisionOnDeviceAutoMLImageLabelerOptions firebaseVisionOnDeviceAutoMLImageLabelerOptions) throws FirebaseMLException {
        FirebaseVisionImageLabeler firebaseVisionImageLabeler;
        synchronized (FirebaseVisionImageLabeler.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            zzqh<FirebaseVisionOnDeviceAutoMLImageLabelerOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionOnDeviceAutoMLImageLabelerOptions);
            Map<zzqh<FirebaseVisionOnDeviceAutoMLImageLabelerOptions>, FirebaseVisionImageLabeler> map = zzbtt;
            firebaseVisionImageLabeler = map.get(zzj);
            if (firebaseVisionImageLabeler == null) {
                FirebaseVisionImageLabeler firebaseVisionImageLabeler2 = new FirebaseVisionImageLabeler(new zzsp(zzqf, firebaseVisionOnDeviceAutoMLImageLabelerOptions));
                map.put(zzj, firebaseVisionImageLabeler2);
                firebaseVisionImageLabeler = firebaseVisionImageLabeler2;
            }
        }
        return firebaseVisionImageLabeler;
    }

    public static synchronized FirebaseVisionImageLabeler zza(zzqf zzqf, FirebaseVisionCloudImageLabelerOptions firebaseVisionCloudImageLabelerOptions) {
        FirebaseVisionImageLabeler firebaseVisionImageLabeler;
        synchronized (FirebaseVisionImageLabeler.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Persistence key must not be null");
            zzqh<FirebaseVisionCloudImageLabelerOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionCloudImageLabelerOptions);
            Map<zzqh<FirebaseVisionCloudImageLabelerOptions>, FirebaseVisionImageLabeler> map = zzbts;
            firebaseVisionImageLabeler = map.get(zzj);
            if (firebaseVisionImageLabeler == null) {
                FirebaseVisionCloudDetectorOptions.Builder maxResults = new FirebaseVisionCloudDetectorOptions.Builder().setMaxResults(20);
                if (firebaseVisionCloudImageLabelerOptions.isEnforceCertFingerprintMatch()) {
                    maxResults.enforceCertFingerprintMatch();
                }
                firebaseVisionImageLabeler = new FirebaseVisionImageLabeler(new zzsj(zzqf, maxResults.build()), firebaseVisionCloudImageLabelerOptions);
                map.put(zzj, firebaseVisionImageLabeler);
            }
        }
        return firebaseVisionImageLabeler;
    }

    public Task<List<FirebaseVisionImageLabel>> processImage(FirebaseVisionImage firebaseVisionImage) {
        Preconditions.checkState((this.zzbtn == null && this.zzbtm == null && this.zzbto == null) ? false : true, "One of on-device, cloud, or on-device AutoML image labeler should be set.");
        zzsl zzsl = this.zzbtn;
        if (zzsl != null) {
            return zzsl.detectInImage(firebaseVisionImage);
        }
        zzsp zzsp = this.zzbto;
        if (zzsp != null) {
            return zzsp.detectInImage(firebaseVisionImage);
        }
        return this.zzbtm.detectInImage(firebaseVisionImage).continueWith(new zzb(this));
    }

    public int getImageLabelerType() {
        return this.zzbtq;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        zzsl zzsl = this.zzbtn;
        if (zzsl != null) {
            zzsl.close();
        }
        zzsj zzsj = this.zzbtm;
        if (zzsj != null) {
            zzsj.close();
        }
        zzsp zzsp = this.zzbto;
        if (zzsp != null) {
            zzsp.close();
        }
    }

    private FirebaseVisionImageLabeler(zzsl zzsl) {
        this(zzsl, null, null, null);
    }

    private FirebaseVisionImageLabeler(zzsj zzsj, FirebaseVisionCloudImageLabelerOptions firebaseVisionCloudImageLabelerOptions) {
        this(null, zzsj, null, firebaseVisionCloudImageLabelerOptions);
    }

    private FirebaseVisionImageLabeler(zzsp zzsp) {
        this(null, null, zzsp, null);
    }

    private FirebaseVisionImageLabeler(zzsl zzsl, zzsj zzsj, zzsp zzsp, FirebaseVisionCloudImageLabelerOptions firebaseVisionCloudImageLabelerOptions) {
        Preconditions.checkArgument((zzsl == null && zzsj == null && zzsp == null) ? false : true, "One of on-device, cloud or on-device AutoML image labeler should be set.");
        this.zzbtn = zzsl;
        this.zzbtm = zzsj;
        this.zzbtp = firebaseVisionCloudImageLabelerOptions;
        this.zzbto = zzsp;
        if (zzsj != null) {
            this.zzbtq = 2;
        } else if (zzsl != null) {
            this.zzbtq = 1;
        } else {
            this.zzbtq = 3;
        }
    }
}
