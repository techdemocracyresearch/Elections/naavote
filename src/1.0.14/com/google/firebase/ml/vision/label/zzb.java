package com.google.firebase.ml.vision.label;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import java.util.LinkedList;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final class zzb implements Continuation<List<FirebaseVisionImageLabel>, List<FirebaseVisionImageLabel>> {
    private final /* synthetic */ FirebaseVisionImageLabeler zzbtl;

    zzb(FirebaseVisionImageLabeler firebaseVisionImageLabeler) {
        this.zzbtl = firebaseVisionImageLabeler;
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.google.android.gms.tasks.Task] */
    @Override // com.google.android.gms.tasks.Continuation
    public final /* synthetic */ List<FirebaseVisionImageLabel> then(Task<List<FirebaseVisionImageLabel>> task) throws Exception {
        LinkedList linkedList = new LinkedList();
        for (FirebaseVisionImageLabel firebaseVisionImageLabel : task.getResult()) {
            if (Float.compare(firebaseVisionImageLabel.getConfidence(), this.zzbtl.zzbtp.getConfidenceThreshold()) >= 0) {
                linkedList.add(firebaseVisionImageLabel);
            }
        }
        return linkedList;
    }
}
