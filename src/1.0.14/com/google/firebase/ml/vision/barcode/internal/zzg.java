package com.google.firebase.ml.vision.barcode.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.firebase_ml.zzb;
import com.google.android.gms.internal.firebase_ml.zzd;
import com.google.android.gms.internal.firebase_ml.zzsb;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzg extends zzb implements IBarcodeDetector {
    zzg(IBinder iBinder) {
        super(iBinder, "com.google.firebase.ml.vision.barcode.internal.IBarcodeDetector");
    }

    @Override // com.google.firebase.ml.vision.barcode.internal.IBarcodeDetector
    public final void start() throws RemoteException {
        zzb(1, obtainAndWriteInterfaceToken());
    }

    @Override // com.google.firebase.ml.vision.barcode.internal.IBarcodeDetector
    public final IObjectWrapper zzb(IObjectWrapper iObjectWrapper, zzsb zzsb) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzd.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzd.zza(obtainAndWriteInterfaceToken, zzsb);
        Parcel zza = zza(2, obtainAndWriteInterfaceToken);
        IObjectWrapper asInterface = IObjectWrapper.Stub.asInterface(zza.readStrongBinder());
        zza.recycle();
        return asInterface;
    }

    @Override // com.google.firebase.ml.vision.barcode.internal.IBarcodeDetector
    public final void stop() throws RemoteException {
        zzb(3, obtainAndWriteInterfaceToken());
    }
}
