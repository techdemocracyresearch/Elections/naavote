package com.google.firebase.ml.vision.barcode;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqh;
import com.google.android.gms.internal.firebase_ml.zzrz;
import com.google.android.gms.internal.firebase_ml.zzwz;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.barcode.internal.zzc;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionBarcodeDetector extends zzrz<List<FirebaseVisionBarcode>> implements Closeable {
    private static final Map<zzqh<FirebaseVisionBarcodeDetectorOptions>, FirebaseVisionBarcodeDetector> zzbim = new HashMap();

    public static synchronized FirebaseVisionBarcodeDetector zza(zzqf zzqf, FirebaseVisionBarcodeDetectorOptions firebaseVisionBarcodeDetectorOptions) {
        FirebaseVisionBarcodeDetector firebaseVisionBarcodeDetector;
        synchronized (FirebaseVisionBarcodeDetector.class) {
            Preconditions.checkNotNull(zzqf, "You must provide a valid MlKitContext.");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Firebase app name must not be null");
            Preconditions.checkNotNull(zzqf.getApplicationContext(), "You must provide a valid Context.");
            Preconditions.checkNotNull(firebaseVisionBarcodeDetectorOptions, "You must provide a valid FirebaseVisionBarcodeDetectorOptions.");
            zzqh<FirebaseVisionBarcodeDetectorOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionBarcodeDetectorOptions);
            Map<zzqh<FirebaseVisionBarcodeDetectorOptions>, FirebaseVisionBarcodeDetector> map = zzbim;
            firebaseVisionBarcodeDetector = map.get(zzj);
            if (firebaseVisionBarcodeDetector == null) {
                firebaseVisionBarcodeDetector = new FirebaseVisionBarcodeDetector(zzqf, firebaseVisionBarcodeDetectorOptions);
                map.put(zzj, firebaseVisionBarcodeDetector);
            }
        }
        return firebaseVisionBarcodeDetector;
    }

    private FirebaseVisionBarcodeDetector(zzqf zzqf, FirebaseVisionBarcodeDetectorOptions firebaseVisionBarcodeDetectorOptions) {
        super(zzqf, new zzc(zzqf, firebaseVisionBarcodeDetectorOptions));
        zzqg.zza(zzqf, 1).zza(zzns.zzad.zzma().zza((zzns.zzam) ((zzwz) zzns.zzam.zzmt().zzc(firebaseVisionBarcodeDetectorOptions.zzqi()).zzvb())), zzod.ON_DEVICE_BARCODE_CREATE);
    }

    public Task<List<FirebaseVisionBarcode>> detectInImage(FirebaseVisionImage firebaseVisionImage) {
        return super.zza(firebaseVisionImage, false, false);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzrz, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        super.close();
    }
}
