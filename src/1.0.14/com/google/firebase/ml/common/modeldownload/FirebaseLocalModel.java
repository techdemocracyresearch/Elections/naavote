package com.google.firebase.ml.common.modeldownload;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzwz;
import com.google.firebase.ml.common.internal.modeldownload.zzn;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class FirebaseLocalModel {
    private final String zzbkn;
    private final String zzbmv;
    private final String zzbmw;

    public String getFilePath() {
        return this.zzbmv;
    }

    public String getAssetFilePath() {
        return this.zzbmw;
    }

    protected FirebaseLocalModel(String str, String str2, String str3) {
        this.zzbkn = str;
        this.zzbmv = str2;
        this.zzbmw = str3;
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static class Builder {
        private final String zzbkn;
        private String zzbmv = null;
        private String zzbmw = null;

        public Builder(String str) {
            Preconditions.checkNotEmpty(str, "Model name can not be empty");
            this.zzbkn = str;
        }

        public Builder setFilePath(String str) {
            Preconditions.checkNotEmpty(str, "Model Source file path can not be empty");
            Preconditions.checkArgument(this.zzbmw == null, "A local model source is either from local file or for asset, you can not set both.");
            this.zzbmv = str;
            return this;
        }

        public Builder setAssetFilePath(String str) {
            Preconditions.checkNotEmpty(str, "Model Source file path can not be empty");
            Preconditions.checkArgument(this.zzbmv == null, "A local model source is either from local file or for asset, you can not set both.");
            this.zzbmw = str;
            return this;
        }

        public FirebaseLocalModel build() {
            String str = this.zzbmv;
            Preconditions.checkArgument((str != null && this.zzbmw == null) || (str == null && this.zzbmw != null), "Set either filePath or assetFilePath.");
            return new FirebaseLocalModel(this.zzbkn, this.zzbmv, this.zzbmw);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseLocalModel)) {
            return false;
        }
        FirebaseLocalModel firebaseLocalModel = (FirebaseLocalModel) obj;
        return Objects.equal(this.zzbkn, firebaseLocalModel.zzbkn) && Objects.equal(this.zzbmv, firebaseLocalModel.zzbmv) && Objects.equal(this.zzbmw, firebaseLocalModel.zzbmw);
    }

    public int hashCode() {
        return Objects.hashCode(this.zzbkn, this.zzbmv, this.zzbmw);
    }

    public final zzns.zzak zza(zzn zzn) {
        zzns.zzaj.zzc zzc;
        zzns.zzak.zza zzmn = zzns.zzak.zzmn();
        zzns.zzaj.zza zzd = zzns.zzaj.zzml().zzd(zzn.zzow());
        String str = this.zzbmv;
        if (str == null) {
            str = this.zzbmw;
        }
        zzns.zzaj.zza zzbe = zzd.zzbe(str);
        if (this.zzbmv != null) {
            zzc = zzns.zzaj.zzc.LOCAL;
        } else if (this.zzbmw != null) {
            zzc = zzns.zzaj.zzc.APP_ASSET;
        } else {
            zzc = zzns.zzaj.zzc.SOURCE_UNKNOWN;
        }
        return (zzns.zzak) ((zzwz) zzmn.zza(zzbe.zza(zzc)).zzvb());
    }
}
