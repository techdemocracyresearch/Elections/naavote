package com.google.firebase.ml.common.internal.modeldownload;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzs {
    public static final zzs zzbls = new zzs(zzr.OK, null);
    private final zzr zzblt;
    private final String zzblu;

    public zzs(zzr zzr, String str) {
        this.zzblt = zzr;
        this.zzblu = str;
    }

    public final boolean isValid() {
        return this.zzblt == zzr.OK;
    }

    public final zzr zzoy() {
        return this.zzblt;
    }
}
