package com.google.firebase.ml.common.internal.modeldownload;

import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Preconditions;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import kotlin.UByte;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzah {
    private static final GmsLogger zzbin = new GmsLogger("RemoteModelUtils", "");
    private final zzp zzbmr;

    zzah(zzp zzp) {
        Preconditions.checkNotNull(zzp);
        this.zzbmr = zzp;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x004f */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007c  */
    private static String zzh(File file) throws IOException {
        Throwable th;
        FileInputStream fileInputStream;
        int i;
        FileInputStream fileInputStream2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            fileInputStream = new FileInputStream(file);
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    instance.update(bArr, 0, read);
                }
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                for (byte b : digest) {
                    String hexString = Integer.toHexString(b & UByte.MAX_VALUE);
                    if (hexString.length() == 1) {
                        sb.append('0');
                    }
                    sb.append(hexString);
                }
                String sb2 = sb.toString();
                fileInputStream.close();
                return sb2;
            } catch (NoSuchAlgorithmException unused) {
                zzbin.e("RemoteModelUtils", "Do not have SHA-256 algorithm");
                if (fileInputStream != null) {
                }
                return null;
            } catch (FileNotFoundException unused2) {
                zzbin.e("RemoteModelUtils", "Temp file is not found");
                if (fileInputStream != null) {
                }
                return null;
            } catch (IOException unknown) {
                try {
                    zzbin.e("RemoteModelUtils", "Cannot read the temp file for SHA-256 check");
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    fileInputStream2 = fileInputStream;
                    if (fileInputStream2 != null) {
                        fileInputStream2.close();
                    }
                    throw th;
                }
            }
        } catch (NoSuchAlgorithmException unused3) {
            fileInputStream = null;
            zzbin.e("RemoteModelUtils", "Do not have SHA-256 algorithm");
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            return null;
        } catch (FileNotFoundException unused4) {
            fileInputStream = null;
            zzbin.e("RemoteModelUtils", "Temp file is not found");
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            return null;
        } catch (IOException unused5) {
            fileInputStream = null;
        } catch (Throwable th3) {
            th = th3;
            if (fileInputStream2 != null) {
            }
            throw th;
        }
    }

    static boolean zza(File file, String str) {
        String str2;
        try {
            str2 = zzh(file);
        } catch (IOException unused) {
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(file.getAbsolutePath());
            gmsLogger.d("RemoteModelUtils", valueOf.length() != 0 ? "Failed to close the tmp FileInputStream: ".concat(valueOf) : new String("Failed to close the tmp FileInputStream: "));
            str2 = "";
        }
        GmsLogger gmsLogger2 = zzbin;
        String valueOf2 = String.valueOf(str2);
        gmsLogger2.d("RemoteModelUtils", valueOf2.length() != 0 ? "Calculated hash value is: ".concat(valueOf2) : new String("Calculated hash value is: "));
        return str.equals(str2);
    }

    /* access modifiers changed from: package-private */
    public final zzs zzb(File file, zzw zzw) {
        return this.zzbmr.zza(file, zzw);
    }
}
