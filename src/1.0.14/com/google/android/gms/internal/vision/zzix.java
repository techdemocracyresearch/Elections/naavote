package com.google.android.gms.internal.vision;

import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzix extends zzjd {
    private final /* synthetic */ zziw zzaah;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzix(zziw zziw) {
        super(zziw, null);
        this.zzaah = zziw;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.google.android.gms.internal.vision.zzjd, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new zziy(this.zzaah, null);
    }

    /* synthetic */ zzix(zziw zziw, zziv zziv) {
        this(zziw);
    }
}
