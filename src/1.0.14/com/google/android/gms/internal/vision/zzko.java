package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzkf;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public final class zzko implements zzgy {
    static final zzgy zzhf = new zzko();

    private zzko() {
    }

    @Override // com.google.android.gms.internal.vision.zzgy
    public final boolean zzg(int i) {
        return zzkf.zze.zzc.zzcb(i) != null;
    }
}
