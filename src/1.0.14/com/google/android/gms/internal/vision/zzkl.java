package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzkf;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
final class zzkl implements zzgv<zzkf.zze.zza> {
    zzkl() {
    }

    /* Return type fixed from 'com.google.android.gms.internal.vision.zzgw' to match base method */
    @Override // com.google.android.gms.internal.vision.zzgv
    public final /* synthetic */ zzkf.zze.zza zzh(int i) {
        return zzkf.zze.zza.zzca(i);
    }
}
