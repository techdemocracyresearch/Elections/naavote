package com.google.android.gms.internal.vision;

import java.util.AbstractMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzdn extends zzdf<Map.Entry<K, V>> {
    private final /* synthetic */ zzdk zzmf;

    zzdn(zzdk zzdk) {
        this.zzmf = zzdk;
    }

    public final int size() {
        return zzdk.zza(this.zzmf);
    }

    @Override // java.util.List
    public final /* synthetic */ Object get(int i) {
        zzct.zzc(i, zzdk.zza(this.zzmf));
        int i2 = i * 2;
        return new AbstractMap.SimpleImmutableEntry(zzdk.zzb(this.zzmf)[i2], zzdk.zzb(this.zzmf)[i2 + 1]);
    }
}
