package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzfp {
    private final byte[] buffer;
    private final zzga zzsj;

    private zzfp(int i) {
        byte[] bArr = new byte[i];
        this.buffer = bArr;
        this.zzsj = zzga.zze(bArr);
    }

    public final zzfh zzev() {
        this.zzsj.zzfh();
        return new zzfr(this.buffer);
    }

    public final zzga zzew() {
        return this.zzsj;
    }

    /* synthetic */ zzfp(int i, zzfk zzfk) {
        this(i);
    }
}
