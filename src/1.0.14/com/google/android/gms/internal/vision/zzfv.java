package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.util.Arrays;
import kotlin.UByte;
import kotlin.jvm.internal.ByteCompanionObject;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzfv extends zzft {
    private final byte[] buffer;
    private int limit;
    private int pos;
    private final boolean zzsq;
    private int zzsr;
    private int zzss;
    private int zzst;
    private int zzsu;

    private zzfv(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.zzsu = Integer.MAX_VALUE;
        this.buffer = bArr;
        this.limit = i2 + i;
        this.pos = i;
        this.zzss = i;
        this.zzsq = z;
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzex() throws IOException {
        if (zzdt()) {
            this.zzst = 0;
            return 0;
        }
        int zzfa = zzfa();
        this.zzst = zzfa;
        if ((zzfa >>> 3) != 0) {
            return zzfa;
        }
        throw zzhc.zzgp();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final void zzar(int i) throws zzhc {
        if (this.zzst != i) {
            throw zzhc.zzgq();
        }
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final boolean zzas(int i) throws IOException {
        int zzex;
        int i2 = i & 7;
        int i3 = 0;
        if (i2 == 0) {
            if (this.limit - this.pos >= 10) {
                while (i3 < 10) {
                    byte[] bArr = this.buffer;
                    int i4 = this.pos;
                    this.pos = i4 + 1;
                    if (bArr[i4] < 0) {
                        i3++;
                    }
                }
                throw zzhc.zzgo();
            }
            while (i3 < 10) {
                if (zzff() < 0) {
                    i3++;
                }
            }
            throw zzhc.zzgo();
            return true;
        } else if (i2 == 1) {
            zzaw(8);
            return true;
        } else if (i2 == 2) {
            zzaw(zzfa());
            return true;
        } else if (i2 == 3) {
            do {
                zzex = zzex();
                if (zzex == 0) {
                    break;
                }
            } while (zzas(zzex));
            zzar(((i >>> 3) << 3) | 4);
            return true;
        } else if (i2 == 4) {
            return false;
        } else {
            if (i2 == 5) {
                zzaw(4);
                return true;
            }
            throw zzhc.zzgr();
        }
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final double readDouble() throws IOException {
        return Double.longBitsToDouble(zzfd());
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final float readFloat() throws IOException {
        return Float.intBitsToFloat(zzfc());
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final long zzdw() throws IOException {
        return zzfb();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final long zzdx() throws IOException {
        return zzfb();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzdy() throws IOException {
        return zzfa();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final long zzdz() throws IOException {
        return zzfd();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzea() throws IOException {
        return zzfc();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final boolean zzeb() throws IOException {
        return zzfb() != 0;
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final String readString() throws IOException {
        int zzfa = zzfa();
        if (zzfa > 0 && zzfa <= this.limit - this.pos) {
            String str = new String(this.buffer, this.pos, zzfa, zzgt.UTF_8);
            this.pos += zzfa;
            return str;
        } else if (zzfa == 0) {
            return "";
        } else {
            if (zzfa < 0) {
                throw zzhc.zzgn();
            }
            throw zzhc.zzgm();
        }
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final String zzec() throws IOException {
        int zzfa = zzfa();
        if (zzfa > 0) {
            int i = this.limit;
            int i2 = this.pos;
            if (zzfa <= i - i2) {
                String zzh = zzjs.zzh(this.buffer, i2, zzfa);
                this.pos += zzfa;
                return zzh;
            }
        }
        if (zzfa == 0) {
            return "";
        }
        if (zzfa <= 0) {
            throw zzhc.zzgn();
        }
        throw zzhc.zzgm();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final zzfh zzed() throws IOException {
        byte[] bArr;
        int zzfa = zzfa();
        if (zzfa > 0) {
            int i = this.limit;
            int i2 = this.pos;
            if (zzfa <= i - i2) {
                zzfh zza = zzfh.zza(this.buffer, i2, zzfa);
                this.pos += zzfa;
                return zza;
            }
        }
        if (zzfa == 0) {
            return zzfh.zzsd;
        }
        if (zzfa > 0) {
            int i3 = this.limit;
            int i4 = this.pos;
            if (zzfa <= i3 - i4) {
                int i5 = zzfa + i4;
                this.pos = i5;
                bArr = Arrays.copyOfRange(this.buffer, i4, i5);
                return zzfh.zzd(bArr);
            }
        }
        if (zzfa > 0) {
            throw zzhc.zzgm();
        } else if (zzfa == 0) {
            bArr = zzgt.zzxi;
            return zzfh.zzd(bArr);
        } else {
            throw zzhc.zzgn();
        }
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzee() throws IOException {
        return zzfa();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzef() throws IOException {
        return zzfa();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzeg() throws IOException {
        return zzfc();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final long zzeh() throws IOException {
        return zzfd();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzei() throws IOException {
        return zzav(zzfa());
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final long zzej() throws IOException {
        return zzr(zzfb());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
        if (r2[r3] >= 0) goto L_0x0068;
     */
    private final int zzfa() throws IOException {
        int i;
        int i2 = this.pos;
        int i3 = this.limit;
        if (i3 != i2) {
            byte[] bArr = this.buffer;
            int i4 = i2 + 1;
            byte b = bArr[i2];
            if (b >= 0) {
                this.pos = i4;
                return b;
            } else if (i3 - i4 >= 9) {
                int i5 = i4 + 1;
                int i6 = b ^ (bArr[i4] << 7);
                if (i6 < 0) {
                    i = i6 ^ -128;
                } else {
                    int i7 = i5 + 1;
                    int i8 = i6 ^ (bArr[i5] << 14);
                    if (i8 >= 0) {
                        i = i8 ^ 16256;
                    } else {
                        i5 = i7 + 1;
                        int i9 = i8 ^ (bArr[i7] << 21);
                        if (i9 < 0) {
                            i = i9 ^ -2080896;
                        } else {
                            i7 = i5 + 1;
                            byte b2 = bArr[i5];
                            i = (i9 ^ (b2 << 28)) ^ 266354560;
                            if (b2 < 0) {
                                i5 = i7 + 1;
                                if (bArr[i7] < 0) {
                                    i7 = i5 + 1;
                                    if (bArr[i5] < 0) {
                                        i5 = i7 + 1;
                                        if (bArr[i7] < 0) {
                                            i7 = i5 + 1;
                                            if (bArr[i5] < 0) {
                                                i5 = i7 + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i5 = i7;
                }
                this.pos = i5;
                return i;
            }
        }
        return (int) zzey();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b0, code lost:
        if (((long) r2[r0]) >= 0) goto L_0x00b4;
     */
    private final long zzfb() throws IOException {
        long j;
        long j2;
        long j3;
        int i;
        int i2 = this.pos;
        int i3 = this.limit;
        if (i3 != i2) {
            byte[] bArr = this.buffer;
            int i4 = i2 + 1;
            byte b = bArr[i2];
            if (b >= 0) {
                this.pos = i4;
                return (long) b;
            } else if (i3 - i4 >= 9) {
                int i5 = i4 + 1;
                int i6 = b ^ (bArr[i4] << 7);
                if (i6 < 0) {
                    i = i6 ^ -128;
                } else {
                    int i7 = i5 + 1;
                    int i8 = i6 ^ (bArr[i5] << 14);
                    if (i8 >= 0) {
                        i5 = i7;
                        j = (long) (i8 ^ 16256);
                    } else {
                        i5 = i7 + 1;
                        int i9 = i8 ^ (bArr[i7] << 21);
                        if (i9 < 0) {
                            i = i9 ^ -2080896;
                        } else {
                            long j4 = (long) i9;
                            int i10 = i5 + 1;
                            long j5 = j4 ^ (((long) bArr[i5]) << 28);
                            if (j5 >= 0) {
                                j3 = 266354560;
                            } else {
                                i5 = i10 + 1;
                                long j6 = j5 ^ (((long) bArr[i10]) << 35);
                                if (j6 < 0) {
                                    j2 = -34093383808L;
                                } else {
                                    i10 = i5 + 1;
                                    j5 = j6 ^ (((long) bArr[i5]) << 42);
                                    if (j5 >= 0) {
                                        j3 = 4363953127296L;
                                    } else {
                                        i5 = i10 + 1;
                                        j6 = j5 ^ (((long) bArr[i10]) << 49);
                                        if (j6 < 0) {
                                            j2 = -558586000294016L;
                                        } else {
                                            int i11 = i5 + 1;
                                            long j7 = (j6 ^ (((long) bArr[i5]) << 56)) ^ 71499008037633920L;
                                            if (j7 < 0) {
                                                i5 = i11 + 1;
                                            } else {
                                                i5 = i11;
                                            }
                                            j = j7;
                                        }
                                    }
                                }
                                j = j6 ^ j2;
                            }
                            j = j5 ^ j3;
                            i5 = i10;
                        }
                    }
                    this.pos = i5;
                    return j;
                }
                j = (long) i;
                this.pos = i5;
                return j;
            }
        }
        return zzey();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzft
    public final long zzey() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte zzff = zzff();
            j |= ((long) (zzff & ByteCompanionObject.MAX_VALUE)) << i;
            if ((zzff & ByteCompanionObject.MIN_VALUE) == 0) {
                return j;
            }
        }
        throw zzhc.zzgo();
    }

    private final int zzfc() throws IOException {
        int i = this.pos;
        if (this.limit - i >= 4) {
            byte[] bArr = this.buffer;
            this.pos = i + 4;
            return ((bArr[i + 3] & UByte.MAX_VALUE) << 24) | (bArr[i] & UByte.MAX_VALUE) | ((bArr[i + 1] & UByte.MAX_VALUE) << 8) | ((bArr[i + 2] & UByte.MAX_VALUE) << 16);
        }
        throw zzhc.zzgm();
    }

    private final long zzfd() throws IOException {
        int i = this.pos;
        if (this.limit - i >= 8) {
            byte[] bArr = this.buffer;
            this.pos = i + 8;
            return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
        }
        throw zzhc.zzgm();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzat(int i) throws zzhc {
        if (i >= 0) {
            int zzez = i + zzez();
            int i2 = this.zzsu;
            if (zzez <= i2) {
                this.zzsu = zzez;
                zzfe();
                return i2;
            }
            throw zzhc.zzgm();
        }
        throw zzhc.zzgn();
    }

    private final void zzfe() {
        int i = this.limit + this.zzsr;
        this.limit = i;
        int i2 = i - this.zzss;
        int i3 = this.zzsu;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.zzsr = i4;
            this.limit = i - i4;
            return;
        }
        this.zzsr = 0;
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final void zzau(int i) {
        this.zzsu = i;
        zzfe();
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final boolean zzdt() throws IOException {
        return this.pos == this.limit;
    }

    @Override // com.google.android.gms.internal.vision.zzft
    public final int zzez() {
        return this.pos - this.zzss;
    }

    private final byte zzff() throws IOException {
        int i = this.pos;
        if (i != this.limit) {
            byte[] bArr = this.buffer;
            this.pos = i + 1;
            return bArr[i];
        }
        throw zzhc.zzgm();
    }

    private final void zzaw(int i) throws IOException {
        if (i >= 0) {
            int i2 = this.limit;
            int i3 = this.pos;
            if (i <= i2 - i3) {
                this.pos = i3 + i;
                return;
            }
        }
        if (i < 0) {
            throw zzhc.zzgn();
        }
        throw zzhc.zzgm();
    }
}
