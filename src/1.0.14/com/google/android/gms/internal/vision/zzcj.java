package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public final class zzcj implements zzgy {
    static final zzgy zzhf = new zzcj();

    private zzcj() {
    }

    @Override // com.google.android.gms.internal.vision.zzgy
    public final boolean zzg(int i) {
        return zzci.zzr(i) != null;
    }
}
