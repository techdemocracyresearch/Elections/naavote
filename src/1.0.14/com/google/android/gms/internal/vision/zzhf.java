package com.google.android.gms.internal.vision;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzhf<K> implements Map.Entry<K, Object> {
    private Map.Entry<K, zzhd> zzyc;

    private zzhf(Map.Entry<K, zzhd> entry) {
        this.zzyc = entry;
    }

    @Override // java.util.Map.Entry
    public final K getKey() {
        return this.zzyc.getKey();
    }

    @Override // java.util.Map.Entry
    public final Object getValue() {
        if (this.zzyc.getValue() == null) {
            return null;
        }
        return zzhd.zzgu();
    }

    public final zzhd zzgw() {
        return this.zzyc.getValue();
    }

    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        if (obj instanceof zzic) {
            return this.zzyc.getValue().zzi((zzic) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
