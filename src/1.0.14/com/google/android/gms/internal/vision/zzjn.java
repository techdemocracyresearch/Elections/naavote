package com.google.android.gms.internal.vision;

import java.util.ListIterator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzjn implements ListIterator<String> {
    private ListIterator<String> zzaar;
    private final /* synthetic */ int zzaas;
    private final /* synthetic */ zzjo zzaat;

    zzjn(zzjo zzjo, int i) {
        this.zzaat = zzjo;
        this.zzaas = i;
        this.zzaar = zzjo.zzaau.listIterator(i);
    }

    public final boolean hasNext() {
        return this.zzaar.hasNext();
    }

    public final boolean hasPrevious() {
        return this.zzaar.hasPrevious();
    }

    public final int nextIndex() {
        return this.zzaar.nextIndex();
    }

    public final int previousIndex() {
        return this.zzaar.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void add(String str) {
        throw new UnsupportedOperationException();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void set(String str) {
        throw new UnsupportedOperationException();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.ListIterator
    public final /* synthetic */ String previous() {
        return this.zzaar.previous();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator, java.util.ListIterator
    public final /* synthetic */ String next() {
        return this.zzaar.next();
    }
}
