package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsj extends zzrr<List<FirebaseVisionImageLabel>> {
    public zzsj(zzqf zzqf, FirebaseVisionCloudDetectorOptions firebaseVisionCloudDetectorOptions) {
        super(zzqf, "LABEL_DETECTION", firebaseVisionCloudDetectorOptions);
        zzqg.zza(zzqf, 1).zza(zzns.zzad.zzma(), zzod.CLOUD_IMAGE_LABEL_CREATE);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzqk() {
        return 640;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzql() {
        return 480;
    }

    public final Task<List<FirebaseVisionImageLabel>> detectInImage(FirebaseVisionImage firebaseVisionImage) {
        zzqg.zza(this.zzbkb, 1).zza(zzns.zzad.zzma(), zzod.CLOUD_IMAGE_LABEL_DETECT);
        return super.zza(firebaseVisionImage);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final /* synthetic */ List<FirebaseVisionImageLabel> zza(zzkl zzkl, float f) {
        if (zzkl.zzij() == null) {
            return new ArrayList();
        }
        List<zzkv> zzij = zzkl.zzij();
        ArrayList arrayList = new ArrayList();
        for (zzkv zzkv : zzij) {
            FirebaseVisionImageLabel zza = FirebaseVisionImageLabel.zza(zzkv);
            if (zza != null) {
                arrayList.add(zza);
            }
        }
        return arrayList;
    }
}
