package com.google.android.gms.internal.firebase_ml;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.exifinterface.media.ExifInterface;
import com.facebook.common.util.UriUtil;
import com.google.android.gms.common.internal.GmsLogger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsd {
    private static final GmsLogger zzbin = new GmsLogger("MLKitImageUtils", "");
    private static zzsd zzbsi = new zzsd();

    private zzsd() {
    }

    public static zzsd zzqq() {
        return zzbsi;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f A[Catch:{ FileNotFoundException -> 0x005e }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    public static Bitmap zza(ContentResolver contentResolver, Uri uri) throws IOException {
        Matrix matrix;
        Bitmap createBitmap;
        Matrix matrix2;
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri);
            int zzb = zzb(contentResolver, uri);
            Matrix matrix3 = new Matrix();
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            switch (zzb) {
                case 2:
                    matrix2 = new Matrix();
                    matrix2.postScale(-1.0f, 1.0f);
                    matrix = matrix2;
                    if (matrix == null && bitmap != (createBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true))) {
                        bitmap.recycle();
                        return createBitmap;
                    }
                case 3:
                    matrix3.postRotate(180.0f);
                    matrix = matrix3;
                    return matrix == null ? bitmap : bitmap;
                case 4:
                    matrix3.postScale(1.0f, -1.0f);
                    matrix = matrix3;
                    if (matrix == null) {
                    }
                    break;
                case 5:
                    matrix3.postRotate(90.0f);
                    matrix3.postScale(-1.0f, 1.0f);
                    matrix = matrix3;
                    if (matrix == null) {
                    }
                    break;
                case 6:
                    matrix3.postRotate(90.0f);
                    matrix = matrix3;
                    if (matrix == null) {
                    }
                    break;
                case 7:
                    matrix3.postRotate(-90.0f);
                    matrix3.postScale(-1.0f, 1.0f);
                    matrix = matrix3;
                    if (matrix == null) {
                    }
                    break;
                case 8:
                    matrix3.postRotate(-90.0f);
                    matrix = matrix3;
                    if (matrix == null) {
                    }
                    break;
                default:
                    matrix2 = null;
                    matrix = matrix2;
                    if (matrix == null) {
                    }
                    break;
            }
        } catch (FileNotFoundException e) {
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(uri);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 21);
            sb.append("Could not open file: ");
            sb.append(valueOf);
            gmsLogger.e("MLKitImageUtils", sb.toString(), e);
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0063 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0064  */
    private static int zzb(ContentResolver contentResolver, Uri uri) {
        ExifInterface exifInterface;
        IOException e;
        if (!UriUtil.LOCAL_CONTENT_SCHEME.equals(uri.getScheme()) && !UriUtil.LOCAL_FILE_SCHEME.equals(uri.getScheme())) {
            return 0;
        }
        ExifInterface exifInterface2 = null;
        try {
            InputStream openInputStream = contentResolver.openInputStream(uri);
            try {
                exifInterface = new ExifInterface(openInputStream);
                if (openInputStream != null) {
                    try {
                        openInputStream.close();
                    } catch (IOException e2) {
                        e = e2;
                        exifInterface2 = exifInterface;
                    }
                }
            } catch (Throwable th) {
                zzne.zza(th, th);
            }
        } catch (IOException e3) {
            e = e3;
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(uri);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
            sb.append("failed to open file to read rotation meta data: ");
            sb.append(valueOf);
            gmsLogger.e("MLKitImageUtils", sb.toString(), e);
            exifInterface = exifInterface2;
            if (exifInterface != null) {
            }
        }
        if (exifInterface != null) {
            return 0;
        }
        return exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        throw th;
    }
}
