package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzjf;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzgx extends zzjf {
    @zzjg("Accept")
    private List<String> accept;
    @zzjg("Accept-Encoding")
    private List<String> acceptEncoding = new ArrayList(Collections.singleton("gzip"));
    @zzjg("Age")
    private List<Long> age;
    @zzjg("WWW-Authenticate")
    private List<String> authenticate;
    @zzjg("Authorization")
    private List<String> authorization;
    @zzjg("Cache-Control")
    private List<String> cacheControl;
    @zzjg("Content-Encoding")
    private List<String> contentEncoding;
    @zzjg("Content-Length")
    private List<Long> contentLength;
    @zzjg("Content-MD5")
    private List<String> contentMD5;
    @zzjg("Content-Range")
    private List<String> contentRange;
    @zzjg("Content-Type")
    private List<String> contentType;
    @zzjg("Cookie")
    private List<String> cookie;
    @zzjg("Date")
    private List<String> date;
    @zzjg("ETag")
    private List<String> etag;
    @zzjg("Expires")
    private List<String> expires;
    @zzjg("If-Match")
    private List<String> ifMatch;
    @zzjg("If-Modified-Since")
    private List<String> ifModifiedSince;
    @zzjg("If-None-Match")
    private List<String> ifNoneMatch;
    @zzjg("If-Range")
    private List<String> ifRange;
    @zzjg("If-Unmodified-Since")
    private List<String> ifUnmodifiedSince;
    @zzjg("Last-Modified")
    private List<String> lastModified;
    @zzjg("Location")
    private List<String> location;
    @zzjg("MIME-Version")
    private List<String> mimeVersion;
    @zzjg("Range")
    private List<String> range;
    @zzjg("Retry-After")
    private List<String> retryAfter;
    @zzjg("User-Agent")
    private List<String> userAgent;

    public zzgx() {
        super(EnumSet.of(zzjf.zzb.IGNORE_CASE));
    }

    public final zzgx zzx(String str) {
        this.authorization = zza((Object) null);
        return this;
    }

    public final String getContentType() {
        return (String) zza((List) this.contentType);
    }

    public final zzgx zzy(String str) {
        this.ifModifiedSince = zza((Object) null);
        return this;
    }

    public final zzgx zzz(String str) {
        this.ifMatch = zza((Object) null);
        return this;
    }

    public final zzgx zzaa(String str) {
        this.ifNoneMatch = zza((Object) null);
        return this;
    }

    public final zzgx zzab(String str) {
        this.ifUnmodifiedSince = zza((Object) null);
        return this;
    }

    public final zzgx zzac(String str) {
        this.ifRange = zza((Object) null);
        return this;
    }

    public final String getLocation() {
        return (String) zza((List) this.location);
    }

    public final String getUserAgent() {
        return (String) zza((List) this.userAgent);
    }

    public final zzgx zzad(String str) {
        this.userAgent = zza(str);
        return this;
    }

    private static void zza(Logger logger, StringBuilder sb, StringBuilder sb2, zzhk zzhk, String str, Object obj, Writer writer) throws IOException {
        String str2;
        if (obj != null && !zzix.isNull(obj)) {
            if (obj instanceof Enum) {
                str2 = zzjd.zza((Enum) obj).getName();
            } else {
                str2 = obj.toString();
            }
            String str3 = (("Authorization".equalsIgnoreCase(str) || "Cookie".equalsIgnoreCase(str)) && (logger == null || !logger.isLoggable(Level.ALL))) ? "<Not Logged>" : str2;
            if (sb != null) {
                sb.append(str);
                sb.append(": ");
                sb.append(str3);
                sb.append(zzjt.zzaig);
            }
            if (sb2 != null) {
                sb2.append(" -H '");
                sb2.append(str);
                sb2.append(": ");
                sb2.append(str3);
                sb2.append("'");
            }
            if (zzhk != null) {
                zzhk.addHeader(str, str2);
            }
            if (writer != null) {
                writer.write(str);
                writer.write(": ");
                writer.write(str2);
                writer.write("\r\n");
            }
        }
    }

    static void zza(zzgx zzgx, StringBuilder sb, StringBuilder sb2, Logger logger, zzhk zzhk) throws IOException {
        HashSet hashSet = new HashSet();
        for (Map.Entry<String, Object> entry : zzgx.entrySet()) {
            String key = entry.getKey();
            Object[] objArr = {key};
            if (hashSet.add(key)) {
                Object value = entry.getValue();
                if (value != null) {
                    zzjd zzao = zzgx.zzie().zzao(key);
                    if (zzao != null) {
                        key = zzao.getName();
                    }
                    Class<?> cls = value.getClass();
                    if ((value instanceof Iterable) || cls.isArray()) {
                        for (Object obj : zzjs.zzi(value)) {
                            zza(logger, sb, sb2, zzhk, key, obj, null);
                        }
                    } else {
                        zza(logger, sb, sb2, zzhk, key, value, null);
                    }
                }
            } else {
                throw new IllegalArgumentException(zzms.zzb("multiple headers of the same name (headers are case insensitive): %s", objArr));
            }
        }
    }

    public final void zza(zzhj zzhj, StringBuilder sb) throws IOException {
        clear();
        zzha zzha = new zzha(this, sb);
        int zzgk = zzhj.zzgk();
        for (int i = 0; i < zzgk; i++) {
            String zzaf = zzhj.zzaf(i);
            String zzag = zzhj.zzag(i);
            List<Type> list = zzha.zzach;
            zziv zziv = zzha.zzacg;
            zzis zzis = zzha.zzace;
            StringBuilder sb2 = zzha.zzacf;
            if (sb2 != null) {
                StringBuilder sb3 = new StringBuilder(String.valueOf(zzaf).length() + 2 + String.valueOf(zzag).length());
                sb3.append(zzaf);
                sb3.append(": ");
                sb3.append(zzag);
                sb2.append(sb3.toString());
                sb2.append(zzjt.zzaig);
            }
            zzjd zzao = zziv.zzao(zzaf);
            if (zzao != null) {
                Type zza = zzix.zza(list, zzao.getGenericType());
                if (zzjs.zzc(zza)) {
                    Class<?> zzb = zzjs.zzb(list, zzjs.zzd(zza));
                    zzis.zza(zzao.zzia(), zzb, zza(zzb, list, zzag));
                } else if (zzjs.zza(zzjs.zzb(list, zza), (Class<?>) Iterable.class)) {
                    Collection<Object> collection = (Collection) zzao.zzh(this);
                    if (collection == null) {
                        collection = zzix.zzb(zza);
                        zzao.zzb(this, collection);
                    }
                    collection.add(zza(zza == Object.class ? null : zzjs.zze(zza), list, zzag));
                } else {
                    zzao.zzb(this, zza(zza, list, zzag));
                }
            } else {
                ArrayList arrayList = (ArrayList) get(zzaf);
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    zzgx zzgx = (zzgx) zzb(zzaf, arrayList);
                }
                arrayList.add(zzag);
            }
        }
        zzha.zzace.zzhw();
    }

    private static <T> T zza(List<T> list) {
        if (list == null) {
            return null;
        }
        return list.get(0);
    }

    private static <T> List<T> zza(T t) {
        if (t == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(t);
        return arrayList;
    }

    private static Object zza(Type type, List<Type> list, String str) {
        return zzix.zza(zzix.zza(list, type), str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf
    public final /* synthetic */ zzjf zzfd() {
        return (zzgx) clone();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf
    public final /* synthetic */ zzjf zzb(String str, Object obj) {
        return (zzgx) super.zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, java.util.AbstractMap, java.lang.Object
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        return (zzgx) super.clone();
    }
}
