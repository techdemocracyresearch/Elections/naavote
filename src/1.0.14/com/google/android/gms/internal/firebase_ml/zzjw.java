package com.google.android.gms.internal.firebase_ml;

import com.bumptech.glide.load.Key;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjw {
    private static final zzjz zzaij = new zzjy("-_.*", true);
    private static final zzjz zzaik = new zzjy("-_.!~*'()@:$&,;=", false);
    private static final zzjz zzail = new zzjy("-_.!~*'()@:$&,;=+/?", false);
    private static final zzjz zzaim = new zzjy("-_.!~*'():$&,;=", false);
    private static final zzjz zzain = new zzjy("-_.!~*'()@:$,;/?:", false);

    public static String zzaq(String str) {
        return zzaij.zzaw(str);
    }

    public static String zzar(String str) {
        try {
            return URLDecoder.decode(str, Key.STRING_CHARSET_NAME);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String zzas(String str) {
        return zzaik.zzaw(str);
    }

    public static String zzat(String str) {
        return zzail.zzaw(str);
    }

    public static String zzau(String str) {
        return zzaim.zzaw(str);
    }

    public static String zzav(String str) {
        return zzain.zzaw(str);
    }
}
