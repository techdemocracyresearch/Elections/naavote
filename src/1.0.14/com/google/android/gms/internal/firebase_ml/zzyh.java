package com.google.android.gms.internal.firebase_ml;

import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
interface zzyh {
    int zzd(int i, Object obj, Object obj2);

    Object zzd(Object obj, Object obj2);

    Map<?, ?> zzu(Object obj);

    zzyf<?, ?> zzv(Object obj);

    Map<?, ?> zzw(Object obj);

    boolean zzx(Object obj);

    Object zzy(Object obj);

    Object zzz(Object obj);
}
