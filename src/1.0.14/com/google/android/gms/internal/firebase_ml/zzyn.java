package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzyn extends zzym, Cloneable {
    zzyn zza(zzyk zzyk);

    zzyk zzva();

    zzyk zzvb();
}
