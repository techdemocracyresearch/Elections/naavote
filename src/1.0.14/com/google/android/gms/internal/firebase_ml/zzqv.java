package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import com.google.android.gms.clearcut.ClearcutLogger;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzqg;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzqv implements zzqg.zzb {
    private static final GmsLogger zzbin = new GmsLogger("MlStatsLogger", "");
    private final ClearcutLogger zzbkp;

    public zzqv(Context context) {
        this.zzbkp = ClearcutLogger.anonymousLogger(context, "FIREBASE_ML_SDK");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqg.zzb
    public final void zza(zzns.zzad zzad) {
        GmsLogger gmsLogger = zzbin;
        String valueOf = String.valueOf(zzad);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 30);
        sb.append("Logging FirebaseMlSdkLogEvent ");
        sb.append(valueOf);
        gmsLogger.d("MlStatsLogger", sb.toString());
        this.zzbkp.newEvent(zzad.toByteArray()).log();
    }
}
