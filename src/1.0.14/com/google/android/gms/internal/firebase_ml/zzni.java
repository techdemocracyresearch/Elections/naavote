package com.google.android.gms.internal.firebase_ml;

import java.util.List;
import java.util.Objects;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzni extends zznh {
    private final zzng zzama = new zzng();

    zzni() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zznh
    public final void zza(Throwable th, Throwable th2) {
        if (th2 != th) {
            Objects.requireNonNull(th2, "The suppressed exception cannot be null.");
            this.zzama.zza(th, true).add(th2);
            return;
        }
        throw new IllegalArgumentException("Self suppression is not allowed.", th2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zznh
    public final void zzb(Throwable th) {
        th.printStackTrace();
        List<Throwable> zza = this.zzama.zza(th, false);
        if (zza != null) {
            synchronized (zza) {
                for (Throwable th2 : zza) {
                    System.err.print("Suppressed: ");
                    th2.printStackTrace();
                }
            }
        }
    }
}
