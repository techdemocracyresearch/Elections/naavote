package com.google.android.gms.internal.firebase_ml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzxw extends zzxu {
    private static final Class<?> zzcnj = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private zzxw() {
        super();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzxu
    public final void zzb(Object obj, long j) {
        Object obj2;
        List list = (List) zzaac.zzp(obj, j);
        if (list instanceof zzxv) {
            obj2 = ((zzxv) list).zzvo();
        } else if (!zzcnj.isAssignableFrom(list.getClass())) {
            if (!(list instanceof zzyw) || !(list instanceof zzxl)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                zzxl zzxl = (zzxl) list;
                if (zzxl.zztl()) {
                    zzxl.zztm();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        zzaac.zza(obj, j, obj2);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzxu
    public final <E> void zza(Object obj, Object obj2, long j) {
        ArrayList arrayList;
        List zzc = zzc(obj2, j);
        int size = zzc.size();
        List zzc2 = zzc(obj, j);
        if (zzc2.isEmpty()) {
            if (zzc2 instanceof zzxv) {
                zzc2 = new zzxs(size);
            } else if (!(zzc2 instanceof zzyw) || !(zzc2 instanceof zzxl)) {
                zzc2 = new ArrayList(size);
            } else {
                zzc2 = ((zzxl) zzc2).zzcv(size);
            }
            zzaac.zza(obj, j, zzc2);
        } else {
            if (zzcnj.isAssignableFrom(zzc2.getClass())) {
                ArrayList arrayList2 = new ArrayList(zzc2.size() + size);
                arrayList2.addAll(zzc2);
                zzaac.zza(obj, j, arrayList2);
                arrayList = arrayList2;
            } else if (zzc2 instanceof zzaab) {
                zzxs zzxs = new zzxs(zzc2.size() + size);
                zzxs.addAll((zzaab) zzc2);
                zzaac.zza(obj, j, zzxs);
                arrayList = zzxs;
            } else if ((zzc2 instanceof zzyw) && (zzc2 instanceof zzxl)) {
                zzxl zzxl = (zzxl) zzc2;
                if (!zzxl.zztl()) {
                    zzc2 = zzxl.zzcv(zzc2.size() + size);
                    zzaac.zza(obj, j, zzc2);
                }
            }
            zzc2 = arrayList;
        }
        int size2 = zzc2.size();
        int size3 = zzc.size();
        if (size2 > 0 && size3 > 0) {
            zzc2.addAll(zzc);
        }
        if (size2 > 0) {
            zzc = zzc2;
        }
        zzaac.zza(obj, j, zzc);
    }

    private static <E> List<E> zzc(Object obj, long j) {
        return (List) zzaac.zzp(obj, j);
    }
}
