package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzvk;
import com.google.android.gms.internal.firebase_ml.zzvl;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzvk<MessageType extends zzvl<MessageType, BuilderType>, BuilderType extends zzvk<MessageType, BuilderType>> implements zzyn {
    /* access modifiers changed from: protected */
    public abstract BuilderType zza(MessageType messagetype);

    /* renamed from: zztf */
    public abstract BuilderType clone();

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.android.gms.internal.firebase_ml.zzvk<MessageType extends com.google.android.gms.internal.firebase_ml.zzvl<MessageType, BuilderType>, BuilderType extends com.google.android.gms.internal.firebase_ml.zzvk<MessageType, BuilderType>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.firebase_ml.zzyn
    public final /* synthetic */ zzyn zza(zzyk zzyk) {
        if (zzuv().getClass().isInstance(zzyk)) {
            return zza((zzvl) zzyk);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
