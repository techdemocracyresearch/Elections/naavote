package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final class zzhs extends zzhk {
    private final HttpURLConnection zzadz;

    zzhs(HttpURLConnection httpURLConnection) {
        this.zzadz = httpURLConnection;
        httpURLConnection.setInstanceFollowRedirects(false);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhk
    public final void addHeader(String str, String str2) {
        this.zzadz.addRequestProperty(str, str2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhk
    public final void zza(int i, int i2) {
        this.zzadz.setReadTimeout(i2);
        this.zzadz.setConnectTimeout(i);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhk
    public final zzhj zzgm() throws IOException {
        HttpURLConnection httpURLConnection = this.zzadz;
        if (zzgl() != null) {
            String contentType = getContentType();
            if (contentType != null) {
                addHeader("Content-Type", contentType);
            }
            String contentEncoding = getContentEncoding();
            if (contentEncoding != null) {
                addHeader("Content-Encoding", contentEncoding);
            }
            long contentLength = getContentLength();
            int i = (contentLength > 0 ? 1 : (contentLength == 0 ? 0 : -1));
            if (i >= 0) {
                httpURLConnection.setRequestProperty("Content-Length", Long.toString(contentLength));
            }
            String requestMethod = httpURLConnection.getRequestMethod();
            if ("POST".equals(requestMethod) || "PUT".equals(requestMethod)) {
                httpURLConnection.setDoOutput(true);
                if (i < 0 || contentLength > 2147483647L) {
                    httpURLConnection.setChunkedStreamingMode(0);
                } else {
                    httpURLConnection.setFixedLengthStreamingMode((int) contentLength);
                }
                OutputStream outputStream = httpURLConnection.getOutputStream();
                try {
                    zzgl().writeTo(outputStream);
                    try {
                    } catch (IOException e) {
                        throw e;
                    }
                } finally {
                    try {
                        outputStream.close();
                    } catch (IOException unused) {
                    }
                }
            } else {
                Object[] objArr = {requestMethod};
                if (!(i == 0)) {
                    throw new IllegalArgumentException(zzms.zzb("%s with non-zero content length is not supported", objArr));
                }
            }
        }
        try {
            httpURLConnection.connect();
            return new zzhr(httpURLConnection);
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }
}
