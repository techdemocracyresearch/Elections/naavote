package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzaan {
    DOUBLE(zzaaq.DOUBLE, 1),
    FLOAT(zzaaq.FLOAT, 5),
    INT64(zzaaq.LONG, 0),
    UINT64(zzaaq.LONG, 0),
    INT32(zzaaq.INT, 0),
    FIXED64(zzaaq.LONG, 1),
    FIXED32(zzaaq.INT, 5),
    BOOL(zzaaq.BOOLEAN, 0),
    STRING(zzaaq.STRING, 2) {
    },
    GROUP(zzaaq.MESSAGE, 3) {
    },
    MESSAGE(zzaaq.MESSAGE, 2) {
    },
    BYTES(zzaaq.BYTE_STRING, 2) {
    },
    UINT32(zzaaq.INT, 0),
    ENUM(zzaaq.ENUM, 0),
    SFIXED32(zzaaq.INT, 5),
    SFIXED64(zzaaq.LONG, 1),
    SINT32(zzaaq.INT, 0),
    SINT64(zzaaq.LONG, 0);
    
    private final zzaaq zzcrw;
    private final int zzcrx;

    private zzaan(zzaaq zzaaq, int i) {
        this.zzcrw = zzaaq;
        this.zzcrx = i;
    }

    public final zzaaq zzxi() {
        return this.zzcrw;
    }

    public final int zzxj() {
        return this.zzcrx;
    }

    /* synthetic */ zzaan(zzaaq zzaaq, int i, zzaak zzaak) {
        this(zzaaq, i);
    }
}
