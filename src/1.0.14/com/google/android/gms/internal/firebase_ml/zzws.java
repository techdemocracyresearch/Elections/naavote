package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzws {
    private static final zzwq<?> zzcip = new zzwp();
    private static final zzwq<?> zzciq = zzuf();

    private static zzwq<?> zzuf() {
        try {
            return (zzwq) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    static zzwq<?> zzug() {
        return zzcip;
    }

    static zzwq<?> zzuh() {
        zzwq<?> zzwq = zzciq;
        if (zzwq != null) {
            return zzwq;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
