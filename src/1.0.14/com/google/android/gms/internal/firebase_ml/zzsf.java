package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.vision.Frame;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsf implements zzpy {
    public final Frame zzbrv;
    public final FirebaseVisionImage zzbsk;

    public zzsf(FirebaseVisionImage firebaseVisionImage, Frame frame) {
        this.zzbsk = firebaseVisionImage;
        this.zzbrv = frame;
    }
}
