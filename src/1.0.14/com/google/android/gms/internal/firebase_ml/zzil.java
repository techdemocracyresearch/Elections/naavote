package com.google.android.gms.internal.firebase_ml;

import java.io.EOFException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzil extends zzib {
    private final zzig zzafb;
    private final zzsz zzafe;
    private List<String> zzaff = new ArrayList();
    private zzih zzafg;
    private String zzafh;

    zzil(zzig zzig, zzsz zzsz) {
        this.zzafb = zzig;
        this.zzafe = zzsz;
        zzsz.setLenient(true);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final void close() throws IOException {
        this.zzafe.close();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final String zzhe() {
        if (this.zzaff.isEmpty()) {
            return null;
        }
        List<String> list = this.zzaff;
        return list.get(list.size() - 1);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final zzih zzhd() {
        return this.zzafg;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final zzhx zzhb() {
        return this.zzafb;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final byte zzhg() {
        zzhu();
        return Byte.parseByte(this.zzafh);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final short zzhh() {
        zzhu();
        return Short.parseShort(this.zzafh);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final int getIntValue() {
        zzhu();
        return Integer.parseInt(this.zzafh);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final float zzhi() {
        zzhu();
        return Float.parseFloat(this.zzafh);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final BigInteger zzhl() {
        zzhu();
        return new BigInteger(this.zzafh);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final BigDecimal zzhm() {
        zzhu();
        return new BigDecimal(this.zzafh);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final double zzhk() {
        zzhu();
        return Double.parseDouble(this.zzafh);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final long zzhj() {
        zzhu();
        return Long.parseLong(this.zzafh);
    }

    private final void zzhu() {
        zzml.checkArgument(this.zzafg == zzih.VALUE_NUMBER_INT || this.zzafg == zzih.VALUE_NUMBER_FLOAT);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final String getText() {
        return this.zzafh;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final zzih zzhc() throws IOException {
        zztb zztb;
        zzih zzih;
        if (this.zzafg != null) {
            int i = zzik.zzaem[this.zzafg.ordinal()];
            if (i == 1) {
                this.zzafe.beginArray();
                this.zzaff.add(null);
            } else if (i == 2) {
                this.zzafe.beginObject();
                this.zzaff.add(null);
            }
        }
        try {
            zztb = this.zzafe.zzrb();
        } catch (EOFException unused) {
            zztb = zztb.END_DOCUMENT;
        }
        switch (zzik.zzafd[zztb.ordinal()]) {
            case 1:
                this.zzafh = "[";
                this.zzafg = zzih.START_ARRAY;
                break;
            case 2:
                this.zzafh = "]";
                this.zzafg = zzih.END_ARRAY;
                List<String> list = this.zzaff;
                list.remove(list.size() - 1);
                this.zzafe.endArray();
                break;
            case 3:
                this.zzafh = "{";
                this.zzafg = zzih.START_OBJECT;
                break;
            case 4:
                this.zzafh = "}";
                this.zzafg = zzih.END_OBJECT;
                List<String> list2 = this.zzaff;
                list2.remove(list2.size() - 1);
                this.zzafe.endObject();
                break;
            case 5:
                if (!this.zzafe.nextBoolean()) {
                    this.zzafh = "false";
                    this.zzafg = zzih.VALUE_FALSE;
                    break;
                } else {
                    this.zzafh = "true";
                    this.zzafg = zzih.VALUE_TRUE;
                    break;
                }
            case 6:
                this.zzafh = "null";
                this.zzafg = zzih.VALUE_NULL;
                this.zzafe.nextNull();
                break;
            case 7:
                this.zzafh = this.zzafe.nextString();
                this.zzafg = zzih.VALUE_STRING;
                break;
            case 8:
                String nextString = this.zzafe.nextString();
                this.zzafh = nextString;
                if (nextString.indexOf(46) == -1) {
                    zzih = zzih.VALUE_NUMBER_INT;
                } else {
                    zzih = zzih.VALUE_NUMBER_FLOAT;
                }
                this.zzafg = zzih;
                break;
            case 9:
                this.zzafh = this.zzafe.nextName();
                this.zzafg = zzih.FIELD_NAME;
                List<String> list3 = this.zzaff;
                list3.set(list3.size() - 1, this.zzafh);
                break;
            default:
                this.zzafh = null;
                this.zzafg = null;
                break;
        }
        return this.zzafg;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzib
    public final zzib zzhf() throws IOException {
        if (this.zzafg != null) {
            int i = zzik.zzaem[this.zzafg.ordinal()];
            if (i == 1) {
                this.zzafe.skipValue();
                this.zzafh = "]";
                this.zzafg = zzih.END_ARRAY;
            } else if (i == 2) {
                this.zzafe.skipValue();
                this.zzafh = "}";
                this.zzafg = zzih.END_OBJECT;
            }
        }
        return this;
    }
}
