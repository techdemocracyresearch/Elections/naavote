package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzwn extends zzvp<Double> implements zzxl<Double>, zzyw, RandomAccess {
    private static final zzwn zzcie;
    private int size;
    private double[] zzcif;

    zzwn() {
        this(new double[10], 0);
    }

    private zzwn(double[] dArr, int i) {
        this.zzcif = dArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zztn();
        if (i2 >= i) {
            double[] dArr = this.zzcif;
            System.arraycopy(dArr, i2, dArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzwn)) {
            return super.equals(obj);
        }
        zzwn zzwn = (zzwn) obj;
        if (this.size != zzwn.size) {
            return false;
        }
        double[] dArr = zzwn.zzcif;
        for (int i = 0; i < this.size; i++) {
            if (Double.doubleToLongBits(this.zzcif[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + zzxd.zzaf(Double.doubleToLongBits(this.zzcif[i2]));
        }
        return i;
    }

    public final int indexOf(Object obj) {
        if (!(obj instanceof Double)) {
            return -1;
        }
        double doubleValue = ((Double) obj).doubleValue();
        int size2 = size();
        for (int i = 0; i < size2; i++) {
            if (this.zzcif[i] == doubleValue) {
                return i;
            }
        }
        return -1;
    }

    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final int size() {
        return this.size;
    }

    public final void zze(double d) {
        zztn();
        int i = this.size;
        double[] dArr = this.zzcif;
        if (i == dArr.length) {
            double[] dArr2 = new double[(((i * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.zzcif = dArr2;
        }
        double[] dArr3 = this.zzcif;
        int i2 = this.size;
        this.size = i2 + 1;
        dArr3[i2] = d;
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final boolean addAll(Collection<? extends Double> collection) {
        zztn();
        zzxd.checkNotNull(collection);
        if (!(collection instanceof zzwn)) {
            return super.addAll(collection);
        }
        zzwn zzwn = (zzwn) collection;
        int i = zzwn.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.zzcif;
            if (i3 > dArr.length) {
                this.zzcif = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(zzwn.zzcif, 0, this.zzcif, this.size, zzwn.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean remove(Object obj) {
        zztn();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Double.valueOf(this.zzcif[i]))) {
                double[] dArr = this.zzcif;
                System.arraycopy(dArr, i + 1, dArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzct(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
    }

    private final String zzcu(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Double set(int i, Double d) {
        double doubleValue = d.doubleValue();
        zztn();
        zzct(i);
        double[] dArr = this.zzcif;
        double d2 = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d2);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Double remove(int i) {
        zztn();
        zzct(i);
        double[] dArr = this.zzcif;
        double d = dArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Double.valueOf(d);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ void add(int i, Double d) {
        int i2;
        double doubleValue = d.doubleValue();
        zztn();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
        double[] dArr = this.zzcif;
        if (i2 < dArr.length) {
            System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
        } else {
            double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.zzcif, i, dArr2, i + 1, this.size - i);
            this.zzcif = dArr2;
        }
        this.zzcif[i] = doubleValue;
        this.size++;
        this.modCount++;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection, java.util.AbstractList
    public final /* synthetic */ boolean add(Double d) {
        zze(d.doubleValue());
        return true;
    }

    /* Return type fixed from 'com.google.android.gms.internal.firebase_ml.zzxl' to match base method */
    @Override // com.google.android.gms.internal.firebase_ml.zzxl
    public final /* synthetic */ zzxl<Double> zzcv(int i) {
        if (i >= this.size) {
            return new zzwn(Arrays.copyOf(this.zzcif, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        zzct(i);
        return Double.valueOf(this.zzcif[i]);
    }

    static {
        zzwn zzwn = new zzwn(new double[0], 0);
        zzcie = zzwn;
        zzwn.zztm();
    }
}
