package com.google.android.gms.internal.firebase_ml;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzls<T> extends zzmi<T> {
    static final zzls<Object> zzajo = new zzls<>();

    private zzls() {
    }

    public final boolean equals(@NullableDecl Object obj) {
        return obj == this;
    }

    public final int hashCode() {
        return 2040732332;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzmi
    public final boolean isPresent() {
        return false;
    }

    public final String toString() {
        return "Optional.absent()";
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzmi
    public final T get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }
}
