package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Type;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzww {
    DOUBLE(0, zzwy.SCALAR, zzxm.DOUBLE),
    FLOAT(1, zzwy.SCALAR, zzxm.FLOAT),
    INT64(2, zzwy.SCALAR, zzxm.LONG),
    UINT64(3, zzwy.SCALAR, zzxm.LONG),
    INT32(4, zzwy.SCALAR, zzxm.INT),
    FIXED64(5, zzwy.SCALAR, zzxm.LONG),
    FIXED32(6, zzwy.SCALAR, zzxm.INT),
    BOOL(7, zzwy.SCALAR, zzxm.BOOLEAN),
    STRING(8, zzwy.SCALAR, zzxm.STRING),
    MESSAGE(9, zzwy.SCALAR, zzxm.MESSAGE),
    BYTES(10, zzwy.SCALAR, zzxm.BYTE_STRING),
    UINT32(11, zzwy.SCALAR, zzxm.INT),
    ENUM(12, zzwy.SCALAR, zzxm.ENUM),
    SFIXED32(13, zzwy.SCALAR, zzxm.INT),
    SFIXED64(14, zzwy.SCALAR, zzxm.LONG),
    SINT32(15, zzwy.SCALAR, zzxm.INT),
    SINT64(16, zzwy.SCALAR, zzxm.LONG),
    GROUP(17, zzwy.SCALAR, zzxm.MESSAGE),
    DOUBLE_LIST(18, zzwy.VECTOR, zzxm.DOUBLE),
    FLOAT_LIST(19, zzwy.VECTOR, zzxm.FLOAT),
    INT64_LIST(20, zzwy.VECTOR, zzxm.LONG),
    UINT64_LIST(21, zzwy.VECTOR, zzxm.LONG),
    INT32_LIST(22, zzwy.VECTOR, zzxm.INT),
    FIXED64_LIST(23, zzwy.VECTOR, zzxm.LONG),
    FIXED32_LIST(24, zzwy.VECTOR, zzxm.INT),
    BOOL_LIST(25, zzwy.VECTOR, zzxm.BOOLEAN),
    STRING_LIST(26, zzwy.VECTOR, zzxm.STRING),
    MESSAGE_LIST(27, zzwy.VECTOR, zzxm.MESSAGE),
    BYTES_LIST(28, zzwy.VECTOR, zzxm.BYTE_STRING),
    UINT32_LIST(29, zzwy.VECTOR, zzxm.INT),
    ENUM_LIST(30, zzwy.VECTOR, zzxm.ENUM),
    SFIXED32_LIST(31, zzwy.VECTOR, zzxm.INT),
    SFIXED64_LIST(32, zzwy.VECTOR, zzxm.LONG),
    SINT32_LIST(33, zzwy.VECTOR, zzxm.INT),
    SINT64_LIST(34, zzwy.VECTOR, zzxm.LONG),
    DOUBLE_LIST_PACKED(35, zzwy.PACKED_VECTOR, zzxm.DOUBLE),
    FLOAT_LIST_PACKED(36, zzwy.PACKED_VECTOR, zzxm.FLOAT),
    INT64_LIST_PACKED(37, zzwy.PACKED_VECTOR, zzxm.LONG),
    UINT64_LIST_PACKED(38, zzwy.PACKED_VECTOR, zzxm.LONG),
    INT32_LIST_PACKED(39, zzwy.PACKED_VECTOR, zzxm.INT),
    FIXED64_LIST_PACKED(40, zzwy.PACKED_VECTOR, zzxm.LONG),
    FIXED32_LIST_PACKED(41, zzwy.PACKED_VECTOR, zzxm.INT),
    BOOL_LIST_PACKED(42, zzwy.PACKED_VECTOR, zzxm.BOOLEAN),
    UINT32_LIST_PACKED(43, zzwy.PACKED_VECTOR, zzxm.INT),
    ENUM_LIST_PACKED(44, zzwy.PACKED_VECTOR, zzxm.ENUM),
    SFIXED32_LIST_PACKED(45, zzwy.PACKED_VECTOR, zzxm.INT),
    SFIXED64_LIST_PACKED(46, zzwy.PACKED_VECTOR, zzxm.LONG),
    SINT32_LIST_PACKED(47, zzwy.PACKED_VECTOR, zzxm.INT),
    SINT64_LIST_PACKED(48, zzwy.PACKED_VECTOR, zzxm.LONG),
    GROUP_LIST(49, zzwy.VECTOR, zzxm.MESSAGE),
    MAP(50, zzwy.MAP, zzxm.VOID);
    
    private static final zzww[] zzcky;
    private static final Type[] zzckz = new Type[0];
    private final int id;
    private final zzxm zzcku;
    private final zzwy zzckv;
    private final Class<?> zzckw;
    private final boolean zzckx;

    private zzww(int i, zzwy zzwy, zzxm zzxm) {
        int i2;
        this.id = i;
        this.zzckv = zzwy;
        this.zzcku = zzxm;
        int i3 = zzwv.zzcit[zzwy.ordinal()];
        boolean z = true;
        if (i3 == 1) {
            this.zzckw = zzxm.zzvk();
        } else if (i3 != 2) {
            this.zzckw = null;
        } else {
            this.zzckw = zzxm.zzvk();
        }
        this.zzckx = (zzwy != zzwy.SCALAR || (i2 = zzwv.zzciu[zzxm.ordinal()]) == 1 || i2 == 2 || i2 == 3) ? false : z;
    }

    public final int id() {
        return this.id;
    }

    static {
        zzww[] values = values();
        zzcky = new zzww[values.length];
        for (zzww zzww : values) {
            zzcky[zzww.id] = zzww;
        }
    }
}
