package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzxt {
    private static final zzwo zzche = zzwo.zzuc();
    private zzvv zzcne;
    private volatile zzyk zzcnf;
    private volatile zzvv zzcng;

    public int hashCode() {
        return 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzxt)) {
            return false;
        }
        zzxt zzxt = (zzxt) obj;
        zzyk zzyk = this.zzcnf;
        zzyk zzyk2 = zzxt.zzcnf;
        if (zzyk == null && zzyk2 == null) {
            return zztg().equals(zzxt.zztg());
        }
        if (zzyk != null && zzyk2 != null) {
            return zzyk.equals(zzyk2);
        }
        if (zzyk != null) {
            return zzyk.equals(zzxt.zzh(zzyk.zzuv()));
        }
        return zzh(zzyk2.zzuv()).equals(zzyk2);
    }

    private final zzyk zzh(zzyk zzyk) {
        if (this.zzcnf == null) {
            synchronized (this) {
                if (this.zzcnf == null) {
                    try {
                        this.zzcnf = zzyk;
                        this.zzcng = zzvv.zzchp;
                    } catch (zzxk unused) {
                        this.zzcnf = zzyk;
                        this.zzcng = zzvv.zzchp;
                    }
                }
            }
        }
        return this.zzcnf;
    }

    public final zzyk zzi(zzyk zzyk) {
        zzyk zzyk2 = this.zzcnf;
        this.zzcne = null;
        this.zzcng = null;
        this.zzcnf = zzyk;
        return zzyk2;
    }

    public final int zzuo() {
        if (this.zzcng != null) {
            return this.zzcng.size();
        }
        if (this.zzcnf != null) {
            return this.zzcnf.zzuo();
        }
        return 0;
    }

    public final zzvv zztg() {
        if (this.zzcng != null) {
            return this.zzcng;
        }
        synchronized (this) {
            if (this.zzcng != null) {
                return this.zzcng;
            }
            if (this.zzcnf == null) {
                this.zzcng = zzvv.zzchp;
            } else {
                this.zzcng = this.zzcnf.zztg();
            }
            return this.zzcng;
        }
    }
}
