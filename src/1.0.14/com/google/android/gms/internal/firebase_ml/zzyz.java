package com.google.android.gms.internal.firebase_ml;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzyz {
    private static final zzyz zzcou = new zzyz();
    private final zzzh zzcov = new zzyb();
    private final ConcurrentMap<Class<?>, zzze<?>> zzcow = new ConcurrentHashMap();

    public static zzyz zzwh() {
        return zzcou;
    }

    public final <T> zzze<T> zzl(Class<T> cls) {
        zzxd.checkNotNull(cls, "messageType");
        zzze<T> zzze = (zzze<T>) this.zzcow.get(cls);
        if (zzze != null) {
            return zzze;
        }
        zzze<T> zzk = this.zzcov.zzk(cls);
        zzxd.checkNotNull(cls, "messageType");
        zzxd.checkNotNull(zzk, "schema");
        zzze<T> zzze2 = (zzze<T>) this.zzcow.putIfAbsent(cls, zzk);
        return zzze2 != null ? zzze2 : zzk;
    }

    public final <T> zzze<T> zzad(T t) {
        return zzl(t.getClass());
    }

    private zzyz() {
    }
}
