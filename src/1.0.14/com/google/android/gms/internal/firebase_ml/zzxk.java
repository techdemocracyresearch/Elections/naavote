package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzxk extends IOException {
    private zzyk zzcmk = null;

    public zzxk(String str) {
        super(str);
    }

    public final zzxk zzg(zzyk zzyk) {
        this.zzcmk = zzyk;
        return this;
    }

    static zzxk zzve() {
        return new zzxk("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    static zzxk zzvf() {
        return new zzxk("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static zzxk zzvg() {
        return new zzxk("Protocol message contained an invalid tag (zero).");
    }

    static zzxn zzvh() {
        return new zzxn("Protocol message tag had invalid wire type.");
    }

    static zzxk zzvi() {
        return new zzxk("Failed to parse the message.");
    }

    static zzxk zzvj() {
        return new zzxk("Protocol message had invalid UTF-8.");
    }
}
