package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzabh {

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zza extends zzwz<zza, zzb> implements zzym {
        private static final zza zzcvj;
        private static volatile zzyx<zza> zzh;
        private int zzcvh;
        private zzi zzcvi;
        private int zzj;

        /* renamed from: com.google.android.gms.internal.firebase_ml.zzabh$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum EnumC0008zza implements zzxc {
            UNKNOWN_ENGINE(0),
            TFLITE(1);
            
            private static final zzxf<EnumC0008zza> zzac = new zzabk();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static EnumC0008zza zzek(int i) {
                if (i == 0) {
                    return UNKNOWN_ENGINE;
                }
                if (i != 1) {
                    return null;
                }
                return TFLITE;
            }

            public static zzxe zzf() {
                return zzabj.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private EnumC0008zza(int i) {
                this.value = i;
            }
        }

        private zza() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zza, zzb> implements zzym {
            private zzb() {
                super(zza.zzcvj);
            }

            /* synthetic */ zzb(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzcvj, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဉ\u0001", new Object[]{"zzj", "zzcvh", EnumC0008zza.zzf(), "zzcvi"});
                case 4:
                    return zzcvj;
                case 5:
                    zzyx<zza> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zza.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcvj);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zza zza = new zza();
            zzcvj = zza;
            zzwz.zza(zza.class, zza);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzb extends zzwz<zzb, zza> implements zzym {
        private static final zzb zzcvw;
        private static volatile zzyx<zzb> zzh;
        private String zzcuk = "";
        private int zzcvn;
        private String zzcvo = "";
        private String zzcvp = "";
        private String zzcvq = "";
        private String zzcvr = "";
        private String zzcvs = "";
        private String zzcvt = "";
        private String zzcvu = "";
        private String zzcvv = "";
        private int zzj;

        private zzb() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
            private zza() {
                super(zzb.zzcvw);
            }

            /* synthetic */ zza(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcvw, "\u0001\n\u0000\u0001\u0001\n\n\u0000\u0000\u0000\u0001င\u0000\u0002ဈ\u0001\u0003ဈ\u0002\u0004ဈ\u0003\u0005ဈ\u0004\u0006ဈ\u0005\u0007ဈ\u0006\bဈ\u0007\tဈ\b\nဈ\t", new Object[]{"zzj", "zzcvn", "zzcvo", "zzcvp", "zzcvq", "zzcvr", "zzcvs", "zzcvt", "zzcvu", "zzcuk", "zzcvv"});
                case 4:
                    return zzcvw;
                case 5:
                    zzyx<zzb> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzb.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcvw);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzb zzb = new zzb();
            zzcvw = zzb;
            zzwz.zza(zzb.class, zzb);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzc extends zzwz<zzc, C0009zzc> implements zzym {
        private static final zzc zzcwk;
        private static volatile zzyx<zzc> zzh;
        private int zzbfp;
        private String zzctp = "";
        private int zzcwd;
        private String zzcwe = "";
        private zza zzcwf;
        private zzd zzcwg;
        private int zzcwh;
        private zzxl<zzf> zzcwi = zzus();
        private int zzcwj;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            UNKNOWN_ACTION(0),
            INITIALIZATION(1),
            COMPILATION(2),
            EXECUTION(3),
            TEARDOWN(4);
            
            private static final zzxf<zza> zzac = new zzabm();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzel(int i) {
                if (i == 0) {
                    return UNKNOWN_ACTION;
                }
                if (i == 1) {
                    return INITIALIZATION;
                }
                if (i == 2) {
                    return COMPILATION;
                }
                if (i == 3) {
                    return EXECUTION;
                }
                if (i != 4) {
                    return null;
                }
                return TEARDOWN;
            }

            public static zzxe zzf() {
                return zzabl.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzcwn;
            private static volatile zzyx<zzb> zzh;
            private int zzcwl;
            private int zzcwm;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzcwn);
                }

                /* synthetic */ zza(zzabi zzabi) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzabi.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzcwn, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001င\u0000\u0002င\u0001", new Object[]{"zzj", "zzcwl", "zzcwm"});
                    case 4:
                        return zzcwn;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzcwn);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzcwn = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzd extends zzwz<zzd, zza> implements zzym {
            private static final zzd zzcwr;
            private static volatile zzyx<zzd> zzh;
            private zzb zzcwo;
            private zzb zzcwp;
            private boolean zzcwq;
            private int zzj;

            private zzd() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzd, zza> implements zzym {
                private zza() {
                    super(zzd.zzcwr);
                }

                /* synthetic */ zza(zzabi zzabi) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzabi.zzi[i - 1]) {
                    case 1:
                        return new zzd();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzcwr, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဇ\u0002", new Object[]{"zzj", "zzcwo", "zzcwp", "zzcwq"});
                    case 4:
                        return zzcwr;
                    case 5:
                        zzyx<zzd> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzd.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzcwr);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzd zzd = new zzd();
                zzcwr = zzd;
                zzwz.zza(zzd.class, zzd);
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zze implements zzxc {
            UNKNOWN_STATUS(0),
            COMPLETED_EVENT(1),
            MISSING_END_EVENT(2),
            HANG(3),
            ABANDONED_FROM_HANG(4),
            FORCED_CRASH_FROM_HANG(5);
            
            private static final zzxf<zze> zzac = new zzabo();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zze zzem(int i) {
                if (i == 0) {
                    return UNKNOWN_STATUS;
                }
                if (i == 1) {
                    return COMPLETED_EVENT;
                }
                if (i == 2) {
                    return MISSING_END_EVENT;
                }
                if (i == 3) {
                    return HANG;
                }
                if (i == 4) {
                    return ABANDONED_FROM_HANG;
                }
                if (i != 5) {
                    return null;
                }
                return FORCED_CRASH_FROM_HANG;
            }

            public static zzxe zzf() {
                return zzabn.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zze(int i) {
                this.value = i;
            }
        }

        private zzc() {
        }

        /* renamed from: com.google.android.gms.internal.firebase_ml.zzabh$zzc$zzc  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class C0009zzc extends zzwz.zzb<zzc, C0009zzc> implements zzym {
            private C0009zzc() {
                super(zzc.zzcwk);
            }

            /* synthetic */ C0009zzc(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new C0009zzc(null);
                case 3:
                    return zza(zzcwk, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0001\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဈ\u0002\u0004ဈ\u0003\u0005ဉ\u0004\u0006ဉ\u0005\u0007င\u0006\b\u001b\tင\u0007", new Object[]{"zzj", "zzcwd", zza.zzf(), "zzbfp", zze.zzf(), "zzctp", "zzcwe", "zzcwf", "zzcwg", "zzcwh", "zzcwi", zzf.class, "zzcwj"});
                case 4:
                    return zzcwk;
                case 5:
                    zzyx<zzc> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzc.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcwk);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzc zzc = new zzc();
            zzcwk = zzc;
            zzwz.zza(zzc.class, zzc);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzd extends zzwz<zzd, zza> implements zzym {
        private static final zzd zzcxd;
        private static volatile zzyx<zzd> zzh;
        private int zzctn;
        private int zzj;

        private zzd() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzd, zza> implements zzym {
            private zza() {
                super(zzd.zzcxd);
            }

            /* synthetic */ zza(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzd();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcxd, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001င\u0000", new Object[]{"zzj", "zzctn"});
                case 4:
                    return zzcxd;
                case 5:
                    zzyx<zzd> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzd.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcxd);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzd zzd = new zzd();
            zzcxd = zzd;
            zzwz.zza(zzd.class, zzd);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zze extends zzwz<zze, zza> implements zzym {
        private static final zze zzcxm;
        private static volatile zzyx<zze> zzh;
        private byte zzch = 2;
        private zzb zzcxe;
        private zzh zzcxf;
        private int zzcxg;
        private zzc zzcxh;
        private zzj zzcxi;
        private long zzcxj;
        private boolean zzcxk;
        private int zzcxl;
        private int zzj;
        private long zznz;

        private zze() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zze, zza> implements zzym {
            private zza() {
                super(zze.zzcxm);
            }

            /* synthetic */ zza(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            int i2 = 1;
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zze();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcxm, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0000\u0001\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဌ\u0002\u0004ဉ\u0003\u0005ᐉ\u0004\u0006ဂ\u0005\u0007ဂ\u0006\bဇ\u0007\tင\b", new Object[]{"zzj", "zzcxe", "zzcxf", "zzcxg", zzabq.zzf(), "zzcxh", "zzcxi", "zznz", "zzcxj", "zzcxk", "zzcxl"});
                case 4:
                    return zzcxm;
                case 5:
                    zzyx<zze> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zze.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcxm);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return Byte.valueOf(this.zzch);
                case 7:
                    if (obj == null) {
                        i2 = 0;
                    }
                    this.zzch = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zze zze = new zze();
            zzcxm = zze;
            zzwz.zza(zze.class, zze);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzf extends zzwz<zzf, zza> implements zzym {
        private static final zzf zzcxn;
        private static volatile zzyx<zzf> zzh;
        private zzxg zzaps = zzup();

        private zzf() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzf, zza> implements zzym {
            private zza() {
                super(zzf.zzcxn);
            }

            /* synthetic */ zza(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzf();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcxn, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u0016", new Object[]{"zzaps"});
                case 4:
                    return zzcxn;
                case 5:
                    zzyx<zzf> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzf.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcxn);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzf zzf = new zzf();
            zzcxn = zzf;
            zzwz.zza(zzf.class, zzf);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzg extends zzwz<zzg, zza> implements zzym {
        private static final zzg zzcxo;
        private static volatile zzyx<zzg> zzh;
        private String zzctv = "";
        private int zzctz;
        private int zzj;

        private zzg() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzg, zza> implements zzym {
            private zza() {
                super(zzg.zzcxo);
            }

            /* synthetic */ zza(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzg();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcxo, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002င\u0001", new Object[]{"zzj", "zzctv", "zzctz"});
                case 4:
                    return zzcxo;
                case 5:
                    zzyx<zzg> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzg.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcxo);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzg zzg = new zzg();
            zzcxo = zzg;
            zzwz.zza(zzg.class, zzg);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzh extends zzwz<zzh, zzb> implements zzym {
        private static final zzh zzcxq;
        private static volatile zzyx<zzh> zzh;
        private zzxl<zza> zzcxp = zzus();

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz<zza, C0010zza> implements zzym {
            private static final zza zzcxs;
            private static volatile zzyx<zza> zzh;
            private int zzapr;
            private String zzbak = "";
            private String zzbal = "";
            private long zzcxr;
            private int zzj;

            private zza() {
            }

            /* renamed from: com.google.android.gms.internal.firebase_ml.zzabh$zzh$zza$zza  reason: collision with other inner class name */
            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class C0010zza extends zzwz.zzb<zza, C0010zza> implements zzym {
                private C0010zza() {
                    super(zza.zzcxs);
                }

                /* synthetic */ C0010zza(zzabi zzabi) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzabi.zzi[i - 1]) {
                    case 1:
                        return new zza();
                    case 2:
                        return new C0010zza(null);
                    case 3:
                        return zza(zzcxs, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဈ\u0000\u0002င\u0001\u0003ဈ\u0002\u0004ဂ\u0003", new Object[]{"zzj", "zzbak", "zzapr", "zzbal", "zzcxr"});
                    case 4:
                        return zzcxs;
                    case 5:
                        zzyx<zza> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zza.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzcxs);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zza zza = new zza();
                zzcxs = zza;
                zzwz.zza(zza.class, zza);
            }
        }

        private zzh() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zzh, zzb> implements zzym {
            private zzb() {
                super(zzh.zzcxq);
            }

            /* synthetic */ zzb(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzh();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzcxq, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzcxp", zza.class});
                case 4:
                    return zzcxq;
                case 5:
                    zzyx<zzh> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzh.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcxq);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzh zzh2 = new zzh();
            zzcxq = zzh2;
            zzwz.zza(zzh.class, zzh2);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzi extends zzwz<zzi, zzb> implements zzym {
        private static final zzi zzcxv;
        private static volatile zzyx<zzi> zzh;
        private int zzcut;
        private zzg zzcxt;
        private zzd zzcxu;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            DELEGATE_NONE(0),
            NNAPI(1),
            GPU(2),
            HEXAGON(3);
            
            private static final zzxf<zza> zzac = new zzabt();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzeo(int i) {
                if (i == 0) {
                    return DELEGATE_NONE;
                }
                if (i == 1) {
                    return NNAPI;
                }
                if (i == 2) {
                    return GPU;
                }
                if (i != 3) {
                    return null;
                }
                return HEXAGON;
            }

            public static zzxe zzf() {
                return zzabs.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        private zzi() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zzi, zzb> implements zzym {
            private zzb() {
                super(zzi.zzcxv);
            }

            /* synthetic */ zzb(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzi();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzcxv, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzcut", zza.zzf(), "zzcxt", "zzcxu"});
                case 4:
                    return zzcxv;
                case 5:
                    zzyx<zzi> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzi.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcxv);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzi zzi = new zzi();
            zzcxv = zzi;
            zzwz.zza(zzi.class, zzi);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzj extends zzwz.zzc<zzj, zza> implements zzym {
        private static final zzj zzcyb;
        private static volatile zzyx<zzj> zzh;
        private byte zzch = 2;

        private zzj() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzd<zzj, zza> implements zzym {
            private zza() {
                super(zzj.zzcyb);
            }

            /* synthetic */ zza(zzabi zzabi) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            int i2 = 1;
            switch (zzabi.zzi[i - 1]) {
                case 1:
                    return new zzj();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzcyb, "\u0001\u0000", (Object[]) null);
                case 4:
                    return zzcyb;
                case 5:
                    zzyx<zzj> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzj.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzcyb);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return Byte.valueOf(this.zzch);
                case 7:
                    if (obj == null) {
                        i2 = 0;
                    }
                    this.zzch = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzj zzj = new zzj();
            zzcyb = zzj;
            zzwz.zza(zzj.class, zzj);
        }
    }
}
