package com.google.android.gms.internal.firebase_ml;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzmg {
    @NullableDecl
    String name;
    @NullableDecl
    Object value;
    @NullableDecl
    zzmg zzaka;

    private zzmg() {
    }
}
