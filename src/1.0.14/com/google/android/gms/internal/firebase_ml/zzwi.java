package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzwi extends zzvs {
    private static final Logger logger = Logger.getLogger(zzwi.class.getName());
    private static final boolean zzchy = zzaac.zzxc();
    zzwl zzchz;

    public static int zzaa(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    public static int zzac(long j) {
        return 8;
    }

    public static int zzad(long j) {
        return 8;
    }

    private static long zzae(long j) {
        return (j >> 63) ^ (j << 1);
    }

    public static int zzay(boolean z) {
        return 1;
    }

    public static int zzd(double d) {
        return 8;
    }

    public static int zzdh(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    public static int zzdj(int i) {
        return 4;
    }

    public static int zzdk(int i) {
        return 4;
    }

    private static int zzdn(int i) {
        return (i >> 31) ^ (i << 1);
    }

    public static zzwi zzg(byte[] bArr) {
        return new zza(bArr, 0, bArr.length);
    }

    public static int zzr(float f) {
        return 4;
    }

    public abstract void writeTag(int i, int i2) throws IOException;

    public abstract void zza(int i, long j) throws IOException;

    public abstract void zza(int i, zzvv zzvv) throws IOException;

    public abstract void zza(int i, zzyk zzyk) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(int i, zzyk zzyk, zzze zzze) throws IOException;

    public abstract void zza(int i, boolean z) throws IOException;

    public abstract void zzb(int i, zzvv zzvv) throws IOException;

    public abstract void zzb(int i, String str) throws IOException;

    public abstract void zzb(zzyk zzyk) throws IOException;

    public abstract void zzc(int i, long j) throws IOException;

    public abstract void zzc(zzvv zzvv) throws IOException;

    public abstract void zzck(String str) throws IOException;

    public abstract void zzd(byte b) throws IOException;

    public abstract void zzdb(int i) throws IOException;

    public abstract void zzdc(int i) throws IOException;

    public abstract void zzde(int i) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zze(byte[] bArr, int i, int i2) throws IOException;

    public abstract void zzh(int i, int i2) throws IOException;

    public abstract void zzi(int i, int i2) throws IOException;

    public abstract void zzk(int i, int i2) throws IOException;

    public abstract int zzty();

    public abstract void zzw(long j) throws IOException;

    public abstract void zzy(long j) throws IOException;

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static class zzb extends IOException {
        zzb() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        zzb(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        zzb(String str, Throwable th) {
            super(r3.length() != 0 ? "CodedOutputStream was writing to a flat byte array and ran out of space.: ".concat(r3) : new String("CodedOutputStream was writing to a flat byte array and ran out of space.: "), th);
            String valueOf = String.valueOf(str);
        }
    }

    private zzwi() {
    }

    public final void zzj(int i, int i2) throws IOException {
        zzi(i, zzdn(i2));
    }

    public final void zzb(int i, long j) throws IOException {
        zza(i, zzae(j));
    }

    public final void zza(int i, float f) throws IOException {
        zzk(i, Float.floatToRawIntBits(f));
    }

    public final void zza(int i, double d) throws IOException {
        zzc(i, Double.doubleToRawLongBits(d));
    }

    public final void zzdd(int i) throws IOException {
        zzdc(zzdn(i));
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static class zza extends zzwi {
        private final byte[] buffer;
        private final int limit;
        private final int offset;
        private int position;

        zza(byte[] bArr, int i, int i2) {
            super();
            Objects.requireNonNull(bArr, "buffer");
            int i3 = i2 + 0;
            if ((i2 | 0 | (bArr.length - i3)) >= 0) {
                this.buffer = bArr;
                this.offset = 0;
                this.position = 0;
                this.limit = i3;
                return;
            }
            throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), 0, Integer.valueOf(i2)));
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void writeTag(int i, int i2) throws IOException {
            zzdc((i << 3) | i2);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzh(int i, int i2) throws IOException {
            writeTag(i, 0);
            zzdb(i2);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzi(int i, int i2) throws IOException {
            writeTag(i, 0);
            zzdc(i2);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzk(int i, int i2) throws IOException {
            writeTag(i, 5);
            zzde(i2);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zza(int i, long j) throws IOException {
            writeTag(i, 0);
            zzw(j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzc(int i, long j) throws IOException {
            writeTag(i, 1);
            zzy(j);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zza(int i, boolean z) throws IOException {
            writeTag(i, 0);
            zzd(z ? (byte) 1 : 0);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzb(int i, String str) throws IOException {
            writeTag(i, 2);
            zzck(str);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zza(int i, zzvv zzvv) throws IOException {
            writeTag(i, 2);
            zzc(zzvv);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzc(zzvv zzvv) throws IOException {
            zzdc(zzvv.size());
            zzvv.zza(this);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zze(byte[] bArr, int i, int i2) throws IOException {
            zzdc(i2);
            write(bArr, 0, i2);
        }

        /* access modifiers changed from: package-private */
        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zza(int i, zzyk zzyk, zzze zzze) throws IOException {
            writeTag(i, 2);
            zzvl zzvl = (zzvl) zzyk;
            int zzth = zzvl.zzth();
            if (zzth == -1) {
                zzth = zzze.zzaa(zzvl);
                zzvl.zzcs(zzth);
            }
            zzdc(zzth);
            zzze.zza(zzyk, this.zzchz);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zza(int i, zzyk zzyk) throws IOException {
            writeTag(1, 3);
            zzi(2, i);
            writeTag(3, 2);
            zzb(zzyk);
            writeTag(1, 4);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzb(int i, zzvv zzvv) throws IOException {
            writeTag(1, 3);
            zzi(2, i);
            zza(3, zzvv);
            writeTag(1, 4);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzb(zzyk zzyk) throws IOException {
            zzdc(zzyk.zzuo());
            zzyk.zzb(this);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzd(byte b) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i = this.position;
                this.position = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzdb(int i) throws IOException {
            if (i >= 0) {
                zzdc(i);
            } else {
                zzw((long) i);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzdc(int i) throws IOException {
            if (!zzwi.zzchy || zzvo.zztj() || zzty() < 5) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.buffer;
                    int i2 = this.position;
                    this.position = i2 + 1;
                    bArr[i2] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.buffer;
                    int i3 = this.position;
                    this.position = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
                }
            } else if ((i & -128) == 0) {
                byte[] bArr3 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                zzaac.zza(bArr3, (long) i4, (byte) i);
            } else {
                byte[] bArr4 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                zzaac.zza(bArr4, (long) i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & -128) == 0) {
                    byte[] bArr5 = this.buffer;
                    int i7 = this.position;
                    this.position = i7 + 1;
                    zzaac.zza(bArr5, (long) i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.buffer;
                int i8 = this.position;
                this.position = i8 + 1;
                zzaac.zza(bArr6, (long) i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & -128) == 0) {
                    byte[] bArr7 = this.buffer;
                    int i10 = this.position;
                    this.position = i10 + 1;
                    zzaac.zza(bArr7, (long) i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.buffer;
                int i11 = this.position;
                this.position = i11 + 1;
                zzaac.zza(bArr8, (long) i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & -128) == 0) {
                    byte[] bArr9 = this.buffer;
                    int i13 = this.position;
                    this.position = i13 + 1;
                    zzaac.zza(bArr9, (long) i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.buffer;
                int i14 = this.position;
                this.position = i14 + 1;
                zzaac.zza(bArr10, (long) i14, (byte) (i12 | 128));
                byte[] bArr11 = this.buffer;
                int i15 = this.position;
                this.position = i15 + 1;
                zzaac.zza(bArr11, (long) i15, (byte) (i12 >>> 7));
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzde(int i) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i2 = this.position;
                int i3 = i2 + 1;
                this.position = i3;
                bArr[i2] = (byte) i;
                int i4 = i3 + 1;
                this.position = i4;
                bArr[i3] = (byte) (i >> 8);
                int i5 = i4 + 1;
                this.position = i5;
                bArr[i4] = (byte) (i >> 16);
                this.position = i5 + 1;
                bArr[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzw(long j) throws IOException {
            if (!zzwi.zzchy || zzty() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.buffer;
                    int i = this.position;
                    this.position = i + 1;
                    bArr[i] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.buffer;
                    int i2 = this.position;
                    this.position = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
                }
            } else {
                while ((j & -128) != 0) {
                    byte[] bArr3 = this.buffer;
                    int i3 = this.position;
                    this.position = i3 + 1;
                    zzaac.zza(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                byte[] bArr4 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                zzaac.zza(bArr4, (long) i4, (byte) ((int) j));
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzy(long j) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i = this.position;
                int i2 = i + 1;
                this.position = i2;
                bArr[i] = (byte) ((int) j);
                int i3 = i2 + 1;
                this.position = i3;
                bArr[i2] = (byte) ((int) (j >> 8));
                int i4 = i3 + 1;
                this.position = i4;
                bArr[i3] = (byte) ((int) (j >> 16));
                int i5 = i4 + 1;
                this.position = i5;
                bArr[i4] = (byte) ((int) (j >> 24));
                int i6 = i5 + 1;
                this.position = i6;
                bArr[i5] = (byte) ((int) (j >> 32));
                int i7 = i6 + 1;
                this.position = i7;
                bArr[i6] = (byte) ((int) (j >> 40));
                int i8 = i7 + 1;
                this.position = i8;
                bArr[i7] = (byte) ((int) (j >> 48));
                this.position = i8 + 1;
                bArr[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        private final void write(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.buffer, this.position, i2);
                this.position += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(i2)), e);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzvs
        public final void zzb(byte[] bArr, int i, int i2) throws IOException {
            write(bArr, i, i2);
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final void zzck(String str) throws IOException {
            int i = this.position;
            try {
                int zzdh = zzdh(str.length() * 3);
                int zzdh2 = zzdh(str.length());
                if (zzdh2 == zzdh) {
                    int i2 = i + zzdh2;
                    this.position = i2;
                    int zza = zzaaf.zza(str, this.buffer, i2, zzty());
                    this.position = i;
                    zzdc((zza - i) - zzdh2);
                    this.position = zza;
                    return;
                }
                zzdc(zzaaf.zzb(str));
                this.position = zzaaf.zza(str, this.buffer, this.position, zzty());
            } catch (zzaai e) {
                this.position = i;
                zza(str, e);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzb(e2);
            }
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzwi
        public final int zzty() {
            return this.limit - this.position;
        }
    }

    public final void zzx(long j) throws IOException {
        zzw(zzae(j));
    }

    public final void zzq(float f) throws IOException {
        zzde(Float.floatToRawIntBits(f));
    }

    public final void zzc(double d) throws IOException {
        zzy(Double.doubleToRawLongBits(d));
    }

    public final void zzax(boolean z) throws IOException {
        zzd(z ? (byte) 1 : 0);
    }

    public static int zzl(int i, int i2) {
        return zzdf(i) + zzdg(i2);
    }

    public static int zzm(int i, int i2) {
        return zzdf(i) + zzdh(i2);
    }

    public static int zzn(int i, int i2) {
        return zzdf(i) + zzdh(zzdn(i2));
    }

    public static int zzo(int i, int i2) {
        return zzdf(i) + 4;
    }

    public static int zzp(int i, int i2) {
        return zzdf(i) + 4;
    }

    public static int zzd(int i, long j) {
        return zzdf(i) + zzaa(j);
    }

    public static int zze(int i, long j) {
        return zzdf(i) + zzaa(j);
    }

    public static int zzf(int i, long j) {
        return zzdf(i) + zzaa(zzae(j));
    }

    public static int zzg(int i, long j) {
        return zzdf(i) + 8;
    }

    public static int zzh(int i, long j) {
        return zzdf(i) + 8;
    }

    public static int zzb(int i, float f) {
        return zzdf(i) + 4;
    }

    public static int zzb(int i, double d) {
        return zzdf(i) + 8;
    }

    public static int zzb(int i, boolean z) {
        return zzdf(i) + 1;
    }

    public static int zzq(int i, int i2) {
        return zzdf(i) + zzdg(i2);
    }

    public static int zzc(int i, String str) {
        return zzdf(i) + zzcl(str);
    }

    public static int zzc(int i, zzvv zzvv) {
        int zzdf = zzdf(i);
        int size = zzvv.size();
        return zzdf + zzdh(size) + size;
    }

    public static int zza(int i, zzxt zzxt) {
        int zzdf = zzdf(i);
        int zzuo = zzxt.zzuo();
        return zzdf + zzdh(zzuo) + zzuo;
    }

    static int zzb(int i, zzyk zzyk, zzze zzze) {
        return zzdf(i) + zza(zzyk, zzze);
    }

    public static int zzb(int i, zzyk zzyk) {
        return (zzdf(1) << 1) + zzm(2, i) + zzdf(3) + zzc(zzyk);
    }

    public static int zzd(int i, zzvv zzvv) {
        return (zzdf(1) << 1) + zzm(2, i) + zzc(3, zzvv);
    }

    public static int zzb(int i, zzxt zzxt) {
        return (zzdf(1) << 1) + zzm(2, i) + zza(3, zzxt);
    }

    public static int zzdf(int i) {
        return zzdh(i << 3);
    }

    public static int zzdg(int i) {
        if (i >= 0) {
            return zzdh(i);
        }
        return 10;
    }

    public static int zzdi(int i) {
        return zzdh(zzdn(i));
    }

    public static int zzz(long j) {
        return zzaa(j);
    }

    public static int zzab(long j) {
        return zzaa(zzae(j));
    }

    public static int zzdl(int i) {
        return zzdg(i);
    }

    public static int zzcl(String str) {
        int i;
        try {
            i = zzaaf.zzb(str);
        } catch (zzaai unused) {
            i = str.getBytes(zzxd.UTF_8).length;
        }
        return zzdh(i) + i;
    }

    public static int zza(zzxt zzxt) {
        int zzuo = zzxt.zzuo();
        return zzdh(zzuo) + zzuo;
    }

    public static int zzd(zzvv zzvv) {
        int size = zzvv.size();
        return zzdh(size) + size;
    }

    public static int zzh(byte[] bArr) {
        int length = bArr.length;
        return zzdh(length) + length;
    }

    public static int zzc(zzyk zzyk) {
        int zzuo = zzyk.zzuo();
        return zzdh(zzuo) + zzuo;
    }

    static int zza(zzyk zzyk, zzze zzze) {
        zzvl zzvl = (zzvl) zzyk;
        int zzth = zzvl.zzth();
        if (zzth == -1) {
            zzth = zzze.zzaa(zzvl);
            zzvl.zzcs(zzth);
        }
        return zzdh(zzth) + zzth;
    }

    static int zzdm(int i) {
        return zzdh(i) + i;
    }

    public final void zztz() {
        if (zzty() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(String str, zzaai zzaai) throws IOException {
        logger.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) zzaai);
        byte[] bytes = str.getBytes(zzxd.UTF_8);
        try {
            zzdc(bytes.length);
            zzb(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new zzb(e);
        } catch (zzb e2) {
            throw e2;
        }
    }

    @Deprecated
    static int zzc(int i, zzyk zzyk, zzze zzze) {
        int zzdf = zzdf(i) << 1;
        zzvl zzvl = (zzvl) zzyk;
        int zzth = zzvl.zzth();
        if (zzth == -1) {
            zzth = zzze.zzaa(zzvl);
            zzvl.zzcs(zzth);
        }
        return zzdf + zzth;
    }

    @Deprecated
    public static int zzd(zzyk zzyk) {
        return zzyk.zzuo();
    }

    @Deprecated
    public static int zzdo(int i) {
        return zzdh(i);
    }
}
