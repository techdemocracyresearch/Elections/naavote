package com.google.android.gms.internal.firebase_ml;

import okio.Utf8;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzim extends zzip {
    private static final byte[] zzafi = {13, 10};
    private static final byte[] zzafj = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] zzafk = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] zzafl = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, Utf8.REPLACEMENT_BYTE, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, Utf8.REPLACEMENT_BYTE, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private final byte[] zzafm;
    private final byte[] zzafn;
    private final byte[] zzafo;
    private final int zzafp;
    private final int zzafq;

    public zzim() {
        this(0);
    }

    private zzim(int i) {
        this(0, zzafi);
    }

    private zzim(int i, byte[] bArr) {
        this(0, bArr, false);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0043  */
    private zzim(int i, byte[] bArr, boolean z) {
        super(3, 4, i, bArr == null ? 0 : bArr.length);
        boolean z2;
        this.zzafn = zzafl;
        if (bArr != null) {
            if (bArr != null) {
                int length = bArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    byte b = bArr[i2];
                    if (this.zzafw == b || zza(b)) {
                        z2 = true;
                    } else {
                        i2++;
                    }
                }
                z2 = true;
                if (!z2) {
                    String zzd = zzir.zzd(bArr);
                    throw new IllegalArgumentException("lineSeparator must not contain base64 characters: [" + zzd + "]");
                } else if (i > 0) {
                    this.zzafq = bArr.length + 4;
                    byte[] bArr2 = new byte[bArr.length];
                    this.zzafo = bArr2;
                    System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                } else {
                    this.zzafq = 4;
                    this.zzafo = null;
                }
            }
            z2 = false;
            if (!z2) {
            }
        } else {
            this.zzafq = 4;
            this.zzafo = null;
        }
        this.zzafp = this.zzafq - 1;
        this.zzafm = z ? zzafk : zzafj;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzip
    public final void zza(byte[] bArr, int i, int i2, zzio zzio) {
        if (!zzio.eof) {
            if (i2 < 0) {
                zzio.eof = true;
                if (zzio.zzafu != 0 || this.zzafz != 0) {
                    byte[] zza = zza(this.zzafq, zzio);
                    int i3 = zzio.pos;
                    int i4 = zzio.zzafu;
                    if (i4 != 0) {
                        if (i4 == 1) {
                            int i5 = zzio.pos;
                            zzio.pos = i5 + 1;
                            zza[i5] = this.zzafm[(zzio.zzafr >> 2) & 63];
                            int i6 = zzio.pos;
                            zzio.pos = i6 + 1;
                            zza[i6] = this.zzafm[(zzio.zzafr << 4) & 63];
                            if (this.zzafm == zzafj) {
                                int i7 = zzio.pos;
                                zzio.pos = i7 + 1;
                                zza[i7] = this.zzafw;
                                int i8 = zzio.pos;
                                zzio.pos = i8 + 1;
                                zza[i8] = this.zzafw;
                            }
                        } else if (i4 == 2) {
                            int i9 = zzio.pos;
                            zzio.pos = i9 + 1;
                            zza[i9] = this.zzafm[(zzio.zzafr >> 10) & 63];
                            int i10 = zzio.pos;
                            zzio.pos = i10 + 1;
                            zza[i10] = this.zzafm[(zzio.zzafr >> 4) & 63];
                            int i11 = zzio.pos;
                            zzio.pos = i11 + 1;
                            zza[i11] = this.zzafm[(zzio.zzafr << 2) & 63];
                            if (this.zzafm == zzafj) {
                                int i12 = zzio.pos;
                                zzio.pos = i12 + 1;
                                zza[i12] = this.zzafw;
                            }
                        } else {
                            throw new IllegalStateException("Impossible modulus " + zzio.zzafu);
                        }
                    }
                    zzio.zzaft += zzio.pos - i3;
                    if (this.zzafz > 0 && zzio.zzaft > 0) {
                        System.arraycopy(this.zzafo, 0, zza, zzio.pos, this.zzafo.length);
                        zzio.pos += this.zzafo.length;
                        return;
                    }
                    return;
                }
                return;
            }
            int i13 = 0;
            while (i13 < i2) {
                byte[] zza2 = zza(this.zzafq, zzio);
                zzio.zzafu = (zzio.zzafu + 1) % 3;
                int i14 = i + 1;
                byte b = bArr[i];
                int i15 = b;
                if (b < 0) {
                    i15 = b + 256;
                }
                zzio.zzafr = (zzio.zzafr << 8) + (i15 == 1 ? 1 : 0);
                if (zzio.zzafu == 0) {
                    int i16 = zzio.pos;
                    zzio.pos = i16 + 1;
                    zza2[i16] = this.zzafm[(zzio.zzafr >> 18) & 63];
                    int i17 = zzio.pos;
                    zzio.pos = i17 + 1;
                    zza2[i17] = this.zzafm[(zzio.zzafr >> 12) & 63];
                    int i18 = zzio.pos;
                    zzio.pos = i18 + 1;
                    zza2[i18] = this.zzafm[(zzio.zzafr >> 6) & 63];
                    int i19 = zzio.pos;
                    zzio.pos = i19 + 1;
                    zza2[i19] = this.zzafm[zzio.zzafr & 63];
                    zzio.zzaft += 4;
                    if (this.zzafz > 0 && this.zzafz <= zzio.zzaft) {
                        System.arraycopy(this.zzafo, 0, zza2, zzio.pos, this.zzafo.length);
                        zzio.pos += this.zzafo.length;
                        zzio.zzaft = 0;
                    }
                }
                i13++;
                i = i14;
            }
        }
    }

    public static String zzb(byte[] bArr) {
        if (!(bArr == null || bArr.length == 0)) {
            zzim zzim = new zzim(0, zzafi, true);
            long zzc = zzim.zzc(bArr);
            if (zzc > 2147483647L) {
                throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + zzc + ") than the specified maximum size of 2147483647");
            } else if (!(bArr == null || bArr.length == 0)) {
                zzio zzio = new zzio();
                zzim.zza(bArr, 0, bArr.length, zzio);
                zzim.zza(bArr, 0, -1, zzio);
                int i = zzio.pos - zzio.zzafs;
                byte[] bArr2 = new byte[i];
                if (zzio.buffer != null) {
                    int min = Math.min(zzio.buffer != null ? zzio.pos - zzio.zzafs : 0, i);
                    System.arraycopy(zzio.buffer, zzio.zzafs, bArr2, 0, min);
                    zzio.zzafs += min;
                    if (zzio.zzafs >= zzio.pos) {
                        zzio.buffer = null;
                    }
                } else {
                    boolean z = zzio.eof;
                }
                bArr = bArr2;
            }
        }
        return zzir.zzd(bArr);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzip
    public final boolean zza(byte b) {
        if (b < 0) {
            return false;
        }
        byte[] bArr = this.zzafn;
        return b < bArr.length && bArr[b] != -1;
    }
}
