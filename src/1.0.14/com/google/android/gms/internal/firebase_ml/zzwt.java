package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwt;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzwt<T extends zzwt<T>> extends Comparable<T> {
    zzyn zza(zzyn zzyn, zzyk zzyk);

    zzyt zza(zzyt zzyt, zzyt zzyt2);

    int zzd();

    zzaan zzui();

    zzaaq zzuj();

    boolean zzuk();

    boolean zzul();
}
