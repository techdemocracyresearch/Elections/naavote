package com.google.android.gms.vision;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import com.facebook.imagepipeline.common.RotationOptions;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.Frame;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public class CameraSource {
    public static final int CAMERA_FACING_BACK = 0;
    public static final int CAMERA_FACING_FRONT = 1;
    private int facing;
    private int rotation;
    private Context zze;
    private final Object zzf;
    private Camera zzg;
    private Size zzh;
    private float zzi;
    private int zzj;
    private int zzk;
    private boolean zzl;
    private String zzm;
    private SurfaceTexture zzn;
    private boolean zzo;
    private Thread zzp;
    private zzb zzq;
    private Map<byte[], ByteBuffer> zzr;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public interface PictureCallback {
        void onPictureTaken(byte[] bArr);
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public interface ShutterCallback {
        void onShutter();
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public class zza implements Camera.PreviewCallback {
        private zza() {
        }

        public final void onPreviewFrame(byte[] bArr, Camera camera) {
            CameraSource.this.zzq.zza(bArr, camera);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    static class zzc implements Camera.ShutterCallback {
        private ShutterCallback zzaa;

        private zzc() {
        }

        public final void onShutter() {
            ShutterCallback shutterCallback = this.zzaa;
            if (shutterCallback != null) {
                shutterCallback.onShutter();
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    class zzd implements Camera.PictureCallback {
        private PictureCallback zzab;

        private zzd() {
        }

        public final void onPictureTaken(byte[] bArr, Camera camera) {
            PictureCallback pictureCallback = this.zzab;
            if (pictureCallback != null) {
                pictureCallback.onPictureTaken(bArr);
            }
            synchronized (CameraSource.this.zzf) {
                if (CameraSource.this.zzg != null) {
                    CameraSource.this.zzg.startPreview();
                }
            }
        }
    }

    public void release() {
        synchronized (this.zzf) {
            stop();
            this.zzq.release();
        }
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static class zze {
        private Size zzac;
        private Size zzad;

        public zze(Camera.Size size, @Nullable Camera.Size size2) {
            this.zzac = new Size(size.width, size.height);
            if (size2 != null) {
                this.zzad = new Size(size2.width, size2.height);
            }
        }

        public final Size zzb() {
            return this.zzac;
        }

        @Nullable
        public final Size zzc() {
            return this.zzad;
        }
    }

    public CameraSource start() throws IOException {
        synchronized (this.zzf) {
            if (this.zzg != null) {
                return this;
            }
            this.zzg = zza();
            SurfaceTexture surfaceTexture = new SurfaceTexture(100);
            this.zzn = surfaceTexture;
            this.zzg.setPreviewTexture(surfaceTexture);
            this.zzo = true;
            this.zzg.startPreview();
            Thread thread = new Thread(this.zzq);
            this.zzp = thread;
            thread.setName("gms.vision.CameraSource");
            this.zzq.setActive(true);
            this.zzp.start();
            return this;
        }
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public class zzb implements Runnable {
        private final Object lock = new Object();
        private Detector<?> zzt;
        private long zzv = SystemClock.elapsedRealtime();
        private boolean zzw = true;
        private long zzx;
        private int zzy = 0;
        private ByteBuffer zzz;

        zzb(Detector<?> detector) {
            this.zzt = detector;
        }

        /* access modifiers changed from: package-private */
        public final void release() {
            this.zzt.release();
            this.zzt = null;
        }

        /* access modifiers changed from: package-private */
        public final void setActive(boolean z) {
            synchronized (this.lock) {
                this.zzw = z;
                this.lock.notifyAll();
            }
        }

        /* access modifiers changed from: package-private */
        public final void zza(byte[] bArr, Camera camera) {
            synchronized (this.lock) {
                ByteBuffer byteBuffer = this.zzz;
                if (byteBuffer != null) {
                    camera.addCallbackBuffer(byteBuffer.array());
                    this.zzz = null;
                }
                if (!CameraSource.this.zzr.containsKey(bArr)) {
                    Log.d("CameraSource", "Skipping frame. Could not find ByteBuffer associated with the image data from the camera.");
                    return;
                }
                this.zzx = SystemClock.elapsedRealtime() - this.zzv;
                this.zzy++;
                this.zzz = (ByteBuffer) CameraSource.this.zzr.get(bArr);
                this.lock.notifyAll();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r6.zzt.receiveFrame(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0073, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0075, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
            android.util.Log.e("CameraSource", "Exception thrown from receiver.", r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x008c, code lost:
            r6.zzs.zzg.addCallbackBuffer(r2.array());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0099, code lost:
            throw r0;
         */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x001f  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x001d A[SYNTHETIC] */
        public final void run() {
            boolean z;
            while (true) {
                synchronized (this.lock) {
                    while (true) {
                        z = this.zzw;
                        if (z && this.zzz == null) {
                            try {
                                this.lock.wait();
                            } catch (InterruptedException e) {
                                Log.d("CameraSource", "Frame processing loop terminated.", e);
                                return;
                            }
                        } else if (!z) {
                            Frame build = new Frame.Builder().setImageData(this.zzz, CameraSource.this.zzh.getWidth(), CameraSource.this.zzh.getHeight(), 17).setId(this.zzy).setTimestampMillis(this.zzx).setRotation(CameraSource.this.rotation).build();
                            ByteBuffer byteBuffer = this.zzz;
                            this.zzz = null;
                        } else {
                            return;
                        }
                    }
                    if (!z) {
                    }
                }
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
    public static class Builder {
        private final Detector<?> zzt;
        private CameraSource zzu;

        public Builder(Context context, Detector<?> detector) {
            CameraSource cameraSource = new CameraSource();
            this.zzu = cameraSource;
            if (context == null) {
                throw new IllegalArgumentException("No context supplied.");
            } else if (detector != null) {
                this.zzt = detector;
                cameraSource.zze = context;
            } else {
                throw new IllegalArgumentException("No detector supplied.");
            }
        }

        public Builder setRequestedFps(float f) {
            if (f > 0.0f) {
                this.zzu.zzi = f;
                return this;
            }
            StringBuilder sb = new StringBuilder(28);
            sb.append("Invalid fps: ");
            sb.append(f);
            throw new IllegalArgumentException(sb.toString());
        }

        public Builder setRequestedPreviewSize(int i, int i2) {
            if (i <= 0 || i > 1000000 || i2 <= 0 || i2 > 1000000) {
                StringBuilder sb = new StringBuilder(45);
                sb.append("Invalid preview size: ");
                sb.append(i);
                sb.append("x");
                sb.append(i2);
                throw new IllegalArgumentException(sb.toString());
            }
            this.zzu.zzj = i;
            this.zzu.zzk = i2;
            return this;
        }

        public Builder setFacing(int i) {
            if (i == 0 || i == 1) {
                this.zzu.facing = i;
                return this;
            }
            StringBuilder sb = new StringBuilder(27);
            sb.append("Invalid camera: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }

        public Builder setAutoFocusEnabled(boolean z) {
            this.zzu.zzl = z;
            return this;
        }

        public Builder setFocusMode(String str) {
            if (!str.equals("continuous-video") && !str.equals("continuous-picture")) {
                Log.w("CameraSource", String.format("FocusMode %s is not supported for now.", str));
                str = null;
            }
            this.zzu.zzm = str;
            return this;
        }

        public CameraSource build() {
            CameraSource cameraSource = this.zzu;
            cameraSource.getClass();
            cameraSource.zzq = new zzb(this.zzt);
            return this.zzu;
        }
    }

    public CameraSource start(SurfaceHolder surfaceHolder) throws IOException {
        synchronized (this.zzf) {
            if (this.zzg != null) {
                return this;
            }
            Camera zza2 = zza();
            this.zzg = zza2;
            zza2.setPreviewDisplay(surfaceHolder);
            this.zzg.startPreview();
            this.zzp = new Thread(this.zzq);
            this.zzq.setActive(true);
            this.zzp.start();
            this.zzo = false;
            return this;
        }
    }

    public void stop() {
        synchronized (this.zzf) {
            this.zzq.setActive(false);
            Thread thread = this.zzp;
            if (thread != null) {
                try {
                    thread.join();
                } catch (InterruptedException unused) {
                    Log.d("CameraSource", "Frame processing thread interrupted on release.");
                }
                this.zzp = null;
            }
            Camera camera = this.zzg;
            if (camera != null) {
                camera.stopPreview();
                this.zzg.setPreviewCallbackWithBuffer(null);
                try {
                    if (this.zzo) {
                        this.zzg.setPreviewTexture(null);
                    } else {
                        this.zzg.setPreviewDisplay(null);
                    }
                } catch (Exception e) {
                    String valueOf = String.valueOf(e);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 32);
                    sb.append("Failed to clear camera preview: ");
                    sb.append(valueOf);
                    Log.e("CameraSource", sb.toString());
                }
                this.zzg.release();
                this.zzg = null;
            }
            this.zzr.clear();
        }
    }

    public Size getPreviewSize() {
        return this.zzh;
    }

    public int getCameraFacing() {
        return this.facing;
    }

    public void takePicture(ShutterCallback shutterCallback, PictureCallback pictureCallback) {
        synchronized (this.zzf) {
            if (this.zzg != null) {
                zzc zzc2 = new zzc();
                zzc2.zzaa = shutterCallback;
                zzd zzd2 = new zzd();
                zzd2.zzab = pictureCallback;
                this.zzg.takePicture(zzc2, null, null, zzd2);
            }
        }
    }

    private CameraSource() {
        this.zzf = new Object();
        this.facing = 0;
        this.zzi = 30.0f;
        this.zzj = 1024;
        this.zzk = 768;
        this.zzl = false;
        this.zzr = new HashMap();
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01b4  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01eb  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01f1  */
    private final Camera zza() throws IOException {
        int i;
        Camera.CameraInfo cameraInfo;
        int i2;
        int i3;
        int i4 = this.facing;
        Camera.CameraInfo cameraInfo2 = new Camera.CameraInfo();
        int i5 = 0;
        while (true) {
            if (i5 >= Camera.getNumberOfCameras()) {
                i5 = -1;
                break;
            }
            Camera.getCameraInfo(i5, cameraInfo2);
            if (cameraInfo2.facing == i4) {
                break;
            }
            i5++;
        }
        if (i5 != -1) {
            Camera open = Camera.open(i5);
            int i6 = this.zzj;
            int i7 = this.zzk;
            Camera.Parameters parameters = open.getParameters();
            List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
            List<Camera.Size> supportedPictureSizes = parameters.getSupportedPictureSizes();
            ArrayList arrayList = new ArrayList();
            for (Camera.Size size : supportedPreviewSizes) {
                float f = ((float) size.width) / ((float) size.height);
                Iterator<Camera.Size> it = supportedPictureSizes.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Camera.Size next = it.next();
                    if (Math.abs(f - (((float) next.width) / ((float) next.height))) < 0.01f) {
                        arrayList.add(new zze(size, next));
                        break;
                    }
                }
            }
            if (arrayList.size() == 0) {
                Log.w("CameraSource", "No preview sizes have a corresponding same-aspect-ratio picture size");
                for (Camera.Size size2 : supportedPreviewSizes) {
                    arrayList.add(new zze(size2, null));
                }
            }
            ArrayList arrayList2 = arrayList;
            int size3 = arrayList2.size();
            int i8 = Integer.MAX_VALUE;
            zze zze2 = null;
            int i9 = 0;
            int i10 = Integer.MAX_VALUE;
            while (i9 < size3) {
                Object obj = arrayList2.get(i9);
                i9++;
                zze zze3 = (zze) obj;
                Size zzb2 = zze3.zzb();
                int abs = Math.abs(zzb2.getWidth() - i6) + Math.abs(zzb2.getHeight() - i7);
                if (abs < i10) {
                    zze2 = zze3;
                    i10 = abs;
                }
            }
            if (zze2 != null) {
                Size zzc2 = zze2.zzc();
                this.zzh = zze2.zzb();
                int i11 = (int) (this.zzi * 1000.0f);
                int[] iArr = null;
                for (int[] iArr2 : open.getParameters().getSupportedPreviewFpsRange()) {
                    int abs2 = Math.abs(i11 - iArr2[0]) + Math.abs(i11 - iArr2[1]);
                    if (abs2 < i8) {
                        iArr = iArr2;
                        i8 = abs2;
                    }
                }
                if (iArr != null) {
                    Camera.Parameters parameters2 = open.getParameters();
                    if (zzc2 != null) {
                        parameters2.setPictureSize(zzc2.getWidth(), zzc2.getHeight());
                    }
                    parameters2.setPreviewSize(this.zzh.getWidth(), this.zzh.getHeight());
                    parameters2.setPreviewFpsRange(iArr[0], iArr[1]);
                    parameters2.setPreviewFormat(17);
                    int rotation2 = ((WindowManager) this.zze.getSystemService("window")).getDefaultDisplay().getRotation();
                    if (rotation2 != 0) {
                        if (rotation2 == 1) {
                            i = 90;
                        } else if (rotation2 == 2) {
                            i = RotationOptions.ROTATE_180;
                        } else if (rotation2 != 3) {
                            StringBuilder sb = new StringBuilder(31);
                            sb.append("Bad rotation value: ");
                            sb.append(rotation2);
                            Log.e("CameraSource", sb.toString());
                        } else {
                            i = RotationOptions.ROTATE_270;
                        }
                        cameraInfo = new Camera.CameraInfo();
                        Camera.getCameraInfo(i5, cameraInfo);
                        if (cameraInfo.facing != 1) {
                            i2 = (cameraInfo.orientation + i) % 360;
                            i3 = (360 - i2) % 360;
                        } else {
                            i2 = ((cameraInfo.orientation - i) + 360) % 360;
                            i3 = i2;
                        }
                        this.rotation = i2 / 90;
                        open.setDisplayOrientation(i3);
                        parameters2.setRotation(i2);
                        if (this.zzm != null) {
                            if (parameters2.getSupportedFocusModes().contains(this.zzm)) {
                                parameters2.setFocusMode(this.zzm);
                            } else {
                                Log.w("CameraSource", String.format("FocusMode %s is not supported on this device.", this.zzm));
                                this.zzm = null;
                            }
                        }
                        if (this.zzm == null && this.zzl) {
                            if (!parameters2.getSupportedFocusModes().contains("continuous-video")) {
                                parameters2.setFocusMode("continuous-video");
                                this.zzm = "continuous-video";
                            } else {
                                Log.i("CameraSource", "Camera auto focus is not supported on this device.");
                            }
                        }
                        open.setParameters(parameters2);
                        open.setPreviewCallbackWithBuffer(new zza());
                        open.addCallbackBuffer(zza(this.zzh));
                        open.addCallbackBuffer(zza(this.zzh));
                        open.addCallbackBuffer(zza(this.zzh));
                        open.addCallbackBuffer(zza(this.zzh));
                        return open;
                    }
                    i = 0;
                    cameraInfo = new Camera.CameraInfo();
                    Camera.getCameraInfo(i5, cameraInfo);
                    if (cameraInfo.facing != 1) {
                    }
                    this.rotation = i2 / 90;
                    open.setDisplayOrientation(i3);
                    parameters2.setRotation(i2);
                    if (this.zzm != null) {
                    }
                    if (!parameters2.getSupportedFocusModes().contains("continuous-video")) {
                    }
                    open.setParameters(parameters2);
                    open.setPreviewCallbackWithBuffer(new zza());
                    open.addCallbackBuffer(zza(this.zzh));
                    open.addCallbackBuffer(zza(this.zzh));
                    open.addCallbackBuffer(zza(this.zzh));
                    open.addCallbackBuffer(zza(this.zzh));
                    return open;
                }
                throw new IOException("Could not find suitable preview frames per second range.");
            }
            throw new IOException("Could not find suitable preview size.");
        }
        throw new IOException("Could not find requested camera.");
    }

    private final byte[] zza(Size size) {
        byte[] bArr = new byte[(((int) Math.ceil(((double) ((long) ((size.getHeight() * size.getWidth()) * ImageFormat.getBitsPerPixel(17)))) / 8.0d)) + 1)];
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        if (!wrap.hasArray() || wrap.array() != bArr) {
            throw new IllegalStateException("Failed to create valid buffer for camera source.");
        }
        this.zzr.put(bArr, wrap);
        return bArr;
    }
}
