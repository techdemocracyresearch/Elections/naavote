package com.facebook.react;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import com.brentvatne.react.ReactVideoPackage;
import com.facebook.react.shell.MainPackageConfig;
import com.facebook.react.shell.MainReactPackage;
import com.faizal.OtpVerify.RNOtpVerifyPackage;
import com.gantix.JailMonkey.JailMonkeyPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.rnfs.RNFSPackage;
import com.sha256lib.Sha256Package;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.swmansion.rnscreens.RNScreensPackage;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
import com.toyberman.RNSslPinningPackage;
import fr.snapp.imagebase64.RNImgToBase64Package;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
import java.util.ArrayList;
import java.util.Arrays;
import me.furtado.smsretriever.RNSmsRetrieverPackage;
import org.reactnative.maskedview.RNCMaskedViewPackage;

public class PackageList {
    private Application application;
    private MainPackageConfig mConfig;
    private ReactNativeHost reactNativeHost;

    public PackageList(ReactNativeHost reactNativeHost2) {
        this(reactNativeHost2, (MainPackageConfig) null);
    }

    public PackageList(Application application2) {
        this(application2, (MainPackageConfig) null);
    }

    public PackageList(ReactNativeHost reactNativeHost2, MainPackageConfig mainPackageConfig) {
        this.reactNativeHost = reactNativeHost2;
        this.mConfig = mainPackageConfig;
    }

    public PackageList(Application application2, MainPackageConfig mainPackageConfig) {
        this.reactNativeHost = null;
        this.application = application2;
        this.mConfig = mainPackageConfig;
    }

    private ReactNativeHost getReactNativeHost() {
        return this.reactNativeHost;
    }

    private Resources getResources() {
        return getApplication().getResources();
    }

    private Application getApplication() {
        ReactNativeHost reactNativeHost2 = this.reactNativeHost;
        if (reactNativeHost2 == null) {
            return this.application;
        }
        return reactNativeHost2.getApplication();
    }

    private Context getApplicationContext() {
        return getApplication().getApplicationContext();
    }

    public ArrayList<ReactPackage> getPackages() {
        return new ArrayList<>(Arrays.asList(new MainReactPackage(this.mConfig), new AsyncStoragePackage(), new RNCMaskedViewPackage(), new NetInfoPackage(), new JailMonkeyPackage(), new RNDeviceInfo(), new DocumentPickerPackage(), new RNFSPackage(), new RNGestureHandlerPackage(), new RNImgToBase64Package(), new RNOtpVerifyPackage(), new ReanimatedPackage(), new SafeAreaContextPackage(), new RNScreensPackage(), new Sha256Package(), new RNSmsRetrieverPackage(), new RNSslPinningPackage(), new VectorIconsPackage(), new ReactVideoPackage(), new RNCWebViewPackage()));
    }
}
