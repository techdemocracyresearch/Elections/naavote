package com.face.liveness.camera;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.facebook.imagepipeline.common.RotationOptions;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "CameraPreview";
    private int cameraId;
    Camera.PreviewCallback cameraPreviewCallBack;
    private Camera mCamera;
    private Context mContext;
    private SurfaceHolder mHolder;
    private Camera.Size mPictureSize;
    private Camera.Size mPreviewSize;
    public List<Camera.Size> mSupportedPictureSizes;
    public List<Camera.Size> mSupportedPreviewSizes;

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    public CameraPreview(Context context, Camera camera, int i, Camera.PreviewCallback previewCallback) {
        super(context);
        this.mContext = context;
        this.cameraId = i;
        this.mCamera = camera;
        this.cameraPreviewCallBack = previewCallback;
        List<Camera.Size> supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
        this.mSupportedPreviewSizes = supportedPreviewSizes;
        for (Camera.Size size : supportedPreviewSizes) {
            Log.i(TAG, "humanReadableByteCount Supported preview sizes" + size.width + "/" + size.height);
        }
        Log.i(TAG, "humanReadableByteCount*************************************************");
        List<Camera.Size> supportedPictureSizes = this.mCamera.getParameters().getSupportedPictureSizes();
        this.mSupportedPictureSizes = supportedPictureSizes;
        for (Camera.Size size2 : supportedPictureSizes) {
            Log.i(TAG, "\n\n\nhumanReadableByteCount Supported Picture sizes" + size2.width + "/" + size2.height);
        }
        Log.i(TAG, "humanReadableByteCount*************************************************");
        SurfaceHolder holder = getHolder();
        this.mHolder = holder;
        holder.addCallback(this);
        this.mHolder.setType(3);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:3|4|5|6|(1:8)(2:9|(1:11)(2:12|(1:14)))|15|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00dd, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00de, code lost:
        android.util.Log.d(com.face.liveness.camera.CameraPreview.TAG, "Error starting camera preview: " + r5.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0034 */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0044 A[Catch:{ Exception -> 0x00dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0048 A[Catch:{ Exception -> 0x00dd }] */
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        Log.e(TAG, "surfaceChanged => w=" + i2 + ", h=" + i3);
        if (this.mHolder.getSurface() != null) {
            this.mCamera.stopPreview();
            Camera.Parameters parameters = this.mCamera.getParameters();
            List<String> supportedFocusModes = parameters.getSupportedFocusModes();
            if (!supportedFocusModes.contains("continuous-picture")) {
                parameters.setFocusMode("continuous-picture");
            } else if (supportedFocusModes.contains("continuous-video")) {
                parameters.setFocusMode("continuous-video");
            } else if (supportedFocusModes.contains("auto")) {
                parameters.setFocusMode("auto");
            }
            Log.i(TAG, "humanReadableByteCount Optimal Preview size" + this.mPreviewSize.width + "/" + this.mPreviewSize.height);
            Log.i(TAG, "humanReadableByteCount Optimal Picture size" + this.mPictureSize.width + "/" + this.mPictureSize.height);
            parameters.setPreviewSize(this.mPreviewSize.width, this.mPreviewSize.height);
            parameters.setPictureSize(this.mPictureSize.width, this.mPictureSize.height);
            this.mCamera.setParameters(parameters);
            this.mCamera.setDisplayOrientation(setCameraDisplayOrientation((Activity) this.mContext, this.cameraId));
            this.mCamera.setPreviewDisplay(this.mHolder);
            this.mCamera.setPreviewCallback(this.cameraPreviewCallBack);
            this.mCamera.startPreview();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        float f;
        int i3;
        int resolveSize = resolveSize(getSuggestedMinimumWidth(), i);
        int resolveSize2 = resolveSize(getSuggestedMinimumHeight(), i2);
        List<Camera.Size> list = this.mSupportedPreviewSizes;
        if (list != null) {
            this.mPreviewSize = getOptimalPreviewSize(list, resolveSize, resolveSize2);
        }
        List<Camera.Size> list2 = this.mSupportedPictureSizes;
        if (list2 != null) {
            this.mPictureSize = getOptimalPreviewSize(list2, resolveSize, resolveSize2);
        }
        Camera.Size size = this.mPreviewSize;
        if (size != null) {
            if (size.height >= this.mPreviewSize.width) {
                f = (float) this.mPreviewSize.height;
                i3 = this.mPreviewSize.width;
            } else {
                f = (float) this.mPreviewSize.width;
                i3 = this.mPreviewSize.height;
            }
            setMeasuredDimension(resolveSize, (int) (((float) resolveSize) * (f / ((float) i3))));
        }
    }

    public Camera.Size getOptimalPreviewSize(List<Camera.Size> list, int i, int i2) {
        double d = ((double) i2) / ((double) i);
        Camera.Size size = null;
        if (list == null) {
            return null;
        }
        double d2 = Double.MAX_VALUE;
        double d3 = Double.MAX_VALUE;
        for (Camera.Size size2 : list) {
            if (Math.abs((((double) size2.height) / ((double) size2.width)) - d) <= 0.1d && ((double) Math.abs(size2.height - i2)) < d3) {
                d3 = (double) Math.abs(size2.height - i2);
                size = size2;
            }
        }
        if (size == null) {
            for (Camera.Size size3 : list) {
                if (((double) Math.abs(size3.height - i2)) < d2) {
                    d2 = (double) Math.abs(size3.height - i2);
                    size = size3;
                }
            }
        }
        return size;
    }

    public int setCameraDisplayOrientation(Activity activity, int i) {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(i, cameraInfo);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int i2 = 0;
        if (rotation != 0) {
            if (rotation == 1) {
                i2 = 90;
            } else if (rotation == 2) {
                i2 = RotationOptions.ROTATE_180;
            } else if (rotation == 3) {
                i2 = RotationOptions.ROTATE_270;
            }
        }
        if (cameraInfo.facing == 1) {
            return (360 - ((cameraInfo.orientation + i2) % 360)) % 360;
        }
        return ((cameraInfo.orientation - i2) + 360) % 360;
    }
}
