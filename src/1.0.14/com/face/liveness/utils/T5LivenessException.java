package com.face.liveness.utils;

public class T5LivenessException extends RuntimeException {
    public T5LivenessException(String str) {
        super(str);
    }
}
