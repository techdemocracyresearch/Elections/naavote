package com.face.liveness.view.gif;

import java.util.ArrayList;
import java.util.List;

public class GifHeader {
    int bgColor;
    int bgIndex;
    GifFrame currentFrame;
    int frameCount = 0;
    List<GifFrame> frames = new ArrayList();
    int[] gct = null;
    boolean gctFlag;
    int gctSize;
    int height;
    int loopCount = 0;
    int pixelAspect;
    int status = 0;
    int width;

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public int getNumFrames() {
        return this.frameCount;
    }

    public int getStatus() {
        return this.status;
    }
}
