package com.face.liveness.view.gif;

/* access modifiers changed from: package-private */
public class GifFrame {
    int bufferFrameStart;
    int delay;
    int dispose;
    int ih;
    boolean interlace;
    int iw;
    int ix;
    int iy;
    int[] lct;
    int transIndex;
    boolean transparency;

    GifFrame() {
    }
}
