package com.faizal.OtpVerify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.Status;

public class OtpBroadcastReceiver extends BroadcastReceiver {
    private static final String EVENT = "com.faizalshap.otpVerify:otpReceived";
    private ReactApplicationContext mContext;

    public OtpBroadcastReceiver(ReactApplicationContext reactApplicationContext) {
        this.mContext = reactApplicationContext;
    }

    private void receiveMessage(String str) {
        ReactApplicationContext reactApplicationContext = this.mContext;
        if (reactApplicationContext != null && reactApplicationContext.hasActiveCatalystInstance()) {
            new WritableNativeMap().putString("message", str);
            ((DeviceEventManagerModule.RCTDeviceEventEmitter) this.mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)).emit(EVENT, str);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            int statusCode = ((Status) extras.get(SmsRetriever.EXTRA_STATUS)).getStatusCode();
            if (statusCode == 0) {
                String str = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                receiveMessage(str);
                Log.d("SMS", str);
            } else if (statusCode == 15) {
                Log.d("SMS", "Timeout error");
                ((DeviceEventManagerModule.RCTDeviceEventEmitter) this.mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)).emit(EVENT, "Timeout Error.");
            }
        }
    }
}
