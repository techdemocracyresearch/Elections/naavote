package com.mvote.helloworld;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.CertificatePinner;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.cache.DiskLruCache;

public class MvoteUid {
    String captcha_url = "https://resident.uidai.gov.in:443/CaptchaSecurityImages.php?width=100&height=40&characters=5";
    OkHttpClient client;
    Response response;
    String response_body_str = null;
    String session_id = null;
    String uid_hostname = "resident.uidai.gov.in";
    String uid_url = "https://resident.uidai.gov.in:443/offline-kyc";

    public MvoteUid() {
        setup();
        first_page();
    }

    public void setup() {
        try {
            this.client = new OkHttpClient.Builder().certificatePinner(new CertificatePinner.Builder().add(this.uid_hostname, "sha256/iJw/o6cctw1og2LQJmij+NmkF3sUsqpbv99umik8I4o=").build()).cookieJar(new CookieJar() {
                /* class com.mvote.helloworld.MvoteUid.AnonymousClass1 */
                private final HashMap<HttpUrl, List<Cookie>> cookieStore = new HashMap<>();

                @Override // okhttp3.CookieJar
                public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                    this.cookieStore.put(httpUrl, list);
                }

                @Override // okhttp3.CookieJar
                public List<Cookie> loadForRequest(HttpUrl httpUrl) {
                    List<Cookie> list = this.cookieStore.get(httpUrl);
                    return list != null ? list : new ArrayList();
                }
            }).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void first_page() {
        try {
            Response execute = this.client.newCall(new Request.Builder().url(this.uid_url).build()).execute();
            this.response = execute;
            this.response_body_str = execute.body().string();
            this.session_id = this.response.headers().get("Set-Cookie").split("=")[1].split(";")[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String get_captcha() {
        try {
            Request.Builder url = new Request.Builder().url(this.captcha_url);
            Response execute = this.client.newCall(url.addHeader("Cookie", "PHPSESSID=" + this.session_id).build()).execute();
            this.response = execute;
            Bitmap decodeStream = BitmapFactory.decodeStream(execute.body().byteStream());
            this.response.headers();
            return getStringImage(decodeStream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
    }

    public String captcha_post_page(String str, String str2) {
        try {
            FormBody build = new FormBody.Builder().add("uidno", str).add("security_code", str2).add("task", "genOtp").add("boxchecked", "0").build();
            Request.Builder addHeader = new Request.Builder().url(this.uid_url).addHeader("Connection", "close").addHeader("Cache-Control", "max-age=0").addHeader("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1).addHeader("Origin", "https://resident.uidai.gov.in").addHeader("Content-Type", "application/x-www-form-urlencoded").addHeader("User-Agent", "Mozilla/5.0 (Linux; Android 10; RMX1971 Build/QKQ1.190918.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/89.0.4389.86 Mobile Safari/537.36").addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,/;q=0.8,application/signed-exchange;v=b3;q=0.9").addHeader("X-Requested-With", "in.mvote").addHeader("Sec-Fetch-Site", "same-origin").addHeader("Sec-Fetch-Mode", "navigate").addHeader("Sec-Fetch-User", "?1").addHeader("Sec-Fetch-Dest", "document").addHeader("Referer", "https://resident.uidai.gov.in/offline-kyc").addHeader("Accept-Encoding", "gzip, deflate").addHeader("Accept-Language", "en-US,en-GB;q=0.9,en;q=0.8");
            Response execute = this.client.newCall(addHeader.addHeader("Cookie", "PHPSESSID=" + this.session_id).post(build).build()).execute();
            this.response = execute;
            String string = execute.body().string();
            this.response_body_str = string;
            if (string.contains("Please Enter Valid Captcha")) {
                return "CAPTCHA_ERROR";
            }
            this.response.headers();
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "EXCEPTION";
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x014a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x014f, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0150, code lost:
        r8.addSuppressed(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0153, code lost:
        throw r0;
     */
    public String send_otp(String str, String str2, String str3) {
        try {
            FormBody build = new FormBody.Builder().add("task", "valOtp").add("boxchecked", "0").add("zipcode", str2).add("totp", str).add("task", "valOtp").add("boxchecked", "0").build();
            Request.Builder addHeader = new Request.Builder().url(this.uid_url).addHeader("Connection", "close").addHeader("Cache-Control", "max-age=0").addHeader("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1).addHeader("Origin", "https://resident.uidai.gov.in").addHeader("Content-Type", "application/x-www-form-urlencoded").addHeader("User-Agent", "Mozilla/5.0 (Linux; Android 10; RMX1971 Build/QKQ1.190918.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/89.0.4389.86 Mobile Safari/537.36").addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,/;q=0.8,application/signed-exchange;v=b3;q=0.9").addHeader("X-Requested-With", "in.mvote").addHeader("Sec-Fetch-Site", "same-origin").addHeader("Sec-Fetch-Mode", "navigate").addHeader("Sec-Fetch-User", "?1").addHeader("Sec-Fetch-Dest", "document").addHeader("this.getReferer", "https://resident.uidai.gov.in/offline-kyc").addHeader("Accept-Encoding", "gzip, deflate").addHeader("Accept-Language", "en-US,en-GB;q=0.9,en;q=0.8");
            Response execute = this.client.newCall(addHeader.addHeader("Cookie", "PHPSESSID=" + this.session_id).post(build).build()).execute();
            this.response = execute;
            if (execute.body().contentType().toString().contains("text/html")) {
                return "OTP_ERROR";
            }
            InputStream byteStream = this.response.body().byteStream();
            try {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + str3);
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file, false);
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = byteStream.read(bArr);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        byteStream.close();
                        this.response.headers();
                        return "SUCCESS";
                    }
                }
            } catch (Throwable th) {
                byteStream.close();
                throw th;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "SUCCESS";
        }
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            TrustManager[] trustManagerArr = {new X509TrustManager() {
                /* class com.mvote.helloworld.MvoteUid.AnonymousClass2 */

                @Override // javax.net.ssl.X509TrustManager
                public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
                }

                @Override // javax.net.ssl.X509TrustManager
                public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }};
            SSLContext instance = SSLContext.getInstance("SSL");
            instance.init(null, trustManagerArr, new SecureRandom());
            SSLSocketFactory socketFactory = instance.getSocketFactory();
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(socketFactory, (X509TrustManager) trustManagerArr[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                /* class com.mvote.helloworld.MvoteUid.AnonymousClass3 */

                public boolean verify(String str, SSLSession sSLSession) {
                    return true;
                }
            });
            return builder.cookieJar(new CookieJar() {
                /* class com.mvote.helloworld.MvoteUid.AnonymousClass4 */
                private final HashMap<HttpUrl, List<Cookie>> cookieStore = new HashMap<>();

                @Override // okhttp3.CookieJar
                public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                    this.cookieStore.put(httpUrl, list);
                }

                @Override // okhttp3.CookieJar
                public List<Cookie> loadForRequest(HttpUrl httpUrl) {
                    List<Cookie> list = this.cookieStore.get(httpUrl);
                    return list != null ? list : new ArrayList();
                }
            }).build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
