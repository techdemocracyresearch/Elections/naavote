package com.franmontiel.persistentcookiejar.persistence;

import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import kotlin.UByte;
import okhttp3.Cookie;

public class SerializableCookie implements Serializable {
    private static long NON_VALID_EXPIRES_AT = -1;
    private static final String TAG = "SerializableCookie";
    private static final long serialVersionUID = -8594045714036645534L;
    private transient Cookie cookie;

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0034 A[SYNTHETIC, Splitter:B:18:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0043 A[SYNTHETIC, Splitter:B:26:0x0043] */
    public String encode(Cookie cookie2) {
        Throwable th;
        IOException e;
        ObjectOutputStream objectOutputStream;
        this.cookie = cookie2;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream2 = null;
        try {
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream.writeObject(this);
                try {
                    objectOutputStream.close();
                } catch (IOException e2) {
                    Log.d(TAG, "Stream not closed in encodeCookie", e2);
                }
                return byteArrayToHexString(byteArrayOutputStream.toByteArray());
            } catch (IOException e3) {
                e = e3;
                try {
                    Log.d(TAG, "IOException in encodeCookie", e);
                    if (objectOutputStream != null) {
                        try {
                            objectOutputStream.close();
                        } catch (IOException e4) {
                            Log.d(TAG, "Stream not closed in encodeCookie", e4);
                        }
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    objectOutputStream2 = objectOutputStream;
                    if (objectOutputStream2 != null) {
                    }
                    throw th;
                }
            }
        } catch (IOException e5) {
            e = e5;
            objectOutputStream = null;
            Log.d(TAG, "IOException in encodeCookie", e);
            if (objectOutputStream != null) {
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            if (objectOutputStream2 != null) {
                try {
                    objectOutputStream2.close();
                } catch (IOException e6) {
                    Log.d(TAG, "Stream not closed in encodeCookie", e6);
                }
            }
            throw th;
        }
    }

    private static String byteArrayToHexString(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            int i = b & UByte.MAX_VALUE;
            if (i < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(i));
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004d A[SYNTHETIC, Splitter:B:27:0x004d] */
    public Cookie decode(String str) {
        Throwable th;
        ObjectInputStream objectInputStream;
        IOException e;
        ClassNotFoundException e2;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(hexStringToByteArray(str));
        Cookie cookie2 = null;
        try {
            objectInputStream = new ObjectInputStream(byteArrayInputStream);
            try {
                cookie2 = ((SerializableCookie) objectInputStream.readObject()).cookie;
                try {
                    objectInputStream.close();
                } catch (IOException e3) {
                    Log.d(TAG, "Stream not closed in decodeCookie", e3);
                }
            } catch (IOException e4) {
                e = e4;
                Log.d(TAG, "IOException in decodeCookie", e);
                if (objectInputStream != null) {
                }
                return cookie2;
            } catch (ClassNotFoundException e5) {
                e2 = e5;
                try {
                    Log.d(TAG, "ClassNotFoundException in decodeCookie", e2);
                    if (objectInputStream != null) {
                    }
                    return cookie2;
                } catch (Throwable th2) {
                    th = th2;
                    if (objectInputStream != null) {
                    }
                    throw th;
                }
            }
        } catch (IOException e6) {
            e = e6;
            objectInputStream = null;
            Log.d(TAG, "IOException in decodeCookie", e);
            if (objectInputStream != null) {
                objectInputStream.close();
            }
            return cookie2;
        } catch (ClassNotFoundException e7) {
            e2 = e7;
            objectInputStream = null;
            Log.d(TAG, "ClassNotFoundException in decodeCookie", e2);
            if (objectInputStream != null) {
                objectInputStream.close();
            }
            return cookie2;
        } catch (Throwable th3) {
            objectInputStream = null;
            th = th3;
            if (objectInputStream != null) {
                try {
                    objectInputStream.close();
                } catch (IOException e8) {
                    Log.d(TAG, "Stream not closed in decodeCookie", e8);
                }
            }
            throw th;
        }
        return cookie2;
    }

    private static byte[] hexStringToByteArray(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.cookie.name());
        objectOutputStream.writeObject(this.cookie.value());
        objectOutputStream.writeLong(this.cookie.persistent() ? this.cookie.expiresAt() : NON_VALID_EXPIRES_AT);
        objectOutputStream.writeObject(this.cookie.domain());
        objectOutputStream.writeObject(this.cookie.path());
        objectOutputStream.writeBoolean(this.cookie.secure());
        objectOutputStream.writeBoolean(this.cookie.httpOnly());
        objectOutputStream.writeBoolean(this.cookie.hostOnly());
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        Cookie.Builder builder = new Cookie.Builder();
        builder.name((String) objectInputStream.readObject());
        builder.value((String) objectInputStream.readObject());
        long readLong = objectInputStream.readLong();
        if (readLong != NON_VALID_EXPIRES_AT) {
            builder.expiresAt(readLong);
        }
        String str = (String) objectInputStream.readObject();
        builder.domain(str);
        builder.path((String) objectInputStream.readObject());
        if (objectInputStream.readBoolean()) {
            builder.secure();
        }
        if (objectInputStream.readBoolean()) {
            builder.httpOnly();
        }
        if (objectInputStream.readBoolean()) {
            builder.hostOnlyDomain(str);
        }
        this.cookie = builder.build();
    }
}
