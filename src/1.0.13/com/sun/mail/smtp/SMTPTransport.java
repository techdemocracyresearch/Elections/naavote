package com.sun.mail.smtp;

import androidx.recyclerview.widget.ItemTouchHelper;
import com.facebook.react.modules.systeminfo.AndroidInfoHelpers;
import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64EncoderStream;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.SocketFetcher;
import com.sun.mail.util.TraceInputStream;
import com.sun.mail.util.TraceOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.mail.internet.ParseException;

public class SMTPTransport extends Transport {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final byte[] CRLF = {13, 10};
    private static final String UNKNOWN = "UNKNOWN";
    private static char[] hexchar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final String[] ignoreList = {"Bcc", "Content-Length"};
    private Address[] addresses;
    private SMTPOutputStream dataStream;
    private int defaultPort;
    private MessagingException exception;
    private Hashtable extMap;
    private Address[] invalidAddr;
    private boolean isSSL;
    private int lastReturnCode;
    private String lastServerResponse;
    private LineInputStream lineInputStream;
    private String localHostName;
    private DigestMD5 md5support;
    private MimeMessage message;
    private String name;
    private PrintStream out;
    private boolean quitWait;
    private boolean reportSuccess;
    private String saslRealm;
    private boolean sendPartiallyFailed;
    private BufferedInputStream serverInput;
    private OutputStream serverOutput;
    private Socket serverSocket;
    private boolean useRset;
    private boolean useStartTLS;
    private Address[] validSentAddr;
    private Address[] validUnsentAddr;

    public SMTPTransport(Session session, URLName uRLName) {
        this(session, uRLName, "smtp", 25, false);
    }

    protected SMTPTransport(Session session, URLName uRLName, String str, int i, boolean z) {
        super(session, uRLName);
        this.name = "smtp";
        this.defaultPort = 25;
        boolean z2 = false;
        this.isSSL = false;
        this.sendPartiallyFailed = false;
        this.quitWait = false;
        this.saslRealm = UNKNOWN;
        str = uRLName != null ? uRLName.getProtocol() : str;
        this.name = str;
        this.defaultPort = i;
        this.isSSL = z;
        this.out = session.getDebugOut();
        String property = session.getProperty("mail." + str + ".quitwait");
        this.quitWait = property == null || property.equalsIgnoreCase("true");
        String property2 = session.getProperty("mail." + str + ".reportsuccess");
        this.reportSuccess = property2 != null && property2.equalsIgnoreCase("true");
        String property3 = session.getProperty("mail." + str + ".starttls.enable");
        this.useStartTLS = property3 != null && property3.equalsIgnoreCase("true");
        String property4 = session.getProperty("mail." + str + ".userset");
        if (property4 != null && property4.equalsIgnoreCase("true")) {
            z2 = true;
        }
        this.useRset = z2;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0081 */
    public synchronized String getLocalHost() {
        String str = this.localHostName;
        if (str == null || str.length() <= 0) {
            Session session = this.session;
            this.localHostName = session.getProperty("mail." + this.name + ".localhost");
        }
        String str2 = this.localHostName;
        if (str2 == null || str2.length() <= 0) {
            Session session2 = this.session;
            this.localHostName = session2.getProperty("mail." + this.name + ".localaddress");
        }
        String str3 = this.localHostName;
        if (str3 == null || str3.length() <= 0) {
            InetAddress localHost = InetAddress.getLocalHost();
            String hostName = localHost.getHostName();
            this.localHostName = hostName;
            if (hostName == null) {
                this.localHostName = "[" + localHost.getHostAddress() + "]";
            }
        }
        return this.localHostName;
    }

    public synchronized void setLocalHost(String str) {
        this.localHostName = str;
    }

    public synchronized void connect(Socket socket) throws MessagingException {
        this.serverSocket = socket;
        super.connect();
    }

    public synchronized String getSASLRealm() {
        if (this.saslRealm == UNKNOWN) {
            Session session = this.session;
            String property = session.getProperty("mail." + this.name + ".sasl.realm");
            this.saslRealm = property;
            if (property == null) {
                Session session2 = this.session;
                this.saslRealm = session2.getProperty("mail." + this.name + ".saslrealm");
            }
        }
        return this.saslRealm;
    }

    public synchronized void setSASLRealm(String str) {
        this.saslRealm = str;
    }

    public synchronized boolean getReportSuccess() {
        return this.reportSuccess;
    }

    public synchronized void setReportSuccess(boolean z) {
        this.reportSuccess = z;
    }

    public synchronized boolean getStartTLS() {
        return this.useStartTLS;
    }

    public synchronized void setStartTLS(boolean z) {
        this.useStartTLS = z;
    }

    public synchronized boolean getUseRset() {
        return this.useRset;
    }

    public synchronized void setUseRset(boolean z) {
        this.useRset = z;
    }

    public synchronized String getLastServerResponse() {
        return this.lastServerResponse;
    }

    public synchronized int getLastReturnCode() {
        return this.lastReturnCode;
    }

    private synchronized DigestMD5 getMD5() {
        if (this.md5support == null) {
            this.md5support = new DigestMD5(this.debug ? this.out : null);
        }
        return this.md5support;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01e2  */
    @Override // javax.mail.Service
    public boolean protocolConnect(String str, int i, String str2, String str3) throws MessagingException {
        int i2;
        DigestMD5 md5;
        Session session = this.session;
        String property = session.getProperty("mail." + this.name + ".ehlo");
        boolean z = property == null || !property.equalsIgnoreCase("false");
        Session session2 = this.session;
        String property2 = session2.getProperty("mail." + this.name + ".auth");
        boolean z2 = property2 != null && property2.equalsIgnoreCase("true");
        if (this.debug) {
            PrintStream printStream = this.out;
            printStream.println("DEBUG SMTP: useEhlo " + z + ", useAuth " + z2);
        }
        if (z2 && (str2 == null || str3 == null)) {
            return false;
        }
        int i3 = -1;
        if (i == -1) {
            Session session3 = this.session;
            String property3 = session3.getProperty("mail." + this.name + ".port");
            i2 = property3 != null ? Integer.parseInt(property3) : this.defaultPort;
        } else {
            i2 = i;
        }
        String str4 = (str == null || str.length() == 0) ? AndroidInfoHelpers.DEVICE_LOCALHOST : str;
        if (this.serverSocket != null) {
            openServer();
        } else {
            openServer(str4, i2);
        }
        if (!(z ? ehlo(getLocalHost()) : false)) {
            helo(getLocalHost());
        }
        if (this.useStartTLS && supportsExtension("STARTTLS")) {
            startTLS();
            ehlo(getLocalHost());
        }
        if ((z2 || !(str2 == null || str3 == null)) && (supportsExtension("AUTH") || supportsExtension("AUTH=LOGIN"))) {
            if (this.debug) {
                this.out.println("DEBUG SMTP: Attempt to authenticate");
                if (!supportsAuthentication("LOGIN") && supportsExtension("AUTH=LOGIN")) {
                    this.out.println("DEBUG SMTP: use AUTH=LOGIN hack");
                }
            }
            if (supportsAuthentication("LOGIN") || supportsExtension("AUTH=LOGIN")) {
                int simpleCommand = simpleCommand("AUTH LOGIN");
                if (simpleCommand == 530) {
                    startTLS();
                    simpleCommand = simpleCommand("AUTH LOGIN");
                }
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    BASE64EncoderStream bASE64EncoderStream = new BASE64EncoderStream(byteArrayOutputStream, Integer.MAX_VALUE);
                    if (simpleCommand == 334) {
                        bASE64EncoderStream.write(ASCIIUtility.getBytes(str2));
                        bASE64EncoderStream.flush();
                        simpleCommand = simpleCommand(byteArrayOutputStream.toByteArray());
                        byteArrayOutputStream.reset();
                    }
                    if (simpleCommand == 334) {
                        bASE64EncoderStream.write(ASCIIUtility.getBytes(str3));
                        bASE64EncoderStream.flush();
                        simpleCommand = simpleCommand(byteArrayOutputStream.toByteArray());
                        byteArrayOutputStream.reset();
                    }
                    if (simpleCommand != 235) {
                        closeConnection();
                        return false;
                    }
                } catch (IOException unused) {
                    if (simpleCommand != 235) {
                        closeConnection();
                        return false;
                    }
                } catch (Throwable th) {
                    if (simpleCommand != 235) {
                        closeConnection();
                        return false;
                    }
                    throw th;
                }
            } else if (supportsAuthentication("PLAIN")) {
                int simpleCommand2 = simpleCommand("AUTH PLAIN");
                try {
                    ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                    BASE64EncoderStream bASE64EncoderStream2 = new BASE64EncoderStream(byteArrayOutputStream2, Integer.MAX_VALUE);
                    if (simpleCommand2 == 334) {
                        bASE64EncoderStream2.write(0);
                        bASE64EncoderStream2.write(ASCIIUtility.getBytes(str2));
                        bASE64EncoderStream2.write(0);
                        bASE64EncoderStream2.write(ASCIIUtility.getBytes(str3));
                        bASE64EncoderStream2.flush();
                        simpleCommand2 = simpleCommand(byteArrayOutputStream2.toByteArray());
                    }
                    if (simpleCommand2 != 235) {
                        closeConnection();
                        return false;
                    }
                } catch (IOException unused2) {
                    if (simpleCommand2 != 235) {
                        closeConnection();
                        return false;
                    }
                } catch (Throwable th2) {
                    if (simpleCommand2 != 235) {
                        closeConnection();
                        return false;
                    }
                    throw th2;
                }
            } else if (supportsAuthentication("DIGEST-MD5") && (md5 = getMD5()) != null) {
                int simpleCommand3 = simpleCommand("AUTH DIGEST-MD5");
                if (simpleCommand3 == 334) {
                    try {
                        simpleCommand3 = simpleCommand(md5.authClient(str4, str2, str3, getSASLRealm(), this.lastServerResponse));
                        if (simpleCommand3 == 334) {
                            if (md5.authServer(this.lastServerResponse)) {
                                i3 = simpleCommand(new byte[0]);
                            }
                            if (i3 != 235) {
                                closeConnection();
                                return false;
                            }
                        }
                    } catch (Exception e) {
                        if (this.debug) {
                            PrintStream printStream2 = this.out;
                            printStream2.println("DEBUG SMTP: DIGEST-MD5: " + e);
                        }
                        if (simpleCommand3 != 235) {
                            closeConnection();
                            return false;
                        }
                    } catch (Throwable th3) {
                        if (simpleCommand3 != 235) {
                            closeConnection();
                            return false;
                        }
                        throw th3;
                    }
                }
                i3 = simpleCommand3;
                if (i3 != 235) {
                }
            }
        }
        return true;
    }

    @Override // javax.mail.Transport
    public synchronized void sendMessage(Message message2, Address[] addressArr) throws MessagingException, SendFailedException {
        checkConnected();
        if (!(message2 instanceof MimeMessage)) {
            if (this.debug) {
                this.out.println("DEBUG SMTP: Can only send RFC822 msgs");
            }
            throw new MessagingException("SMTP can only send RFC822 messages");
        }
        for (int i = 0; i < addressArr.length; i++) {
            if (!(addressArr[i] instanceof InternetAddress)) {
                throw new MessagingException(addressArr[i] + " is not an InternetAddress");
            }
        }
        this.message = (MimeMessage) message2;
        this.addresses = addressArr;
        this.validUnsentAddr = addressArr;
        expandGroups();
        boolean allow8bitMIME = message2 instanceof SMTPMessage ? ((SMTPMessage) message2).getAllow8bitMIME() : false;
        if (!allow8bitMIME) {
            Session session = this.session;
            String property = session.getProperty("mail." + this.name + ".allow8bitmime");
            allow8bitMIME = property != null && property.equalsIgnoreCase("true");
        }
        if (this.debug) {
            PrintStream printStream = this.out;
            printStream.println("DEBUG SMTP: use8bit " + allow8bitMIME);
        }
        if (allow8bitMIME && supportsExtension("8BITMIME") && convertTo8Bit(this.message)) {
            try {
                this.message.saveChanges();
            } catch (MessagingException unused) {
            }
        }
        try {
            mailFrom();
            rcptTo();
            this.message.writeTo(data(), ignoreList);
            finishData();
            if (this.sendPartiallyFailed) {
                if (this.debug) {
                    this.out.println("DEBUG SMTP: Sending partially failed because of invalid destination addresses");
                }
                notifyTransportListeners(3, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
                throw new SMTPSendFailedException(".", this.lastReturnCode, this.lastServerResponse, this.exception, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
            }
            notifyTransportListeners(1, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
            this.invalidAddr = null;
            this.validUnsentAddr = null;
            this.validSentAddr = null;
            this.addresses = null;
            this.message = null;
            this.exception = null;
            this.sendPartiallyFailed = false;
        } catch (MessagingException e) {
            if (this.debug) {
                e.printStackTrace(this.out);
            }
            notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
            throw e;
        } catch (IOException e2) {
            if (this.debug) {
                e2.printStackTrace(this.out);
            }
            try {
                closeConnection();
            } catch (MessagingException unused2) {
            }
            notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
            throw new MessagingException("IOException while sending message", e2);
        } catch (Throwable th) {
            this.invalidAddr = null;
            this.validUnsentAddr = null;
            this.validSentAddr = null;
            this.addresses = null;
            this.message = null;
            this.exception = null;
            this.sendPartiallyFailed = false;
            throw th;
        }
    }

    @Override // javax.mail.Service
    public synchronized void close() throws MessagingException {
        int readServerResponse;
        if (super.isConnected()) {
            try {
                if (this.serverSocket != null) {
                    sendCommand("QUIT");
                    if (!(!this.quitWait || (readServerResponse = readServerResponse()) == 221 || readServerResponse == -1)) {
                        PrintStream printStream = this.out;
                        printStream.println("DEBUG SMTP: QUIT failed with " + readServerResponse);
                    }
                }
            } finally {
                closeConnection();
            }
        }
    }

    private void closeConnection() throws MessagingException {
        try {
            Socket socket = this.serverSocket;
            if (socket != null) {
                socket.close();
            }
            this.serverSocket = null;
            this.serverOutput = null;
            this.serverInput = null;
            this.lineInputStream = null;
            if (super.isConnected()) {
                super.close();
            }
        } catch (IOException e) {
            throw new MessagingException("Server Close Failed", e);
        } catch (Throwable th) {
            this.serverSocket = null;
            this.serverOutput = null;
            this.serverInput = null;
            this.lineInputStream = null;
            if (super.isConnected()) {
                super.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:24|25|26|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        closeConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x002f, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x002b */
    @Override // javax.mail.Service
    public synchronized boolean isConnected() {
        if (!super.isConnected()) {
            return false;
        }
        if (this.useRset) {
            sendCommand("RSET");
        } else {
            sendCommand("NOOP");
        }
        int readServerResponse = readServerResponse();
        if (readServerResponse >= 0 && readServerResponse != 421) {
            return true;
        }
        try {
            closeConnection();
        } catch (MessagingException unused) {
        }
        return false;
    }

    private void expandGroups() {
        Vector vector = null;
        int i = 0;
        while (true) {
            Address[] addressArr = this.addresses;
            if (i >= addressArr.length) {
                break;
            }
            InternetAddress internetAddress = (InternetAddress) addressArr[i];
            if (internetAddress.isGroup()) {
                if (vector == null) {
                    Vector vector2 = new Vector();
                    for (int i2 = 0; i2 < i; i2++) {
                        vector2.addElement(this.addresses[i2]);
                    }
                    vector = vector2;
                }
                try {
                    InternetAddress[] group = internetAddress.getGroup(true);
                    if (group != null) {
                        for (InternetAddress internetAddress2 : group) {
                            vector.addElement(internetAddress2);
                        }
                    } else {
                        vector.addElement(internetAddress);
                    }
                } catch (ParseException unused) {
                    vector.addElement(internetAddress);
                }
            } else if (vector != null) {
                vector.addElement(internetAddress);
            }
            i++;
        }
        if (vector != null) {
            InternetAddress[] internetAddressArr = new InternetAddress[vector.size()];
            vector.copyInto(internetAddressArr);
            this.addresses = internetAddressArr;
        }
    }

    private boolean convertTo8Bit(MimePart mimePart) {
        try {
            if (mimePart.isMimeType("text/*")) {
                String encoding = mimePart.getEncoding();
                if (encoding == null) {
                    return false;
                }
                if (!((encoding.equalsIgnoreCase("quoted-printable") || encoding.equalsIgnoreCase("base64")) && is8Bit(mimePart.getInputStream()))) {
                    return false;
                }
                mimePart.setContent(mimePart.getContent(), mimePart.getContentType());
                mimePart.setHeader("Content-Transfer-Encoding", "8bit");
                return true;
            } else if (!mimePart.isMimeType("multipart/*")) {
                return false;
            } else {
                MimeMultipart mimeMultipart = (MimeMultipart) mimePart.getContent();
                int count = mimeMultipart.getCount();
                boolean z = false;
                for (int i = 0; i < count; i++) {
                    try {
                        if (convertTo8Bit((MimePart) mimeMultipart.getBodyPart(i))) {
                            z = true;
                        }
                    } catch (IOException | MessagingException unused) {
                    }
                }
                return z;
            }
        } catch (IOException | MessagingException unused2) {
            return false;
        }
    }

    private boolean is8Bit(InputStream inputStream) {
        boolean z = false;
        int i = 0;
        while (true) {
            try {
                int read = inputStream.read();
                if (read < 0) {
                    if (this.debug && z) {
                        this.out.println("DEBUG SMTP: found an 8bit part");
                    }
                    return z;
                }
                int i2 = read & 255;
                if (i2 == 13 || i2 == 10) {
                    i = 0;
                } else if (i2 == 0 || (i = i + 1) > 998) {
                    return false;
                }
                if (i2 > 127) {
                    z = true;
                }
            } catch (IOException unused) {
                return false;
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Service
    public void finalize() throws Throwable {
        super.finalize();
        try {
            closeConnection();
        } catch (MessagingException unused) {
        }
    }

    /* access modifiers changed from: protected */
    public void helo(String str) throws MessagingException {
        if (str != null) {
            issueCommand("HELO " + str, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
            return;
        }
        issueCommand("HELO", ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
    }

    /* access modifiers changed from: protected */
    public boolean ehlo(String str) throws MessagingException {
        String str2;
        if (str != null) {
            str2 = "EHLO " + str;
        } else {
            str2 = "EHLO";
        }
        sendCommand(str2);
        int readServerResponse = readServerResponse();
        if (readServerResponse == 250) {
            BufferedReader bufferedReader = new BufferedReader(new StringReader(this.lastServerResponse));
            this.extMap = new Hashtable();
            boolean z = true;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    } else if (z) {
                        z = false;
                    } else if (readLine.length() >= 5) {
                        String substring = readLine.substring(4);
                        int indexOf = substring.indexOf(32);
                        String str3 = "";
                        if (indexOf > 0) {
                            str3 = substring.substring(indexOf + 1);
                            substring = substring.substring(0, indexOf);
                        }
                        if (this.debug) {
                            this.out.println("DEBUG SMTP: Found extension \"" + substring + "\", arg \"" + str3 + "\"");
                        }
                        this.extMap.put(substring.toUpperCase(Locale.ENGLISH), str3);
                    }
                } catch (IOException unused) {
                }
            }
        }
        return readServerResponse == 250;
    }

    /* access modifiers changed from: protected */
    public void mailFrom() throws MessagingException {
        InternetAddress internetAddress;
        Address[] from;
        MimeMessage mimeMessage = this.message;
        String str = null;
        String envelopeFrom = mimeMessage instanceof SMTPMessage ? ((SMTPMessage) mimeMessage).getEnvelopeFrom() : null;
        if (envelopeFrom == null || envelopeFrom.length() <= 0) {
            envelopeFrom = this.session.getProperty("mail." + this.name + ".from");
        }
        if (envelopeFrom == null || envelopeFrom.length() <= 0) {
            MimeMessage mimeMessage2 = this.message;
            if (mimeMessage2 == null || (from = mimeMessage2.getFrom()) == null || from.length <= 0) {
                internetAddress = InternetAddress.getLocalAddress(this.session);
            } else {
                internetAddress = from[0];
            }
            if (internetAddress != null) {
                envelopeFrom = internetAddress.getAddress();
            } else {
                throw new MessagingException("can't determine local email address");
            }
        }
        String str2 = "MAIL FROM:" + normalizeAddress(envelopeFrom);
        if (supportsExtension("DSN")) {
            MimeMessage mimeMessage3 = this.message;
            String dSNRet = mimeMessage3 instanceof SMTPMessage ? ((SMTPMessage) mimeMessage3).getDSNRet() : null;
            if (dSNRet == null) {
                dSNRet = this.session.getProperty("mail." + this.name + ".dsn.ret");
            }
            if (dSNRet != null) {
                str2 = String.valueOf(str2) + " RET=" + dSNRet;
            }
        }
        if (supportsExtension("AUTH")) {
            MimeMessage mimeMessage4 = this.message;
            String submitter = mimeMessage4 instanceof SMTPMessage ? ((SMTPMessage) mimeMessage4).getSubmitter() : null;
            if (submitter == null) {
                submitter = this.session.getProperty("mail." + this.name + ".submitter");
            }
            if (submitter != null) {
                try {
                    str2 = String.valueOf(str2) + " AUTH=" + xtext(submitter);
                } catch (IllegalArgumentException e) {
                    if (this.debug) {
                        this.out.println("DEBUG SMTP: ignoring invalid submitter: " + submitter + ", Exception: " + e);
                    }
                }
            }
        }
        MimeMessage mimeMessage5 = this.message;
        if (mimeMessage5 instanceof SMTPMessage) {
            str = ((SMTPMessage) mimeMessage5).getMailExtension();
        }
        if (str == null) {
            str = this.session.getProperty("mail." + this.name + ".mailextension");
        }
        if (str != null && str.length() > 0) {
            str2 = String.valueOf(str2) + " " + str;
        }
        issueSendCommand(str2, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x020a, code lost:
        r16.lastServerResponse = r1;
        r16.lastReturnCode = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x020e, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01e9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01ef, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01f3, code lost:
        if (r16.debug != false) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01f5, code lost:
        r0.printStackTrace(r16.out);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:90:0x01eb */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0210  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x028a  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x02af  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x00a1 A[SYNTHETIC] */
    public void rcptTo() throws MessagingException {
        String str;
        boolean z;
        int i;
        Address[] addressArr;
        InternetAddress internetAddress;
        String str2;
        int readServerResponse;
        Exception exc;
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        this.invalidAddr = null;
        this.validUnsentAddr = null;
        this.validSentAddr = null;
        MimeMessage mimeMessage = this.message;
        boolean sendPartial = mimeMessage instanceof SMTPMessage ? ((SMTPMessage) mimeMessage).getSendPartial() : false;
        boolean z2 = true;
        if (!sendPartial) {
            String property = this.session.getProperty("mail." + this.name + ".sendpartial");
            sendPartial = property != null && property.equalsIgnoreCase("true");
        }
        if (this.debug && sendPartial) {
            this.out.println("DEBUG SMTP: sendPartial set");
        }
        if (supportsExtension("DSN")) {
            MimeMessage mimeMessage2 = this.message;
            str = mimeMessage2 instanceof SMTPMessage ? ((SMTPMessage) mimeMessage2).getDSNNotify() : null;
            if (str == null) {
                str = this.session.getProperty("mail." + this.name + ".dsn.notify");
            }
            if (str != null) {
                z = true;
                MessagingException messagingException = null;
                i = 0;
                boolean z3 = false;
                while (true) {
                    addressArr = this.addresses;
                    if (i < addressArr.length) {
                        if (sendPartial && vector.size() == 0) {
                            z3 = true;
                        }
                        if (z3) {
                            Address[] addressArr2 = new Address[vector3.size()];
                            this.invalidAddr = addressArr2;
                            vector3.copyInto(addressArr2);
                            this.validUnsentAddr = new Address[(vector.size() + vector2.size())];
                            int i2 = 0;
                            int i3 = 0;
                            while (i2 < vector.size()) {
                                this.validUnsentAddr[i3] = (Address) vector.elementAt(i2);
                                i2++;
                                i3++;
                            }
                            int i4 = 0;
                            while (i4 < vector2.size()) {
                                this.validUnsentAddr[i3] = (Address) vector2.elementAt(i4);
                                i4++;
                                i3++;
                            }
                        } else if (this.reportSuccess || (sendPartial && (vector3.size() > 0 || vector2.size() > 0))) {
                            this.sendPartiallyFailed = z2;
                            this.exception = messagingException;
                            Address[] addressArr3 = new Address[vector3.size()];
                            this.invalidAddr = addressArr3;
                            vector3.copyInto(addressArr3);
                            Address[] addressArr4 = new Address[vector2.size()];
                            this.validUnsentAddr = addressArr4;
                            vector2.copyInto(addressArr4);
                            Address[] addressArr5 = new Address[vector.size()];
                            this.validSentAddr = addressArr5;
                            vector.copyInto(addressArr5);
                        } else {
                            this.validSentAddr = this.addresses;
                        }
                        if (this.debug) {
                            Address[] addressArr6 = this.validSentAddr;
                            if (addressArr6 != null && addressArr6.length > 0) {
                                this.out.println("DEBUG SMTP: Verified Addresses");
                                for (int i5 = 0; i5 < this.validSentAddr.length; i5++) {
                                    this.out.println("DEBUG SMTP:   " + this.validSentAddr[i5]);
                                }
                            }
                            Address[] addressArr7 = this.validUnsentAddr;
                            if (addressArr7 != null && addressArr7.length > 0) {
                                this.out.println("DEBUG SMTP: Valid Unsent Addresses");
                                for (int i6 = 0; i6 < this.validUnsentAddr.length; i6++) {
                                    this.out.println("DEBUG SMTP:   " + this.validUnsentAddr[i6]);
                                }
                            }
                            Address[] addressArr8 = this.invalidAddr;
                            if (addressArr8 != null && addressArr8.length > 0) {
                                this.out.println("DEBUG SMTP: Invalid Addresses");
                                for (int i7 = 0; i7 < this.invalidAddr.length; i7++) {
                                    this.out.println("DEBUG SMTP:   " + this.invalidAddr[i7]);
                                }
                            }
                        }
                        if (z3) {
                            if (this.debug) {
                                this.out.println("DEBUG SMTP: Sending failed because of invalid destination addresses");
                            }
                            notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
                            String str3 = this.lastServerResponse;
                            int i8 = this.lastReturnCode;
                            if (this.serverSocket != null) {
                                issueCommand("RSET", ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
                            }
                            this.lastServerResponse = str3;
                            this.lastReturnCode = i8;
                            throw new SendFailedException("Invalid Addresses", messagingException, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
                        }
                        return;
                    }
                    internetAddress = (InternetAddress) addressArr[i];
                    str2 = "RCPT TO:" + normalizeAddress(internetAddress.getAddress());
                    if (z) {
                        str2 = String.valueOf(str2) + " NOTIFY=" + str;
                    }
                    sendCommand(str2);
                    readServerResponse = readServerResponse();
                    if (readServerResponse == 250 || readServerResponse == 251) {
                        vector.addElement(internetAddress);
                        if (this.reportSuccess) {
                            exc = new SMTPAddressSucceededException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                            if (messagingException != null) {
                                messagingException.setNextException(exc);
                            }
                        }
                        i++;
                        z2 = true;
                    } else {
                        if (!(readServerResponse == 501 || readServerResponse == 503)) {
                            switch (readServerResponse) {
                                default:
                                    switch (readServerResponse) {
                                        case 550:
                                        case 551:
                                        case 553:
                                            break;
                                        case 552:
                                            break;
                                        default:
                                            if (readServerResponse >= 400 && readServerResponse <= 499) {
                                                vector2.addElement(internetAddress);
                                            } else if (readServerResponse < 500 || readServerResponse > 599) {
                                                break;
                                            } else {
                                                vector3.addElement(internetAddress);
                                            }
                                            if (!sendPartial) {
                                                z3 = true;
                                            }
                                            exc = new SMTPAddressFailedException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                                            if (messagingException != null) {
                                                messagingException.setNextException(exc);
                                                break;
                                            }
                                            break;
                                    }
                                case 450:
                                case 451:
                                case 452:
                                    if (!sendPartial) {
                                        z3 = true;
                                    }
                                    vector2.addElement(internetAddress);
                                    exc = new SMTPAddressFailedException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                                    if (messagingException != null) {
                                        messagingException.setNextException(exc);
                                        break;
                                    }
                                    break;
                            }
                            i++;
                            z2 = true;
                        }
                        if (!sendPartial) {
                            z3 = true;
                        }
                        vector3.addElement(internetAddress);
                        exc = new SMTPAddressFailedException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                        if (messagingException != null) {
                            messagingException.setNextException(exc);
                            i++;
                            z2 = true;
                        }
                    }
                    messagingException = exc;
                    i++;
                    z2 = true;
                }
                if (this.debug) {
                    this.out.println("DEBUG SMTP: got response code " + readServerResponse + ", with response: " + this.lastServerResponse);
                }
                String str4 = this.lastServerResponse;
                int i9 = this.lastReturnCode;
                if (this.serverSocket != null) {
                    issueCommand("RSET", ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
                }
                this.lastServerResponse = str4;
                this.lastReturnCode = i9;
                throw new SMTPAddressFailedException(internetAddress, str2, readServerResponse, str4);
            }
        } else {
            str = null;
        }
        z = false;
        MessagingException messagingException2 = null;
        i = 0;
        boolean z32 = false;
        while (true) {
            addressArr = this.addresses;
            if (i < addressArr.length) {
            }
            i++;
            z2 = true;
        }
        if (this.debug) {
        }
        String str42 = this.lastServerResponse;
        int i92 = this.lastReturnCode;
        if (this.serverSocket != null) {
        }
        this.lastServerResponse = str42;
        this.lastReturnCode = i92;
        throw new SMTPAddressFailedException(internetAddress, str2, readServerResponse, str42);
    }

    /* access modifiers changed from: protected */
    public OutputStream data() throws MessagingException {
        issueSendCommand("DATA", 354);
        SMTPOutputStream sMTPOutputStream = new SMTPOutputStream(this.serverOutput);
        this.dataStream = sMTPOutputStream;
        return sMTPOutputStream;
    }

    /* access modifiers changed from: protected */
    public void finishData() throws IOException, MessagingException {
        this.dataStream.ensureAtBOL();
        issueSendCommand(".", ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
    }

    /* access modifiers changed from: protected */
    public void startTLS() throws MessagingException {
        issueCommand("STARTTLS", 220);
        try {
            Socket socket = this.serverSocket;
            Properties properties = this.session.getProperties();
            this.serverSocket = SocketFetcher.startTLS(socket, properties, "mail." + this.name);
            initStreams();
        } catch (IOException e) {
            closeConnection();
            throw new MessagingException("Could not convert socket to TLS", e);
        }
    }

    private void openServer(String str, int i) throws MessagingException {
        if (this.debug) {
            PrintStream printStream = this.out;
            printStream.println("DEBUG SMTP: trying to connect to host \"" + str + "\", port " + i + ", isSSL " + this.isSSL);
        }
        try {
            Properties properties = this.session.getProperties();
            Socket socket = SocketFetcher.getSocket(str, i, properties, "mail." + this.name, this.isSSL);
            this.serverSocket = socket;
            i = socket.getPort();
            initStreams();
            int readServerResponse = readServerResponse();
            if (readServerResponse != 220) {
                this.serverSocket.close();
                this.serverSocket = null;
                this.serverOutput = null;
                this.serverInput = null;
                this.lineInputStream = null;
                if (this.debug) {
                    PrintStream printStream2 = this.out;
                    printStream2.println("DEBUG SMTP: could not connect to host \"" + str + "\", port: " + i + ", response: " + readServerResponse + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                }
                throw new MessagingException("Could not connect to SMTP host: " + str + ", port: " + i + ", response: " + readServerResponse);
            } else if (this.debug) {
                PrintStream printStream3 = this.out;
                printStream3.println("DEBUG SMTP: connected to host \"" + str + "\", port: " + i + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
        } catch (UnknownHostException e) {
            throw new MessagingException("Unknown SMTP host: " + str, e);
        } catch (IOException e2) {
            throw new MessagingException("Could not connect to SMTP host: " + str + ", port: " + i, e2);
        }
    }

    private void openServer() throws MessagingException {
        try {
            int port = this.serverSocket.getPort();
            String hostName = this.serverSocket.getInetAddress().getHostName();
            if (this.debug) {
                PrintStream printStream = this.out;
                printStream.println("DEBUG SMTP: starting protocol to host \"" + hostName + "\", port " + port);
            }
            initStreams();
            int readServerResponse = readServerResponse();
            if (readServerResponse != 220) {
                this.serverSocket.close();
                this.serverSocket = null;
                this.serverOutput = null;
                this.serverInput = null;
                this.lineInputStream = null;
                if (this.debug) {
                    PrintStream printStream2 = this.out;
                    printStream2.println("DEBUG SMTP: got bad greeting from host \"" + hostName + "\", port: " + port + ", response: " + readServerResponse + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                }
                throw new MessagingException("Got bad greeting from SMTP host: " + hostName + ", port: " + port + ", response: " + readServerResponse);
            } else if (this.debug) {
                PrintStream printStream3 = this.out;
                printStream3.println("DEBUG SMTP: protocol started to host \"" + hostName + "\", port: " + port + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
        } catch (IOException e) {
            throw new MessagingException("Could not start protocol to SMTP host: " + UNKNOWN + ", port: " + -1, e);
        }
    }

    private void initStreams() throws IOException {
        Properties properties = this.session.getProperties();
        PrintStream debugOut = this.session.getDebugOut();
        boolean debug = this.session.getDebug();
        String property = properties.getProperty("mail.debug.quote");
        boolean z = property != null && property.equalsIgnoreCase("true");
        TraceInputStream traceInputStream = new TraceInputStream(this.serverSocket.getInputStream(), debugOut);
        traceInputStream.setTrace(debug);
        traceInputStream.setQuote(z);
        TraceOutputStream traceOutputStream = new TraceOutputStream(this.serverSocket.getOutputStream(), debugOut);
        traceOutputStream.setTrace(debug);
        traceOutputStream.setQuote(z);
        this.serverOutput = new BufferedOutputStream(traceOutputStream);
        this.serverInput = new BufferedInputStream(traceInputStream);
        this.lineInputStream = new LineInputStream(this.serverInput);
    }

    public synchronized void issueCommand(String str, int i) throws MessagingException {
        sendCommand(str);
        if (readServerResponse() != i) {
            throw new MessagingException(this.lastServerResponse);
        }
    }

    private void issueSendCommand(String str, int i) throws MessagingException {
        sendCommand(str);
        int readServerResponse = readServerResponse();
        if (readServerResponse != i) {
            Address[] addressArr = this.validSentAddr;
            int length = addressArr == null ? 0 : addressArr.length;
            Address[] addressArr2 = this.validUnsentAddr;
            int length2 = addressArr2 == null ? 0 : addressArr2.length;
            Address[] addressArr3 = new Address[(length + length2)];
            if (length > 0) {
                System.arraycopy(addressArr, 0, addressArr3, 0, length);
            }
            if (length2 > 0) {
                System.arraycopy(this.validUnsentAddr, 0, addressArr3, length, length2);
            }
            this.validSentAddr = null;
            this.validUnsentAddr = addressArr3;
            if (this.debug) {
                PrintStream printStream = this.out;
                printStream.println("DEBUG SMTP: got response code " + readServerResponse + ", with response: " + this.lastServerResponse);
            }
            String str2 = this.lastServerResponse;
            int i2 = this.lastReturnCode;
            if (this.serverSocket != null) {
                issueCommand("RSET", ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
            }
            this.lastServerResponse = str2;
            this.lastReturnCode = i2;
            throw new SMTPSendFailedException(str, readServerResponse, this.lastServerResponse, this.exception, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
        }
    }

    public synchronized int simpleCommand(String str) throws MessagingException {
        sendCommand(str);
        return readServerResponse();
    }

    /* access modifiers changed from: protected */
    public int simpleCommand(byte[] bArr) throws MessagingException {
        sendCommand(bArr);
        return readServerResponse();
    }

    /* access modifiers changed from: protected */
    public void sendCommand(String str) throws MessagingException {
        sendCommand(ASCIIUtility.getBytes(str));
    }

    private void sendCommand(byte[] bArr) throws MessagingException {
        try {
            this.serverOutput.write(bArr);
            this.serverOutput.write(CRLF);
            this.serverOutput.flush();
        } catch (IOException e) {
            throw new MessagingException("Can't send command to SMTP host", e);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0061, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0064, code lost:
        if (r6.debug != false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0066, code lost:
        r1.printStackTrace(r6.out);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:20:0x005d, B:26:0x006c] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x005d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x006c */
    public int readServerResponse() throws MessagingException {
        String readLine;
        int i;
        StringBuffer stringBuffer = new StringBuffer(100);
        do {
            try {
                readLine = this.lineInputStream.readLine();
                if (readLine == null) {
                    String stringBuffer2 = stringBuffer.toString();
                    if (stringBuffer2.length() == 0) {
                        stringBuffer2 = "[EOF]";
                    }
                    this.lastServerResponse = stringBuffer2;
                    this.lastReturnCode = -1;
                    if (this.debug) {
                        PrintStream printStream = this.out;
                        printStream.println("DEBUG SMTP: EOF: " + stringBuffer2);
                    }
                    return -1;
                }
                stringBuffer.append(readLine);
                stringBuffer.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            } catch (IOException e) {
                if (this.debug) {
                    PrintStream printStream2 = this.out;
                    printStream2.println("DEBUG SMTP: exception reading response: " + e);
                }
                this.lastServerResponse = "";
                this.lastReturnCode = 0;
                throw new MessagingException("Exception reading response", e);
            }
        } while (isNotLastLine(readLine));
        String stringBuffer3 = stringBuffer.toString();
        if (stringBuffer3 != null && stringBuffer3.length() >= 3) {
            i = Integer.parseInt(stringBuffer3.substring(0, 3));
            try {
                close();
            } catch (MessagingException e2) {
                if (this.debug) {
                    e2.printStackTrace(this.out);
                }
            }
        }
        i = -1;
        if (i == -1 && this.debug) {
            PrintStream printStream3 = this.out;
            printStream3.println("DEBUG SMTP: bad server response: " + stringBuffer3);
        }
        this.lastServerResponse = stringBuffer3;
        this.lastReturnCode = i;
        return i;
    }

    /* access modifiers changed from: protected */
    public void checkConnected() {
        if (!super.isConnected()) {
            throw new IllegalStateException("Not connected");
        }
    }

    private boolean isNotLastLine(String str) {
        return str != null && str.length() >= 4 && str.charAt(3) == '-';
    }

    private String normalizeAddress(String str) {
        if (str.startsWith("<") || str.endsWith(">")) {
            return str;
        }
        return "<" + str + ">";
    }

    public boolean supportsExtension(String str) {
        Hashtable hashtable = this.extMap;
        return (hashtable == null || hashtable.get(str.toUpperCase(Locale.ENGLISH)) == null) ? false : true;
    }

    public String getExtensionParameter(String str) {
        Hashtable hashtable = this.extMap;
        if (hashtable == null) {
            return null;
        }
        return (String) hashtable.get(str.toUpperCase(Locale.ENGLISH));
    }

    /* access modifiers changed from: protected */
    public boolean supportsAuthentication(String str) {
        String str2;
        Hashtable hashtable = this.extMap;
        if (hashtable == null || (str2 = (String) hashtable.get("AUTH")) == null) {
            return false;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str2);
        while (stringTokenizer.hasMoreTokens()) {
            if (stringTokenizer.nextToken().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    protected static String xtext(String str) {
        StringBuffer stringBuffer = null;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt < 128) {
                if (charAt < '!' || charAt > '~' || charAt == '+' || charAt == '=') {
                    if (stringBuffer == null) {
                        stringBuffer = new StringBuffer(str.length() + 4);
                        stringBuffer.append(str.substring(0, i));
                    }
                    stringBuffer.append('+');
                    stringBuffer.append(hexchar[(charAt & 240) >> 4]);
                    stringBuffer.append(hexchar[charAt & 15]);
                } else if (stringBuffer != null) {
                    stringBuffer.append(charAt);
                }
            } else {
                throw new IllegalArgumentException("Non-ASCII character in SMTP submitter: " + str);
            }
        }
        if (stringBuffer != null) {
            return stringBuffer.toString();
        }
        return str;
    }
}
