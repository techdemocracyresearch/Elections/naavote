package com.swmansion.rnscreens;

import android.content.Context;
import android.content.ContextWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.ChoreographerCompat;
import com.facebook.react.modules.core.ReactChoreographer;
import com.swmansion.rnscreens.Screen;
import com.swmansion.rnscreens.ScreenFragment;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class ScreenContainer<T extends ScreenFragment> extends ViewGroup {
    private FragmentTransaction mCurrentTransaction;
    protected FragmentManager mFragmentManager;
    private final ChoreographerCompat.FrameCallback mFrameCallback = new ChoreographerCompat.FrameCallback() {
        /* class com.swmansion.rnscreens.ScreenContainer.AnonymousClass1 */

        @Override // com.facebook.react.modules.core.ChoreographerCompat.FrameCallback
        public void doFrame(long j) {
            ScreenContainer.this.updateIfNeeded();
        }
    };
    private boolean mIsAttached;
    private final ChoreographerCompat.FrameCallback mLayoutCallback = new ChoreographerCompat.FrameCallback() {
        /* class com.swmansion.rnscreens.ScreenContainer.AnonymousClass2 */

        @Override // com.facebook.react.modules.core.ChoreographerCompat.FrameCallback
        public void doFrame(long j) {
            ScreenContainer.this.mLayoutEnqueued = false;
            ScreenContainer screenContainer = ScreenContainer.this;
            screenContainer.measure(View.MeasureSpec.makeMeasureSpec(screenContainer.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(ScreenContainer.this.getHeight(), 1073741824));
            ScreenContainer screenContainer2 = ScreenContainer.this;
            screenContainer2.layout(screenContainer2.getLeft(), ScreenContainer.this.getTop(), ScreenContainer.this.getRight(), ScreenContainer.this.getBottom());
        }
    };
    private boolean mLayoutEnqueued = false;
    private boolean mNeedUpdate;
    private ScreenFragment mParentScreenFragment = null;
    private FragmentTransaction mProcessingTransaction;
    protected final ArrayList<T> mScreenFragments = new ArrayList<>();

    public ScreenContainer(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            getChildAt(i5).layout(0, 0, getWidth(), getHeight());
        }
    }

    public void removeView(View view) {
        if (view == getFocusedChild()) {
            ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 2);
        }
        super.removeView(view);
    }

    public void requestLayout() {
        super.requestLayout();
        if (!this.mLayoutEnqueued && this.mLayoutCallback != null) {
            this.mLayoutEnqueued = true;
            ReactChoreographer.getInstance().postFrameCallback(ReactChoreographer.CallbackType.NATIVE_ANIMATED_MODULE, this.mLayoutCallback);
        }
    }

    public boolean isNested() {
        return this.mParentScreenFragment != null;
    }

    /* access modifiers changed from: protected */
    public void markUpdated() {
        if (!this.mNeedUpdate) {
            this.mNeedUpdate = true;
            ReactChoreographer.getInstance().postFrameCallback(ReactChoreographer.CallbackType.NATIVE_ANIMATED_MODULE, this.mFrameCallback);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyChildUpdate() {
        markUpdated();
    }

    /* access modifiers changed from: protected */
    public T adapt(Screen screen) {
        return (T) new ScreenFragment(screen);
    }

    /* access modifiers changed from: protected */
    public void addScreen(Screen screen, int i) {
        T adapt = adapt(screen);
        screen.setFragment(adapt);
        this.mScreenFragments.add(i, adapt);
        screen.setContainer(this);
        markUpdated();
    }

    /* access modifiers changed from: protected */
    public void removeScreenAt(int i) {
        this.mScreenFragments.get(i).getScreen().setContainer(null);
        this.mScreenFragments.remove(i);
        markUpdated();
    }

    /* access modifiers changed from: protected */
    public void removeAllScreens() {
        Iterator<T> it = this.mScreenFragments.iterator();
        while (it.hasNext()) {
            it.next().getScreen().setContainer(null);
        }
        this.mScreenFragments.clear();
        markUpdated();
    }

    /* access modifiers changed from: protected */
    public int getScreenCount() {
        return this.mScreenFragments.size();
    }

    /* access modifiers changed from: protected */
    public Screen getScreenAt(int i) {
        return this.mScreenFragments.get(i).getScreen();
    }

    public Screen getTopScreen() {
        Iterator<T> it = this.mScreenFragments.iterator();
        while (it.hasNext()) {
            T next = it.next();
            if (getActivityState(next) == Screen.ActivityState.ON_TOP) {
                return next.getScreen();
            }
        }
        return null;
    }

    private void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
        updateIfNeeded();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004e  */
    private void setupFragmentManager() {
        boolean z;
        boolean z2;
        ViewParent viewParent = this;
        while (true) {
            z = viewParent instanceof ReactRootView;
            if (!z && !(viewParent instanceof Screen) && viewParent.getParent() != null) {
                viewParent = viewParent.getParent();
            }
        }
        if (viewParent instanceof Screen) {
            ScreenFragment fragment = ((Screen) viewParent).getFragment();
            setFragmentManager(fragment.getChildFragmentManager());
            this.mParentScreenFragment = fragment;
            fragment.registerChildScreenContainer(this);
        } else if (z) {
            Context context = ((ReactRootView) viewParent).getContext();
            while (true) {
                z2 = context instanceof FragmentActivity;
                if (!z2 && (context instanceof ContextWrapper)) {
                    context = ((ContextWrapper) context).getBaseContext();
                } else if (!z2) {
                    setFragmentManager(((FragmentActivity) context).getSupportFragmentManager());
                    return;
                } else {
                    throw new IllegalStateException("In order to use RNScreens components your app's activity need to extend ReactFragmentActivity or ReactCompatActivity");
                }
            }
            if (!z2) {
            }
        } else {
            throw new IllegalStateException("ScreenContainer is not attached under ReactRootView");
        }
    }

    /* access modifiers changed from: protected */
    public FragmentTransaction getOrCreateTransaction() {
        if (this.mCurrentTransaction == null) {
            FragmentTransaction beginTransaction = this.mFragmentManager.beginTransaction();
            this.mCurrentTransaction = beginTransaction;
            beginTransaction.setReorderingAllowed(true);
        }
        return this.mCurrentTransaction;
    }

    /* access modifiers changed from: protected */
    public void tryCommitTransaction() {
        final FragmentTransaction fragmentTransaction = this.mCurrentTransaction;
        if (fragmentTransaction != null) {
            this.mProcessingTransaction = fragmentTransaction;
            fragmentTransaction.runOnCommit(new Runnable() {
                /* class com.swmansion.rnscreens.ScreenContainer.AnonymousClass3 */

                public void run() {
                    if (ScreenContainer.this.mProcessingTransaction == fragmentTransaction) {
                        ScreenContainer.this.mProcessingTransaction = null;
                    }
                }
            });
            this.mCurrentTransaction.commitAllowingStateLoss();
            this.mCurrentTransaction = null;
        }
    }

    private void attachScreen(ScreenFragment screenFragment) {
        getOrCreateTransaction().add(getId(), screenFragment);
    }

    private void moveToFront(ScreenFragment screenFragment) {
        FragmentTransaction orCreateTransaction = getOrCreateTransaction();
        orCreateTransaction.remove(screenFragment);
        orCreateTransaction.add(getId(), screenFragment);
    }

    private void detachScreen(ScreenFragment screenFragment) {
        getOrCreateTransaction().remove(screenFragment);
    }

    /* access modifiers changed from: protected */
    public Screen.ActivityState getActivityState(ScreenFragment screenFragment) {
        return screenFragment.getScreen().getActivityState();
    }

    /* access modifiers changed from: protected */
    public boolean hasScreen(ScreenFragment screenFragment) {
        return this.mScreenFragments.contains(screenFragment);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mIsAttached = true;
        this.mNeedUpdate = true;
        setupFragmentManager();
    }

    private void removeMyFragments() {
        FragmentTransaction beginTransaction = this.mFragmentManager.beginTransaction();
        boolean z = false;
        for (Fragment fragment : this.mFragmentManager.getFragments()) {
            if ((fragment instanceof ScreenFragment) && ((ScreenFragment) fragment).mScreenView.getContainer() == this) {
                beginTransaction.remove(fragment);
                z = true;
            }
        }
        if (z) {
            beginTransaction.commitNowAllowingStateLoss();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        FragmentManager fragmentManager = this.mFragmentManager;
        if (fragmentManager != null && !fragmentManager.isDestroyed()) {
            removeMyFragments();
            this.mFragmentManager.executePendingTransactions();
        }
        ScreenFragment screenFragment = this.mParentScreenFragment;
        if (screenFragment != null) {
            screenFragment.unregisterChildScreenContainer(this);
            this.mParentScreenFragment = null;
        }
        super.onDetachedFromWindow();
        this.mIsAttached = false;
        removeAllViews();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).measure(i, i2);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void updateIfNeeded() {
        if (this.mNeedUpdate && this.mIsAttached && this.mFragmentManager != null) {
            this.mNeedUpdate = false;
            onUpdate();
        }
    }

    private final void onUpdate() {
        this.mFragmentManager.executePendingTransactions();
        performUpdate();
        notifyContainerUpdate();
    }

    /* access modifiers changed from: protected */
    public void performUpdate() {
        HashSet hashSet = new HashSet(this.mFragmentManager.getFragments());
        Iterator<T> it = this.mScreenFragments.iterator();
        while (it.hasNext()) {
            T next = it.next();
            if (getActivityState(next) == Screen.ActivityState.INACTIVE && next.isAdded()) {
                detachScreen(next);
            }
            hashSet.remove(next);
        }
        boolean z = false;
        if (!hashSet.isEmpty()) {
            Object[] array = hashSet.toArray();
            for (int i = 0; i < array.length; i++) {
                if ((array[i] instanceof ScreenFragment) && ((ScreenFragment) array[i]).getScreen().getContainer() == null) {
                    detachScreen((ScreenFragment) array[i]);
                }
            }
        }
        boolean z2 = getTopScreen() == null;
        Iterator<T> it2 = this.mScreenFragments.iterator();
        while (it2.hasNext()) {
            T next2 = it2.next();
            Screen.ActivityState activityState = getActivityState(next2);
            if (activityState != Screen.ActivityState.INACTIVE && !next2.isAdded()) {
                attachScreen(next2);
                z = true;
            } else if (activityState != Screen.ActivityState.INACTIVE && z) {
                moveToFront(next2);
            }
            next2.getScreen().setTransitioning(z2);
        }
        tryCommitTransaction();
    }

    /* access modifiers changed from: protected */
    public void notifyContainerUpdate() {
        Screen topScreen = getTopScreen();
        if (topScreen != null) {
            topScreen.getFragment().onContainerUpdate();
        }
    }
}
