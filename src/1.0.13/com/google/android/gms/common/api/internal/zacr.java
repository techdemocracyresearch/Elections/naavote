package com.google.android.gms.common.api.internal;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
public final class zacr implements zacq {
    private final /* synthetic */ zaco zaa;

    zacr(zaco zaco) {
        this.zaa = zaco;
    }

    @Override // com.google.android.gms.common.api.internal.zacq
    public final void zaa(BasePendingResult<?> basePendingResult) {
        this.zaa.zab.remove(basePendingResult);
    }
}
