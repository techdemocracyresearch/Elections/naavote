package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final /* synthetic */ class zzbd implements zzcu {
    static final zzcu zzfy = new zzbd();

    private zzbd() {
    }

    @Override // com.google.android.gms.internal.vision.zzcu
    public final Object get() {
        return zzbe.zzaf();
    }
}
