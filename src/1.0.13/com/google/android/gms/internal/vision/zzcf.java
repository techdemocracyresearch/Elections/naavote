package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public final class zzcf implements zzgy {
    static final zzgy zzhf = new zzcf();

    private zzcf() {
    }

    @Override // com.google.android.gms.internal.vision.zzgy
    public final boolean zzg(int i) {
        return zzce.zzq(i) != null;
    }
}
