package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzkf;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
final class zzkm implements zzgv<zzkf.zze.zzc> {
    zzkm() {
    }

    /* Return type fixed from 'com.google.android.gms.internal.vision.zzgw' to match base method */
    @Override // com.google.android.gms.internal.vision.zzgv
    public final /* synthetic */ zzkf.zze.zzc zzh(int i) {
        return zzkf.zze.zzc.zzcb(i);
    }
}
