package com.google.android.gms.internal.vision;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzjl extends zzjj<zzjm, zzjm> {
    zzjl() {
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final boolean zza(zzis zzis) {
        return false;
    }

    private static void zza(Object obj, zzjm zzjm) {
        ((zzgs) obj).zzwj = zzjm;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final void zzh(Object obj) {
        ((zzgs) obj).zzwj.zzdp();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ int zzs(zzjm zzjm) {
        return zzjm.zzgf();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ int zzy(zzjm zzjm) {
        return zzjm.zzii();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ zzjm zzh(zzjm zzjm, zzjm zzjm2) {
        zzjm zzjm3 = zzjm;
        zzjm zzjm4 = zzjm2;
        if (zzjm4.equals(zzjm.zzig())) {
            return zzjm3;
        }
        return zzjm.zza(zzjm3, zzjm4);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.google.android.gms.internal.vision.zzkg] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zzc(zzjm zzjm, zzkg zzkg) throws IOException {
        zzjm.zza(zzkg);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.google.android.gms.internal.vision.zzkg] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zza(zzjm zzjm, zzkg zzkg) throws IOException {
        zzjm.zzb(zzkg);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zzg(Object obj, zzjm zzjm) {
        zza(obj, zzjm);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ zzjm zzx(Object obj) {
        zzjm zzjm = ((zzgs) obj).zzwj;
        if (zzjm != zzjm.zzig()) {
            return zzjm;
        }
        zzjm zzih = zzjm.zzih();
        zza(obj, zzih);
        return zzih;
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ zzjm zzw(Object obj) {
        return ((zzgs) obj).zzwj;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zzf(Object obj, zzjm zzjm) {
        zza(obj, zzjm);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ zzjm zzo(zzjm zzjm) {
        zzjm zzjm2 = zzjm;
        zzjm2.zzdp();
        return zzjm2;
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ zzjm zzif() {
        return zzjm.zzih();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, java.lang.Object] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zza(zzjm zzjm, int i, zzjm zzjm2) {
        zzjm.zzb((i << 3) | 3, zzjm2);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, com.google.android.gms.internal.vision.zzfh] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zza(zzjm zzjm, int i, zzfh zzfh) {
        zzjm.zzb((i << 3) | 2, zzfh);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, long] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zzb(zzjm zzjm, int i, long j) {
        zzjm.zzb((i << 3) | 1, Long.valueOf(j));
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zzc(zzjm zzjm, int i, int i2) {
        zzjm.zzb((i << 3) | 5, Integer.valueOf(i2));
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, long] */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzjj
    public final /* synthetic */ void zza(zzjm zzjm, int i, long j) {
        zzjm.zzb(i << 3, Long.valueOf(j));
    }
}
