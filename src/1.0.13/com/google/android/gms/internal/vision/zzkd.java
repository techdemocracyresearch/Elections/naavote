package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public enum zzkd {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(zzfh.zzsd),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzya;

    private zzkd(Object obj) {
        this.zzya = obj;
    }
}
