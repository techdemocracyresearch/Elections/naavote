package com.google.android.gms.internal.vision;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import kotlin.UByte;
import kotlin.jvm.internal.ByteCompanionObject;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzfg extends zzfe {
    private final byte[] buffer;
    private int limit;
    private int pos;
    private int tag;
    private final boolean zzsa = true;
    private final int zzsb;
    private int zzsc;

    public zzfg(ByteBuffer byteBuffer, boolean z) {
        super(null);
        this.buffer = byteBuffer.array();
        int arrayOffset = byteBuffer.arrayOffset() + byteBuffer.position();
        this.pos = arrayOffset;
        this.zzsb = arrayOffset;
        this.limit = byteBuffer.arrayOffset() + byteBuffer.limit();
    }

    private final boolean zzdt() {
        return this.pos == this.limit;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzdu() throws IOException {
        if (zzdt()) {
            return Integer.MAX_VALUE;
        }
        int zzek = zzek();
        this.tag = zzek;
        if (zzek == this.zzsc) {
            return Integer.MAX_VALUE;
        }
        return zzek >>> 3;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int getTag() {
        return this.tag;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0037  */
    @Override // com.google.android.gms.internal.vision.zzis
    public final boolean zzdv() throws IOException {
        int i;
        int i2;
        if (zzdt() || (i = this.tag) == (i2 = this.zzsc)) {
            return false;
        }
        int i3 = i & 7;
        if (i3 == 0) {
            int i4 = this.limit;
            int i5 = this.pos;
            if (i4 - i5 >= 10) {
                byte[] bArr = this.buffer;
                int i6 = 0;
                while (true) {
                    if (i6 >= 10) {
                        break;
                    }
                    int i7 = i5 + 1;
                    if (bArr[i5] >= 0) {
                        this.pos = i7;
                        break;
                    }
                    i6++;
                    i5 = i7;
                }
                return true;
            }
            for (int i8 = 0; i8 < 10; i8++) {
                if (readByte() >= 0) {
                    return true;
                }
            }
            throw zzhc.zzgo();
        } else if (i3 == 1) {
            zzai(8);
            return true;
        } else if (i3 == 2) {
            zzai(zzek());
            return true;
        } else if (i3 == 3) {
            this.zzsc = ((i >>> 3) << 3) | 4;
            while (zzdu() != Integer.MAX_VALUE && zzdv()) {
                while (zzdu() != Integer.MAX_VALUE) {
                    while (zzdu() != Integer.MAX_VALUE) {
                    }
                }
            }
            if (this.tag == this.zzsc) {
                this.zzsc = i2;
                return true;
            }
            throw zzhc.zzgs();
        } else if (i3 == 5) {
            zzai(4);
            return true;
        } else {
            throw zzhc.zzgr();
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final double readDouble() throws IOException {
        zzak(1);
        return Double.longBitsToDouble(zzeo());
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final float readFloat() throws IOException {
        zzak(5);
        return Float.intBitsToFloat(zzen());
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzdw() throws IOException {
        zzak(0);
        return zzel();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzdx() throws IOException {
        zzak(0);
        return zzel();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzdy() throws IOException {
        zzak(0);
        return zzek();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzdz() throws IOException {
        zzak(1);
        return zzeo();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzea() throws IOException {
        zzak(5);
        return zzen();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final boolean zzeb() throws IOException {
        zzak(0);
        if (zzek() != 0) {
            return true;
        }
        return false;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final String readString() throws IOException {
        return zzj(false);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final String zzec() throws IOException {
        return zzj(true);
    }

    private final String zzj(boolean z) throws IOException {
        zzak(2);
        int zzek = zzek();
        if (zzek == 0) {
            return "";
        }
        zzaj(zzek);
        if (z) {
            byte[] bArr = this.buffer;
            int i = this.pos;
            if (!zzjs.zzf(bArr, i, i + zzek)) {
                throw zzhc.zzgt();
            }
        }
        String str = new String(this.buffer, this.pos, zzek, zzgt.UTF_8);
        this.pos += zzek;
        return str;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zza(Class<T> cls, zzgd zzgd) throws IOException {
        zzak(2);
        return (T) zzb(zzin.zzho().zzf(cls), zzgd);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zza(zzir<T> zzir, zzgd zzgd) throws IOException {
        zzak(2);
        return (T) zzb(zzir, zzgd);
    }

    private final <T> T zzb(zzir<T> zzir, zzgd zzgd) throws IOException {
        int zzek = zzek();
        zzaj(zzek);
        int i = this.limit;
        int i2 = this.pos + zzek;
        this.limit = i2;
        try {
            T newInstance = zzir.newInstance();
            zzir.zza(newInstance, this, zzgd);
            zzir.zzh(newInstance);
            if (this.pos == i2) {
                return newInstance;
            }
            throw zzhc.zzgs();
        } finally {
            this.limit = i;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zzb(Class<T> cls, zzgd zzgd) throws IOException {
        zzak(3);
        return (T) zzd(zzin.zzho().zzf(cls), zzgd);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> T zzc(zzir<T> zzir, zzgd zzgd) throws IOException {
        zzak(3);
        return (T) zzd(zzir, zzgd);
    }

    private final <T> T zzd(zzir<T> zzir, zzgd zzgd) throws IOException {
        int i = this.zzsc;
        this.zzsc = ((this.tag >>> 3) << 3) | 4;
        try {
            T newInstance = zzir.newInstance();
            zzir.zza(newInstance, this, zzgd);
            zzir.zzh(newInstance);
            if (this.tag == this.zzsc) {
                return newInstance;
            }
            throw zzhc.zzgs();
        } finally {
            this.zzsc = i;
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final zzfh zzed() throws IOException {
        zzfh zzfh;
        zzak(2);
        int zzek = zzek();
        if (zzek == 0) {
            return zzfh.zzsd;
        }
        zzaj(zzek);
        if (this.zzsa) {
            zzfh = zzfh.zzb(this.buffer, this.pos, zzek);
        } else {
            zzfh = zzfh.zza(this.buffer, this.pos, zzek);
        }
        this.pos += zzek;
        return zzfh;
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzee() throws IOException {
        zzak(0);
        return zzek();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzef() throws IOException {
        zzak(0);
        return zzek();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzeg() throws IOException {
        zzak(5);
        return zzen();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzeh() throws IOException {
        zzak(1);
        return zzeo();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final int zzei() throws IOException {
        zzak(0);
        return zzft.zzav(zzek());
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final long zzej() throws IOException {
        zzak(0);
        return zzft.zzr(zzel());
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zza(List<Double> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgb) {
            zzgb zzgb = (zzgb) list;
            int i3 = this.tag & 7;
            if (i3 == 1) {
                do {
                    zzgb.zzc(readDouble());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = zzek();
                zzal(zzek);
                int i4 = this.pos + zzek;
                while (this.pos < i4) {
                    zzgb.zzc(Double.longBitsToDouble(zzeq()));
                }
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i5 = this.tag & 7;
            if (i5 == 1) {
                do {
                    list.add(Double.valueOf(readDouble()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i5 == 2) {
                int zzek2 = zzek();
                zzal(zzek2);
                int i6 = this.pos + zzek2;
                while (this.pos < i6) {
                    list.add(Double.valueOf(Double.longBitsToDouble(zzeq())));
                }
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzb(List<Float> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgo) {
            zzgo zzgo = (zzgo) list;
            int i3 = this.tag & 7;
            if (i3 == 2) {
                int zzek = zzek();
                zzam(zzek);
                int i4 = this.pos + zzek;
                while (this.pos < i4) {
                    zzgo.zzu(Float.intBitsToFloat(zzep()));
                }
            } else if (i3 == 5) {
                do {
                    zzgo.zzu(readFloat());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i5 = this.tag & 7;
            if (i5 == 2) {
                int zzek2 = zzek();
                zzam(zzek2);
                int i6 = this.pos + zzek2;
                while (this.pos < i6) {
                    list.add(Float.valueOf(Float.intBitsToFloat(zzep())));
                }
            } else if (i5 == 5) {
                do {
                    list.add(Float.valueOf(readFloat()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzc(List<Long> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzhq.zzac(zzdw());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzhq.zzac(zzel());
                }
                zzan(zzek);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Long.valueOf(zzdw()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Long.valueOf(zzel()));
                }
                zzan(zzek2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzd(List<Long> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzhq.zzac(zzdx());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzhq.zzac(zzel());
                }
                zzan(zzek);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Long.valueOf(zzdx()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Long.valueOf(zzel()));
                }
                zzan(zzek2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zze(List<Integer> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzgu.zzbm(zzdy());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzgu.zzbm(zzek());
                }
                zzan(zzek);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Integer.valueOf(zzdy()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Integer.valueOf(zzek()));
                }
                zzan(zzek2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzf(List<Long> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i3 = this.tag & 7;
            if (i3 == 1) {
                do {
                    zzhq.zzac(zzdz());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = zzek();
                zzal(zzek);
                int i4 = this.pos + zzek;
                while (this.pos < i4) {
                    zzhq.zzac(zzeq());
                }
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i5 = this.tag & 7;
            if (i5 == 1) {
                do {
                    list.add(Long.valueOf(zzdz()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i5 == 2) {
                int zzek2 = zzek();
                zzal(zzek2);
                int i6 = this.pos + zzek2;
                while (this.pos < i6) {
                    list.add(Long.valueOf(zzeq()));
                }
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzg(List<Integer> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i3 = this.tag & 7;
            if (i3 == 2) {
                int zzek = zzek();
                zzam(zzek);
                int i4 = this.pos + zzek;
                while (this.pos < i4) {
                    zzgu.zzbm(zzep());
                }
            } else if (i3 == 5) {
                do {
                    zzgu.zzbm(zzea());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i5 = this.tag & 7;
            if (i5 == 2) {
                int zzek2 = zzek();
                zzam(zzek2);
                int i6 = this.pos + zzek2;
                while (this.pos < i6) {
                    list.add(Integer.valueOf(zzep()));
                }
            } else if (i5 == 5) {
                do {
                    list.add(Integer.valueOf(zzea()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzh(List<Boolean> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzff) {
            zzff zzff = (zzff) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzff.addBoolean(zzeb());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzff.addBoolean(zzek() != 0);
                }
                zzan(zzek);
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Boolean.valueOf(zzeb()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Boolean.valueOf(zzek() != 0));
                }
                zzan(zzek2);
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void readStringList(List<String> list) throws IOException {
        zza(list, false);
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzi(List<String> list) throws IOException {
        zza(list, true);
    }

    private final void zza(List<String> list, boolean z) throws IOException {
        int i;
        int i2;
        if ((this.tag & 7) != 2) {
            throw zzhc.zzgr();
        } else if (!(list instanceof zzhj) || z) {
            do {
                list.add(zzj(z));
                if (!zzdt()) {
                    i = this.pos;
                } else {
                    return;
                }
            } while (zzek() == this.tag);
            this.pos = i;
        } else {
            zzhj zzhj = (zzhj) list;
            do {
                zzhj.zzc(zzed());
                if (!zzdt()) {
                    i2 = this.pos;
                } else {
                    return;
                }
            } while (zzek() == this.tag);
            this.pos = i2;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> void zza(List<T> list, zzir<T> zzir, zzgd zzgd) throws IOException {
        int i;
        int i2 = this.tag;
        if ((i2 & 7) == 2) {
            do {
                list.add(zzb(zzir, zzgd));
                if (!zzdt()) {
                    i = this.pos;
                } else {
                    return;
                }
            } while (zzek() == i2);
            this.pos = i;
            return;
        }
        throw zzhc.zzgr();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzis
    public final <T> void zzb(List<T> list, zzir<T> zzir, zzgd zzgd) throws IOException {
        int i;
        int i2 = this.tag;
        if ((i2 & 7) == 3) {
            do {
                list.add(zzd(zzir, zzgd));
                if (!zzdt()) {
                    i = this.pos;
                } else {
                    return;
                }
            } while (zzek() == i2);
            this.pos = i;
            return;
        }
        throw zzhc.zzgr();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzj(List<zzfh> list) throws IOException {
        int i;
        if ((this.tag & 7) == 2) {
            do {
                list.add(zzed());
                if (!zzdt()) {
                    i = this.pos;
                } else {
                    return;
                }
            } while (zzek() == this.tag);
            this.pos = i;
            return;
        }
        throw zzhc.zzgr();
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzk(List<Integer> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzgu.zzbm(zzee());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzgu.zzbm(zzek());
                }
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Integer.valueOf(zzee()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Integer.valueOf(zzek()));
                }
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzl(List<Integer> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzgu.zzbm(zzef());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzgu.zzbm(zzek());
                }
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Integer.valueOf(zzef()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Integer.valueOf(zzek()));
                }
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzm(List<Integer> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i3 = this.tag & 7;
            if (i3 == 2) {
                int zzek = zzek();
                zzam(zzek);
                int i4 = this.pos + zzek;
                while (this.pos < i4) {
                    zzgu.zzbm(zzep());
                }
            } else if (i3 == 5) {
                do {
                    zzgu.zzbm(zzeg());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i5 = this.tag & 7;
            if (i5 == 2) {
                int zzek2 = zzek();
                zzam(zzek2);
                int i6 = this.pos + zzek2;
                while (this.pos < i6) {
                    list.add(Integer.valueOf(zzep()));
                }
            } else if (i5 == 5) {
                do {
                    list.add(Integer.valueOf(zzeg()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzn(List<Long> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i3 = this.tag & 7;
            if (i3 == 1) {
                do {
                    zzhq.zzac(zzeh());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = zzek();
                zzal(zzek);
                int i4 = this.pos + zzek;
                while (this.pos < i4) {
                    zzhq.zzac(zzeq());
                }
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i5 = this.tag & 7;
            if (i5 == 1) {
                do {
                    list.add(Long.valueOf(zzeh()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i5 == 2) {
                int zzek2 = zzek();
                zzal(zzek2);
                int i6 = this.pos + zzek2;
                while (this.pos < i6) {
                    list.add(Long.valueOf(zzeq()));
                }
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzo(List<Integer> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzgu) {
            zzgu zzgu = (zzgu) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzgu.zzbm(zzei());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzgu.zzbm(zzft.zzav(zzek()));
                }
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Integer.valueOf(zzei()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Integer.valueOf(zzft.zzav(zzek())));
                }
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    @Override // com.google.android.gms.internal.vision.zzis
    public final void zzp(List<Long> list) throws IOException {
        int i;
        int i2;
        if (list instanceof zzhq) {
            zzhq zzhq = (zzhq) list;
            int i3 = this.tag & 7;
            if (i3 == 0) {
                do {
                    zzhq.zzac(zzej());
                    if (!zzdt()) {
                        i2 = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i2;
            } else if (i3 == 2) {
                int zzek = this.pos + zzek();
                while (this.pos < zzek) {
                    zzhq.zzac(zzft.zzr(zzel()));
                }
            } else {
                throw zzhc.zzgr();
            }
        } else {
            int i4 = this.tag & 7;
            if (i4 == 0) {
                do {
                    list.add(Long.valueOf(zzej()));
                    if (!zzdt()) {
                        i = this.pos;
                    } else {
                        return;
                    }
                } while (zzek() == this.tag);
                this.pos = i;
            } else if (i4 == 2) {
                int zzek2 = this.pos + zzek();
                while (this.pos < zzek2) {
                    list.add(Long.valueOf(zzft.zzr(zzel())));
                }
            } else {
                throw zzhc.zzgr();
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.util.Map<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzis
    public final <K, V> void zza(Map<K, V> map, zzht<K, V> zzht, zzgd zzgd) throws IOException {
        zzak(2);
        int zzek = zzek();
        zzaj(zzek);
        int i = this.limit;
        this.limit = this.pos + zzek;
        try {
            Object obj = zzht.zzyt;
            Object obj2 = zzht.zzgd;
            while (true) {
                int zzdu = zzdu();
                if (zzdu == Integer.MAX_VALUE) {
                    map.put(obj, obj2);
                    return;
                } else if (zzdu == 1) {
                    obj = zza(zzht.zzys, (Class<?>) null, (zzgd) null);
                } else if (zzdu != 2) {
                    try {
                        if (!zzdv()) {
                            throw new zzhc("Unable to parse map entry.");
                        }
                    } catch (zzhb unused) {
                        if (!zzdv()) {
                            throw new zzhc("Unable to parse map entry.");
                        }
                    }
                } else {
                    obj2 = zza(zzht.zzyu, zzht.zzgd.getClass(), zzgd);
                }
            }
        } finally {
            this.limit = i;
        }
    }

    private final Object zza(zzka zzka, Class<?> cls, zzgd zzgd) throws IOException {
        switch (zzfd.zzrx[zzka.ordinal()]) {
            case 1:
                return Boolean.valueOf(zzeb());
            case 2:
                return zzed();
            case 3:
                return Double.valueOf(readDouble());
            case 4:
                return Integer.valueOf(zzef());
            case 5:
                return Integer.valueOf(zzea());
            case 6:
                return Long.valueOf(zzdz());
            case 7:
                return Float.valueOf(readFloat());
            case 8:
                return Integer.valueOf(zzdy());
            case 9:
                return Long.valueOf(zzdx());
            case 10:
                return zza(cls, zzgd);
            case 11:
                return Integer.valueOf(zzeg());
            case 12:
                return Long.valueOf(zzeh());
            case 13:
                return Integer.valueOf(zzei());
            case 14:
                return Long.valueOf(zzej());
            case 15:
                return zzj(true);
            case 16:
                return Integer.valueOf(zzee());
            case 17:
                return Long.valueOf(zzdw());
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    private final int zzek() throws IOException {
        int i;
        int i2 = this.pos;
        int i3 = this.limit;
        if (i3 != i2) {
            byte[] bArr = this.buffer;
            int i4 = i2 + 1;
            byte b = bArr[i2];
            if (b >= 0) {
                this.pos = i4;
                return b;
            } else if (i3 - i4 < 9) {
                return (int) zzem();
            } else {
                int i5 = i4 + 1;
                int i6 = b ^ (bArr[i4] << 7);
                if (i6 < 0) {
                    i = i6 ^ -128;
                } else {
                    int i7 = i5 + 1;
                    int i8 = i6 ^ (bArr[i5] << 14);
                    if (i8 >= 0) {
                        i = i8 ^ 16256;
                    } else {
                        i5 = i7 + 1;
                        int i9 = i8 ^ (bArr[i7] << 21);
                        if (i9 < 0) {
                            i = i9 ^ -2080896;
                        } else {
                            i7 = i5 + 1;
                            byte b2 = bArr[i5];
                            i = (i9 ^ (b2 << 28)) ^ 266354560;
                            if (b2 < 0) {
                                i5 = i7 + 1;
                                if (bArr[i7] < 0) {
                                    i7 = i5 + 1;
                                    if (bArr[i5] < 0) {
                                        i5 = i7 + 1;
                                        if (bArr[i7] < 0) {
                                            i7 = i5 + 1;
                                            if (bArr[i5] < 0) {
                                                i5 = i7 + 1;
                                                if (bArr[i7] < 0) {
                                                    throw zzhc.zzgo();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i5 = i7;
                }
                this.pos = i5;
                return i;
            }
        } else {
            throw zzhc.zzgm();
        }
    }

    private final long zzel() throws IOException {
        long j;
        long j2;
        long j3;
        int i;
        int i2 = this.pos;
        int i3 = this.limit;
        if (i3 != i2) {
            byte[] bArr = this.buffer;
            int i4 = i2 + 1;
            byte b = bArr[i2];
            if (b >= 0) {
                this.pos = i4;
                return (long) b;
            } else if (i3 - i4 < 9) {
                return zzem();
            } else {
                int i5 = i4 + 1;
                int i6 = b ^ (bArr[i4] << 7);
                if (i6 < 0) {
                    i = i6 ^ -128;
                } else {
                    int i7 = i5 + 1;
                    int i8 = i6 ^ (bArr[i5] << 14);
                    if (i8 >= 0) {
                        i5 = i7;
                        j = (long) (i8 ^ 16256);
                    } else {
                        i5 = i7 + 1;
                        int i9 = i8 ^ (bArr[i7] << 21);
                        if (i9 < 0) {
                            i = i9 ^ -2080896;
                        } else {
                            long j4 = (long) i9;
                            int i10 = i5 + 1;
                            long j5 = j4 ^ (((long) bArr[i5]) << 28);
                            if (j5 >= 0) {
                                j3 = 266354560;
                            } else {
                                i5 = i10 + 1;
                                long j6 = j5 ^ (((long) bArr[i10]) << 35);
                                if (j6 < 0) {
                                    j2 = -34093383808L;
                                } else {
                                    i10 = i5 + 1;
                                    j5 = j6 ^ (((long) bArr[i5]) << 42);
                                    if (j5 >= 0) {
                                        j3 = 4363953127296L;
                                    } else {
                                        i5 = i10 + 1;
                                        j6 = j5 ^ (((long) bArr[i10]) << 49);
                                        if (j6 < 0) {
                                            j2 = -558586000294016L;
                                        } else {
                                            int i11 = i5 + 1;
                                            long j7 = (j6 ^ (((long) bArr[i5]) << 56)) ^ 71499008037633920L;
                                            if (j7 < 0) {
                                                i5 = i11 + 1;
                                                if (((long) bArr[i11]) < 0) {
                                                    throw zzhc.zzgo();
                                                }
                                            } else {
                                                i5 = i11;
                                            }
                                            j = j7;
                                        }
                                    }
                                }
                                j = j6 ^ j2;
                            }
                            j = j5 ^ j3;
                            i5 = i10;
                        }
                    }
                    this.pos = i5;
                    return j;
                }
                j = (long) i;
                this.pos = i5;
                return j;
            }
        } else {
            throw zzhc.zzgm();
        }
    }

    private final long zzem() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte readByte = readByte();
            j |= ((long) (readByte & ByteCompanionObject.MAX_VALUE)) << i;
            if ((readByte & ByteCompanionObject.MIN_VALUE) == 0) {
                return j;
            }
        }
        throw zzhc.zzgo();
    }

    private final byte readByte() throws IOException {
        int i = this.pos;
        if (i != this.limit) {
            byte[] bArr = this.buffer;
            this.pos = i + 1;
            return bArr[i];
        }
        throw zzhc.zzgm();
    }

    private final int zzen() throws IOException {
        zzaj(4);
        return zzep();
    }

    private final long zzeo() throws IOException {
        zzaj(8);
        return zzeq();
    }

    private final int zzep() {
        int i = this.pos;
        byte[] bArr = this.buffer;
        this.pos = i + 4;
        return ((bArr[i + 3] & UByte.MAX_VALUE) << 24) | (bArr[i] & UByte.MAX_VALUE) | ((bArr[i + 1] & UByte.MAX_VALUE) << 8) | ((bArr[i + 2] & UByte.MAX_VALUE) << 16);
    }

    private final long zzeq() {
        int i = this.pos;
        byte[] bArr = this.buffer;
        this.pos = i + 8;
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    private final void zzai(int i) throws IOException {
        zzaj(i);
        this.pos += i;
    }

    private final void zzaj(int i) throws IOException {
        if (i < 0 || i > this.limit - this.pos) {
            throw zzhc.zzgm();
        }
    }

    private final void zzak(int i) throws IOException {
        if ((this.tag & 7) != i) {
            throw zzhc.zzgr();
        }
    }

    private final void zzal(int i) throws IOException {
        zzaj(i);
        if ((i & 7) != 0) {
            throw zzhc.zzgs();
        }
    }

    private final void zzam(int i) throws IOException {
        zzaj(i);
        if ((i & 3) != 0) {
            throw zzhc.zzgs();
        }
    }

    private final void zzan(int i) throws IOException {
        if (this.pos != i) {
            throw zzhc.zzgm();
        }
    }
}
