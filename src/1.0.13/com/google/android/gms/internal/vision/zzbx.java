package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzgs;
import java.util.List;
import kotlin.text.Typography;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public final class zzbx {
    public static final zzgs.zzg<zzkf, List<zzb>> zzir = zzgs.zza(zzkf.zziz(), zzb.zzbi(), null, 202056002, zzka.MESSAGE, false, zzb.class);

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zzb extends zzgs<zzb, zza> implements zzie {
        private static volatile zzil<zzb> zzbd;
        private static final zzb zzjb;
        private int zzbf;
        private int zziz;
        private zzgz<C0019zzb> zzja = zzgh();

        /* renamed from: com.google.android.gms.internal.vision.zzbx$zzb$zzb  reason: collision with other inner class name */
        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class C0019zzb extends zzgs<C0019zzb, zza> implements zzie {
            private static volatile zzil<C0019zzb> zzbd;
            private static final C0019zzb zzjf;
            private int zzbf;
            private float zzjc;
            private float zzjd;
            private float zzje;

            private C0019zzb() {
            }

            /* renamed from: com.google.android.gms.internal.vision.zzbx$zzb$zzb$zza */
            /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
            public static final class zza extends zzgs.zza<C0019zzb, zza> implements zzie {
                private zza() {
                    super(C0019zzb.zzjf);
                }

                /* synthetic */ zza(zzby zzby) {
                    this();
                }
            }

            public final float getX() {
                return this.zzjc;
            }

            public final float getY() {
                return this.zzjd;
            }

            /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zzb$zzb>] */
            /* access modifiers changed from: protected */
            /* JADX WARNING: Unknown variable types count: 1 */
            @Override // com.google.android.gms.internal.vision.zzgs
            public final Object zza(int i, Object obj, Object obj2) {
                zzil<C0019zzb> zzil;
                switch (zzby.zzbe[i - 1]) {
                    case 1:
                        return new C0019zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzjf, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ခ\u0000\u0002ခ\u0001\u0003ခ\u0002", new Object[]{"zzbf", "zzjc", "zzjd", "zzje"});
                    case 4:
                        return zzjf;
                    case 5:
                        zzil<C0019zzb> zzil2 = zzbd;
                        zzil<C0019zzb> zzil3 = zzil2;
                        if (zzil2 == null) {
                            synchronized (C0019zzb.class) {
                                zzil<C0019zzb> zzil4 = zzbd;
                                zzil = zzil4;
                                if (zzil4 == null) {
                                    ?? zzc = new zzgs.zzc(zzjf);
                                    zzbd = zzc;
                                    zzil = zzc;
                                }
                            }
                            zzil3 = zzil;
                        }
                        return zzil3;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                C0019zzb zzb = new C0019zzb();
                zzjf = zzb;
                zzgs.zza(C0019zzb.class, zzb);
            }
        }

        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public enum zzc implements zzgw {
            CONTOUR_UNKNOWN(0),
            FACE_OVAL(1),
            LEFT_EYEBROW_TOP(2),
            LEFT_EYEBROW_BOTTOM(3),
            RIGHT_EYEBROW_TOP(4),
            RIGHT_EYEBROW_BOTTOM(5),
            LEFT_EYE(6),
            RIGHT_EYE(7),
            UPPER_LIP_TOP(8),
            UPPER_LIP_BOTTOM(9),
            LOWER_LIP_TOP(10),
            LOWER_LIP_BOTTOM(11),
            NOSE_BRIDGE(12),
            NOSE_BOTTOM(13),
            LEFT_CHEEK_CENTER(14),
            RIGHT_CHEEK_CENTER(15);
            
            private static final zzgv<zzc> zzhc = new zzcc();
            private final int value;

            @Override // com.google.android.gms.internal.vision.zzgw
            public final int zzag() {
                return this.value;
            }

            public static zzc zzp(int i) {
                switch (i) {
                    case 0:
                        return CONTOUR_UNKNOWN;
                    case 1:
                        return FACE_OVAL;
                    case 2:
                        return LEFT_EYEBROW_TOP;
                    case 3:
                        return LEFT_EYEBROW_BOTTOM;
                    case 4:
                        return RIGHT_EYEBROW_TOP;
                    case 5:
                        return RIGHT_EYEBROW_BOTTOM;
                    case 6:
                        return LEFT_EYE;
                    case 7:
                        return RIGHT_EYE;
                    case 8:
                        return UPPER_LIP_TOP;
                    case 9:
                        return UPPER_LIP_BOTTOM;
                    case 10:
                        return LOWER_LIP_TOP;
                    case 11:
                        return LOWER_LIP_BOTTOM;
                    case 12:
                        return NOSE_BRIDGE;
                    case 13:
                        return NOSE_BOTTOM;
                    case 14:
                        return LEFT_CHEEK_CENTER;
                    case 15:
                        return RIGHT_CHEEK_CENTER;
                    default:
                        return null;
                }
            }

            public static zzgy zzah() {
                return zzcd.zzhf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzc(int i) {
                this.value = i;
            }
        }

        private zzb() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class zza extends zzgs.zza<zzb, zza> implements zzie {
            private zza() {
                super(zzb.zzjb);
            }

            /* synthetic */ zza(zzby zzby) {
                this();
            }
        }

        public final zzc zzbf() {
            zzc zzp = zzc.zzp(this.zziz);
            return zzp == null ? zzc.CONTOUR_UNKNOWN : zzp;
        }

        public final List<C0019zzb> zzbg() {
            return this.zzja;
        }

        public final int zzbh() {
            return this.zzja.size();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zzb>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzb> zzil;
            switch (zzby.zzbe[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzjb, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001ဌ\u0000\u0002\u001b", new Object[]{"zzbf", "zziz", zzc.zzah(), "zzja", C0019zzb.class});
                case 4:
                    return zzjb;
                case 5:
                    zzil<zzb> zzil2 = zzbd;
                    zzil<zzb> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzb.class) {
                            zzil<zzb> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc2 = new zzgs.zzc(zzjb);
                                zzbd = zzc2;
                                zzil = zzc2;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public static zzb zzbi() {
            return zzjb;
        }

        static {
            zzb zzb = new zzb();
            zzjb = zzb;
            zzgs.zza(zzb.class, zzb);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zzc extends zzgs<zzc, zza> implements zzie {
        private static volatile zzil<zzc> zzbd;
        private static final zzc zzjz;
        private int zzbf;
        private zzkn zzjx;
        private byte zzjy = 2;

        private zzc() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class zza extends zzgs.zza<zzc, zza> implements zzie {
            private zza() {
                super(zzc.zzjz);
            }

            /* synthetic */ zza(zzby zzby) {
                this();
            }
        }

        public final zzkn zzbl() {
            zzkn zzkn = this.zzjx;
            return zzkn == null ? zzkn.zzjm() : zzkn;
        }

        public static zzc zza(byte[] bArr, zzgd zzgd) throws zzhc {
            return (zzc) zzgs.zza(zzjz, bArr, zzgd);
        }

        /* JADX WARN: Type inference failed for: r3v14, types: [com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zzc>, com.google.android.gms.internal.vision.zzgs$zzc] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzc> zzil;
            int i2 = 1;
            switch (zzby.zzbe[i - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzjz, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0001\u0001ᐉ\u0000", new Object[]{"zzbf", "zzjx"});
                case 4:
                    return zzjz;
                case 5:
                    zzil<zzc> zzil2 = zzbd;
                    zzil<zzc> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzc.class) {
                            zzil<zzc> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzjz);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return Byte.valueOf(this.zzjy);
                case 7:
                    if (obj == null) {
                        i2 = 0;
                    }
                    this.zzjy = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzc zzc = new zzc();
            zzjz = zzc;
            zzgs.zza(zzc.class, zzc);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zza extends zzgs<zza, C0018zza> implements zzie {
        private static volatile zzil<zza> zzbd;
        private static final zza zziu;
        private int zzbf;
        private zzg zzis;
        private zzg zzit;

        private zza() {
        }

        /* renamed from: com.google.android.gms.internal.vision.zzbx$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class C0018zza extends zzgs.zza<zza, C0018zza> implements zzie {
            private C0018zza() {
                super(zza.zziu);
            }

            public final C0018zza zzc(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zza) this.zzwh).zza((zza) zzg);
                return this;
            }

            public final C0018zza zzd(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zza) this.zzwh).zzb((zza) zzg);
                return this;
            }

            /* synthetic */ C0018zza(zzby zzby) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzg zzg) {
            zzg.getClass();
            this.zzis = zzg;
            this.zzbf |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzg zzg) {
            zzg.getClass();
            this.zzit = zzg;
            this.zzbf |= 2;
        }

        public static C0018zza zzbd() {
            return (C0018zza) zziu.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zza>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zza> zzil;
            switch (zzby.zzbe[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0018zza(null);
                case 3:
                    return zza(zziu, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001", new Object[]{"zzbf", "zzis", "zzit"});
                case 4:
                    return zziu;
                case 5:
                    zzil<zza> zzil2 = zzbd;
                    zzil<zza> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zza.class) {
                            zzil<zza> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zziu);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zza zza = new zza();
            zziu = zza;
            zzgs.zza(zza.class, zza);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zzd extends zzgs<zzd, zza> implements zzie {
        private static volatile zzil<zzd> zzbd;
        private static final zzd zzkm;
        private int zzbf;
        private float zzka = 0.1f;
        private int zzkb = 1;
        private int zzkc = 1;
        private int zzkd = 1;
        private boolean zzke;
        private boolean zzkf;
        private float zzkg = 45.0f;
        private float zzkh = 0.5f;
        private boolean zzki;
        private zze zzkj;
        private zza zzkk;
        private zzf zzkl;

        private zzd() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class zza extends zzgs.zza<zzd, zza> implements zzie {
            private zza() {
                super(zzd.zzkm);
            }

            public final zza zze(float f) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zzd((zzd) f);
                return this;
            }

            public final zza zzb(zzce zzce) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) zzce);
                return this;
            }

            public final zza zzb(zzbz zzbz) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) zzbz);
                return this;
            }

            public final zza zzb(zzci zzci) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) zzci);
                return this;
            }

            public final zza zzd(boolean z) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) z);
                return this;
            }

            public final zza zze(boolean z) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zzb((zzd) z);
                return this;
            }

            public final zza zzf(boolean z) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zzc(true);
                return this;
            }

            public final zza zza(zze.zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) ((zze) ((zzgs) zza.zzgc())));
                return this;
            }

            public final zza zza(zza.C0018zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) ((zza) ((zzgs) zza.zzgc())));
                return this;
            }

            public final zza zza(zzf.zza zza) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzd) this.zzwh).zza((zzd) ((zzf) ((zzgs) zza.zzgc())));
                return this;
            }

            /* synthetic */ zza(zzby zzby) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzd(float f) {
            this.zzbf |= 1;
            this.zzka = f;
        }

        public final zzce zzbn() {
            zzce zzq = zzce.zzq(this.zzkb);
            return zzq == null ? zzce.NO_LANDMARK : zzq;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzce zzce) {
            this.zzkb = zzce.zzag();
            this.zzbf |= 2;
        }

        public final zzbz zzbo() {
            zzbz zzo = zzbz.zzo(this.zzkc);
            return zzo == null ? zzbz.NO_CLASSIFICATION : zzo;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzbz zzbz) {
            this.zzkc = zzbz.zzag();
            this.zzbf |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzci zzci) {
            this.zzkd = zzci.zzag();
            this.zzbf |= 8;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(boolean z) {
            this.zzbf |= 16;
            this.zzke = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(boolean z) {
            this.zzbf |= 32;
            this.zzkf = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzc(boolean z) {
            this.zzbf |= 256;
            this.zzki = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zze zze) {
            zze.getClass();
            this.zzkj = zze;
            this.zzbf |= 512;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zza zza2) {
            zza2.getClass();
            this.zzkk = zza2;
            this.zzbf |= 1024;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzf zzf) {
            zzf.getClass();
            this.zzkl = zzf;
            this.zzbf |= 2048;
        }

        public static zza zzbp() {
            return (zza) zzkm.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zzd>, com.google.android.gms.internal.vision.zzgs$zzc] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzd> zzil;
            switch (zzby.zzbe[i - 1]) {
                case 1:
                    return new zzd();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzkm, "\u0001\f\u0000\u0001\u0001\f\f\u0000\u0000\u0000\u0001ခ\u0000\u0002ဌ\u0001\u0003ဌ\u0002\u0004ဌ\u0003\u0005ဇ\u0004\u0006ဇ\u0005\u0007ခ\u0006\bခ\u0007\tဇ\b\nဉ\t\u000bဉ\n\fဉ\u000b", new Object[]{"zzbf", "zzka", "zzkb", zzce.zzah(), "zzkc", zzbz.zzah(), "zzkd", zzci.zzah(), "zzke", "zzkf", "zzkg", "zzkh", "zzki", "zzkj", "zzkk", "zzkl"});
                case 4:
                    return zzkm;
                case 5:
                    zzil<zzd> zzil2 = zzbd;
                    zzil<zzd> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzd.class) {
                            zzil<zzd> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzkm);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzd zzd = new zzd();
            zzkm = zzd;
            zzgs.zza(zzd.class, zzd);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zze extends zzgs<zze, zza> implements zzie {
        private static volatile zzil<zze> zzbd;
        private static final zze zzkq;
        private int zzbf;
        private zzg zzkn;
        private zzg zzko;
        private zzg zzkp;

        private zze() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class zza extends zzgs.zza<zze, zza> implements zzie {
            private zza() {
                super(zze.zzkq);
            }

            public final zza zzh(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zze) this.zzwh).zze(zzg);
                return this;
            }

            public final zza zzi(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zze) this.zzwh).zzf(zzg);
                return this;
            }

            public final zza zzj(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zze) this.zzwh).zzg(zzg);
                return this;
            }

            /* synthetic */ zza(zzby zzby) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zze(zzg zzg) {
            zzg.getClass();
            this.zzkn = zzg;
            this.zzbf |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzf(zzg zzg) {
            zzg.getClass();
            this.zzko = zzg;
            this.zzbf |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzg(zzg zzg) {
            zzg.getClass();
            this.zzkp = zzg;
            this.zzbf |= 4;
        }

        public static zza zzbr() {
            return (zza) zzkq.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zze>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zze> zzil;
            switch (zzby.zzbe[i - 1]) {
                case 1:
                    return new zze();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzkq, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzbf", "zzkn", "zzko", "zzkp"});
                case 4:
                    return zzkq;
                case 5:
                    zzil<zze> zzil2 = zzbd;
                    zzil<zze> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zze.class) {
                            zzil<zze> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzkq);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zze zze = new zze();
            zzkq = zze;
            zzgs.zza(zze.class, zze);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zzf extends zzgs<zzf, zza> implements zzie {
        private static volatile zzil<zzf> zzbd;
        private static final zzf zzla;
        private int zzbf;
        private zzg zzkw;
        private zzg zzkx;
        private zzg zzky;
        private zzg zzkz;

        private zzf() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class zza extends zzgs.zza<zzf, zza> implements zzie {
            private zza() {
                super(zzf.zzla);
            }

            public final zza zzk(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzo(zzg);
                return this;
            }

            public final zza zzl(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzp(zzg);
                return this;
            }

            public final zza zzm(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzq(zzg);
                return this;
            }

            public final zza zzn(zzg zzg) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzf) this.zzwh).zzr(zzg);
                return this;
            }

            /* synthetic */ zza(zzby zzby) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzo(zzg zzg) {
            zzg.getClass();
            this.zzkw = zzg;
            this.zzbf |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzp(zzg zzg) {
            zzg.getClass();
            this.zzkx = zzg;
            this.zzbf |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzq(zzg zzg) {
            zzg.getClass();
            this.zzky = zzg;
            this.zzbf |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzr(zzg zzg) {
            zzg.getClass();
            this.zzkz = zzg;
            this.zzbf |= 8;
        }

        public static zza zzbt() {
            return (zza) zzla.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zzf>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzf> zzil;
            switch (zzby.zzbe[i - 1]) {
                case 1:
                    return new zzf();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzla, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဉ\u0003", new Object[]{"zzbf", "zzkw", "zzkx", "zzky", "zzkz"});
                case 4:
                    return zzla;
                case 5:
                    zzil<zzf> zzil2 = zzbd;
                    zzil<zzf> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzf.class) {
                            zzil<zzf> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzla);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzf zzf = new zzf();
            zzla = zzf;
            zzgs.zza(zzf.class, zzf);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
    public static final class zzg extends zzgs<zzg, zza> implements zzie {
        private static volatile zzil<zzg> zzbd;
        private static final zzg zzli;
        private int zzbf;
        private zzfh zzlg = zzfh.zzsd;
        private String zzlh = "";

        private zzg() {
        }

        /* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
        public static final class zza extends zzgs.zza<zzg, zza> implements zzie {
            private zza() {
                super(zzg.zzli);
            }

            public final zza zzj(String str) {
                if (this.zzwi) {
                    zzfy();
                    this.zzwi = false;
                }
                ((zzg) this.zzwh).zzi(str);
                return this;
            }

            /* synthetic */ zza(zzby zzby) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzi(String str) {
            str.getClass();
            this.zzbf |= 2;
            this.zzlh = str;
        }

        public static zza zzbv() {
            return (zza) zzli.zzge();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzgs$zzc, com.google.android.gms.internal.vision.zzil<com.google.android.gms.internal.vision.zzbx$zzg>] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.vision.zzgs
        public final Object zza(int i, Object obj, Object obj2) {
            zzil<zzg> zzil;
            switch (zzby.zzbe[i - 1]) {
                case 1:
                    return new zzg();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzli, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ည\u0000\u0002ဈ\u0001", new Object[]{"zzbf", "zzlg", "zzlh"});
                case 4:
                    return zzli;
                case 5:
                    zzil<zzg> zzil2 = zzbd;
                    zzil<zzg> zzil3 = zzil2;
                    if (zzil2 == null) {
                        synchronized (zzg.class) {
                            zzil<zzg> zzil4 = zzbd;
                            zzil = zzil4;
                            if (zzil4 == null) {
                                ?? zzc = new zzgs.zzc(zzli);
                                zzbd = zzc;
                                zzil = zzc;
                            }
                        }
                        zzil3 = zzil;
                    }
                    return zzil3;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzg zzg = new zzg();
            zzli = zzg;
            zzgs.zza(zzg.class, zzg);
        }
    }
}
