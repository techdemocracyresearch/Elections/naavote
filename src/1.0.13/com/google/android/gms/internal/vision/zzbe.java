package com.google.android.gms.internal.vision;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nullable;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public abstract class zzbe<T> {
    private static Context zze = null;
    private static final Object zzfz = new Object();
    private static boolean zzga = false;
    private static zzcu<zzcn<zzba>> zzgb;
    private static final AtomicInteger zzge = new AtomicInteger();
    private final String name;
    private final zzbk zzgc;
    private final T zzgd;
    private volatile int zzgf;
    private volatile T zzgg;

    public static void init(Context context) {
        synchronized (zzfz) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (zze != context) {
                zzaq.zzx();
                zzbj.zzx();
                zzav.zzaa();
                zzge.incrementAndGet();
                zze = context;
                zzgb = zzcx.zza(zzbd.zzfy);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public abstract T zza(Object obj);

    public static void maybeInit(Context context) {
        synchronized (zzfz) {
            if (zze == null) {
                init(context);
            }
        }
    }

    static void zzab() {
        zzge.incrementAndGet();
    }

    private zzbe(zzbk zzbk, String str, T t) {
        this.zzgf = -1;
        if (zzbk.zzgl == null && zzbk.zzgm == null) {
            throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
        } else if (zzbk.zzgl == null || zzbk.zzgm == null) {
            this.zzgc = zzbk;
            this.name = str;
            this.zzgd = t;
        } else {
            throw new IllegalArgumentException("Must pass one of SharedPreferences file name or ContentProvider URI");
        }
    }

    private final String zze(String str) {
        if (str != null && str.isEmpty()) {
            return this.name;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.name);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    public final String zzac() {
        return zze(this.zzgc.zzgo);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045  */
    public final T get() {
        T t;
        zzcn<zzba> zzcn;
        int i = zzge.get();
        if (this.zzgf < i) {
            synchronized (this) {
                if (this.zzgf < i) {
                    if (zze != null) {
                        if (this.zzgc.zzgq) {
                            t = zzae();
                            if (t == null) {
                                t = zzad();
                                if (t != null) {
                                }
                            }
                            zzcn = zzgb.get();
                            if (zzcn.isPresent()) {
                                String zza = zzcn.get().zza(this.zzgc.zzgm, this.zzgc.zzgl, this.zzgc.zzgo, this.name);
                                t = zza == null ? this.zzgd : zza(zza);
                            }
                            this.zzgg = t;
                            this.zzgf = i;
                        } else {
                            t = zzad();
                            if (t == null) {
                                t = zzae();
                                if (t != null) {
                                }
                            }
                            zzcn = zzgb.get();
                            if (zzcn.isPresent()) {
                            }
                            this.zzgg = t;
                            this.zzgf = i;
                        }
                        t = this.zzgd;
                        zzcn = zzgb.get();
                        if (zzcn.isPresent()) {
                        }
                        this.zzgg = t;
                        this.zzgf = i;
                    } else {
                        throw new IllegalStateException("Must call PhenotypeFlag.init() first");
                    }
                }
            }
        }
        return this.zzgg;
    }

    @Nullable
    private final T zzad() {
        zzau zzau;
        Object zzb;
        boolean z = false;
        if (!this.zzgc.zzgr) {
            String str = (String) zzav.zze(zze).zzb("gms:phenotype:phenotype_flag:debug_bypass_phenotype");
            if (str != null && zzal.zzev.matcher(str).matches()) {
                z = true;
            }
        }
        if (!z) {
            if (this.zzgc.zzgm == null) {
                zzau = zzbj.zzb(zze, this.zzgc.zzgl);
            } else if (!zzbc.zza(zze, this.zzgc.zzgm)) {
                zzau = null;
            } else if (this.zzgc.zzgs) {
                ContentResolver contentResolver = zze.getContentResolver();
                String lastPathSegment = this.zzgc.zzgm.getLastPathSegment();
                String packageName = zze.getPackageName();
                StringBuilder sb = new StringBuilder(String.valueOf(lastPathSegment).length() + 1 + String.valueOf(packageName).length());
                sb.append(lastPathSegment);
                sb.append("#");
                sb.append(packageName);
                zzau = zzaq.zza(contentResolver, zzbb.getContentProviderUri(sb.toString()));
            } else {
                zzau = zzaq.zza(zze.getContentResolver(), this.zzgc.zzgm);
            }
            if (!(zzau == null || (zzb = zzau.zzb(zzac())) == null)) {
                return zza(zzb);
            }
        } else if (Log.isLoggable("PhenotypeFlag", 3)) {
            String valueOf = String.valueOf(zzac());
            Log.d("PhenotypeFlag", valueOf.length() != 0 ? "Bypass reading Phenotype values for flag: ".concat(valueOf) : new String("Bypass reading Phenotype values for flag: "));
        }
        return null;
    }

    @Nullable
    private final T zzae() {
        if (!this.zzgc.zzgp && (this.zzgc.zzgt == null || this.zzgc.zzgt.apply(zze).booleanValue())) {
            Object zzb = zzav.zze(zze).zzb(this.zzgc.zzgp ? null : zze(this.zzgc.zzgn));
            if (zzb != null) {
                return zza(zzb);
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static zzbe<Long> zza(zzbk zzbk, String str, long j) {
        return new zzbg(zzbk, str, Long.valueOf(j));
    }

    /* access modifiers changed from: private */
    public static zzbe<Boolean> zza(zzbk zzbk, String str, boolean z) {
        return new zzbf(zzbk, str, Boolean.valueOf(z));
    }

    /* access modifiers changed from: private */
    public static <T> zzbe<T> zza(zzbk zzbk, String str, T t, zzbh<T> zzbh) {
        return new zzbi(zzbk, str, t, zzbh);
    }

    static final /* synthetic */ zzcn zzaf() {
        new zzaz();
        return zzaz.zzf(zze);
    }

    /* synthetic */ zzbe(zzbk zzbk, String str, Object obj, zzbg zzbg) {
        this(zzbk, str, obj);
    }
}
