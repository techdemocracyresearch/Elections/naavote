package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final /* synthetic */ class zzap implements zzaw {
    private final zzaq zzfj;

    zzap(zzaq zzaq) {
        this.zzfj = zzaq;
    }

    @Override // com.google.android.gms.internal.vision.zzaw
    public final Object zzt() {
        return this.zzfj.zzy();
    }
}
