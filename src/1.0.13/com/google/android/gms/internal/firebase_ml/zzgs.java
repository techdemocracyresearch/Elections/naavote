package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzgs implements zzgw {
    @Override // com.google.android.gms.internal.firebase_ml.zzgw
    public final String getName() {
        return "gzip";
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgw
    public final void zza(zzjq zzjq, OutputStream outputStream) throws IOException {
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(new zzgr(this, outputStream));
        zzjq.writeTo(gZIPOutputStream);
        gZIPOutputStream.close();
    }
}
