package com.google.android.gms.internal.firebase_ml;

import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public enum zzabb implements zzxc {
    HANG_DETECTION_DEFAULT(0),
    HANG_DETECTION_NONE(1),
    HANG_DETECTION_LOG_ONLY(2),
    HANG_DETECTION_CRASH_PROCESS(3),
    HANG_DETECTION_ABANDON_THREAD(4);
    
    private static final zzxf<zzabb> zzac = new zzabd();
    private final int value;

    @Override // com.google.android.gms.internal.firebase_ml.zzxc
    public final int zzd() {
        return this.value;
    }

    public static zzabb zzei(int i) {
        if (i == 0) {
            return HANG_DETECTION_DEFAULT;
        }
        if (i == 1) {
            return HANG_DETECTION_NONE;
        }
        if (i == 2) {
            return HANG_DETECTION_LOG_ONLY;
        }
        if (i == 3) {
            return HANG_DETECTION_CRASH_PROCESS;
        }
        if (i != 4) {
            return null;
        }
        return HANG_DETECTION_ABANDON_THREAD;
    }

    public static zzxe zzf() {
        return zzabc.zzan;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
    }

    private zzabb(int i) {
        this.value = i;
    }
}
