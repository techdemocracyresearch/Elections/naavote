package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.auth.api.AuthProxy;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.util.AndroidUtilsLight;
import com.google.android.gms.common.util.Hex;
import com.google.android.gms.internal.firebase_ml.zzgd;
import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.common.FirebaseMLException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzrt implements zzpu<zzkl, zzrs> {
    private static final Map<FirebaseApp, zzrt> zzbim = new HashMap();
    private static final GmsLogger zzbin = new GmsLogger("ImageAnnotatorTask", "");
    private static final zzhh zzbrf = new zzht();
    private static final zzhx zzbrg = zzig.zzht();
    private final String zzaar;
    private String zzbrh;
    private final boolean zzbri;
    private final GoogleApiClient zzbrj;
    private final zzkj zzbrk;

    public static synchronized zzrt zza(FirebaseApp firebaseApp, boolean z, GoogleApiClient googleApiClient) {
        zzrt zzrt;
        synchronized (zzrt.class) {
            Map<FirebaseApp, zzrt> map = zzbim;
            zzrt = map.get(firebaseApp);
            if (zzrt == null) {
                zzrt = new zzrt(firebaseApp, z, googleApiClient);
                map.put(firebaseApp, zzrt);
            }
        }
        return zzrt;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return null;
    }

    private zzrt(FirebaseApp firebaseApp, boolean z, GoogleApiClient googleApiClient) {
        this.zzbri = z;
        if (z) {
            this.zzbrj = googleApiClient;
            this.zzbrk = new zzrw(this);
        } else {
            this.zzbrj = null;
            this.zzbrk = new zzrv(this, zza(firebaseApp), firebaseApp);
        }
        this.zzaar = String.format("FirebaseML_%s", firebaseApp.getName());
    }

    private static String zza(FirebaseApp firebaseApp) {
        Bundle bundle;
        String apiKey = firebaseApp.getOptions().getApiKey();
        Context applicationContext = firebaseApp.getApplicationContext();
        try {
            ApplicationInfo applicationInfo = applicationContext.getPackageManager().getApplicationInfo(applicationContext.getPackageName(), 128);
            if (applicationInfo == null || (bundle = applicationInfo.metaData) == null) {
                return apiKey;
            }
            String string = bundle.getString("com.firebase.ml.cloud.ApiKeyForDebug");
            return !TextUtils.isEmpty(string) ? string : apiKey;
        } catch (PackageManager.NameNotFoundException e) {
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(applicationContext.getPackageName());
            gmsLogger.e("ImageAnnotatorTask", valueOf.length() != 0 ? "No such package: ".concat(valueOf) : new String("No such package: "), e);
            return apiKey;
        }
    }

    /* access modifiers changed from: private */
    public static String zzb(Context context, String str) {
        try {
            byte[] packageCertificateHashBytes = AndroidUtilsLight.getPackageCertificateHashBytes(context, str);
            if (packageCertificateHashBytes != null) {
                return Hex.bytesToStringUppercase(packageCertificateHashBytes, false);
            }
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(str);
            gmsLogger.e("ImageAnnotatorTask", valueOf.length() != 0 ? "Could not get fingerprint hash: ".concat(valueOf) : new String("Could not get fingerprint hash: "));
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            GmsLogger gmsLogger2 = zzbin;
            String valueOf2 = String.valueOf(str);
            gmsLogger2.e("ImageAnnotatorTask", valueOf2.length() != 0 ? "No such package: ".concat(valueOf2) : new String("No such package: "), e);
            return null;
        }
    }

    private final String getSpatulaHeader() throws FirebaseMLException {
        if (!this.zzbrj.isConnected()) {
            this.zzbrj.blockingConnect(3, TimeUnit.SECONDS);
        }
        try {
            return AuthProxy.ProxyApi.getSpatulaHeader(this.zzbrj).await(3, TimeUnit.SECONDS).getSpatulaHeader();
        } catch (SecurityException unused) {
            return null;
        }
    }

    public final zzkl zza(zzrs zzrs) throws FirebaseMLException {
        zzkk zzc = new zzkk().zzc(Collections.singletonList(new zzki().zzb(zzrs.features).zza(new zzkx().zze(zzrs.zzbrd)).zza(zzrs.imageContext)));
        int i = 14;
        try {
            zzkc zzih = ((zzkf) new zzkf(zzbrf, zzbrg, new zzrx(this)).zzm(this.zzaar)).zza(this.zzbrk).zzih();
            if (this.zzbri) {
                String spatulaHeader = getSpatulaHeader();
                this.zzbrh = spatulaHeader;
                if (TextUtils.isEmpty(spatulaHeader)) {
                    zzbin.e("ImageAnnotatorTask", "Failed to contact Google Play services for certificate fingerprint matching. Please ensure you have latest Google Play services installed");
                    throw new FirebaseMLException("Failed to contact Google Play services for certificate fingerprint matching. Please ensure you have latest Google Play services installed", 14);
                }
            }
            List<zzkl> zzin = ((zzkn) new zzke(zzih).zza(zzc).zzfm()).zzin();
            if (!(zzin == null || zzin.isEmpty())) {
                return zzin.get(0);
            }
            throw new FirebaseMLException("Empty response from cloud vision api.", 13);
        } catch (zzgf e) {
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(e.zzfj());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 44);
            sb.append("batchAnnotateImages call failed with error: ");
            sb.append(valueOf);
            gmsLogger.e("ImageAnnotatorTask", sb.toString());
            if (this.zzbri) {
                Log.d("ImageAnnotatorTask", "If you are developing / testing on a simulator, either register your development app on Firebase console or turn off enforceCertFingerprintMatch()");
            }
            String message = e.getMessage();
            if (e.getStatusCode() != 400) {
                if (e.zzfj() != null && e.zzfj().zzfe() != null) {
                    i = 13;
                    for (zzgd.zza zza : e.zzfj().zzfe()) {
                        String reason = zza.getReason();
                        if (reason != null) {
                            if (reason.equals("rateLimitExceeded") || reason.equals("dailyLimitExceeded") || reason.equals("userRateLimitExceeded")) {
                                i = 8;
                                continue;
                            } else {
                                if (!reason.equals("accessNotConfigured")) {
                                    if (reason.equals("forbidden") || reason.equals("inactiveBillingState")) {
                                        message = String.format("If you haven't set up billing, please go to Firebase console to set up billing: %s. If you are specifying a debug Api Key override and turned on Api Key restrictions, make sure the restrictions are set up correctly", "https://console.firebase.google.com/u/0/project/_/overview?purchaseBillingPlan=true");
                                    }
                                }
                                i = 7;
                                continue;
                            }
                        }
                        if (i != 13) {
                            break;
                        }
                    }
                } else {
                    i = 13;
                }
            }
            throw new FirebaseMLException(message, i);
        } catch (IOException e2) {
            zzbin.e("ImageAnnotatorTask", "batchAnnotateImages call failed with exception: ", e2);
            throw new FirebaseMLException("Cloud Vision batchAnnotateImages call failure", 13, e2);
        }
    }
}
