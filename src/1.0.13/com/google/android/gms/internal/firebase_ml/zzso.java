package com.google.android.gms.internal.firebase_ml;

import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.vision.Frame;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.internal.modeldownload.zzn;
import com.google.firebase.ml.common.internal.modeldownload.zzt;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLLocalModel;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLRemoteModel;
import com.google.firebase.ml.vision.automl.internal.IOnDeviceAutoMLImageLabeler;
import com.google.firebase.ml.vision.automl.internal.OnDeviceAutoMLImageLabelerOptionsParcel;
import com.google.firebase.ml.vision.automl.internal.zzg;
import com.google.firebase.ml.vision.automl.internal.zzj;
import com.google.firebase.ml.vision.automl.internal.zzl;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzso implements zzpu<List<FirebaseVisionImageLabel>, zzsf>, zzqp {
    private static final AtomicBoolean zzbob = new AtomicBoolean(true);
    private final zzqf zzbkb;
    private final zzqg zzbmd;
    private final AtomicBoolean zzboe = new AtomicBoolean(false);
    private final FirebaseAutoMLLocalModel zzboy;
    private final FirebaseAutoMLRemoteModel zzbpo;
    private final FirebaseVisionOnDeviceAutoMLImageLabelerOptions zzbtz;
    private IOnDeviceAutoMLImageLabeler zzbua;

    zzso(zzqf zzqf, FirebaseVisionOnDeviceAutoMLImageLabelerOptions firebaseVisionOnDeviceAutoMLImageLabelerOptions) {
        this.zzbkb = zzqf;
        this.zzbtz = firebaseVisionOnDeviceAutoMLImageLabelerOptions;
        this.zzbmd = zzqg.zza(zzqf, 5);
        this.zzbpo = firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqu();
        this.zzboy = firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqt();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return this;
    }

    /* access modifiers changed from: private */
    public final synchronized List<FirebaseVisionImageLabel> zza(zzsf zzsf) throws FirebaseMLException {
        Preconditions.checkNotNull(zzsf, "Mobile vision input can not be null");
        Preconditions.checkNotNull(zzsf.zzbrv, "Input frame can not be null");
        boolean z = this.zzboe.get();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.zzbua == null) {
            zza(zzoc.UNKNOWN_ERROR, elapsedRealtime, z, zzsf);
            Log.e("ODAutoMLImgLabelerTask", "On device AutoML Image Labeler is not initialized.");
            throw new FirebaseMLException("Image labeler not initialized.", 13);
        } else if (zzsf.zzbrv.getBitmap() != null) {
            try {
                IObjectWrapper wrap = ObjectWrapper.wrap(zzsf.zzbrv.getBitmap());
                IOnDeviceAutoMLImageLabeler iOnDeviceAutoMLImageLabeler = this.zzbua;
                Frame.Metadata metadata = zzsf.zzbrv.getMetadata();
                zzl[] zza = iOnDeviceAutoMLImageLabeler.zza(wrap, new zzsb(metadata.getWidth(), metadata.getHeight(), metadata.getId(), metadata.getTimestampMillis(), metadata.getRotation()));
                zza(zzoc.NO_ERROR, elapsedRealtime, z, zzsf);
                if (zza == null) {
                    return new ArrayList();
                }
                ArrayList arrayList = new ArrayList();
                for (zzl zzl : zza) {
                    arrayList.add(FirebaseVisionImageLabel.zza(zzl));
                }
                zzbob.set(false);
                return arrayList;
            } catch (RemoteException e) {
                zza(zzoc.UNKNOWN_ERROR, elapsedRealtime, z, zzsf);
                Log.e("ODAutoMLImgLabelerTask", "Error calling on device auto ml inference.", e);
                throw new FirebaseMLException("Cannot run on device automl image labeler.", 13, e);
            }
        } else {
            Log.e("ODAutoMLImgLabelerTask", "No image data found.");
            throw new FirebaseMLException("No image data found.", 3);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void zzol() throws FirebaseMLException {
        String str;
        try {
            if (this.zzbua == null) {
                zzg asInterface = zzj.asInterface(DynamiteModule.load(this.zzbkb.getApplicationContext(), DynamiteModule.PREFER_LOCAL, "com.google.firebase.ml.vision.dynamite.automl").instantiate("com.google.firebase.ml.vision.automl.OnDeviceAutoMLImageLabelerCreator"));
                if (asInterface != null) {
                    IObjectWrapper wrap = ObjectWrapper.wrap(this.zzbkb);
                    FirebaseVisionOnDeviceAutoMLImageLabelerOptions firebaseVisionOnDeviceAutoMLImageLabelerOptions = this.zzbtz;
                    String str2 = null;
                    String modelName = firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqu() != null ? firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqu().getModelName() : null;
                    if (firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqt() != null) {
                        if (firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqt().getAssetFilePath() != null) {
                            str = null;
                            str2 = firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqt().getAssetFilePath();
                        } else if (firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqt().getFilePath() != null) {
                            str = firebaseVisionOnDeviceAutoMLImageLabelerOptions.zzqt().getFilePath();
                        }
                        this.zzbua = asInterface.newOnDeviceAutoMLImageLabeler(wrap, new OnDeviceAutoMLImageLabelerOptionsParcel(firebaseVisionOnDeviceAutoMLImageLabelerOptions.getConfidenceThreshold(), modelName, str2, str));
                    }
                    str = null;
                    this.zzbua = asInterface.newOnDeviceAutoMLImageLabeler(wrap, new OnDeviceAutoMLImageLabelerOptionsParcel(firebaseVisionOnDeviceAutoMLImageLabelerOptions.getConfidenceThreshold(), modelName, str2, str));
                } else {
                    Log.e("ODAutoMLImgLabelerTask", "Error when creating on device AutoML Image Labeler creator.");
                    throw new FirebaseMLException("Can not create on device AutoML Image Labeler.", 14);
                }
            }
            this.zzbua.zzol();
            this.zzboe.set(this.zzbua.zzou());
        } catch (DynamiteModule.LoadingException e) {
            Log.e("ODAutoMLImgLabelerTask", "Error when loading automl module.", e);
            throw new FirebaseMLException("Cannot load automl module. Please add dependency firebase-ml-vision-automl.", 14);
        } catch (RemoteException e2) {
            Log.e("ODAutoMLImgLabelerTask", "Error when creating on device AutoML Image Labeler.", e2);
            throw new FirebaseMLException("Can not create on device AutoML Image Labeler.", 14, e2);
        } catch (RemoteException e3) {
            Log.e("ODAutoMLImgLabelerTask", "Error while loading the AutoML image labeling model.", e3);
            throw new FirebaseMLException("Cannot load the AutoML image labeling model.", 14, e3);
        } catch (Throwable th) {
            throw th;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final void release() {
        try {
            IOnDeviceAutoMLImageLabeler iOnDeviceAutoMLImageLabeler = this.zzbua;
            if (iOnDeviceAutoMLImageLabeler != null) {
                iOnDeviceAutoMLImageLabeler.close();
            }
            zzbob.set(true);
        } catch (RemoteException e) {
            Log.e("ODAutoMLImgLabelerTask", "Error closing on device AutoML Image Labeler", e);
        }
    }

    private final void zza(zzoc zzoc, long j, boolean z, zzsf zzsf) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        this.zzbmd.zza(new zzsr(this, elapsedRealtime, zzoc, zzsf, z), zzod.AUTOML_IMAGE_LABELING_RUN);
        zzqm zzqm = zzsq.zzboj;
        this.zzbmd.zza((zzns.zza.zzb) ((zzwz) zzns.zza.zzb.zzjx().zza(zzoc).zzm(zzbob.get()).zza(zzsa.zzc(zzsf)).zzvb()), elapsedRealtime, zzod.AGGREGATED_AUTO_ML_IMAGE_LABELING_INFERENCE, zzqm);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzns.zzad.zza zza(long j, zzoc zzoc, zzsf zzsf, boolean z) {
        FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel;
        zzns.zzk.zza zzh = zzns.zzk.zzkx().zza(zzns.zzaf.zzme().zzk(j).zzk(zzoc).zzah(zzbob.get()).zzai(true).zzaj(true)).zzh(zzsa.zzc(zzsf));
        if (!z || (firebaseAutoMLRemoteModel = this.zzbpo) == null) {
            FirebaseAutoMLLocalModel firebaseAutoMLLocalModel = this.zzboy;
            if (firebaseAutoMLLocalModel != null) {
                zzh.zza(firebaseAutoMLLocalModel.zza(zzn.AUTOML));
            }
        } else {
            zzh.zza(zzt.zza(firebaseAutoMLRemoteModel, zzn.AUTOML));
        }
        return zzns.zzad.zzma().zza(zzh);
    }
}
