package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zztg {

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzb extends zzwz<zzb, C0017zzb> implements zzym {
        private static final zzb zzbwq;
        private static volatile zzyx<zzb> zzh;
        private float zzawi;
        private int zzbwm;
        private int zzbwn;
        private boolean zzbwo;
        private boolean zzbwp;
        private int zzj;
        private int zzov;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            CLASSIFICATION_UNKNOWN(0),
            CLASSIFICATION_NONE(1),
            CLASSIFICATION_ALL(2);
            
            private static final zzxf<zza> zzac = new zztj();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzce(int i) {
                if (i == 0) {
                    return CLASSIFICATION_UNKNOWN;
                }
                if (i == 1) {
                    return CLASSIFICATION_NONE;
                }
                if (i != 2) {
                    return null;
                }
                return CLASSIFICATION_ALL;
            }

            public static zzxe zzf() {
                return zzti.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzc implements zzxc {
            LANDMARK_UNKNOWN(0),
            LANDMARK_NONE(1),
            LANDMARK_ALL(2),
            LANDMARK_CONTOUR(3);
            
            private static final zzxf<zzc> zzac = new zztk();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzc zzcf(int i) {
                if (i == 0) {
                    return LANDMARK_UNKNOWN;
                }
                if (i == 1) {
                    return LANDMARK_NONE;
                }
                if (i == 2) {
                    return LANDMARK_ALL;
                }
                if (i != 3) {
                    return null;
                }
                return LANDMARK_CONTOUR;
            }

            public static zzxe zzf() {
                return zztl.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzc(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzd implements zzxc {
            MODE_UNKNOWN(0),
            MODE_ACCURATE(1),
            MODE_FAST(2),
            MODE_SELFIE(3);
            
            private static final zzxf<zzd> zzac = new zztn();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzd zzcg(int i) {
                if (i == 0) {
                    return MODE_UNKNOWN;
                }
                if (i == 1) {
                    return MODE_ACCURATE;
                }
                if (i == 2) {
                    return MODE_FAST;
                }
                if (i != 3) {
                    return null;
                }
                return MODE_SELFIE;
            }

            public static zzxe zzf() {
                return zztm.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzd(int i) {
                this.value = i;
            }
        }

        private zzb() {
        }

        /* renamed from: com.google.android.gms.internal.firebase_ml.zztg$zzb$zzb  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class C0017zzb extends zzwz.zzb<zzb, C0017zzb> implements zzym {
            private C0017zzb() {
                super(zzb.zzbwq);
            }

            /* synthetic */ C0017zzb(zztf zztf) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zztf.zzi[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new C0017zzb(null);
                case 3:
                    return zza(zzbwq, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဌ\u0002\u0004ဇ\u0003\u0005ဇ\u0004\u0006ခ\u0005", new Object[]{"zzj", "zzov", zzd.zzf(), "zzbwm", zzc.zzf(), "zzbwn", zza.zzf(), "zzbwo", "zzbwp", "zzawi"});
                case 4:
                    return zzbwq;
                case 5:
                    zzyx<zzb> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzb.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbwq);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzb zzb = new zzb();
            zzbwq = zzb;
            zzwz.zza(zzb.class, zzb);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zza extends zzwz<zza, C0016zza> implements zzym {
        private static final zzxj<Integer, zzvh> zzbwk = new zzth();
        private static final zza zzbwl;
        private static volatile zzyx<zza> zzh;
        private zzxg zzbwj = zzup();

        private zza() {
        }

        /* renamed from: com.google.android.gms.internal.firebase_ml.zztg$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class C0016zza extends zzwz.zzb<zza, C0016zza> implements zzym {
            private C0016zza() {
                super(zza.zzbwl);
            }

            public final C0016zza zzz(Iterable<? extends zzvh> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zza) this.zzclo).zzy(iterable);
                return this;
            }

            /* synthetic */ C0016zza(zztf zztf) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzy(Iterable<? extends zzvh> iterable) {
            if (!this.zzbwj.zztl()) {
                this.zzbwj = zzwz.zza(this.zzbwj);
            }
            for (zzvh zzvh : iterable) {
                this.zzbwj.zzds(zzvh.zzd());
            }
        }

        public static C0016zza zzrq() {
            return (C0016zza) zzbwl.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zztf.zzi[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0016zza(null);
                case 3:
                    return zza(zzbwl, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001e", new Object[]{"zzbwj", zzvh.zzf()});
                case 4:
                    return zzbwl;
                case 5:
                    zzyx<zza> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zza.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbwl);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zza zza = new zza();
            zzbwl = zza;
            zzwz.zza(zza.class, zza);
        }
    }
}
