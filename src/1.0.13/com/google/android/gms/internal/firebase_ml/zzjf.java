package com.google.android.gms.internal.firebase_ml;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzjf extends AbstractMap<String, Object> implements Cloneable {
    final zziv zzacg;
    Map<String, Object> zzahw;

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public enum zzb {
        IGNORE_CASE
    }

    public zzjf() {
        this(EnumSet.noneOf(zzb.class));
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public final class zzc extends AbstractSet<Map.Entry<String, Object>> {
        private final zzjb zzahz;

        zzc() {
            this.zzahz = (zzjb) new zzja(zzjf.this, zzjf.this.zzacg.zzhy()).entrySet();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public final Iterator<Map.Entry<String, Object>> iterator() {
            return new zza(zzjf.this, this.zzahz);
        }

        public final int size() {
            return zzjf.this.zzahw.size() + this.zzahz.size();
        }

        public final void clear() {
            zzjf.this.zzahw.clear();
            this.zzahz.clear();
        }
    }

    public zzjf(EnumSet<zzb> enumSet) {
        this.zzahw = new zziq();
        this.zzacg = zziv.zza(getClass(), enumSet.contains(zzb.IGNORE_CASE));
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    final class zza implements Iterator<Map.Entry<String, Object>> {
        private boolean zzaht;
        private final Iterator<Map.Entry<String, Object>> zzahu;
        private final Iterator<Map.Entry<String, Object>> zzahv;

        zza(zzjf zzjf, zzjb zzjb) {
            this.zzahu = (zzjc) zzjb.iterator();
            this.zzahv = zzjf.zzahw.entrySet().iterator();
        }

        public final boolean hasNext() {
            return this.zzahu.hasNext() || this.zzahv.hasNext();
        }

        public final void remove() {
            if (this.zzaht) {
                this.zzahv.remove();
            }
            this.zzahu.remove();
        }

        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.Iterator
        public final /* synthetic */ Map.Entry<String, Object> next() {
            if (!this.zzaht) {
                if (this.zzahu.hasNext()) {
                    return this.zzahu.next();
                }
                this.zzaht = true;
            }
            return this.zzahv.next();
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final Object get(Object obj) {
        if (!(obj instanceof String)) {
            return null;
        }
        String str = (String) obj;
        zzjd zzao = this.zzacg.zzao(str);
        if (zzao != null) {
            return zzao.zzh(this);
        }
        if (this.zzacg.zzhy()) {
            str = str.toLowerCase(Locale.US);
        }
        return this.zzahw.get(str);
    }

    /* renamed from: zzf */
    public final Object put(String str, Object obj) {
        zzjd zzao = this.zzacg.zzao(str);
        if (zzao != null) {
            Object zzh = zzao.zzh(this);
            zzao.zzb(this, obj);
            return zzh;
        }
        if (this.zzacg.zzhy()) {
            str = str.toLowerCase(Locale.US);
        }
        return this.zzahw.put(str, obj);
    }

    public zzjf zzb(String str, Object obj) {
        zzjd zzao = this.zzacg.zzao(str);
        if (zzao != null) {
            zzao.zzb(this, obj);
        } else {
            if (this.zzacg.zzhy()) {
                str = str.toLowerCase(Locale.US);
            }
            this.zzahw.put(str, obj);
        }
        return this;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.util.Map<? extends java.lang.String, ?>] */
    @Override // java.util.AbstractMap, java.util.Map
    public final void putAll(Map<? extends String, ? extends Object> map) {
        for (Map.Entry<? extends String, ? extends Object> entry : map.entrySet()) {
            zzb((String) entry.getKey(), entry.getValue());
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final Object remove(Object obj) {
        if (!(obj instanceof String)) {
            return null;
        }
        String str = (String) obj;
        if (this.zzacg.zzao(str) == null) {
            if (this.zzacg.zzhy()) {
                str = str.toLowerCase(Locale.US);
            }
            return this.zzahw.remove(str);
        }
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<String, Object>> entrySet() {
        return new zzc();
    }

    /* renamed from: zzfd */
    public zzjf clone() {
        try {
            zzjf zzjf = (zzjf) super.clone();
            zzix.zza(this, zzjf);
            zzjf.zzahw = (Map) zzix.clone(this.zzahw);
            return zzjf;
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException(e);
        }
    }

    public final zziv zzie() {
        return this.zzacg;
    }
}
