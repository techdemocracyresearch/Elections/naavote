package com.google.android.gms.internal.firebase_ml;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzha {
    final zzis zzace;
    final StringBuilder zzacf;
    final zziv zzacg;
    final List<Type> zzach;

    public zzha(zzgx zzgx, StringBuilder sb) {
        Class<?> cls = zzgx.getClass();
        this.zzach = Arrays.asList(cls);
        this.zzacg = zziv.zza(cls, true);
        this.zzacf = sb;
        this.zzace = new zzis(zzgx);
    }
}
