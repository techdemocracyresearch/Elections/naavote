package com.google.android.gms.internal.firebase_ml;

import java.io.PrintStream;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzne {
    private static final zznh zzalv;
    private static final int zzalw;

    public static void zza(Throwable th, Throwable th2) {
        zzalv.zza(th, th2);
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    static final class zza extends zznh {
        zza() {
        }

        @Override // com.google.android.gms.internal.firebase_ml.zznh
        public final void zza(Throwable th, Throwable th2) {
        }

        @Override // com.google.android.gms.internal.firebase_ml.zznh
        public final void zzb(Throwable th) {
            th.printStackTrace();
        }
    }

    public static void zzb(Throwable th) {
        zzalv.zzb(th);
    }

    private static Integer zzjp() {
        try {
            return (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
        } catch (Exception e) {
            System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
            e.printStackTrace(System.err);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
    static {
        zznh zznh;
        Integer num;
        Throwable th;
        int i = 1;
        try {
            num = zzjp();
            if (num != null) {
                try {
                    if (num.intValue() >= 19) {
                        zznh = new zznk();
                        zzalv = zznh;
                        if (num != null) {
                            i = num.intValue();
                        }
                        zzalw = i;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    PrintStream printStream = System.err;
                    String name = zza.class.getName();
                    StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 133);
                    sb.append("An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy ");
                    sb.append(name);
                    sb.append("will be used. The error is: ");
                    printStream.println(sb.toString());
                    th.printStackTrace(System.err);
                    zznh = new zza();
                    zzalv = zznh;
                    if (num != null) {
                    }
                    zzalw = i;
                }
            }
            if (!Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic")) {
                zznh = new zzni();
            } else {
                zznh = new zza();
            }
        } catch (Throwable th3) {
            th = th3;
            num = null;
            PrintStream printStream2 = System.err;
            String name2 = zza.class.getName();
            StringBuilder sb2 = new StringBuilder(String.valueOf(name2).length() + 133);
            sb2.append("An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy ");
            sb2.append(name2);
            sb2.append("will be used. The error is: ");
            printStream2.println(sb2.toString());
            th.printStackTrace(System.err);
            zznh = new zza();
            zzalv = zznh;
            if (num != null) {
            }
            zzalw = i;
        }
        zzalv = zznh;
        if (num != null) {
        }
        zzalw = i;
    }
}
