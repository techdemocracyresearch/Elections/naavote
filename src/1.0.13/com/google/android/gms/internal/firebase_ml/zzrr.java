package com.google.android.gms.internal.firebase_ml;

import android.util.Pair;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public abstract class zzrr<ResultType> implements Closeable {
    private final zzkz imageContext;
    protected final zzqf zzbkb;
    private final zzqb zzbnm;
    private final zzru zzbrb;
    private final zzku zzbrc;

    protected zzrr(zzqf zzqf, String str, FirebaseVisionCloudDetectorOptions firebaseVisionCloudDetectorOptions) {
        this(zzqf, new zzku().zza(Integer.valueOf(firebaseVisionCloudDetectorOptions.getMaxResults())).zzay(str).zzax(zzrq.zzbv(firebaseVisionCloudDetectorOptions.getModelType())), (zzkz) null, firebaseVisionCloudDetectorOptions.isEnforceCertFingerprintMatch());
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
    }

    /* access modifiers changed from: protected */
    public abstract ResultType zza(zzkl zzkl, float f);

    /* access modifiers changed from: protected */
    public abstract int zzqk();

    /* access modifiers changed from: protected */
    public abstract int zzql();

    protected zzrr(zzqf zzqf, String str, zzkz zzkz, boolean z) {
        this(zzqf, new zzku().zzay(str).zzax(zzrq.zzbv(1)), (zzkz) Preconditions.checkNotNull(zzkz, "ImageContext must not be null"), z);
    }

    private zzrr(zzqf zzqf, zzku zzku, zzkz zzkz, boolean z) {
        Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
        Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Firebase app name must not be null");
        this.zzbrc = (zzku) Preconditions.checkNotNull(zzku);
        this.zzbnm = zzqb.zza(zzqf);
        this.zzbrb = new zzru(this, zzqf.zzoh(), z);
        this.zzbkb = zzqf;
        this.imageContext = zzkz;
    }

    public final Task<ResultType> zza(FirebaseVisionImage firebaseVisionImage) {
        Preconditions.checkNotNull(firebaseVisionImage, "Input image can not be null");
        Pair<byte[], Float> zze = firebaseVisionImage.zze(zzqk(), zzql());
        if (zze.first == null) {
            return Tasks.forException(new FirebaseMLException("Can not convert the image format", 3));
        }
        return this.zzbnm.zza(this.zzbrb, new zzrs((byte[]) zze.first, ((Float) zze.second).floatValue(), Collections.singletonList(this.zzbrc), this.imageContext));
    }
}
