package com.google.android.gms.internal.firebase_ml;

import com.facebook.common.util.UriUtil;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzgo<T> extends zzgg<T> {
    private final Object zzabn;

    /* JADX WARNING: Illegal instructions before constructor call */
    protected zzgo(zzgm zzgm, String str, String str2, Object obj, Class<T> cls) {
        super(zzgm, str, str2, r0, cls);
        zzhw zzhw = null;
        String str3 = null;
        if (obj != null) {
            zzhw = new zzhw(zzgm.zzfp(), obj).zzal(!((zzhz) zzgm.zzfi()).zzgu().isEmpty() ? UriUtil.DATA_SCHEME : str3);
        }
        this.zzabn = obj;
    }

    /* renamed from: zzfq */
    public zzgm zzfk() {
        return (zzgm) super.zzfk();
    }

    /* renamed from: zzd */
    public zzgo<T> zzb(String str, Object obj) {
        return (zzgo) super.zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgg
    public /* synthetic */ zzgg zzc(String str, Object obj) {
        return (zzgo) zzb(str, obj);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzgg
    public final /* synthetic */ IOException zza(zzhd zzhd) {
        return zzgf.zza(((zzgm) zzfk()).zzfp(), zzhd);
    }
}
