package com.google.android.gms.internal.firebase_ml;

import java.util.Comparator;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzvx implements Comparator<zzvv> {
    zzvx() {
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(zzvv zzvv, zzvv zzvv2) {
        zzvv zzvv3 = zzvv;
        zzvv zzvv4 = zzvv2;
        zzwa zzwa = (zzwa) zzvv3.iterator();
        zzwa zzwa2 = (zzwa) zzvv4.iterator();
        while (zzwa.hasNext() && zzwa2.hasNext()) {
            int compare = Integer.compare(zzvv.zzb(zzwa.nextByte()), zzvv.zzb(zzwa2.nextByte()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzvv3.size(), zzvv4.size());
    }
}
