package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzik {
    static final /* synthetic */ int[] zzaem;
    static final /* synthetic */ int[] zzafd;

    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|(2:1|2)|3|5|6|7|8|9|10|11|12|13|14|15|16|17|18|(2:19|20)|21|23|24|(3:25|26|28)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|5|6|7|8|9|10|11|12|13|14|15|16|17|18|(2:19|20)|21|23|24|25|26|28) */
    /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|5|6|7|8|9|10|11|12|13|14|15|16|17|18|(2:19|20)|21|23|24|25|26|28) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0033 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0049 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0054 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0060 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x007d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0028 */
    static {
        int[] iArr = new int[zztb.values().length];
        zzafd = iArr;
        try {
            iArr[zztb.BEGIN_ARRAY.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        zzafd[zztb.END_ARRAY.ordinal()] = 2;
        zzafd[zztb.BEGIN_OBJECT.ordinal()] = 3;
        zzafd[zztb.END_OBJECT.ordinal()] = 4;
        zzafd[zztb.BOOLEAN.ordinal()] = 5;
        zzafd[zztb.NULL.ordinal()] = 6;
        zzafd[zztb.STRING.ordinal()] = 7;
        zzafd[zztb.NUMBER.ordinal()] = 8;
        try {
            zzafd[zztb.NAME.ordinal()] = 9;
        } catch (NoSuchFieldError unused2) {
        }
        int[] iArr2 = new int[zzih.values().length];
        zzaem = iArr2;
        iArr2[zzih.START_ARRAY.ordinal()] = 1;
        try {
            zzaem[zzih.START_OBJECT.ordinal()] = 2;
        } catch (NoSuchFieldError unused3) {
        }
    }
}
