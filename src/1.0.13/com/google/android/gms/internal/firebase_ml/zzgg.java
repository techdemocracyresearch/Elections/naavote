package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzgg<T> extends zzjf {
    private final zzge zzaaw;
    private final String zzaax;
    private final String zzaay;
    private final zzgt zzaaz;
    private zzgx zzaba = new zzgx();
    private zzgx zzabb;
    private int zzabc = -1;
    private String zzabd;
    private Class<T> zzabe;

    protected zzgg(zzge zzge, String str, String str2, zzgt zzgt, Class<T> cls) {
        this.zzabe = (Class) zzml.checkNotNull(cls);
        this.zzaaw = (zzge) zzml.checkNotNull(zzge);
        this.zzaax = (String) zzml.checkNotNull(str);
        this.zzaay = (String) zzml.checkNotNull(str2);
        this.zzaaz = zzgt;
        String zzfg = zzge.zzfg();
        if (zzfg != null) {
            zzgx zzgx = this.zzaba;
            StringBuilder sb = new StringBuilder(String.valueOf(zzfg).length() + 23);
            sb.append(zzfg);
            sb.append(" Google-API-Java-Client");
            zzgx.zzad(sb.toString());
        } else {
            this.zzaba.zzad("Google-API-Java-Client");
        }
        zzgx zzgx2 = (zzgx) this.zzaba.zzb("X-Goog-Api-Client", zzgh.zzfo().zzn(zzge.getClass().getSimpleName()));
    }

    public zzge zzfk() {
        return this.zzaaw;
    }

    public final zzgx zzfl() {
        return this.zzaba;
    }

    /* access modifiers changed from: protected */
    public IOException zza(zzhd zzhd) {
        return new zzhg(zzhd);
    }

    public final T zzfm() throws IOException {
        zzml.checkArgument(true);
        zzml.checkArgument(true);
        zzhc zza = zzfk().zzfh().zza(this.zzaax, new zzgu(zzhm.zza(this.zzaaw.zzff(), this.zzaay, (Object) this, true)), this.zzaaz);
        new zzgb().zzb(zza);
        zza.zza(zzfk().zzfi());
        if (this.zzaaz == null && (this.zzaax.equals("POST") || this.zzaax.equals("PUT") || this.zzaax.equals("PATCH"))) {
            zza.zza(new zzgp());
        }
        zza.zzga().putAll(this.zzaba);
        zza.zza(new zzgs());
        zza.zza(new zzgi(this, zza.zzgc(), zza));
        zzhd zzgf = zza.zzgf();
        this.zzabb = zzgf.zzga();
        this.zzabc = zzgf.getStatusCode();
        this.zzabd = zzgf.getStatusMessage();
        return (T) zzgf.zza(this.zzabe);
    }

    /* renamed from: zzc */
    public zzgg<T> zzb(String str, Object obj) {
        return (zzgg) super.zzb(str, obj);
    }
}
