package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.Dependency;
import com.google.firebase.ml.common.internal.modeldownload.zzaa;
import com.google.firebase.ml.common.internal.modeldownload.zzn;
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel;
import java.util.UUID;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzqu {
    private static final GmsLogger zzbin = new GmsLogger("SharedPrefManager", "");
    public static final Component<?> zzbja = Component.builder(zzqu.class).add(Dependency.required(zzqf.class)).add(Dependency.required(Context.class)).factory(zzqt.zzbil).build();
    private final Context zzbkc;
    private final String zzbkm;

    private zzqu(zzqf zzqf, Context context) {
        this.zzbkc = context;
        this.zzbkm = zzqf.getPersistenceKey();
    }

    public static zzqu zzb(zzqf zzqf) {
        return (zzqu) zzqf.get(zzqu.class);
    }

    public final synchronized boolean zzop() {
        return getSharedPreferences().getBoolean(String.format("logging_%s_%s", "vision", this.zzbkm), true);
    }

    public final synchronized void zzar(boolean z) {
        getSharedPreferences().edit().putBoolean(String.format("logging_%s_%s", "vision", this.zzbkm), z).apply();
    }

    public final synchronized boolean zzoq() {
        return getSharedPreferences().getBoolean(String.format("logging_%s_%s", "model", this.zzbkm), true);
    }

    public final synchronized void zzas(boolean z) {
        getSharedPreferences().edit().putBoolean(String.format("logging_%s_%s", "model", this.zzbkm), z).apply();
    }

    public final synchronized Long zza(FirebaseRemoteModel firebaseRemoteModel) {
        long j = getSharedPreferences().getLong(String.format("downloading_model_id_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), -1);
        if (j < 0) {
            return null;
        }
        return Long.valueOf(j);
    }

    public final synchronized zzn zzbw(String str) {
        return zzbx(getSharedPreferences().getString(String.format("downloading_model_type_%s", str), ""));
    }

    public final synchronized String zzb(FirebaseRemoteModel firebaseRemoteModel) {
        return getSharedPreferences().getString(String.format("downloading_model_hash_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), null);
    }

    public final synchronized String zzc(FirebaseRemoteModel firebaseRemoteModel) {
        return getSharedPreferences().getString(String.format("current_model_hash_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), null);
    }

    public final synchronized zzn zzd(FirebaseRemoteModel firebaseRemoteModel) {
        return zzbx(getSharedPreferences().getString(String.format("current_model_type_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), zzn.UNKNOWN.name()));
    }

    public final synchronized String zze(FirebaseRemoteModel firebaseRemoteModel) {
        return getSharedPreferences().getString(String.format("bad_hash_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), null);
    }

    public final synchronized String zzor() {
        return getSharedPreferences().getString("app_version", null);
    }

    public final synchronized long zzf(FirebaseRemoteModel firebaseRemoteModel) {
        return getSharedPreferences().getLong(String.format("downloading_begin_time_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), 0);
    }

    public final synchronized long zzg(FirebaseRemoteModel firebaseRemoteModel) {
        return getSharedPreferences().getLong(String.format("model_first_use_time_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), 0);
    }

    public final synchronized void zza(FirebaseRemoteModel firebaseRemoteModel, long j) {
        getSharedPreferences().edit().putLong(String.format("model_first_use_time_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), j).apply();
    }

    public final synchronized void zzh(FirebaseRemoteModel firebaseRemoteModel) {
        String zzb = zzb(firebaseRemoteModel);
        getSharedPreferences().edit().remove(String.format("downloading_model_id_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist())).remove(String.format("downloading_model_hash_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist())).remove(String.format("downloading_model_type_%s", zzb)).remove(String.format("downloading_begin_time_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist())).remove(String.format("model_first_use_time_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist())).apply();
    }

    public final synchronized void zzi(FirebaseRemoteModel firebaseRemoteModel) {
        getSharedPreferences().edit().remove(String.format("current_model_hash_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist())).remove(String.format("current_model_type_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist())).commit();
    }

    public final synchronized void zza(FirebaseRemoteModel firebaseRemoteModel, String str, zzn zzn) {
        getSharedPreferences().edit().putString(String.format("current_model_hash_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), str).putString(String.format("current_model_type_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), zzn.name()).apply();
    }

    public final synchronized void zza(long j, zzaa zzaa) {
        String zzpl = zzaa.zzpl();
        String modelHash = zzaa.getModelHash();
        zzn zzpm = zzaa.zzpm();
        getSharedPreferences().edit().putString(String.format("downloading_model_hash_%s_%s", this.zzbkm, zzpl), modelHash).putLong(String.format("downloading_model_id_%s_%s", this.zzbkm, zzpl), j).putString(String.format("downloading_model_type_%s", modelHash), zzpm.name()).putLong(String.format("downloading_begin_time_%s_%s", this.zzbkm, zzpl), SystemClock.elapsedRealtime()).apply();
    }

    public final synchronized void zza(FirebaseRemoteModel firebaseRemoteModel, String str, String str2) {
        getSharedPreferences().edit().putString(String.format("bad_hash_%s_%s", this.zzbkm, firebaseRemoteModel.getUniqueModelNameForPersist()), str).putString("app_version", str2).apply();
    }

    public final synchronized String zzos() {
        String string = getSharedPreferences().getString("ml_sdk_instance_id", null);
        if (string != null) {
            return string;
        }
        String uuid = UUID.randomUUID().toString();
        getSharedPreferences().edit().putString("ml_sdk_instance_id", uuid).apply();
        return uuid;
    }

    private final SharedPreferences getSharedPreferences() {
        return this.zzbkc.getSharedPreferences("com.google.firebase.ml.internal", 0);
    }

    private static zzn zzbx(String str) {
        if (str == null) {
            return zzn.UNKNOWN;
        }
        try {
            return zzn.zzby(str);
        } catch (IllegalArgumentException unused) {
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(str);
            gmsLogger.e("SharedPrefManager", valueOf.length() != 0 ? "Invalid model type ".concat(valueOf) : new String("Invalid model type "));
            return zzn.UNKNOWN;
        }
    }

    static final /* synthetic */ zzqu zze(ComponentContainer componentContainer) {
        return new zzqu((zzqf) componentContainer.get(zzqf.class), (Context) componentContainer.get(Context.class));
    }
}
