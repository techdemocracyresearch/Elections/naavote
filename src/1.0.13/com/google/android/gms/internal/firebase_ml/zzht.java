package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Arrays;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzht extends zzhh {
    private static final String[] zzadg;
    private final HostnameVerifier hostnameVerifier;
    private final zzhq zzaec;
    private final SSLSocketFactory zzaed;

    public zzht() {
        this(null, null, null);
    }

    private zzht(zzhq zzhq, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier2) {
        zzhp zzhp;
        if (System.getProperty("com.google.api.client.should_use_proxy") != null) {
            zzhp = new zzhp(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(System.getProperty("https.proxyHost"), Integer.parseInt(System.getProperty("https.proxyPort")))));
        } else {
            zzhp = new zzhp();
        }
        this.zzaec = zzhp;
        this.zzaed = null;
        this.hostnameVerifier = null;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhh
    public final boolean zzaj(String str) {
        return Arrays.binarySearch(zzadg, str) >= 0;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzhh
    public final /* synthetic */ zzhk zzc(String str, String str2) throws IOException {
        Object[] objArr = {str};
        if (zzaj(str)) {
            HttpURLConnection zza = this.zzaec.zza(new URL(str2));
            zza.setRequestMethod(str);
            if (zza instanceof HttpsURLConnection) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) zza;
            }
            return new zzhs(zza);
        }
        throw new IllegalArgumentException(zzms.zzb("HTTP method %s not supported", objArr));
    }

    static {
        String[] strArr = {"DELETE", "GET", "HEAD", "OPTIONS", "POST", "PUT", "TRACE"};
        zzadg = strArr;
        Arrays.sort(strArr);
    }
}
