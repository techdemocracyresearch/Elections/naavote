package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import android.content.res.Resources;
import android.os.SystemClock;
import androidx.core.os.ConfigurationCompat;
import androidx.core.os.LocaleListCompat;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.Dependency;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public final class zzqg {
    private static final GmsLogger zzbin = new GmsLogger("MlStatsLogger", "");
    private static List<String> zzbje;
    private static boolean zzbjq = false;
    private static boolean zzbjr = false;
    public static final Component<?> zzbjt = Component.builder(zza.class).add(Dependency.required(zzqf.class)).add(Dependency.required(Context.class)).add(Dependency.required(zzqu.class)).add(Dependency.required(zzb.class)).factory(zzqk.zzbil).build();
    private final String zzbjf;
    private final String zzbjg;
    private final String zzbjh;
    private final String zzbji;
    private final String zzbjj;
    private final zzb zzbjk;
    private final zzqu zzbjl;
    private final Task<String> zzbjm;
    private final Task<String> zzbjn;
    private final Map<zzod, Long> zzbjo;
    private final Map<zzod, Object> zzbjp;
    private final int zzbjs;

    public interface zzb {
        void zza(zzns.zzad zzad);
    }

    private zzqg(zzqf zzqf, Context context, zzqu zzqu, zzb zzb2, int i) {
        String str;
        String str2;
        String apiKey;
        this.zzbjo = new HashMap();
        this.zzbjp = new HashMap();
        this.zzbjs = i;
        FirebaseApp zzoh = zzqf.zzoh();
        String str3 = "";
        this.zzbjh = (zzoh == null || (str = zzoh.getOptions().getProjectId()) == null) ? str3 : str;
        FirebaseApp zzoh2 = zzqf.zzoh();
        this.zzbji = (zzoh2 == null || (str2 = zzoh2.getOptions().getGcmSenderId()) == null) ? str3 : str2;
        FirebaseApp zzoh3 = zzqf.zzoh();
        if (!(zzoh3 == null || (apiKey = zzoh3.getOptions().getApiKey()) == null)) {
            str3 = apiKey;
        }
        this.zzbjj = str3;
        this.zzbjf = context.getPackageName();
        this.zzbjg = zzpt.zzb(context);
        this.zzbjl = zzqu;
        this.zzbjk = zzb2;
        this.zzbjm = zzpx.zzof().zza(zzqj.zzbjx);
        zzpx zzof = zzpx.zzof();
        zzqu.getClass();
        this.zzbjn = zzof.zza(zzqi.zza(zzqu));
    }

    public static class zza extends zzps<Integer, zzqg> {
        private final zzb zzbjk;
        private final zzqu zzbjl;
        private final zzqf zzbkb;
        private final Context zzbkc;

        private zza(zzqf zzqf, Context context, zzqu zzqu, zzb zzb) {
            this.zzbkb = zzqf;
            this.zzbkc = context;
            this.zzbjl = zzqu;
            this.zzbjk = zzb;
        }

        @Override // com.google.android.gms.internal.firebase_ml.zzps
        public final /* synthetic */ zzqg create(Integer num) {
            return new zzqg(this.zzbkb, this.zzbkc, this.zzbjl, this.zzbjk, num.intValue());
        }
    }

    public static zzqg zza(zzqf zzqf, int i) {
        Preconditions.checkNotNull(zzqf);
        return (zzqg) ((zza) zzqf.get(zza.class)).get(Integer.valueOf(i));
    }

    public final void zza(zzns.zzad.zza zza2, zzod zzod) {
        zzpx.zzoe().execute(new zzql(this, zza2, zzod));
    }

    public final void zza(zzqo zzqo, zzod zzod) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        boolean z = true;
        if (!zzfz() || (this.zzbjo.get(zzod) != null && elapsedRealtime - this.zzbjo.get(zzod).longValue() <= TimeUnit.SECONDS.toMillis(30))) {
            z = false;
        }
        if (z) {
            this.zzbjo.put(zzod, Long.valueOf(elapsedRealtime));
            zza(zzqo.zzok(), zzod);
        }
    }

    public final <K> void zza(K k, long j, zzod zzod, zzqm<K> zzqm) {
        zzfz();
    }

    private final boolean zzfz() {
        int i = this.zzbjs;
        if (i == 1) {
            return this.zzbjl.zzop();
        }
        if (i != 2) {
            return i == 3 || i == 4 || i == 5;
        }
        return this.zzbjl.zzoq();
    }

    private static synchronized List<String> zzoi() {
        synchronized (zzqg.class) {
            List<String> list = zzbje;
            if (list != null) {
                return list;
            }
            LocaleListCompat locales = ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration());
            zzbje = new ArrayList(locales.size());
            for (int i = 0; i < locales.size(); i++) {
                zzbje.add(zzpt.zza(locales.get(i)));
            }
            return zzbje;
        }
    }

    public final /* synthetic */ void zzb(zzns.zzad.zza zza2, zzod zzod) {
        String str;
        if (!zzfz()) {
            zzbin.d("MlStatsLogger", "Logging is disabled.");
            return;
        }
        String zznx = zza2.zzlz().zznx();
        if ("NA".equals(zznx) || "".equals(zznx)) {
            zznx = "NA";
        }
        zzns.zzbc.zza zzx = zzns.zzbc.zzny().zzbp(this.zzbjf).zzbq(this.zzbjg).zzbr(this.zzbjh).zzbu(this.zzbji).zzbv(this.zzbjj).zzbt(zznx).zzx(zzoi());
        if (this.zzbjm.isSuccessful()) {
            str = this.zzbjm.getResult();
        } else {
            str = zzpv.zzod().getVersion("firebase-ml-common");
        }
        zza2.zza(zzod).zza(zzx.zzbs(str));
        try {
            this.zzbjk.zza((zzns.zzad) ((zzwz) zza2.zzvb()));
        } catch (RuntimeException e) {
            zzbin.e("MlStatsLogger", "Exception thrown from the logging side", e);
        }
    }

    static final /* synthetic */ zza zzc(ComponentContainer componentContainer) {
        return new zza((zzqf) componentContainer.get(zzqf.class), (Context) componentContainer.get(Context.class), (zzqu) componentContainer.get(zzqu.class), (zzb) componentContainer.get(zzb.class));
    }
}
