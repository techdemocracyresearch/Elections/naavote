package com.google.android.gms.internal.firebase_ml;

import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzabq implements zzxc {
    UNKNOWN_EVENT_TYPE(0),
    VALIDATION_TEST(1),
    CONTINUOUS_FEEDBACK(2);
    
    private static final zzxf<zzabq> zzac = new zzabp();
    private final int value;

    @Override // com.google.android.gms.internal.firebase_ml.zzxc
    public final int zzd() {
        return this.value;
    }

    public static zzabq zzen(int i) {
        if (i == 0) {
            return UNKNOWN_EVENT_TYPE;
        }
        if (i == 1) {
            return VALIDATION_TEST;
        }
        if (i != 2) {
            return null;
        }
        return CONTINUOUS_FEEDBACK;
    }

    public static zzxe zzf() {
        return zzabr.zzan;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
    }

    private zzabq(int i) {
        this.value = i;
    }
}
