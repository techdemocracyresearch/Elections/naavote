package com.google.android.gms.internal.firebase_ml;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzi extends zzzj<FieldDescriptorType, Object> {
    zzzi(int i) {
        super(i, null);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzzj
    public final void zztm() {
        if (!isImmutable()) {
            for (int i = 0; i < zzwq(); i++) {
                Map.Entry zzec = zzec(i);
                if (((zzwt) zzec.getKey()).zzuk()) {
                    zzec.setValue(Collections.unmodifiableList((List) zzec.getValue()));
                }
            }
            for (Map.Entry entry : zzwr()) {
                if (((zzwt) entry.getKey()).zzuk()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.zztm();
    }
}
