package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.Dependency;
import java.util.concurrent.Callable;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zzqb {
    private static final GmsLogger zzbiy = new GmsLogger("MLTaskManager", "");
    public static final Component<?> zzbja = Component.builder(zzqb.class).add(Dependency.required(zzqr.class)).factory(zzqc.zzbil).build();
    private final zzqr zzbiz;

    private zzqb(zzqr zzqr) {
        this.zzbiz = zzqr;
    }

    public static synchronized zzqb zza(zzqf zzqf) {
        zzqb zzqb;
        synchronized (zzqb.class) {
            zzqb = (zzqb) zzqf.get(zzqb.class);
        }
        return zzqb;
    }

    public final <T, S extends zzpy> void zza(zzpu<T, S> zzpu) {
        zzqp zzoc = zzpu.zzoc();
        if (zzoc != null) {
            this.zzbiz.zza(zzoc);
        }
    }

    public final <T, S extends zzpy> void zzb(zzpu<T, S> zzpu) {
        zzqp zzoc = zzpu.zzoc();
        if (zzoc != null) {
            this.zzbiz.zzd(zzoc);
        }
    }

    public final synchronized <TResult> Task<TResult> zza(zzqp zzqp, Callable<TResult> callable) {
        Preconditions.checkNotNull(callable, "Operation can not be null");
        Preconditions.checkNotNull(zzqp, "Model resource can not be null");
        zzbiy.d("MLTaskManager", "Execute task");
        this.zzbiz.zzb(zzqp);
        return zzpx.zzof().zza(new zzqa(this, zzqp, callable));
    }

    public final synchronized <T, S extends zzpy> Task<T> zza(zzpu<T, S> zzpu, S s) {
        zzqp zzoc;
        Preconditions.checkNotNull(zzpu, "Operation can not be null");
        Preconditions.checkNotNull(s, "Input can not be null");
        zzbiy.d("MLTaskManager", "Execute task");
        zzoc = zzpu.zzoc();
        if (zzoc != null) {
            this.zzbiz.zzb(zzoc);
        }
        return zzpx.zzof().zza(new zzqd(this, zzoc, zzpu, s));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(zzqp zzqp, zzpu zzpu, zzpy zzpy) throws Exception {
        if (zzqp != null) {
            this.zzbiz.zzf(zzqp);
        }
        return zzpu.zza(zzpy);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzb(zzqp zzqp, Callable callable) throws Exception {
        this.zzbiz.zzf(zzqp);
        return callable.call();
    }

    static final /* synthetic */ zzqb zza(ComponentContainer componentContainer) {
        return new zzqb((zzqr) componentContainer.get(zzqr.class));
    }
}
