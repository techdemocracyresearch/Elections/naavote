package com.google.android.gms.internal.firebase_ml;

import android.graphics.Rect;
import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Preconditions;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.RecognizedLanguage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsy {
    private static final GmsLogger zzbin = new GmsLogger("TextAnnotationConverter", "");

    /* JADX WARNING: Removed duplicated region for block: B:104:0x02ae  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x02b1 A[SYNTHETIC] */
    static FirebaseVisionText zzb(zzlk zzlk, float f) {
        Iterator<zzkm> it;
        Iterator<zzlf> it2;
        FirebaseVisionText.TextBlock textBlock;
        Iterator<zzle> it3;
        Iterator<zzkm> it4;
        Iterator<zzlf> it5;
        String str;
        FirebaseVisionText.Element element;
        if (zzlk == null) {
            return FirebaseVisionText.zzbut;
        }
        if (zzlk.getPages().size() <= 0) {
            zzbin.d("TextAnnotationConverter", "Text Annotation is null, return empty Vision Text");
            return FirebaseVisionText.zzbut;
        }
        if (zzlk.getPages().size() > 1) {
            zzbin.d("TextAnnotationConverter", "Text Annotation has more than one page, which should not happen");
        }
        ArrayList arrayList = new ArrayList();
        Iterator<zzlf> it6 = zzlk.getPages().iterator();
        while (it6.hasNext()) {
            Iterator<zzkm> it7 = it6.next().getBlocks().iterator();
            while (it7.hasNext()) {
                zzkm next = it7.next();
                Preconditions.checkNotNull(next, "Input block can not be null");
                ArrayList arrayList2 = new ArrayList();
                if (next.getParagraphs() == null) {
                    it2 = it6;
                    it = it7;
                } else {
                    Iterator<zzle> it8 = next.getParagraphs().iterator();
                    while (it8.hasNext()) {
                        zzle next2 = it8.next();
                        if (next2 != null) {
                            Preconditions.checkNotNull(next2, "Input Paragraph can not be null");
                            ArrayList arrayList3 = new ArrayList();
                            ArrayList arrayList4 = new ArrayList();
                            HashSet<RecognizedLanguage> hashSet = new HashSet();
                            StringBuilder sb = new StringBuilder();
                            int i = 0;
                            float f2 = 0.0f;
                            while (i < next2.getWords().size()) {
                                zzlq zzlq = next2.getWords().get(i);
                                if (zzlq != null) {
                                    Preconditions.checkNotNull(zzlq, "Input Word can not be null");
                                    Rect zza = zzrq.zza(zzlq.zzil(), f);
                                    it5 = it6;
                                    List<RecognizedLanguage> zze = zze(zzlq.zzim());
                                    Preconditions.checkNotNull(zzlq, "Input Word can not be null");
                                    String str2 = "";
                                    if (zzlq.getSymbols() == null) {
                                        it4 = it7;
                                        str = str2;
                                    } else {
                                        StringBuilder sb2 = new StringBuilder();
                                        for (zzll zzll : zzlq.getSymbols()) {
                                            sb2.append(zzll.getText());
                                            it7 = it7;
                                        }
                                        it4 = it7;
                                        str = sb2.toString();
                                    }
                                    if (str.isEmpty()) {
                                        it3 = it8;
                                        element = null;
                                    } else {
                                        it3 = it8;
                                        element = new FirebaseVisionText.Element(str, zza, zze, zzlq.getConfidence());
                                    }
                                    if (element != null) {
                                        arrayList4.add(element);
                                        float zza2 = f2 + zzrq.zza(element.getConfidence());
                                        hashSet.addAll(element.getRecognizedLanguages());
                                        sb.append(element.getText());
                                        Preconditions.checkNotNull(zzlq, "Input word can not be null");
                                        String zza3 = zza(zzlq);
                                        if (zza3 != null) {
                                            if (zza3.equals("SPACE") || zza3.equals("SURE_SPACE")) {
                                                str2 = " ";
                                            } else if (zza3.equals("HYPHEN")) {
                                                str2 = "-";
                                            }
                                        }
                                        sb.append(str2);
                                        Preconditions.checkNotNull(zzlq, "Input word can not be null");
                                        String zza4 = zza(zzlq);
                                        if (!(zza4 != null && (zza4.equals("EOL_SURE_SPACE") || zza4.equals("LINE_BREAK") || zza4.equals("HYPHEN")))) {
                                            if (i != next2.getWords().size() - 1) {
                                                f2 = zza2;
                                            }
                                        }
                                        Preconditions.checkNotNull(arrayList4, "Input elements can not be null");
                                        ArrayList arrayList5 = arrayList4;
                                        int size = arrayList5.size();
                                        int i2 = 0;
                                        Rect rect = null;
                                        while (i2 < size) {
                                            Object obj = arrayList5.get(i2);
                                            i2++;
                                            FirebaseVisionText.Element element2 = (FirebaseVisionText.Element) obj;
                                            if (element2.getBoundingBox() != null) {
                                                Rect rect2 = rect == null ? new Rect() : rect;
                                                rect2.union(element2.getBoundingBox());
                                                rect = rect2;
                                            }
                                        }
                                        String sb3 = sb.toString();
                                        ArrayList arrayList6 = new ArrayList();
                                        for (RecognizedLanguage recognizedLanguage : hashSet) {
                                            if (!(recognizedLanguage == null || recognizedLanguage.getLanguageCode() == null || recognizedLanguage.getLanguageCode().isEmpty())) {
                                                arrayList6.add(recognizedLanguage);
                                            }
                                        }
                                        arrayList3.add(new FirebaseVisionText.Line(sb3, rect, arrayList6, arrayList4, Float.compare(zza2, 0.0f) > 0 ? Float.valueOf(zza2 / ((float) arrayList4.size())) : null));
                                        ArrayList arrayList7 = new ArrayList();
                                        hashSet.clear();
                                        arrayList4 = arrayList7;
                                        sb = new StringBuilder();
                                        f2 = 0.0f;
                                        i++;
                                        it6 = it5;
                                        it7 = it4;
                                        it8 = it3;
                                    }
                                } else {
                                    it5 = it6;
                                    it4 = it7;
                                    it3 = it8;
                                }
                                i++;
                                it6 = it5;
                                it7 = it4;
                                it8 = it3;
                            }
                            arrayList2.addAll(arrayList3);
                        }
                    }
                    it2 = it6;
                    it = it7;
                    if (!arrayList2.isEmpty()) {
                        StringBuilder sb4 = new StringBuilder();
                        ArrayList arrayList8 = arrayList2;
                        int size2 = arrayList8.size();
                        int i3 = 0;
                        while (i3 < size2) {
                            Object obj2 = arrayList8.get(i3);
                            i3++;
                            sb4.append(((FirebaseVisionText.Line) obj2).getText());
                            sb4.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                        }
                        textBlock = new FirebaseVisionText.TextBlock(sb4.toString(), zzrq.zza(next.zzil(), f), zze(next.zzim()), arrayList2, next.getConfidence());
                        if (textBlock == null) {
                            arrayList.add(textBlock);
                        }
                        it6 = it2;
                        it7 = it;
                    }
                }
                textBlock = null;
                if (textBlock == null) {
                }
                it6 = it2;
                it7 = it;
            }
        }
        return new FirebaseVisionText(zzlk.getText(), arrayList);
    }

    public static List<RecognizedLanguage> zze(zzln zzln) {
        ArrayList arrayList = new ArrayList();
        if (!(zzln == null || zzln.zziy() == null)) {
            for (zzkt zzkt : zzln.zziy()) {
                RecognizedLanguage zza = RecognizedLanguage.zza(zzkt);
                if (zza != null) {
                    arrayList.add(zza);
                }
            }
        }
        return arrayList;
    }

    private static String zza(zzlq zzlq) {
        Preconditions.checkNotNull(zzlq, "Input Word can not be null");
        if (zzlq.getSymbols() == null || zzlq.getSymbols().isEmpty()) {
            return null;
        }
        zzll zzll = (zzll) zznb.zzc(zzlq.getSymbols());
        if (zzll.zzim() == null || zzll.zzim().zzix() == null) {
            return null;
        }
        return ((zzll) zznb.zzc(zzlq.getSymbols())).zzim().zzix().getType();
    }
}
