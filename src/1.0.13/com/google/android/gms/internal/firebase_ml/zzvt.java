package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzvt extends zzvp<Boolean> implements zzxl<Boolean>, zzyw, RandomAccess {
    private static final zzvt zzchm;
    private int size;
    private boolean[] zzchn;

    zzvt() {
        this(new boolean[10], 0);
    }

    private zzvt(boolean[] zArr, int i) {
        this.zzchn = zArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zztn();
        if (i2 >= i) {
            boolean[] zArr = this.zzchn;
            System.arraycopy(zArr, i2, zArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzvt)) {
            return super.equals(obj);
        }
        zzvt zzvt = (zzvt) obj;
        if (this.size != zzvt.size) {
            return false;
        }
        boolean[] zArr = zzvt.zzchn;
        for (int i = 0; i < this.size; i++) {
            if (this.zzchn[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + zzxd.zzaz(this.zzchn[i2]);
        }
        return i;
    }

    public final int indexOf(Object obj) {
        if (!(obj instanceof Boolean)) {
            return -1;
        }
        boolean booleanValue = ((Boolean) obj).booleanValue();
        int size2 = size();
        for (int i = 0; i < size2; i++) {
            if (this.zzchn[i] == booleanValue) {
                return i;
            }
        }
        return -1;
    }

    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final int size() {
        return this.size;
    }

    public final void addBoolean(boolean z) {
        zztn();
        int i = this.size;
        boolean[] zArr = this.zzchn;
        if (i == zArr.length) {
            boolean[] zArr2 = new boolean[(((i * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            this.zzchn = zArr2;
        }
        boolean[] zArr3 = this.zzchn;
        int i2 = this.size;
        this.size = i2 + 1;
        zArr3[i2] = z;
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final boolean addAll(Collection<? extends Boolean> collection) {
        zztn();
        zzxd.checkNotNull(collection);
        if (!(collection instanceof zzvt)) {
            return super.addAll(collection);
        }
        zzvt zzvt = (zzvt) collection;
        int i = zzvt.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.zzchn;
            if (i3 > zArr.length) {
                this.zzchn = Arrays.copyOf(zArr, i3);
            }
            System.arraycopy(zzvt.zzchn, 0, this.zzchn, this.size, zzvt.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean remove(Object obj) {
        zztn();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Boolean.valueOf(this.zzchn[i]))) {
                boolean[] zArr = this.zzchn;
                System.arraycopy(zArr, i + 1, zArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzct(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
    }

    private final String zzcu(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Boolean set(int i, Boolean bool) {
        boolean booleanValue = bool.booleanValue();
        zztn();
        zzct(i);
        boolean[] zArr = this.zzchn;
        boolean z = zArr[i];
        zArr[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Boolean remove(int i) {
        zztn();
        zzct(i);
        boolean[] zArr = this.zzchn;
        boolean z = zArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Boolean.valueOf(z);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ void add(int i, Boolean bool) {
        int i2;
        boolean booleanValue = bool.booleanValue();
        zztn();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
        boolean[] zArr = this.zzchn;
        if (i2 < zArr.length) {
            System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
        } else {
            boolean[] zArr2 = new boolean[(((i2 * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            System.arraycopy(this.zzchn, i, zArr2, i + 1, this.size - i);
            this.zzchn = zArr2;
        }
        this.zzchn[i] = booleanValue;
        this.size++;
        this.modCount++;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection, java.util.AbstractList
    public final /* synthetic */ boolean add(Boolean bool) {
        addBoolean(bool.booleanValue());
        return true;
    }

    /* Return type fixed from 'com.google.android.gms.internal.firebase_ml.zzxl' to match base method */
    @Override // com.google.android.gms.internal.firebase_ml.zzxl
    public final /* synthetic */ zzxl<Boolean> zzcv(int i) {
        if (i >= this.size) {
            return new zzvt(Arrays.copyOf(this.zzchn, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        zzct(i);
        return Boolean.valueOf(this.zzchn[i]);
    }

    static {
        zzvt zzvt = new zzvt(new boolean[0], 0);
        zzchm = zzvt;
        zzvt.zztm();
    }
}
