package com.google.android.gms.internal.firebase_ml;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzgz {
    private static final Pattern zzabx = Pattern.compile("[\\w!#$&.+\\-\\^_]+|[*]");
    private static final Pattern zzaby = Pattern.compile("[\\p{ASCII}&&[^\\p{Cntrl} ;/=\\[\\]\\(\\)\\<\\>\\@\\,\\:\\\"\\?\\=]]+");
    private static final Pattern zzabz;
    private static final Pattern zzaca;
    private String type = "application";
    private String zzacb = "octet-stream";
    private final SortedMap<String, String> zzacc = new TreeMap();
    private String zzacd;

    public zzgz(String str) {
        Matcher matcher = zzabz.matcher(str);
        zzml.checkArgument(matcher.matches(), "Type must be in the 'maintype/subtype; parameter=value' format");
        String group = matcher.group(1);
        Pattern pattern = zzabx;
        zzml.checkArgument(pattern.matcher(group).matches(), "Type contains reserved characters");
        this.type = group;
        this.zzacd = null;
        String group2 = matcher.group(2);
        zzml.checkArgument(pattern.matcher(group2).matches(), "Subtype contains reserved characters");
        this.zzacb = group2;
        this.zzacd = null;
        String group3 = matcher.group(3);
        if (group3 != null) {
            Matcher matcher2 = zzaca.matcher(group3);
            while (matcher2.find()) {
                String group4 = matcher2.group(1);
                String group5 = matcher2.group(3);
                if (group5 == null) {
                    group5 = matcher2.group(2);
                }
                zza(group4, group5);
            }
        }
    }

    private final zzgz zza(String str, String str2) {
        if (str2 == null) {
            this.zzacd = null;
            this.zzacc.remove(str.toLowerCase(Locale.US));
            return this;
        }
        zzml.checkArgument(zzaby.matcher(str).matches(), "Name contains reserved characters");
        this.zzacd = null;
        this.zzacc.put(str.toLowerCase(Locale.US), str2);
        return this;
    }

    static boolean zzae(String str) {
        return zzaby.matcher(str).matches();
    }

    public final String zzft() {
        String str = this.zzacd;
        if (str != null) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.type);
        sb.append('/');
        sb.append(this.zzacb);
        SortedMap<String, String> sortedMap = this.zzacc;
        if (sortedMap != null) {
            for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
                String value = entry.getValue();
                sb.append("; ");
                sb.append(entry.getKey());
                sb.append("=");
                if (!zzae(value)) {
                    String replace = value.replace("\\", "\\\\").replace("\"", "\\\"");
                    StringBuilder sb2 = new StringBuilder(String.valueOf(replace).length() + 2);
                    sb2.append("\"");
                    sb2.append(replace);
                    sb2.append("\"");
                    value = sb2.toString();
                }
                sb.append(value);
            }
        }
        String sb3 = sb.toString();
        this.zzacd = sb3;
        return sb3;
    }

    public final String toString() {
        return zzft();
    }

    private final boolean zza(zzgz zzgz) {
        return zzgz != null && this.type.equalsIgnoreCase(zzgz.type) && this.zzacb.equalsIgnoreCase(zzgz.zzacb);
    }

    public static boolean zzb(String str, String str2) {
        return str2 != null && new zzgz(str).zza(new zzgz(str2));
    }

    public final zzgz zza(Charset charset) {
        zza("charset", charset == null ? null : charset.name());
        return this;
    }

    public final Charset zzfu() {
        String str = this.zzacc.get("charset".toLowerCase(Locale.US));
        if (str == null) {
            return null;
        }
        return Charset.forName(str);
    }

    public final int hashCode() {
        return zzft().hashCode();
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzgz)) {
            return false;
        }
        zzgz zzgz = (zzgz) obj;
        if (!zza(zzgz) || !this.zzacc.equals(zzgz.zzacc)) {
            return false;
        }
        return true;
    }

    static {
        StringBuilder sb = new StringBuilder("[^\\s/=;\"]+".length() + 14 + "[^\\s/=;\"]+".length() + ";.*".length());
        sb.append("\\s*(");
        sb.append("[^\\s/=;\"]+");
        sb.append(")/(");
        sb.append("[^\\s/=;\"]+");
        sb.append(")\\s*(");
        sb.append(";.*");
        sb.append(")?");
        zzabz = Pattern.compile(sb.toString(), 32);
        StringBuilder sb2 = new StringBuilder("\"([^\"]*)\"".length() + 1 + "[^\\s;\"]*".length());
        sb2.append("\"([^\"]*)\"");
        sb2.append("|");
        sb2.append("[^\\s;\"]*");
        String sb3 = sb2.toString();
        StringBuilder sb4 = new StringBuilder("[^\\s/=;\"]+".length() + 12 + String.valueOf(sb3).length());
        sb4.append("\\s*;\\s*(");
        sb4.append("[^\\s/=;\"]+");
        sb4.append(")=(");
        sb4.append(sb3);
        sb4.append(")");
        zzaca = Pattern.compile(sb4.toString());
    }
}
