package com.google.android.gms.internal.firebase_ml;

import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public enum zzaaz implements zzxc {
    ANY_EXECUTION_PREFERENCE(0),
    LOW_LATENCY(1),
    LOW_POWER(2),
    FORCE_CPU(3);
    
    private static final zzxf<zzaaz> zzac = new zzaay();
    private static final zzaaz zzcsw;
    private final int value;

    @Override // com.google.android.gms.internal.firebase_ml.zzxc
    public final int zzd() {
        return this.value;
    }

    public static zzaaz zzeh(int i) {
        if (i == 0) {
            return ANY_EXECUTION_PREFERENCE;
        }
        if (i == 1) {
            return LOW_LATENCY;
        }
        if (i == 2) {
            return LOW_POWER;
        }
        if (i != 3) {
            return null;
        }
        return FORCE_CPU;
    }

    public static zzxe zzf() {
        return zzaba.zzan;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
    }

    private zzaaz(int i) {
        this.value = i;
    }

    static {
        zzaaz zzaaz;
        zzcsw = zzaaz;
    }
}
