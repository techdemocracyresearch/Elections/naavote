package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzyv implements zzys {
    zzyv() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzys
    public final Object newInstance(Object obj) {
        return ((zzwz) obj).zza(zzwz.zzg.zzclw, (Object) null, (Object) null);
    }
}
