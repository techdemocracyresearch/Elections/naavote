package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzrx implements zzhe {
    zzrx(zzrt zzrt) {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhe
    public final void zza(zzhc zzhc) throws IOException {
        zzhc.zzae(10000);
        zzhc.zzad(5000);
    }
}
