package com.google.firebase.ml.vision.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.Image;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Pair;
import com.facebook.imagepipeline.common.RotationOptions;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzsc;
import com.google.android.gms.internal.firebase_ml.zzsd;
import com.google.android.gms.vision.Frame;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionImage {
    private static final zzsc zzbrr = zzsc.zzqp();
    private volatile Bitmap zzbrs;
    private volatile ByteBuffer zzbrt;
    private volatile FirebaseVisionImageMetadata zzbru;
    private volatile Frame zzbrv;
    private volatile byte[] zzbrw;
    private final long zzbrx;

    public static FirebaseVisionImage fromByteBuffer(ByteBuffer byteBuffer, FirebaseVisionImageMetadata firebaseVisionImageMetadata) {
        return new FirebaseVisionImage(byteBuffer, firebaseVisionImageMetadata);
    }

    public static FirebaseVisionImage fromByteArray(byte[] bArr, FirebaseVisionImageMetadata firebaseVisionImageMetadata) {
        return new FirebaseVisionImage(bArr, firebaseVisionImageMetadata);
    }

    public static FirebaseVisionImage fromBitmap(Bitmap bitmap) {
        return new FirebaseVisionImage(bitmap);
    }

    public static FirebaseVisionImage fromMediaImage(Image image, int i) {
        Preconditions.checkNotNull(image, "Please provide a valid image");
        Preconditions.checkArgument(image.getFormat() == 256 || image.getFormat() == 35, "Only JPEG and YUV_420_888 are supported now");
        Image.Plane[] planes = image.getPlanes();
        if (image.getFormat() != 256) {
            return new FirebaseVisionImage(zzsc.zza(planes, image.getWidth(), image.getHeight()), new FirebaseVisionImageMetadata.Builder().setFormat(17).setWidth(image.getWidth()).setHeight(image.getHeight()).setRotation(i).build());
        }
        if (planes == null || planes.length != 1) {
            throw new IllegalArgumentException("Unexpected image format, JPEG should have exactly 1 image plane");
        }
        ByteBuffer buffer = planes[0].getBuffer();
        int remaining = buffer.remaining();
        byte[] bArr = new byte[remaining];
        buffer.get(bArr);
        if (i == 0) {
            return new FirebaseVisionImage(bArr);
        }
        return new FirebaseVisionImage(zza(BitmapFactory.decodeByteArray(bArr, 0, remaining), i));
    }

    public static FirebaseVisionImage fromFilePath(Context context, Uri uri) throws IOException {
        Preconditions.checkNotNull(context, "Please provide a valid Context");
        Preconditions.checkNotNull(uri, "Please provide a valid imageUri");
        zzsd.zzqq();
        return new FirebaseVisionImage(zzsd.zza(context.getContentResolver(), uri));
    }

    private FirebaseVisionImage(ByteBuffer byteBuffer, FirebaseVisionImageMetadata firebaseVisionImageMetadata) {
        this.zzbrx = SystemClock.elapsedRealtime();
        this.zzbrt = (ByteBuffer) Preconditions.checkNotNull(byteBuffer);
        this.zzbru = (FirebaseVisionImageMetadata) Preconditions.checkNotNull(firebaseVisionImageMetadata);
    }

    private FirebaseVisionImage(byte[] bArr, FirebaseVisionImageMetadata firebaseVisionImageMetadata) {
        this(ByteBuffer.wrap((byte[]) Preconditions.checkNotNull(bArr)), firebaseVisionImageMetadata);
    }

    private FirebaseVisionImage(Bitmap bitmap) {
        this.zzbrx = SystemClock.elapsedRealtime();
        this.zzbrs = (Bitmap) Preconditions.checkNotNull(bitmap);
    }

    private FirebaseVisionImage(byte[] bArr) {
        this.zzbrx = SystemClock.elapsedRealtime();
        this.zzbrw = (byte[]) Preconditions.checkNotNull(bArr);
    }

    public Bitmap getBitmap() {
        return zzqm();
    }

    private final Bitmap zzqm() {
        if (this.zzbrs != null) {
            return this.zzbrs;
        }
        synchronized (this) {
            if (this.zzbrs == null) {
                byte[] zzau = zzau(false);
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(zzau, 0, zzau.length);
                if (this.zzbru != null) {
                    decodeByteArray = zza(decodeByteArray, this.zzbru.getRotation());
                }
                this.zzbrs = decodeByteArray;
            }
        }
        return this.zzbrs;
    }

    private static Bitmap zza(Bitmap bitmap, int i) {
        int i2;
        if (i == 0) {
            i2 = 0;
        } else if (i == 1) {
            i2 = 90;
        } else if (i == 2) {
            i2 = RotationOptions.ROTATE_180;
        } else if (i == 3) {
            i2 = RotationOptions.ROTATE_270;
        } else {
            StringBuilder sb = new StringBuilder(29);
            sb.append("Invalid rotation: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        if (i2 == 0) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate((float) i2);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public final Pair<byte[], Float> zze(int i, int i2) {
        int i3;
        int i4;
        byte[] bArr;
        if (this.zzbru != null) {
            boolean z = this.zzbru.getRotation() == 1 || this.zzbru.getRotation() == 3;
            FirebaseVisionImageMetadata firebaseVisionImageMetadata = this.zzbru;
            i3 = z ? firebaseVisionImageMetadata.getHeight() : firebaseVisionImageMetadata.getWidth();
            i4 = z ? this.zzbru.getWidth() : this.zzbru.getHeight();
        } else {
            i3 = zzqm().getWidth();
            i4 = zzqm().getHeight();
        }
        float min = Math.min(((float) i) / ((float) i3), ((float) i2) / ((float) i4));
        if (min < 1.0f) {
            Bitmap zzqm = zzqm();
            Matrix matrix = new Matrix();
            matrix.postScale(min, min);
            bArr = zzsc.zza(Bitmap.createBitmap(zzqm, 0, 0, this.zzbrs.getWidth(), this.zzbrs.getHeight(), matrix, true));
        } else {
            bArr = zzau(true);
            min = 1.0f;
        }
        return Pair.create(bArr, Float.valueOf(min));
    }

    private final byte[] zzau(boolean z) {
        if (this.zzbrw != null) {
            return this.zzbrw;
        }
        synchronized (this) {
            if (this.zzbrw != null) {
                return this.zzbrw;
            } else if (this.zzbrt == null || (z && this.zzbru.getRotation() != 0)) {
                byte[] zza = zzsc.zza(zzqm());
                this.zzbrw = zza;
                return zza;
            } else {
                byte[] zza2 = zzsc.zza(this.zzbrt);
                int format = this.zzbru.getFormat();
                if (format != 17) {
                    if (format == 842094169) {
                        zza2 = zzsc.zzf(zza2);
                    } else {
                        throw new IllegalStateException("Must be one of: IMAGE_FORMAT_NV21, IMAGE_FORMAT_YV12");
                    }
                }
                byte[] zza3 = zzsc.zza(zza2, this.zzbru.getWidth(), this.zzbru.getHeight());
                if (this.zzbru.getRotation() == 0) {
                    this.zzbrw = zza3;
                }
                return zza3;
            }
        }
    }

    public final void zzqn() {
        if (this.zzbrt != null) {
            ByteBuffer byteBuffer = this.zzbrt;
            ByteBuffer allocate = ByteBuffer.allocate(byteBuffer.capacity());
            byteBuffer.rewind();
            allocate.put(byteBuffer);
            byteBuffer.rewind();
            allocate.flip();
            this.zzbrt = allocate;
        }
    }

    public final synchronized Frame zza(boolean z, boolean z2) {
        int i = 0;
        Preconditions.checkArgument(!z || !z2, "Can't restrict to bitmap-only and NV21 byte buffer-only");
        if (this.zzbrv == null) {
            Frame.Builder builder = new Frame.Builder();
            if (this.zzbrt == null || z) {
                builder.setBitmap(zzqm());
            } else {
                int i2 = FirebaseVisionImageMetadata.IMAGE_FORMAT_YV12;
                if (z2 && this.zzbru.getFormat() != 17) {
                    if (this.zzbru.getFormat() == 842094169) {
                        this.zzbrt = ByteBuffer.wrap(zzsc.zzf(zzsc.zza(this.zzbrt)));
                        this.zzbru = new FirebaseVisionImageMetadata.Builder().setFormat(17).setWidth(this.zzbru.getWidth()).setHeight(this.zzbru.getHeight()).setRotation(this.zzbru.getRotation()).build();
                    } else {
                        throw new IllegalStateException("Must be one of: IMAGE_FORMAT_NV21, IMAGE_FORMAT_YV12");
                    }
                }
                ByteBuffer byteBuffer = this.zzbrt;
                int width = this.zzbru.getWidth();
                int height = this.zzbru.getHeight();
                int format = this.zzbru.getFormat();
                if (format == 17) {
                    i2 = 17;
                } else if (format != 842094169) {
                    i2 = 0;
                }
                builder.setImageData(byteBuffer, width, height, i2);
                int rotation = this.zzbru.getRotation();
                if (rotation != 0) {
                    if (rotation == 1) {
                        i = 1;
                    } else if (rotation == 2) {
                        i = 2;
                    } else if (rotation == 3) {
                        i = 3;
                    } else {
                        StringBuilder sb = new StringBuilder(29);
                        sb.append("Invalid rotation: ");
                        sb.append(rotation);
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                builder.setRotation(i);
            }
            builder.setTimestampMillis(this.zzbrx);
            this.zzbrv = builder.build();
        }
        return this.zzbrv;
    }
}
