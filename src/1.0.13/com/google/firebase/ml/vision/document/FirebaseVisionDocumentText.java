package com.google.firebase.ml.vision.document;

import android.graphics.Rect;
import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import com.google.android.gms.internal.firebase_ml.zzkm;
import com.google.android.gms.internal.firebase_ml.zzkt;
import com.google.android.gms.internal.firebase_ml.zzle;
import com.google.android.gms.internal.firebase_ml.zzlf;
import com.google.android.gms.internal.firebase_ml.zzlk;
import com.google.android.gms.internal.firebase_ml.zzll;
import com.google.android.gms.internal.firebase_ml.zzln;
import com.google.android.gms.internal.firebase_ml.zzlq;
import com.google.android.gms.internal.firebase_ml.zzms;
import com.google.android.gms.internal.firebase_ml.zzmw;
import com.google.android.gms.internal.firebase_ml.zzrq;
import com.google.android.gms.internal.firebase_ml.zzsy;
import com.google.firebase.ml.vision.text.RecognizedLanguage;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionDocumentText {
    private static final FirebaseVisionDocumentText zzbsl = new FirebaseVisionDocumentText("", new ArrayList());
    private final List<Block> blocks;
    private final String text;

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Block extends DocumentTextBase {
        private final List<Paragraph> paragraphs;

        public List<Paragraph> getParagraphs() {
            return this.paragraphs;
        }

        private Block(List<RecognizedLanguage> list, RecognizedBreak recognizedBreak, Rect rect, List<Paragraph> list2, String str, Float f) {
            super(list, recognizedBreak, rect, str, f);
            this.paragraphs = list2;
        }

        /* access modifiers changed from: private */
        public static Block zza(zzkm zzkm, float f) {
            ArrayList arrayList = new ArrayList();
            StringBuilder sb = new StringBuilder();
            List<RecognizedLanguage> zze = zzsy.zze(zzkm.zzim());
            if (zzkm.getParagraphs() != null) {
                for (zzle zzle : zzkm.getParagraphs()) {
                    if (zzle != null) {
                        Paragraph zza = Paragraph.zza(zzle, f);
                        if (sb.length() != 0) {
                            sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                        }
                        sb.append(zza.getText());
                        arrayList.add(zza);
                    }
                }
            }
            return new Block(zze, new RecognizedBreak(), zzrq.zza(zzkm.zzil(), f), arrayList, sb.toString(), zzkm.getConfidence());
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Float getConfidence() {
            return super.getConfidence();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ RecognizedBreak getRecognizedBreak() {
            return super.getRecognizedBreak();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ List getRecognizedLanguages() {
            return super.getRecognizedLanguages();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Rect getBoundingBox() {
            return super.getBoundingBox();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ String getText() {
            return super.getText();
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Paragraph extends DocumentTextBase {
        private final List<Word> words;

        public List<Word> getWords() {
            return this.words;
        }

        private Paragraph(List<RecognizedLanguage> list, RecognizedBreak recognizedBreak, Rect rect, List<Word> list2, String str, Float f) {
            super(list, recognizedBreak, rect, str, f);
            this.words = list2;
        }

        /* access modifiers changed from: private */
        public static Paragraph zza(zzle zzle, float f) {
            ArrayList arrayList = new ArrayList();
            StringBuilder sb = new StringBuilder();
            List<RecognizedLanguage> zze = zzsy.zze(zzle.zzim());
            if (zzle.getWords() != null) {
                for (zzlq zzlq : zzle.getWords()) {
                    if (zzlq != null) {
                        Word zza = Word.zza(zzlq, f);
                        sb.append(zza.getText());
                        sb.append(FirebaseVisionDocumentText.zza(zza.getRecognizedBreak()));
                        arrayList.add(zza);
                    }
                }
            }
            return new Paragraph(zze, new RecognizedBreak(), zzrq.zza(zzle.zzil(), f), arrayList, sb.toString(), zzle.getConfidence());
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Float getConfidence() {
            return super.getConfidence();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ RecognizedBreak getRecognizedBreak() {
            return super.getRecognizedBreak();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ List getRecognizedLanguages() {
            return super.getRecognizedLanguages();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Rect getBoundingBox() {
            return super.getBoundingBox();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ String getText() {
            return super.getText();
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class RecognizedBreak {
        public static final int EOL_SURE_SPACE = 3;
        public static final int HYPHEN = 4;
        public static final int LINE_BREAK = 5;
        public static final int SPACE = 1;
        public static final int SURE_SPACE = 2;
        public static final int UNKNOWN = 0;
        private final int type;
        private final boolean zzbso;

        @Retention(RetentionPolicy.CLASS)
        /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
        public @interface BreakType {
        }

        public int getDetectedBreakType() {
            return this.type;
        }

        public boolean getIsPrefix() {
            return this.zzbso;
        }

        private RecognizedBreak(int i, boolean z) {
            this.type = i;
            this.zzbso = z;
        }

        /* access modifiers changed from: private */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x007a  */
        public static RecognizedBreak zzc(zzln zzln) {
            if (zzln == null || zzln.zzix() == null) {
                return null;
            }
            int i = 4;
            boolean z = false;
            if (zzln.zzix().getType() != null) {
                String type2 = zzln.zzix().getType();
                type2.hashCode();
                char c = 65535;
                switch (type2.hashCode()) {
                    case -1651884996:
                        if (type2.equals("SURE_SPACE")) {
                            c = 0;
                            break;
                        }
                        break;
                    case -1571028039:
                        if (type2.equals("EOL_SURE_SPACE")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 79100134:
                        if (type2.equals("SPACE")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1541383380:
                        if (type2.equals("LINE_BREAK")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 2145946930:
                        if (type2.equals("HYPHEN")) {
                            c = 4;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        i = 2;
                        break;
                    case 1:
                        i = 3;
                        break;
                    case 2:
                        i = 1;
                        break;
                    case 3:
                        i = 5;
                        break;
                }
                if (zzln.zzix().zzip() != null) {
                    z = zzln.zzix().zzip().booleanValue();
                }
                return new RecognizedBreak(i, z);
            }
            i = 0;
            if (zzln.zzix().zzip() != null) {
            }
            return new RecognizedBreak(i, z);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Word extends DocumentTextBase {
        private final List<Symbol> symbols;

        public List<Symbol> getSymbols() {
            return this.symbols;
        }

        private Word(List<RecognizedLanguage> list, RecognizedBreak recognizedBreak, Rect rect, List<Symbol> list2, String str, Float f) {
            super(list, recognizedBreak, rect, str, f);
            this.symbols = list2;
        }

        /* access modifiers changed from: private */
        public static Word zza(zzlq zzlq, float f) {
            ArrayList arrayList = new ArrayList();
            StringBuilder sb = new StringBuilder();
            List<RecognizedLanguage> zze = zzsy.zze(zzlq.zzim());
            RecognizedBreak recognizedBreak = null;
            if (zzlq.getSymbols() != null) {
                for (zzll zzll : zzlq.getSymbols()) {
                    if (zzll != null) {
                        Symbol zza = Symbol.zza(zzll, f);
                        RecognizedBreak recognizedBreak2 = zza.getRecognizedBreak();
                        sb.append(zza.getText());
                        arrayList.add(Symbol.zza(zzll, f));
                        recognizedBreak = recognizedBreak2;
                    }
                }
            }
            return new Word(zze, recognizedBreak, zzrq.zza(zzlq.zzil(), f), arrayList, sb.toString(), zzlq.getConfidence());
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Float getConfidence() {
            return super.getConfidence();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ RecognizedBreak getRecognizedBreak() {
            return super.getRecognizedBreak();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ List getRecognizedLanguages() {
            return super.getRecognizedLanguages();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Rect getBoundingBox() {
            return super.getBoundingBox();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ String getText() {
            return super.getText();
        }
    }

    public List<Block> getBlocks() {
        return this.blocks;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Symbol extends DocumentTextBase {
        private Symbol(List<RecognizedLanguage> list, RecognizedBreak recognizedBreak, Rect rect, String str, Float f) {
            super(list, recognizedBreak, rect, str, f);
        }

        /* access modifiers changed from: private */
        public static Symbol zza(zzll zzll, float f) {
            return new Symbol(FirebaseVisionDocumentText.zza(zzll.zzim()), RecognizedBreak.zzc(zzll.zzim()), zzrq.zza(zzll.zzil(), f), zzrq.zzcd(zzll.getText()), zzll.getConfidence());
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Float getConfidence() {
            return super.getConfidence();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ RecognizedBreak getRecognizedBreak() {
            return super.getRecognizedBreak();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ List getRecognizedLanguages() {
            return super.getRecognizedLanguages();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ Rect getBoundingBox() {
            return super.getBoundingBox();
        }

        @Override // com.google.firebase.ml.vision.document.FirebaseVisionDocumentText.DocumentTextBase
        public /* bridge */ /* synthetic */ String getText() {
            return super.getText();
        }
    }

    public String getText() {
        return zzms.zzbb(this.text);
    }

    private FirebaseVisionDocumentText(String str, List<Block> list) {
        this.text = str;
        this.blocks = list;
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class DocumentTextBase {
        private final Float confidence;
        private final String text;
        private final Rect zzbrq;
        private final List<RecognizedLanguage> zzbsm;
        private final RecognizedBreak zzbsn;

        DocumentTextBase(List<RecognizedLanguage> list, RecognizedBreak recognizedBreak, Rect rect, String str, Float f) {
            this.text = str;
            this.zzbsm = list;
            this.zzbsn = recognizedBreak;
            this.zzbrq = rect;
            this.confidence = f;
        }

        public String getText() {
            return this.text;
        }

        public Rect getBoundingBox() {
            return this.zzbrq;
        }

        public List<RecognizedLanguage> getRecognizedLanguages() {
            return this.zzbsm;
        }

        public RecognizedBreak getRecognizedBreak() {
            return this.zzbsn;
        }

        public Float getConfidence() {
            return this.confidence;
        }
    }

    static FirebaseVisionDocumentText zza(zzlk zzlk, float f) {
        if (zzlk == null) {
            return zzbsl;
        }
        String zzcd = zzrq.zzcd(zzlk.getText());
        ArrayList arrayList = new ArrayList();
        if (zzlk.getPages() != null) {
            for (zzlf zzlf : zzlk.getPages()) {
                if (zzlf != null) {
                    for (zzkm zzkm : zzlf.getBlocks()) {
                        if (zzkm != null) {
                            arrayList.add(Block.zza(zzkm, f));
                        }
                    }
                }
            }
        }
        return new FirebaseVisionDocumentText(zzcd, arrayList);
    }

    /* access modifiers changed from: private */
    public static List<RecognizedLanguage> zza(zzln zzln) {
        if (zzln == null) {
            return zzmw.zzji();
        }
        ArrayList arrayList = new ArrayList();
        if (zzln.zziy() != null) {
            for (zzkt zzkt : zzln.zziy()) {
                RecognizedLanguage zza = RecognizedLanguage.zza(zzkt);
                if (zza != null) {
                    arrayList.add(zza);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public static String zza(RecognizedBreak recognizedBreak) {
        if (recognizedBreak == null) {
            return "";
        }
        int detectedBreakType = recognizedBreak.getDetectedBreakType();
        if (detectedBreakType == 1 || detectedBreakType == 2) {
            return " ";
        }
        if (detectedBreakType != 3) {
            if (detectedBreakType == 4) {
                return "-\n";
            }
            if (detectedBreakType != 5) {
                return "";
            }
        }
        return ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
    }
}
