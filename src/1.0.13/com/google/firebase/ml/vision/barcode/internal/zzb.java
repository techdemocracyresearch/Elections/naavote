package com.google.firebase.ml.vision.barcode.internal;

import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzqo;
import com.google.android.gms.internal.firebase_ml.zzsf;
import java.util.List;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final /* synthetic */ class zzb implements zzqo {
    private final long zzbok;
    private final zzoc zzbol;
    private final List zzbon;
    private final zzc zzbqp;
    private final List zzbqq;
    private final zzsf zzbqr;

    zzb(zzc zzc, long j, zzoc zzoc, List list, List list2, zzsf zzsf) {
        this.zzbqp = zzc;
        this.zzbok = j;
        this.zzbol = zzoc;
        this.zzbqq = list;
        this.zzbon = list2;
        this.zzbqr = zzsf;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqo
    public final zzns.zzad.zza zzok() {
        return this.zzbqp.zza(this.zzbok, this.zzbol, this.zzbqq, this.zzbon, this.zzbqr);
    }
}
