package com.google.firebase.ml.vision.text;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionCloudTextRecognizerOptions {
    public static final int DENSE_MODEL = 2;
    public static final int SPARSE_MODEL = 1;
    private final boolean zzbra;
    private final List<String> zzbsj;
    private final int zzbus;

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface CloudTextModelType {
    }

    public List<String> getHintedLanguages() {
        return this.zzbsj;
    }

    public final boolean isEnforceCertFingerprintMatch() {
        return this.zzbra;
    }

    public int getModelType() {
        return this.zzbus;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private boolean zzbra = false;
        private List<String> zzbsj = new ArrayList();
        private int zzbus = 1;

        public Builder setLanguageHints(List<String> list) {
            Preconditions.checkNotNull(list, "Provided hinted languages can not be null");
            this.zzbsj = list;
            Collections.sort(list);
            return this;
        }

        public Builder setModelType(int i) {
            boolean z = true;
            if (!(i == 1 || i == 2)) {
                z = false;
            }
            Preconditions.checkArgument(z, "modelType should be either SPARSE_MODEL or DENSE_MODEL");
            this.zzbus = i;
            return this;
        }

        public Builder enforceCertFingerprintMatch() {
            this.zzbra = true;
            return this;
        }

        public FirebaseVisionCloudTextRecognizerOptions build() {
            return new FirebaseVisionCloudTextRecognizerOptions(this.zzbsj, this.zzbus, this.zzbra);
        }
    }

    private FirebaseVisionCloudTextRecognizerOptions(List<String> list, int i, boolean z) {
        Preconditions.checkNotNull(list, "Provided hinted languages can not be null");
        this.zzbsj = list;
        this.zzbus = i;
        this.zzbra = z;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionCloudTextRecognizerOptions)) {
            return false;
        }
        FirebaseVisionCloudTextRecognizerOptions firebaseVisionCloudTextRecognizerOptions = (FirebaseVisionCloudTextRecognizerOptions) obj;
        return this.zzbsj.equals(firebaseVisionCloudTextRecognizerOptions.getHintedLanguages()) && this.zzbus == firebaseVisionCloudTextRecognizerOptions.zzbus && this.zzbra == firebaseVisionCloudTextRecognizerOptions.zzbra;
    }

    public int hashCode() {
        return Objects.hashCode(this.zzbsj, Integer.valueOf(this.zzbus), Boolean.valueOf(this.zzbra));
    }
}
