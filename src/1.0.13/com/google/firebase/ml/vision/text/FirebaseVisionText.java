package com.google.firebase.ml.vision.text;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.util.SparseArray;
import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzmw;
import com.google.android.gms.vision.text.Text;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionText {
    public static final FirebaseVisionText zzbut = new FirebaseVisionText("", new ArrayList());
    private final String text;
    private final List<TextBlock> zzbuu;

    public List<TextBlock> getTextBlocks() {
        return Collections.unmodifiableList(this.zzbuu);
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Element extends TextBase {
        Element(com.google.android.gms.vision.text.Element element) {
            super(element);
        }

        public Element(String str, Rect rect, List<RecognizedLanguage> list, Float f) {
            super(str, rect, list, f);
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ List getRecognizedLanguages() {
            return super.getRecognizedLanguages();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Float getConfidence() {
            return super.getConfidence();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ String getText() {
            return super.getText();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Point[] getCornerPoints() {
            return super.getCornerPoints();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Rect getBoundingBox() {
            return super.getBoundingBox();
        }
    }

    public String getText() {
        return this.text;
    }

    public FirebaseVisionText(SparseArray<com.google.android.gms.vision.text.TextBlock> sparseArray) {
        this.zzbuu = new ArrayList();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sparseArray.size(); i++) {
            com.google.android.gms.vision.text.TextBlock textBlock = sparseArray.get(sparseArray.keyAt(i));
            if (textBlock != null) {
                TextBlock textBlock2 = new TextBlock(textBlock);
                this.zzbuu.add(textBlock2);
                if (sb.length() != 0) {
                    sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                }
                if (textBlock.getValue() != null) {
                    sb.append(textBlock2.getText());
                }
            }
        }
        this.text = sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class TextBase {
        private final Float confidence;
        private final Point[] cornerPoints;
        private final String text;
        private final Rect zzbrq;
        private final List<RecognizedLanguage> zzbsm;

        TextBase(Text text2) {
            Preconditions.checkNotNull(text2, "Text to construct FirebaseVisionText classes can't be null");
            this.confidence = null;
            this.text = text2.getValue();
            this.zzbrq = text2.getBoundingBox();
            this.cornerPoints = text2.getCornerPoints();
            this.zzbsm = zzmw.zzji();
        }

        private TextBase(String str, Rect rect, List<RecognizedLanguage> list, Float f) {
            Preconditions.checkNotNull(str, "Text string cannot be null");
            Preconditions.checkNotNull(list, "Text languages cannot be null");
            this.confidence = f;
            this.cornerPoints = null;
            this.text = str;
            this.zzbrq = rect;
            this.zzbsm = list;
        }

        public Rect getBoundingBox() {
            return this.zzbrq;
        }

        public Point[] getCornerPoints() {
            return this.cornerPoints;
        }

        public String getText() {
            String str = this.text;
            return str == null ? "" : str;
        }

        public Float getConfidence() {
            return this.confidence;
        }

        public List<RecognizedLanguage> getRecognizedLanguages() {
            return this.zzbsm;
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Line extends TextBase {
        private final List<Element> zzbuv;

        Line(com.google.android.gms.vision.text.Line line) {
            super(line);
            this.zzbuv = new ArrayList();
            for (Text text : line.getComponents()) {
                if (text instanceof com.google.android.gms.vision.text.Element) {
                    this.zzbuv.add(new Element((com.google.android.gms.vision.text.Element) text));
                } else {
                    Log.e("FirebaseVisionText", "A subcomponent of line is should be an element!");
                }
            }
        }

        public Line(String str, Rect rect, List<RecognizedLanguage> list, List<Element> list2, Float f) {
            super(str, rect, list, f);
            this.zzbuv = list2;
        }

        public synchronized List<Element> getElements() {
            return this.zzbuv;
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ List getRecognizedLanguages() {
            return super.getRecognizedLanguages();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Float getConfidence() {
            return super.getConfidence();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ String getText() {
            return super.getText();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Point[] getCornerPoints() {
            return super.getCornerPoints();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Rect getBoundingBox() {
            return super.getBoundingBox();
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class TextBlock extends TextBase {
        private final List<Line> zzbuw;

        TextBlock(com.google.android.gms.vision.text.TextBlock textBlock) {
            super(textBlock);
            this.zzbuw = new ArrayList();
            for (Text text : textBlock.getComponents()) {
                if (text instanceof com.google.android.gms.vision.text.Line) {
                    this.zzbuw.add(new Line((com.google.android.gms.vision.text.Line) text));
                } else {
                    Log.e("FirebaseVisionText", "A subcomponent of textblock is should be a line!");
                }
            }
        }

        public TextBlock(String str, Rect rect, List<RecognizedLanguage> list, List<Line> list2, Float f) {
            super(str, rect, list, f);
            this.zzbuw = list2;
        }

        public synchronized List<Line> getLines() {
            return this.zzbuw;
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ List getRecognizedLanguages() {
            return super.getRecognizedLanguages();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Float getConfidence() {
            return super.getConfidence();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ String getText() {
            return super.getText();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Point[] getCornerPoints() {
            return super.getCornerPoints();
        }

        @Override // com.google.firebase.ml.vision.text.FirebaseVisionText.TextBase
        public /* bridge */ /* synthetic */ Rect getBoundingBox() {
            return super.getBoundingBox();
        }
    }

    public FirebaseVisionText(String str, List<TextBlock> list) {
        ArrayList arrayList = new ArrayList();
        this.zzbuu = arrayList;
        this.text = str;
        arrayList.addAll(list);
    }
}
