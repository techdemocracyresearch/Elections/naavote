package com.google.firebase.ml.vision.automl.internal;

import com.google.firebase.ml.vision.automl.FirebaseAutoMLRemoteModel;
import java.util.concurrent.Callable;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zza implements Callable {
    private final zzb zzbps;
    private final FirebaseAutoMLRemoteModel zzbpt;

    zza(zzb zzb, FirebaseAutoMLRemoteModel firebaseAutoMLRemoteModel) {
        this.zzbps = zzb;
        this.zzbpt = firebaseAutoMLRemoteModel;
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        return this.zzbps.zzb(this.zzbpt);
    }
}
