package com.google.firebase.ml.vision.objects;

import android.graphics.Rect;
import com.google.android.gms.common.internal.Preconditions;
import com.google.firebase.ml.vision.objects.internal.zzj;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionObject {
    public static final int CATEGORY_FASHION_GOOD = 2;
    public static final int CATEGORY_FOOD = 3;
    public static final int CATEGORY_HOME_GOOD = 1;
    public static final int CATEGORY_PLACE = 4;
    public static final int CATEGORY_PLANT = 5;
    public static final int CATEGORY_UNKNOWN = 0;
    private final int category;
    private final Float confidence;
    private final Rect zzbrq;
    private final Integer zzbud;

    @Retention(RetentionPolicy.CLASS)
    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public @interface Category {
    }

    public FirebaseVisionObject(zzj zzj) {
        Preconditions.checkNotNull(zzj, "Returned Object Detector Parcel can not be null");
        int[] iArr = zzj.zzbur;
        Preconditions.checkArgument(iArr.length == 4);
        this.zzbrq = new Rect(iArr[0], iArr[1], iArr[2], iArr[3]);
        this.zzbud = zzj.zzbud;
        this.confidence = zzj.confidence;
        this.category = zzj.category;
    }

    public int getClassificationCategory() {
        return this.category;
    }

    public Rect getBoundingBox() {
        return this.zzbrq;
    }

    public Integer getTrackingId() {
        return this.zzbud;
    }

    public Float getClassificationConfidence() {
        return this.confidence;
    }
}
