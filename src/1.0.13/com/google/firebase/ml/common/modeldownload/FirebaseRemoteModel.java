package com.google.firebase.ml.common.modeldownload;

import android.text.TextUtils;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import java.util.EnumMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class FirebaseRemoteModel {
    private static final Map<BaseModel, String> zzbnc;
    private static final Map<BaseModel, String> zzbnd;
    private final String zzbkn;
    private String zzbmm;
    private final BaseModel zzbnb;

    protected FirebaseRemoteModel(String str, BaseModel baseModel) {
        Preconditions.checkArgument(TextUtils.isEmpty(str) != (baseModel != null) ? false : true, "One of cloud model name and base model cannot be empty");
        this.zzbkn = str;
        this.zzbnb = baseModel;
    }

    public String getModelNameForBackend() {
        String str = this.zzbkn;
        if (str != null) {
            return str;
        }
        return zzbnd.get(this.zzbnb);
    }

    public String getUniqueModelNameForPersist() {
        String str = this.zzbkn;
        if (str != null) {
            return str;
        }
        String valueOf = String.valueOf(zzbnd.get(this.zzbnb));
        return valueOf.length() != 0 ? "COM.GOOGLE.BASE_".concat(valueOf) : new String("COM.GOOGLE.BASE_");
    }

    public boolean isBaseModel() {
        return this.zzbnb != null;
    }

    public String getModelName() {
        return this.zzbkn;
    }

    public boolean baseModelHashMatches(String str) {
        BaseModel baseModel = this.zzbnb;
        if (baseModel == null) {
            return false;
        }
        return str.equals(zzbnc.get(baseModel));
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseRemoteModel)) {
            return false;
        }
        FirebaseRemoteModel firebaseRemoteModel = (FirebaseRemoteModel) obj;
        return Objects.equal(this.zzbkn, firebaseRemoteModel.zzbkn) && Objects.equal(this.zzbnb, firebaseRemoteModel.zzbnb);
    }

    public int hashCode() {
        return Objects.hashCode(this.zzbkn, this.zzbnb);
    }

    public void setModelHash(String str) {
        this.zzbmm = str;
    }

    public String getModelHash() {
        return this.zzbmm;
    }

    static {
        EnumMap enumMap = new EnumMap(BaseModel.class);
        zzbnc = enumMap;
        EnumMap enumMap2 = new EnumMap(BaseModel.class);
        zzbnd = enumMap2;
        enumMap2.put((Object) BaseModel.FACE_DETECTION, (Object) "face_detector_model_m41");
        enumMap2.put((Object) BaseModel.SMART_REPLY, (Object) "smart_reply_model_m41");
        enumMap2.put((Object) BaseModel.TRANSLATE, (Object) "translate_model_m41");
        enumMap.put((Object) BaseModel.FACE_DETECTION, (Object) "modelHash");
        enumMap.put((Object) BaseModel.SMART_REPLY, (Object) "smart_reply_model_hash");
        enumMap.put((Object) BaseModel.TRANSLATE, (Object) "modelHash");
    }
}
