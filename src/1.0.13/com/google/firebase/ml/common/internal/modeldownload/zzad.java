package com.google.firebase.ml.common.internal.modeldownload;

import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.util.IOUtils;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzad {
    private static final GmsLogger zzbin = new GmsLogger("RmModelInfoRetriever", "");

    static zzaa zza(zzqf zzqf, FirebaseRemoteModel firebaseRemoteModel, zzw zzw) throws FirebaseMLException {
        zzaa zzaa;
        if (firebaseRemoteModel.isBaseModel()) {
            zzaa = zzd.zza(firebaseRemoteModel, zzw);
        } else {
            zzaa = zzu.zza(zzqf, zzqf.zzoh(), firebaseRemoteModel, zzw);
        }
        if (zzaa != null) {
            zzw.zza(zzoc.NO_ERROR, false, zzaa.zzpm(), zzns.zzai.zza.MODEL_INFO_RETRIEVAL_SUCCEEDED);
        }
        return zzaa;
    }

    static HttpsURLConnection zza(String str, zzw zzw) throws FirebaseMLException {
        String str2;
        zzoc zzoc;
        String str3;
        if (str == null) {
            return null;
        }
        try {
            zzai zzai = new zzai(str);
            GmsLogger gmsLogger = zzbin;
            String valueOf = String.valueOf(str);
            gmsLogger.d("RmModelInfoRetriever", valueOf.length() != 0 ? "Checking model URL: ".concat(valueOf) : new String("Checking model URL: "));
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) zzai.openConnection();
            httpsURLConnection.setConnectTimeout(CredentialsApi.CREDENTIAL_PICKER_REQUEST_CODE);
            httpsURLConnection.connect();
            int responseCode = httpsURLConnection.getResponseCode();
            if (responseCode == 200 || responseCode == 304) {
                return httpsURLConnection;
            }
            if (responseCode == 408) {
                zzoc = zzoc.TIME_OUT_FETCHING_MODEL_METADATA;
            } else {
                zzoc = zzoc.MODEL_INFO_DOWNLOAD_UNSUCCESSFUL_HTTP_STATUS;
            }
            zzw.zza(zzoc, httpsURLConnection.getResponseCode());
            InputStream errorStream = httpsURLConnection.getErrorStream();
            if (errorStream == null) {
                str3 = "";
            } else {
                str3 = new String(IOUtils.readInputStreamFully(errorStream));
            }
            throw new FirebaseMLException(String.format(Locale.getDefault(), "Failed to connect to Firebase ML console server with HTTP status code: %d and error message: %s", Integer.valueOf(httpsURLConnection.getResponseCode()), str3), 13);
        } catch (SocketTimeoutException e) {
            zzw.zzo(zzoc.TIME_OUT_FETCHING_MODEL_METADATA);
            throw new FirebaseMLException("Failed to get model URL due to time out", 13, e);
        } catch (IOException e2) {
            zzoc zzoc2 = zzoc.MODEL_INFO_DOWNLOAD_CONNECTION_FAILED;
            if (e2 instanceof UnknownHostException) {
                zzoc2 = zzoc.NO_NETWORK_CONNECTION;
                str2 = "Failed to retrieve model info due to no internet connection.";
            } else {
                str2 = "Failed to get model URL";
            }
            zzw.zzo(zzoc2);
            throw new FirebaseMLException(str2, 13, e2);
        }
    }
}
