package com.google.firebase.ml.common.internal.modeldownload;

import android.net.Uri;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzaa {
    private final zzn zzbmh;
    private final String zzbmk;
    private final Uri zzbml;
    private final String zzbmm;

    public zzaa(String str, Uri uri, String str2, zzn zzn) {
        this.zzbmk = str;
        this.zzbml = uri;
        this.zzbmm = str2;
        this.zzbmh = zzn;
    }

    public final String zzpl() {
        return this.zzbmk;
    }

    public final String getModelHash() {
        return this.zzbmm;
    }

    public final zzn zzpm() {
        return this.zzbmh;
    }
}
