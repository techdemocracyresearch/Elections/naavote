package com.google.firebase.iid;

import com.google.android.gms.tasks.Task;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-iid@@20.1.5 */
public interface zzav {
    Task<InstanceIdResult> zza();
}
