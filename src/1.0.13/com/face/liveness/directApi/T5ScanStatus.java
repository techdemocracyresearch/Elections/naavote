package com.face.liveness.directApi;

public enum T5ScanStatus {
    PROCESSING,
    FINISHED
}
