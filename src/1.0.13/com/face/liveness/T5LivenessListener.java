package com.face.liveness;

public interface T5LivenessListener {
    void onError(int i, String str);

    void onSuccess(byte[] bArr, byte[] bArr2);
}
