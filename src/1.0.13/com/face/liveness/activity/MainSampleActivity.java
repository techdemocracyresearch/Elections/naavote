package com.face.liveness.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.face.liveness.LivenessController;
import com.face.liveness.R;
import com.face.liveness.T5LivenessListener;
import com.face.liveness.model.Tech5LivenessRequestModel;
import com.face.liveness.utils.Utils;
import java.io.File;
import java.io.FileOutputStream;

public class MainSampleActivity extends AppCompatActivity {
    private ImageView capturedImage;
    private CheckBox checkBox;
    private CheckBox checkSupportEmail;
    private CheckBox chkShowCaptureButton;
    private CheckBox chkShowImageInstructionDialog;
    private CheckBox chkShowInstructionDialog;
    private boolean cp = false;
    private ImageView livenessImageView;
    private RadioButton radioBackCamera;

    static {
        System.loadLibrary("t5liveness");
    }

    /* access modifiers changed from: protected */
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_sample_main);
        this.capturedImage = (ImageView) findViewById(R.id.capturedImage);
        this.livenessImageView = (ImageView) findViewById(R.id.livenessImage);
        this.radioBackCamera = (RadioButton) findViewById(R.id.radioBackCamera);
        this.checkBox = (CheckBox) findViewById(R.id.row_compress_check_chk);
        this.checkSupportEmail = (CheckBox) findViewById(R.id.chk_support_email);
        this.chkShowInstructionDialog = (CheckBox) findViewById(R.id.chk_show_instruction_dialog);
        this.chkShowImageInstructionDialog = (CheckBox) findViewById(R.id.chk_show_image_instruction_dialog);
        this.chkShowCaptureButton = (CheckBox) findViewById(R.id.chk_show_capture_button);
        ((TextView) findViewById(R.id.tv_build_version)).setText("v 1.6.17.1");
        this.checkBox.setOnClickListener(new View.OnClickListener() {
            /* class com.face.liveness.activity.MainSampleActivity.AnonymousClass1 */

            public void onClick(View view) {
                if (MainSampleActivity.this.checkBox.isChecked()) {
                    MainSampleActivity.this.cp = true;
                }
            }
        });
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.CAMERA", "android.permission.READ_PHONE_STATE"}, 101);
    }

    public void openCamera(View view) {
        int parseInt = Integer.parseInt(getInputRandom().getText().toString());
        int parseInt2 = Integer.parseInt(getInputTimeout().getText().toString());
        Integer.parseInt(getInputCaptureTimeout().getText().toString());
        int parseInt3 = !getInputChallengeStartTime().getText().toString().trim().isEmpty() ? Integer.parseInt(getInputChallengeStartTime().getText().toString()) : 0;
        LivenessController instance = LivenessController.getInstance(this, "eyJna2V5IjoicFJUUnA2VlwvR21xV0lBWDNPOE9KYjNuTlR0UzJXTjNlbENDYkFkZmFEVnZiaEEzSkFPazFlV0NNRER1MTFRWndGeEE0TmNWNjJ5U0dBU2xuRWp4Z2FPVEFjRlJrdnl0VjY1WlZoWDI4c0xhWlZWaWJhR3pSalY5eWdlYmNibUhhIiwiY24iOiJJQnJqM3dFQUFBQlJBQUFBQUFBQUFBQUFBQUFBQUFBQUJnQUFBQUFBQUFBPSJ9");
        String sDKVersion = instance.getSDKVersion();
        Log.i("version code", "sdkversion is " + sDKVersion);
        instance.detectLivenessFace(new Tech5LivenessRequestModel.Builder().setChallengesCount(parseInt).setChallengeTimeoutInSec(parseInt2).setChallengeStartCounterInSec(parseInt3).setIsUseBackCamera(this.radioBackCamera.isChecked()).setIsCompression(this.checkBox.isChecked()).setLanguageCode("en").setIsShowTextInstructions(this.chkShowInstructionDialog.isChecked()).setIsShowImageInstructions(this.chkShowImageInstructionDialog.isChecked()).setIsShowSelfieCaptureButton(this.chkShowCaptureButton.isChecked()).create(), new T5LivenessListener() {
            /* class com.face.liveness.activity.MainSampleActivity.AnonymousClass2 */

            @Override // com.face.liveness.T5LivenessListener
            public void onSuccess(byte[] bArr, byte[] bArr2) {
                try {
                    String str = "ENROLL_" + Utils.incrementUserEnrollCount(MainSampleActivity.this);
                    MainSampleActivity.this.capturedImage.setImageBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
                    MainSampleActivity.this.saveFaceImage(bArr, str, "Selfie");
                    if (bArr2 != null) {
                        MainSampleActivity.this.livenessImageView.setImageBitmap(BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length));
                        MainSampleActivity.this.saveFaceImage(bArr2, str, "Secret_Selfie");
                    }
                } catch (Exception e) {
                    Log.i("ERROR ", e.getMessage());
                }
            }

            @Override // com.face.liveness.T5LivenessListener
            public void onError(int i, String str) {
                Log.i("ERROR", "ERROR CODE ON FAIL Message=" + str);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainSampleActivity.this);
                builder.setTitle("Error Code=" + i).setMessage(str).setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    /* class com.face.liveness.activity.MainSampleActivity.AnonymousClass2.AnonymousClass1 */

                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void saveFaceImage(byte[] bArr, String str, String str2) {
        AsyncTask.execute(new Runnable(str, str2, bArr) {
            /* class com.face.liveness.activity.$$Lambda$MainSampleActivity$aftbeq5t0Dz6scsA31_sWgiLSQ */
            public final /* synthetic */ String f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ byte[] f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                MainSampleActivity.lambda$saveFaceImage$0(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    static /* synthetic */ void lambda$saveFaceImage$0(String str, String str2, byte[] bArr) {
        File file = new File(Utils.createExternalDirectory("LivenessImages"), str);
        file.mkdirs();
        try {
            File file2 = new File(file, str2 + "_" + Utils.getDateTime() + ".jpeg");
            if (file2.exists()) {
                file2.delete();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            fileOutputStream.write(bArr, 0, bArr.length);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] bArr, int i, int i2, int i3) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        options.inSampleSize = calculateInSampleSize(options, i2, i3);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
    }

    /* access modifiers changed from: protected */
    public int sizeOf(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT < 12) {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
        return bitmap.getByteCount();
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outHeight;
        int i4 = options.outWidth;
        int i5 = 1;
        if (i3 > i2 || i4 > i) {
            int i6 = i3 / 2;
            int i7 = i4 / 2;
            while (i6 / i5 >= i2 && i7 / i5 >= i) {
                i5 *= 2;
            }
        }
        return i5;
    }

    private EditText getInputNumTask() {
        return (EditText) findViewById(R.id.input_num_task);
    }

    private EditText getInputTimeout() {
        return (EditText) findViewById(R.id.input_timeout);
    }

    private EditText getInputCaptureTimeout() {
        return (EditText) findViewById(R.id.input_capture_timeout);
    }

    private EditText getInputChallengeStartTime() {
        return (EditText) findViewById(R.id.input_challenge_start_time);
    }

    private EditText getInputRandom() {
        return (EditText) findViewById(R.id.input_random);
    }

    private EditText getInputBlink() {
        return (EditText) findViewById(R.id.input_blink);
    }

    private EditText getInputNumSucceed() {
        return (EditText) findViewById(R.id.input_num_succeed);
    }

    private EditText getInputWaiting() {
        return (EditText) findViewById(R.id.input_waiting);
    }
}
