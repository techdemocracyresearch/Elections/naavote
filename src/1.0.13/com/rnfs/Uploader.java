package com.rnfs;

import android.os.AsyncTask;
import android.webkit.MimeTypeMap;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NoSuchKeyException;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class Uploader extends AsyncTask<UploadParams, int[], UploadResult> {
    private AtomicBoolean mAbort = new AtomicBoolean(false);
    private UploadParams mParams;
    private UploadResult res;

    /* access modifiers changed from: protected */
    public UploadResult doInBackground(UploadParams... uploadParamsArr) {
        this.mParams = uploadParamsArr[0];
        this.res = new UploadResult();
        new Thread(new Runnable() {
            /* class com.rnfs.Uploader.AnonymousClass1 */

            public void run() {
                try {
                    Uploader uploader = Uploader.this;
                    uploader.upload(uploader.mParams, Uploader.this.res);
                    Uploader.this.mParams.onUploadComplete.onUploadComplete(Uploader.this.res);
                } catch (Exception e) {
                    Uploader.this.res.exception = e;
                    Uploader.this.mParams.onUploadComplete.onUploadComplete(Uploader.this.res);
                }
            }
        }).start();
        return this.res;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x036c  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0371  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0376  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x037b  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01cb  */
    private void upload(UploadParams uploadParams, UploadResult uploadResult) throws Exception {
        HttpURLConnection httpURLConnection;
        BufferedReader bufferedReader;
        BufferedInputStream bufferedInputStream;
        DataOutputStream dataOutputStream;
        Throwable th;
        String str;
        HttpURLConnection httpURLConnection2;
        String[] strArr;
        String string;
        String string2;
        String mimeType;
        int i;
        String str2;
        String str3;
        Uploader uploader = this;
        String str4 = "filename";
        String str5 = "name";
        StringBuilder sb = new StringBuilder();
        String str6 = "\r\n";
        sb.append(str6);
        sb.append("--");
        sb.append("*****");
        sb.append("--");
        sb.append(str6);
        String sb2 = sb.toString();
        try {
            Object[] array = uploadParams.files.toArray();
            boolean z = uploadParams.binaryStreamOnly;
            HttpURLConnection httpURLConnection3 = (HttpURLConnection) uploadParams.src.openConnection();
            try {
                httpURLConnection3.setDoOutput(true);
                ReadableMapKeySetIterator keySetIterator = uploadParams.headers.keySetIterator();
                httpURLConnection3.setRequestMethod(uploadParams.method);
                if (!z) {
                    httpURLConnection3.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + "*****");
                }
                while (keySetIterator.hasNextKey()) {
                    String nextKey = keySetIterator.nextKey();
                    httpURLConnection3.setRequestProperty(nextKey, uploadParams.headers.getString(nextKey));
                }
                ReadableMapKeySetIterator keySetIterator2 = uploadParams.fields.keySetIterator();
                String str7 = "";
                while (keySetIterator2.hasNextKey()) {
                    try {
                        String nextKey2 = keySetIterator2.nextKey();
                        str7 = str7 + "--" + "*****" + str6 + "Content-Disposition: form-data; name=\"" + nextKey2 + "\"" + str6 + str6 + uploadParams.fields.getString(nextKey2) + str6;
                        httpURLConnection3 = httpURLConnection3;
                        keySetIterator2 = keySetIterator2;
                        sb2 = sb2;
                    } catch (Throwable th2) {
                        th = th2;
                        httpURLConnection = httpURLConnection3;
                        dataOutputStream = null;
                        bufferedInputStream = null;
                        bufferedReader = null;
                        if (httpURLConnection != null) {
                        }
                        if (dataOutputStream != null) {
                        }
                        if (bufferedInputStream != null) {
                        }
                        if (bufferedReader != null) {
                        }
                        throw th;
                    }
                }
                try {
                    String[] strArr2 = new String[array.length];
                    Iterator<ReadableMap> it = uploadParams.files.iterator();
                    String str8 = "" + str7;
                    int i2 = 0;
                    long j = 0;
                    while (true) {
                        str = "filepath";
                        if (!it.hasNext()) {
                            break;
                        }
                        try {
                            ReadableMap next = it.next();
                            try {
                                string = next.getString(str5);
                                string2 = next.getString(str4);
                                strArr = strArr2;
                                try {
                                    mimeType = next.getString("filetype");
                                } catch (NoSuchKeyException unused) {
                                    string = next.getString(str5);
                                    string2 = next.getString(str4);
                                    mimeType = uploader.getMimeType(next.getString(str));
                                    long length = new File(next.getString(str)).length();
                                    j += length;
                                    if (!z) {
                                    }
                                    str6 = str2;
                                    i2 = i + 1;
                                    str4 = str4;
                                    it = it;
                                    str5 = str5;
                                    strArr2 = strArr;
                                    uploader = this;
                                }
                            } catch (NoSuchKeyException unused2) {
                                strArr = strArr2;
                                string = next.getString(str5);
                                string2 = next.getString(str4);
                                mimeType = uploader.getMimeType(next.getString(str));
                                long length2 = new File(next.getString(str)).length();
                                j += length2;
                                if (!z) {
                                }
                                str6 = str2;
                                i2 = i + 1;
                                str4 = str4;
                                it = it;
                                str5 = str5;
                                strArr2 = strArr;
                                uploader = this;
                            }
                            long length22 = new File(next.getString(str)).length();
                            j += length22;
                            if (!z) {
                                String str9 = "--" + "*****" + str6 + "Content-Disposition: form-data; name=\"" + string + "\"; filename=\"" + string2 + "\"" + str6 + "Content-Type: " + mimeType + str6;
                                i = i2;
                                if (array.length - 1 == i) {
                                    str3 = str6;
                                    j += (long) sb2.length();
                                } else {
                                    str3 = str6;
                                }
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("Content-length: ");
                                sb3.append(length22);
                                str2 = str3;
                                sb3.append(str2);
                                String sb4 = sb3.toString();
                                strArr[i] = str9 + sb4 + str2;
                                str8 = str8 + str9 + sb4 + str2;
                            } else {
                                str2 = str6;
                                i = i2;
                                str8 = str8;
                            }
                            str6 = str2;
                            i2 = i + 1;
                            str4 = str4;
                            it = it;
                            str5 = str5;
                            strArr2 = strArr;
                            uploader = this;
                        } catch (Throwable th3) {
                            th = th3;
                            dataOutputStream = null;
                            bufferedInputStream = null;
                            bufferedReader = null;
                            httpURLConnection = httpURLConnection3;
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            if (dataOutputStream != null) {
                                dataOutputStream.close();
                            }
                            if (bufferedInputStream != null) {
                                bufferedInputStream.close();
                            }
                            if (bufferedReader != null) {
                                bufferedReader.close();
                            }
                            throw th;
                        }
                    }
                    if (uploader.mParams.onUploadBegin != null) {
                        uploader.mParams.onUploadBegin.onUploadBegin();
                    }
                    if (!z) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("");
                        int length3 = (int) (((long) (str8.length() + (array.length * 2))) + j);
                        sb5.append(length3);
                        httpURLConnection2 = httpURLConnection3;
                        httpURLConnection2.setRequestProperty("Content-length", sb5.toString());
                        httpURLConnection2.setFixedLengthStreamingMode(length3);
                    } else {
                        httpURLConnection2 = httpURLConnection3;
                    }
                    httpURLConnection2.connect();
                    dataOutputStream = new DataOutputStream(httpURLConnection2.getOutputStream());
                    if (!z) {
                        try {
                            dataOutputStream.writeBytes(str7);
                        } catch (Throwable th4) {
                            th = th4;
                            httpURLConnection = httpURLConnection2;
                            bufferedInputStream = null;
                            bufferedReader = null;
                            if (httpURLConnection != null) {
                            }
                            if (dataOutputStream != null) {
                            }
                            if (bufferedInputStream != null) {
                            }
                            if (bufferedReader != null) {
                            }
                            throw th;
                        }
                    }
                    Runtime runtime = Runtime.getRuntime();
                    Iterator<ReadableMap> it2 = uploadParams.files.iterator();
                    int i3 = 0;
                    int i4 = 0;
                    while (it2.hasNext()) {
                        ReadableMap next2 = it2.next();
                        if (!z) {
                            dataOutputStream.writeBytes(strArr2[i4]);
                        }
                        File file = new File(next2.getString(str));
                        int length4 = (int) file.length();
                        BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(file));
                        int ceil = (int) Math.ceil((double) (((float) length4) / 100.0f));
                        if (((float) ceil) > ((float) runtime.freeMemory()) / 10.0f) {
                            ceil = (int) Math.ceil((double) (((float) runtime.freeMemory()) / 10.0f));
                        }
                        byte[] bArr = new byte[ceil];
                        i3 = i3;
                        while (true) {
                            int read = bufferedInputStream2.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            dataOutputStream.write(bArr, 0, read);
                            if (uploader.mParams.onUploadProgress != null) {
                                i3 += read;
                                uploader.mParams.onUploadProgress.onUploadProgress((int) j, i3);
                            }
                        }
                        if (!z) {
                            dataOutputStream.writeBytes(str6);
                        }
                        i4++;
                        bufferedInputStream2.close();
                        it2 = it2;
                        str = str;
                    }
                    if (!z) {
                        dataOutputStream.writeBytes(sb2);
                    }
                    dataOutputStream.flush();
                    dataOutputStream.close();
                    bufferedInputStream = new BufferedInputStream(httpURLConnection2.getInputStream());
                    try {
                        bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));
                    } catch (Throwable th5) {
                        th = th5;
                        httpURLConnection = httpURLConnection2;
                        bufferedReader = null;
                        if (httpURLConnection != null) {
                        }
                        if (dataOutputStream != null) {
                        }
                        if (bufferedInputStream != null) {
                        }
                        if (bufferedReader != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th6) {
                    th = th6;
                    httpURLConnection3 = httpURLConnection3;
                    httpURLConnection = httpURLConnection3;
                    dataOutputStream = null;
                    bufferedInputStream = null;
                    bufferedReader = null;
                    if (httpURLConnection != null) {
                    }
                    if (dataOutputStream != null) {
                    }
                    if (bufferedInputStream != null) {
                    }
                    if (bufferedReader != null) {
                    }
                    throw th;
                }
                try {
                    WritableMap createMap = Arguments.createMap();
                    for (Map.Entry<String, List<String>> entry : httpURLConnection2.getHeaderFields().entrySet()) {
                        createMap.putString(entry.getKey(), entry.getValue().get(0));
                    }
                    StringBuilder sb6 = new StringBuilder();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        sb6.append(readLine);
                        sb6.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                    }
                    String sb7 = sb6.toString();
                    int responseCode = httpURLConnection2.getResponseCode();
                    uploader.res.headers = createMap;
                    uploader.res.body = sb7;
                    uploader.res.statusCode = responseCode;
                    if (httpURLConnection2 != null) {
                        httpURLConnection2.disconnect();
                    }
                    dataOutputStream.close();
                    bufferedInputStream.close();
                    bufferedReader.close();
                } catch (Throwable th7) {
                    th = th7;
                    httpURLConnection = httpURLConnection2;
                    if (httpURLConnection != null) {
                    }
                    if (dataOutputStream != null) {
                    }
                    if (bufferedInputStream != null) {
                    }
                    if (bufferedReader != null) {
                    }
                    throw th;
                }
            } catch (Throwable th8) {
                th = th8;
                httpURLConnection = httpURLConnection3;
                dataOutputStream = null;
                bufferedInputStream = null;
                bufferedReader = null;
                if (httpURLConnection != null) {
                }
                if (dataOutputStream != null) {
                }
                if (bufferedInputStream != null) {
                }
                if (bufferedReader != null) {
                }
                throw th;
            }
        } catch (Throwable th9) {
            th = th9;
            dataOutputStream = null;
            bufferedInputStream = null;
            bufferedReader = null;
            httpURLConnection = null;
            if (httpURLConnection != null) {
            }
            if (dataOutputStream != null) {
            }
            if (bufferedInputStream != null) {
            }
            if (bufferedReader != null) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public String getMimeType(String str) {
        String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(str);
        String mimeTypeFromExtension = fileExtensionFromUrl != null ? MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtensionFromUrl.toLowerCase()) : null;
        return mimeTypeFromExtension == null ? "*/*" : mimeTypeFromExtension;
    }

    /* access modifiers changed from: protected */
    public void stop() {
        this.mAbort.set(true);
    }
}
