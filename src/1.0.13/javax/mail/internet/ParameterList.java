package javax.mail.internet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.mail.internet.HeaderTokenizer;
import kotlin.UByte;

public class ParameterList {
    private static boolean applehack = false;
    private static boolean decodeParameters = false;
    private static boolean decodeParametersStrict = false;
    private static boolean encodeParameters = false;
    private static final char[] hex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private String lastName;
    private Map list;
    private Set multisegmentNames;
    private Map slist;

    static {
        try {
            String property = System.getProperty("mail.mime.encodeparameters");
            boolean z = true;
            encodeParameters = property != null && property.equalsIgnoreCase("true");
            String property2 = System.getProperty("mail.mime.decodeparameters");
            decodeParameters = property2 != null && property2.equalsIgnoreCase("true");
            String property3 = System.getProperty("mail.mime.decodeparameters.strict");
            decodeParametersStrict = property3 != null && property3.equalsIgnoreCase("true");
            String property4 = System.getProperty("mail.mime.applefilenames");
            if (property4 == null || !property4.equalsIgnoreCase("true")) {
                z = false;
            }
            applehack = z;
        } catch (SecurityException unused) {
        }
    }

    /* access modifiers changed from: private */
    public static class Value {
        String charset;
        String encodedValue;
        String value;

        private Value() {
        }

        /* synthetic */ Value(Value value2) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public static class MultiValue extends ArrayList {
        String value;

        private MultiValue() {
        }

        /* synthetic */ MultiValue(MultiValue multiValue) {
            this();
        }
    }

    private static class ParamEnum implements Enumeration {
        private Iterator it;

        ParamEnum(Iterator it2) {
            this.it = it2;
        }

        public boolean hasMoreElements() {
            return this.it.hasNext();
        }

        @Override // java.util.Enumeration
        public Object nextElement() {
            return this.it.next();
        }
    }

    public ParameterList() {
        this.list = new LinkedHashMap();
        this.lastName = null;
        if (decodeParameters) {
            this.multisegmentNames = new HashSet();
            this.slist = new HashMap();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0124, code lost:
        throw new javax.mail.internet.ParseException("Expected ';', got \"" + r8.getValue() + "\"");
     */
    public ParameterList(String str) throws ParseException {
        this();
        String str2;
        HeaderTokenizer headerTokenizer = new HeaderTokenizer(str, HeaderTokenizer.MIME);
        while (true) {
            HeaderTokenizer.Token next = headerTokenizer.next();
            int type = next.getType();
            if (type == -4) {
                break;
            } else if (((char) type) == ';') {
                HeaderTokenizer.Token next2 = headerTokenizer.next();
                if (next2.getType() == -4) {
                    break;
                } else if (next2.getType() == -1) {
                    String lowerCase = next2.getValue().toLowerCase(Locale.ENGLISH);
                    HeaderTokenizer.Token next3 = headerTokenizer.next();
                    if (((char) next3.getType()) == '=') {
                        HeaderTokenizer.Token next4 = headerTokenizer.next();
                        int type2 = next4.getType();
                        if (type2 == -1 || type2 == -2) {
                            String value = next4.getValue();
                            this.lastName = lowerCase;
                            if (decodeParameters) {
                                putEncodedName(lowerCase, value);
                            } else {
                                this.list.put(lowerCase, value);
                            }
                        } else {
                            throw new ParseException("Expected parameter value, got \"" + next4.getValue() + "\"");
                        }
                    } else {
                        throw new ParseException("Expected '=', got \"" + next3.getValue() + "\"");
                    }
                } else {
                    throw new ParseException("Expected parameter name, got \"" + next2.getValue() + "\"");
                }
            } else if (!applehack || type != -1 || (str2 = this.lastName) == null || (!str2.equals("name") && !this.lastName.equals("filename"))) {
            } else {
                this.list.put(this.lastName, String.valueOf((String) this.list.get(this.lastName)) + " " + next.getValue());
            }
        }
        if (decodeParameters) {
            combineMultisegmentNames(false);
        }
    }

    private void putEncodedName(String str, String str2) throws ParseException {
        int indexOf = str.indexOf(42);
        if (indexOf < 0) {
            this.list.put(str, str2);
        } else if (indexOf == str.length() - 1) {
            this.list.put(str.substring(0, indexOf), decodeValue(str2));
        } else {
            String substring = str.substring(0, indexOf);
            this.multisegmentNames.add(substring);
            this.list.put(substring, "");
            if (str.endsWith("*")) {
                Value value = new Value(null);
                Value value2 = value;
                value2.encodedValue = str2;
                value2.value = str2;
                str = str.substring(0, str.length() - 1);
                str2 = value;
            }
            this.slist.put(str, str2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:78:0x00d8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00eb A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x00ff A[SYNTHETIC] */
    private void combineMultisegmentNames(boolean z) throws ParseException {
        String str;
        String str2;
        String str3;
        NumberFormatException e;
        NumberFormatException e2;
        UnsupportedEncodingException e3;
        UnsupportedEncodingException e4;
        StringIndexOutOfBoundsException e5;
        StringIndexOutOfBoundsException e6;
        try {
            for (String str4 : this.multisegmentNames) {
                StringBuffer stringBuffer = new StringBuffer();
                MultiValue multiValue = new MultiValue(null);
                int i = 0;
                String str5 = null;
                while (true) {
                    String str6 = String.valueOf(str4) + "*" + i;
                    Object obj = this.slist.get(str6);
                    if (obj == null) {
                        break;
                    }
                    multiValue.add(obj);
                    if (obj instanceof Value) {
                        try {
                            Value value = (Value) obj;
                            str3 = value.encodedValue;
                            if (i == 0) {
                                try {
                                    Value decodeValue = decodeValue(str3);
                                    str = decodeValue.charset;
                                    value.charset = str;
                                    try {
                                        str2 = decodeValue.value;
                                        value.value = str2;
                                    } catch (NumberFormatException e7) {
                                        e = e7;
                                    } catch (UnsupportedEncodingException e8) {
                                        e3 = e8;
                                        if (decodeParametersStrict) {
                                        }
                                        str2 = str3;
                                        stringBuffer.append(str2);
                                        this.slist.remove(str6);
                                        i++;
                                        str5 = str;
                                    } catch (StringIndexOutOfBoundsException e9) {
                                        e5 = e9;
                                        if (decodeParametersStrict) {
                                        }
                                        str2 = str3;
                                        stringBuffer.append(str2);
                                        this.slist.remove(str6);
                                        i++;
                                        str5 = str;
                                    }
                                } catch (NumberFormatException e10) {
                                    e2 = e10;
                                    str = str5;
                                    e = e2;
                                    if (decodeParametersStrict) {
                                        throw new ParseException(e.toString());
                                    }
                                    str2 = str3;
                                    stringBuffer.append(str2);
                                    this.slist.remove(str6);
                                    i++;
                                    str5 = str;
                                } catch (UnsupportedEncodingException e11) {
                                    e4 = e11;
                                    str = str5;
                                    e3 = e4;
                                    if (decodeParametersStrict) {
                                        throw new ParseException(e3.toString());
                                    }
                                    str2 = str3;
                                    stringBuffer.append(str2);
                                    this.slist.remove(str6);
                                    i++;
                                    str5 = str;
                                } catch (StringIndexOutOfBoundsException e12) {
                                    e6 = e12;
                                    str = str5;
                                    e5 = e6;
                                    if (decodeParametersStrict) {
                                        throw new ParseException(e5.toString());
                                    }
                                    str2 = str3;
                                    stringBuffer.append(str2);
                                    this.slist.remove(str6);
                                    i++;
                                    str5 = str;
                                }
                            } else if (str5 == null) {
                                this.multisegmentNames.remove(str4);
                                break;
                            } else {
                                String decodeBytes = decodeBytes(str3, str5);
                                value.value = decodeBytes;
                                str = str5;
                                str2 = decodeBytes;
                            }
                        } catch (NumberFormatException e13) {
                            e2 = e13;
                            str3 = null;
                            str = str5;
                            e = e2;
                            if (decodeParametersStrict) {
                            }
                            str2 = str3;
                            stringBuffer.append(str2);
                            this.slist.remove(str6);
                            i++;
                            str5 = str;
                        } catch (UnsupportedEncodingException e14) {
                            e4 = e14;
                            str3 = null;
                            str = str5;
                            e3 = e4;
                            if (decodeParametersStrict) {
                            }
                            str2 = str3;
                            stringBuffer.append(str2);
                            this.slist.remove(str6);
                            i++;
                            str5 = str;
                        } catch (StringIndexOutOfBoundsException e15) {
                            e6 = e15;
                            str3 = null;
                            str = str5;
                            e5 = e6;
                            if (decodeParametersStrict) {
                            }
                            str2 = str3;
                            stringBuffer.append(str2);
                            this.slist.remove(str6);
                            i++;
                            str5 = str;
                        }
                    } else {
                        str = str5;
                        str2 = (String) obj;
                    }
                    stringBuffer.append(str2);
                    this.slist.remove(str6);
                    i++;
                    str5 = str;
                }
                if (i == 0) {
                    this.list.remove(str4);
                } else {
                    multiValue.value = stringBuffer.toString();
                    this.list.put(str4, multiValue);
                }
            }
            if (this.slist.size() > 0) {
                for (Object obj2 : this.slist.values()) {
                    if (obj2 instanceof Value) {
                        Value value2 = (Value) obj2;
                        Value decodeValue2 = decodeValue(value2.encodedValue);
                        value2.charset = decodeValue2.charset;
                        value2.value = decodeValue2.value;
                    }
                }
                this.list.putAll(this.slist);
            }
            this.multisegmentNames.clear();
            this.slist.clear();
        } catch (Throwable th) {
            if (z) {
                if (this.slist.size() > 0) {
                    for (Object obj3 : this.slist.values()) {
                        if (obj3 instanceof Value) {
                            Value value3 = (Value) obj3;
                            Value decodeValue3 = decodeValue(value3.encodedValue);
                            value3.charset = decodeValue3.charset;
                            value3.value = decodeValue3.value;
                        }
                    }
                    this.list.putAll(this.slist);
                }
                this.multisegmentNames.clear();
                this.slist.clear();
            }
            throw th;
        }
    }

    public int size() {
        return this.list.size();
    }

    public String get(String str) {
        Object obj = this.list.get(str.trim().toLowerCase(Locale.ENGLISH));
        if (obj instanceof MultiValue) {
            return ((MultiValue) obj).value;
        }
        if (obj instanceof Value) {
            return ((Value) obj).value;
        }
        return (String) obj;
    }

    public void set(String str, String str2) {
        if (str != null || str2 == null || !str2.equals("DONE")) {
            String lowerCase = str.trim().toLowerCase(Locale.ENGLISH);
            if (decodeParameters) {
                try {
                    putEncodedName(lowerCase, str2);
                } catch (ParseException unused) {
                    this.list.put(lowerCase, str2);
                }
            } else {
                this.list.put(lowerCase, str2);
            }
        } else if (decodeParameters && this.multisegmentNames.size() > 0) {
            try {
                combineMultisegmentNames(true);
            } catch (ParseException unused2) {
            }
        }
    }

    public void set(String str, String str2, String str3) {
        if (encodeParameters) {
            Value encodeValue = encodeValue(str2, str3);
            if (encodeValue != null) {
                this.list.put(str.trim().toLowerCase(Locale.ENGLISH), encodeValue);
            } else {
                set(str, str2);
            }
        } else {
            set(str, str2);
        }
    }

    public void remove(String str) {
        this.list.remove(str.trim().toLowerCase(Locale.ENGLISH));
    }

    public Enumeration getNames() {
        return new ParamEnum(this.list.keySet().iterator());
    }

    public String toString() {
        return toString(0);
    }

    public String toString(int i) {
        ToStringBuffer toStringBuffer = new ToStringBuffer(i);
        for (String str : this.list.keySet()) {
            Object obj = this.list.get(str);
            if (obj instanceof MultiValue) {
                MultiValue multiValue = (MultiValue) obj;
                String str2 = String.valueOf(str) + "*";
                for (int i2 = 0; i2 < multiValue.size(); i2++) {
                    Object obj2 = multiValue.get(i2);
                    if (obj2 instanceof Value) {
                        toStringBuffer.addNV(String.valueOf(str2) + i2 + "*", ((Value) obj2).encodedValue);
                    } else {
                        toStringBuffer.addNV(String.valueOf(str2) + i2, (String) obj2);
                    }
                }
            } else if (obj instanceof Value) {
                toStringBuffer.addNV(String.valueOf(str) + "*", ((Value) obj).encodedValue);
            } else {
                toStringBuffer.addNV(str, (String) obj);
            }
        }
        return toStringBuffer.toString();
    }

    /* access modifiers changed from: private */
    public static class ToStringBuffer {
        private StringBuffer sb = new StringBuffer();
        private int used;

        public ToStringBuffer(int i) {
            this.used = i;
        }

        public void addNV(String str, String str2) {
            String quote = ParameterList.quote(str2);
            this.sb.append("; ");
            this.used += 2;
            if (this.used + str.length() + quote.length() + 1 > 76) {
                this.sb.append("\r\n\t");
                this.used = 8;
            }
            StringBuffer stringBuffer = this.sb;
            stringBuffer.append(str);
            stringBuffer.append('=');
            int length = this.used + str.length() + 1;
            this.used = length;
            if (length + quote.length() > 76) {
                String fold = MimeUtility.fold(this.used, quote);
                this.sb.append(fold);
                int lastIndexOf = fold.lastIndexOf(10);
                if (lastIndexOf >= 0) {
                    this.used += (fold.length() - lastIndexOf) - 1;
                } else {
                    this.used += fold.length();
                }
            } else {
                this.sb.append(quote);
                this.used += quote.length();
            }
        }

        public String toString() {
            return this.sb.toString();
        }
    }

    /* access modifiers changed from: private */
    public static String quote(String str) {
        return MimeUtility.quote(str, HeaderTokenizer.MIME);
    }

    private static Value encodeValue(String str, String str2) {
        if (MimeUtility.checkAscii(str) == 1) {
            return null;
        }
        try {
            byte[] bytes = str.getBytes(MimeUtility.javaCharset(str2));
            StringBuffer stringBuffer = new StringBuffer(bytes.length + str2.length() + 2);
            stringBuffer.append(str2);
            stringBuffer.append("''");
            for (byte b : bytes) {
                char c = (char) (b & UByte.MAX_VALUE);
                if (c <= ' ' || c >= 127 || c == '*' || c == '\'' || c == '%' || HeaderTokenizer.MIME.indexOf(c) >= 0) {
                    stringBuffer.append('%');
                    char[] cArr = hex;
                    stringBuffer.append(cArr[c >> 4]);
                    stringBuffer.append(cArr[c & 15]);
                } else {
                    stringBuffer.append(c);
                }
            }
            Value value = new Value(null);
            value.charset = str2;
            value.value = str;
            value.encodedValue = stringBuffer.toString();
            return value;
        } catch (UnsupportedEncodingException unused) {
            return null;
        }
    }

    private static Value decodeValue(String str) throws ParseException {
        Value value = new Value(null);
        value.encodedValue = str;
        value.value = str;
        try {
            int indexOf = str.indexOf(39);
            if (indexOf > 0) {
                String substring = str.substring(0, indexOf);
                int i = indexOf + 1;
                int indexOf2 = str.indexOf(39, i);
                if (indexOf2 >= 0) {
                    str.substring(i, indexOf2);
                    String substring2 = str.substring(indexOf2 + 1);
                    value.charset = substring;
                    value.value = decodeBytes(substring2, substring);
                    return value;
                } else if (!decodeParametersStrict) {
                    return value;
                } else {
                    throw new ParseException("Missing language in encoded value: " + str);
                }
            } else if (!decodeParametersStrict) {
                return value;
            } else {
                throw new ParseException("Missing charset in encoded value: " + str);
            }
        } catch (NumberFormatException e) {
            if (decodeParametersStrict) {
                throw new ParseException(e.toString());
            }
        } catch (UnsupportedEncodingException e2) {
            if (decodeParametersStrict) {
                throw new ParseException(e2.toString());
            }
        } catch (StringIndexOutOfBoundsException e3) {
            if (decodeParametersStrict) {
                throw new ParseException(e3.toString());
            }
        }
    }

    private static String decodeBytes(String str, String str2) throws UnsupportedEncodingException {
        byte[] bArr = new byte[str.length()];
        int i = 0;
        int i2 = 0;
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt == '%') {
                charAt = (char) Integer.parseInt(str.substring(i + 1, i + 3), 16);
                i += 2;
            }
            bArr[i2] = (byte) charAt;
            i++;
            i2++;
        }
        return new String(bArr, 0, i2, MimeUtility.javaCharset(str2));
    }
}
