package javax.mail.internet;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.Address;
import javax.mail.Session;
import kotlin.text.Typography;

public class InternetAddress extends Address implements Cloneable {
    private static final String rfc822phrase = HeaderTokenizer.RFC822.replace(' ', (char) 0).replace('\t', (char) 0);
    private static final long serialVersionUID = -7507595530758302903L;
    private static final String specialsNoDot = "()<>,;:\\\"[]@";
    private static final String specialsNoDotNoAt = "()<>,;:\\\"[]";
    protected String address;
    protected String encodedPersonal;
    protected String personal;

    @Override // javax.mail.Address
    public String getType() {
        return "rfc822";
    }

    public InternetAddress() {
    }

    public InternetAddress(String str) throws AddressException {
        InternetAddress[] parse = parse(str, true);
        if (parse.length == 1) {
            this.address = parse[0].address;
            this.personal = parse[0].personal;
            this.encodedPersonal = parse[0].encodedPersonal;
            return;
        }
        throw new AddressException("Illegal address", str);
    }

    public InternetAddress(String str, boolean z) throws AddressException {
        this(str);
        if (z) {
            checkAddress(this.address, true, true);
        }
    }

    public InternetAddress(String str, String str2) throws UnsupportedEncodingException {
        this(str, str2, null);
    }

    public InternetAddress(String str, String str2, String str3) throws UnsupportedEncodingException {
        this.address = str;
        setPersonal(str2, str3);
    }

    @Override // java.lang.Object
    public Object clone() {
        try {
            return (InternetAddress) super.clone();
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public void setPersonal(String str, String str2) throws UnsupportedEncodingException {
        this.personal = str;
        if (str != null) {
            this.encodedPersonal = MimeUtility.encodeWord(str, str2, null);
        } else {
            this.encodedPersonal = null;
        }
    }

    public void setPersonal(String str) throws UnsupportedEncodingException {
        this.personal = str;
        if (str != null) {
            this.encodedPersonal = MimeUtility.encodeWord(str);
        } else {
            this.encodedPersonal = null;
        }
    }

    public String getAddress() {
        return this.address;
    }

    public String getPersonal() {
        String str = this.personal;
        if (str != null) {
            return str;
        }
        String str2 = this.encodedPersonal;
        if (str2 == null) {
            return null;
        }
        try {
            String decodeText = MimeUtility.decodeText(str2);
            this.personal = decodeText;
            return decodeText;
        } catch (Exception unused) {
            return this.encodedPersonal;
        }
    }

    @Override // javax.mail.Address
    public String toString() {
        String str;
        if (this.encodedPersonal == null && (str = this.personal) != null) {
            try {
                this.encodedPersonal = MimeUtility.encodeWord(str);
            } catch (UnsupportedEncodingException unused) {
            }
        }
        String str2 = this.encodedPersonal;
        if (str2 != null) {
            return String.valueOf(quotePhrase(str2)) + " <" + this.address + ">";
        } else if (isGroup() || isSimple()) {
            return this.address;
        } else {
            return "<" + this.address + ">";
        }
    }

    public String toUnicodeString() {
        String personal2 = getPersonal();
        if (personal2 != null) {
            return String.valueOf(quotePhrase(personal2)) + " <" + this.address + ">";
        } else if (isGroup() || isSimple()) {
            return this.address;
        } else {
            return "<" + this.address + ">";
        }
    }

    private static String quotePhrase(String str) {
        int length = str.length();
        boolean z = false;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"' || charAt == '\\') {
                StringBuffer stringBuffer = new StringBuffer(length + 3);
                stringBuffer.append(Typography.quote);
                for (int i2 = 0; i2 < length; i2++) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '\"' || charAt2 == '\\') {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt2);
                }
                stringBuffer.append(Typography.quote);
                return stringBuffer.toString();
            }
            if (!(charAt >= ' ' || charAt == '\r' || charAt == '\n' || charAt == '\t') || charAt >= 127 || rfc822phrase.indexOf(charAt) >= 0) {
                z = true;
            }
        }
        if (!z) {
            return str;
        }
        StringBuffer stringBuffer2 = new StringBuffer(length + 2);
        stringBuffer2.append(Typography.quote);
        stringBuffer2.append(str);
        stringBuffer2.append(Typography.quote);
        return stringBuffer2.toString();
    }

    private static String unquote(String str) {
        if (!str.startsWith("\"") || !str.endsWith("\"")) {
            return str;
        }
        String substring = str.substring(1, str.length() - 1);
        if (substring.indexOf(92) < 0) {
            return substring;
        }
        StringBuffer stringBuffer = new StringBuffer(substring.length());
        int i = 0;
        while (i < substring.length()) {
            char charAt = substring.charAt(i);
            if (charAt == '\\' && i < substring.length() - 1) {
                i++;
                charAt = substring.charAt(i);
            }
            stringBuffer.append(charAt);
            i++;
        }
        return stringBuffer.toString();
    }

    @Override // javax.mail.Address
    public boolean equals(Object obj) {
        if (!(obj instanceof InternetAddress)) {
            return false;
        }
        String address2 = ((InternetAddress) obj).getAddress();
        String str = this.address;
        if (address2 == str) {
            return true;
        }
        if (str == null || !str.equalsIgnoreCase(address2)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.address;
        if (str == null) {
            return 0;
        }
        return str.toLowerCase(Locale.ENGLISH).hashCode();
    }

    public static String toString(Address[] addressArr) {
        return toString(addressArr, 0);
    }

    public static String toString(Address[] addressArr, int i) {
        if (addressArr == null || addressArr.length == 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < addressArr.length; i2++) {
            if (i2 != 0) {
                stringBuffer.append(", ");
                i += 2;
            }
            String address2 = addressArr[i2].toString();
            if (lengthOfFirstSegment(address2) + i > 76) {
                stringBuffer.append("\r\n\t");
                i = 8;
            }
            stringBuffer.append(address2);
            i = lengthOfLastSegment(address2, i);
        }
        return stringBuffer.toString();
    }

    private static int lengthOfFirstSegment(String str) {
        int indexOf = str.indexOf("\r\n");
        if (indexOf != -1) {
            return indexOf;
        }
        return str.length();
    }

    private static int lengthOfLastSegment(String str, int i) {
        int lastIndexOf = str.lastIndexOf("\r\n");
        if (lastIndexOf != -1) {
            return (str.length() - lastIndexOf) - 2;
        }
        return str.length() + i;
    }

    public static InternetAddress getLocalAddress(Session session) {
        String str;
        String str2;
        String str3;
        String str4;
        String property;
        InetAddress localHost;
        if (session == null) {
            try {
                str = System.getProperty("user.name");
                str3 = InetAddress.getLocalHost().getHostName();
                str2 = null;
            } catch (SecurityException | UnknownHostException | AddressException unused) {
            }
        } else {
            str2 = session.getProperty("mail.from");
            if (str2 == null) {
                String property2 = session.getProperty("mail.user");
                if (property2 == null || property2.length() == 0) {
                    property2 = session.getProperty("user.name");
                }
                if (property2 != null) {
                    if (property2.length() != 0) {
                        str4 = property2;
                        property = session.getProperty("mail.host");
                        if ((property == null || property.length() == 0) && (localHost = InetAddress.getLocalHost()) != null) {
                            property = localHost.getHostName();
                        }
                        str3 = property;
                        str = str4;
                    }
                }
                str4 = System.getProperty("user.name");
                property = session.getProperty("mail.host");
                property = localHost.getHostName();
                str3 = property;
                str = str4;
            } else {
                str = null;
                str3 = null;
            }
        }
        if (!(str2 != null || str == null || str.length() == 0 || str3 == null || str3.length() == 0)) {
            str2 = String.valueOf(str) + "@" + str3;
        }
        if (str2 != null) {
            return new InternetAddress(str2);
        }
        return null;
    }

    public static InternetAddress[] parse(String str) throws AddressException {
        return parse(str, true);
    }

    public static InternetAddress[] parse(String str, boolean z) throws AddressException {
        return parse(str, z, false);
    }

    public static InternetAddress[] parseHeader(String str, boolean z) throws AddressException {
        return parse(str, z, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:166:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x023c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0196 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x00dc A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ec  */
    private static InternetAddress[] parse(String str, boolean z, boolean z2) throws AddressException {
        int i;
        int length = str.length();
        Vector vector = new Vector();
        boolean z3 = false;
        int i2 = 0;
        int i3 = -1;
        boolean z4 = false;
        boolean z5 = false;
        int i4 = -1;
        boolean z6 = false;
        int i5 = -1;
        int i6 = -1;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (!(charAt == '\t' || charAt == '\n' || charAt == '\r' || charAt == ' ')) {
                if (charAt == '\"') {
                    if (i3 == -1) {
                        i3 = i2;
                    }
                    i2++;
                    while (i2 < length) {
                        char charAt2 = str.charAt(i2);
                        if (charAt2 != '\"') {
                            if (charAt2 == '\\') {
                                i2++;
                            }
                            i2++;
                        } else if (i2 >= length) {
                            i = 1;
                            z6 = true;
                        } else {
                            throw new AddressException("Missing '\"'", str, i2);
                        }
                    }
                    if (i2 >= length) {
                    }
                } else if (charAt != ',') {
                    if (charAt != '>') {
                        if (charAt == '[') {
                            i2++;
                            while (i2 < length) {
                                char charAt3 = str.charAt(i2);
                                if (charAt3 == '\\') {
                                    i2++;
                                } else if (charAt3 == ']') {
                                    if (i2 >= length) {
                                        throw new AddressException("Missing ']'", str, i2);
                                    }
                                    i = 1;
                                }
                                i2++;
                            }
                            if (i2 >= length) {
                            }
                            i = 1;
                        } else if (charAt == '(') {
                            if (i3 >= 0 && i4 == -1) {
                                i4 = i2;
                            }
                            if (i5 == -1) {
                                i5 = i2 + 1;
                            }
                            int i7 = i2 + 1;
                            int i8 = 1;
                            while (i7 < length && i8 > 0) {
                                char charAt4 = str.charAt(i7);
                                if (charAt4 == '(') {
                                    i8++;
                                } else if (charAt4 == ')') {
                                    i8--;
                                } else if (charAt4 == '\\') {
                                    i7++;
                                }
                                i7++;
                            }
                            if (i8 <= 0) {
                                i2 = i7 - 1;
                                if (i6 == -1) {
                                    i6 = i2;
                                }
                                i = 1;
                            } else {
                                throw new AddressException("Missing ')'", str, i7);
                            }
                        } else if (charAt != ')') {
                            switch (charAt) {
                                case ':':
                                    if (!z4) {
                                        if (i3 == -1) {
                                            i3 = i2;
                                        }
                                        i = 1;
                                        z4 = true;
                                        break;
                                    } else {
                                        throw new AddressException("Nested group", str, i2);
                                    }
                                case ';':
                                    if (i3 == -1) {
                                        i3 = i2;
                                    }
                                    if (z4) {
                                        if (i3 == -1) {
                                            i3 = i2;
                                        }
                                        InternetAddress internetAddress = new InternetAddress();
                                        internetAddress.setAddress(str.substring(i3, i2 + 1).trim());
                                        vector.addElement(internetAddress);
                                        i = 1;
                                        i3 = -1;
                                        z4 = false;
                                        z5 = false;
                                        i4 = -1;
                                        break;
                                    } else {
                                        throw new AddressException("Illegal semicolon, not in group", str, i2);
                                    }
                                case '<':
                                    if (!z5) {
                                        if (!z4) {
                                            if (i3 >= 0) {
                                                i6 = i2;
                                            }
                                            i5 = i3;
                                            i3 = i2 + 1;
                                        }
                                        i2++;
                                        boolean z7 = false;
                                        while (i2 < length) {
                                            char charAt5 = str.charAt(i2);
                                            if (charAt5 != '\"') {
                                                if (charAt5 == '>') {
                                                    if (!z7) {
                                                        if (i2 >= length) {
                                                            i4 = i2;
                                                            i = 1;
                                                            z5 = true;
                                                            break;
                                                        } else if (z7) {
                                                            throw new AddressException("Missing '\"'", str, i2);
                                                        } else {
                                                            throw new AddressException("Missing '>'", str, i2);
                                                        }
                                                    }
                                                } else if (charAt5 == '\\') {
                                                    i2++;
                                                }
                                            } else {
                                                z7 = !z7;
                                            }
                                            i2++;
                                        }
                                        if (i2 >= length) {
                                        }
                                    } else {
                                        throw new AddressException("Extra route-addr", str, i2);
                                    }
                                default:
                                    if (i3 == -1) {
                                        i3 = i2;
                                        i = 1;
                                        break;
                                    }
                                    break;
                            }
                        } else {
                            throw new AddressException("Missing '('", str, i2);
                        }
                        z6 = true;
                    } else {
                        throw new AddressException("Missing '<'", str, i2);
                    }
                } else if (i3 == -1) {
                    i = 1;
                    i3 = -1;
                    z5 = false;
                    i4 = -1;
                    z6 = false;
                } else if (z4) {
                    i = 1;
                    z5 = false;
                } else {
                    if (i4 == -1) {
                        i4 = i2;
                    }
                    String trim = str.substring(i3, i4).trim();
                    if (z6 || z || z2) {
                        if (z || !z2) {
                            checkAddress(trim, z5, false);
                        }
                        InternetAddress internetAddress2 = new InternetAddress();
                        internetAddress2.setAddress(trim);
                        if (i5 >= 0) {
                            internetAddress2.encodedPersonal = unquote(str.substring(i5, i6).trim());
                            i5 = -1;
                            i6 = -1;
                        }
                        vector.addElement(internetAddress2);
                    } else {
                        StringTokenizer stringTokenizer = new StringTokenizer(trim);
                        while (stringTokenizer.hasMoreTokens()) {
                            String nextToken = stringTokenizer.nextToken();
                            checkAddress(nextToken, false, false);
                            InternetAddress internetAddress3 = new InternetAddress();
                            internetAddress3.setAddress(nextToken);
                            vector.addElement(internetAddress3);
                        }
                    }
                    i = 1;
                    i3 = -1;
                    z5 = false;
                    i4 = -1;
                    z6 = false;
                }
                i2 += i;
                z3 = false;
            }
            i = 1;
            i2 += i;
            z3 = false;
        }
        if (i3 >= 0) {
            if (i4 != -1) {
                i2 = i4;
            }
            String trim2 = str.substring(i3, i2).trim();
            if (z6 || z || z2) {
                if (z || !z2) {
                    checkAddress(trim2, z5, z3);
                }
                InternetAddress internetAddress4 = new InternetAddress();
                internetAddress4.setAddress(trim2);
                if (i5 >= 0) {
                    internetAddress4.encodedPersonal = unquote(str.substring(i5, i6).trim());
                }
                vector.addElement(internetAddress4);
            } else {
                StringTokenizer stringTokenizer2 = new StringTokenizer(trim2);
                while (stringTokenizer2.hasMoreTokens()) {
                    String nextToken2 = stringTokenizer2.nextToken();
                    checkAddress(nextToken2, z3, z3);
                    InternetAddress internetAddress5 = new InternetAddress();
                    internetAddress5.setAddress(nextToken2);
                    vector.addElement(internetAddress5);
                }
            }
        }
        InternetAddress[] internetAddressArr = new InternetAddress[vector.size()];
        vector.copyInto(internetAddressArr);
        return internetAddressArr;
    }

    public void validate() throws AddressException {
        checkAddress(getAddress(), true, true);
    }

    private static void checkAddress(String str, boolean z, boolean z2) throws AddressException {
        String str2;
        String str3;
        if (str.indexOf(34) < 0) {
            int i = 0;
            if (z) {
                while (true) {
                    int indexOfAny = indexOfAny(str, ",:", i);
                    if (indexOfAny < 0) {
                        break;
                    } else if (str.charAt(i) != '@') {
                        throw new AddressException("Illegal route-addr", str);
                    } else if (str.charAt(indexOfAny) == ':') {
                        i = indexOfAny + 1;
                        break;
                    } else {
                        i = indexOfAny + 1;
                    }
                }
            }
            int indexOf = str.indexOf(64, i);
            if (indexOf >= 0) {
                if (indexOf == i) {
                    throw new AddressException("Missing local name", str);
                } else if (indexOf != str.length() - 1) {
                    str2 = str.substring(i, indexOf);
                    str3 = str.substring(indexOf + 1);
                } else {
                    throw new AddressException("Missing domain", str);
                }
            } else if (!z2) {
                str3 = null;
                str2 = str;
            } else {
                throw new AddressException("Missing final '@domain'", str);
            }
            if (indexOfAny(str, " \t\n\r") >= 0) {
                throw new AddressException("Illegal whitespace in address", str);
            } else if (indexOfAny(str2, specialsNoDot) >= 0) {
                throw new AddressException("Illegal character in local name", str);
            } else if (str3 != null && str3.indexOf(91) < 0 && indexOfAny(str3, specialsNoDot) >= 0) {
                throw new AddressException("Illegal character in domain", str);
            }
        }
    }

    private boolean isSimple() {
        String str = this.address;
        return str == null || indexOfAny(str, specialsNoDotNoAt) < 0;
    }

    public boolean isGroup() {
        String str = this.address;
        return str != null && str.endsWith(";") && this.address.indexOf(58) > 0;
    }

    public InternetAddress[] getGroup(boolean z) throws AddressException {
        int indexOf;
        String address2 = getAddress();
        if (address2.endsWith(";") && (indexOf = address2.indexOf(58)) >= 0) {
            return parseHeader(address2.substring(indexOf + 1, address2.length() - 1), z);
        }
        return null;
    }

    private static int indexOfAny(String str, String str2) {
        return indexOfAny(str, str2, 0);
    }

    private static int indexOfAny(String str, String str2, int i) {
        try {
            int length = str.length();
            while (i < length) {
                if (str2.indexOf(str.charAt(i)) >= 0) {
                    return i;
                }
                i++;
            }
            return -1;
        } catch (StringIndexOutOfBoundsException unused) {
            return -1;
        }
    }
}
