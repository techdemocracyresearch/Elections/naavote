package javax.mail;

import java.io.IOException;
import java.io.InputStream;

/* access modifiers changed from: package-private */
/* compiled from: Session */
public interface StreamLoader {
    void load(InputStream inputStream) throws IOException;
}
