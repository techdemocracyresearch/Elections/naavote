package fr.snapp.imagebase64;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import com.facebook.common.util.UriUtil;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class RNImgToBase64Module extends ReactContextBaseJavaModule {
    private final ReactApplicationContext reactContext;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return "RNImgToBase64";
    }

    public RNImgToBase64Module(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        this.reactContext = reactApplicationContext;
    }

    @ReactMethod
    public void getBase64String(String str, Promise promise) {
        Bitmap bitmap;
        try {
            if (str.contains(UriUtil.HTTP_SCHEME)) {
                bitmap = getBitmapFromURL(str);
            } else {
                bitmap = MediaStore.Images.Media.getBitmap(this.reactContext.getContentResolver(), Uri.parse(str));
            }
            if (bitmap == null) {
                promise.reject("Error", "Failed to decode Bitmap, uri: " + str);
                return;
            }
            promise.resolve(bitmapToBase64(bitmap));
        } catch (Error e) {
            promise.reject("Error", "Failed to decode Bitmap: " + e);
            e.printStackTrace();
        } catch (Exception e2) {
            promise.reject("Error", "Exception: " + e2);
            e2.printStackTrace();
        }
    }

    public Bitmap getBitmapFromURL(String str) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            return BitmapFactory.decodeStream(httpURLConnection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
    }
}
