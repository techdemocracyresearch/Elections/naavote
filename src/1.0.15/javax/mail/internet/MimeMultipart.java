package javax.mail.internet;

import com.facebook.react.views.textinput.ReactEditTextInputConnectionWrapper;
import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.LineOutputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessageAware;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.MultipartDataSource;
import kotlin.UByte;
import kotlin.jvm.internal.ByteCompanionObject;

public class MimeMultipart extends Multipart {
    private static boolean bmparse = true;
    private static boolean ignoreMissingBoundaryParameter = true;
    private static boolean ignoreMissingEndBoundary = true;
    private boolean complete;
    protected DataSource ds;
    protected boolean parsed;
    private String preamble;

    static {
        try {
            String property = System.getProperty("mail.mime.multipart.ignoremissingendboundary");
            boolean z = false;
            ignoreMissingEndBoundary = property == null || !property.equalsIgnoreCase("false");
            String property2 = System.getProperty("mail.mime.multipart.ignoremissingboundaryparameter");
            ignoreMissingBoundaryParameter = property2 == null || !property2.equalsIgnoreCase("false");
            String property3 = System.getProperty("mail.mime.multipart.bmparse");
            if (property3 == null || !property3.equalsIgnoreCase("false")) {
                z = true;
            }
            bmparse = z;
        } catch (SecurityException unused) {
        }
    }

    public MimeMultipart() {
        this("mixed");
    }

    public MimeMultipart(String str) {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        String uniqueBoundaryValue = UniqueValue.getUniqueBoundaryValue();
        ContentType contentType = new ContentType("multipart", str, null);
        contentType.setParameter("boundary", uniqueBoundaryValue);
        this.contentType = contentType.toString();
    }

    public MimeMultipart(DataSource dataSource) throws MessagingException {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        if (dataSource instanceof MessageAware) {
            setParent(((MessageAware) dataSource).getMessageContext().getPart());
        }
        if (dataSource instanceof MultipartDataSource) {
            setMultipartDataSource((MultipartDataSource) dataSource);
            return;
        }
        this.parsed = false;
        this.ds = dataSource;
        this.contentType = dataSource.getContentType();
    }

    public synchronized void setSubType(String str) throws MessagingException {
        ContentType contentType = new ContentType(this.contentType);
        contentType.setSubType(str);
        this.contentType = contentType.toString();
    }

    @Override // javax.mail.Multipart
    public synchronized int getCount() throws MessagingException {
        parse();
        return super.getCount();
    }

    @Override // javax.mail.Multipart
    public synchronized BodyPart getBodyPart(int i) throws MessagingException {
        parse();
        return super.getBodyPart(i);
    }

    public synchronized BodyPart getBodyPart(String str) throws MessagingException {
        parse();
        int count = getCount();
        for (int i = 0; i < count; i++) {
            MimeBodyPart mimeBodyPart = (MimeBodyPart) getBodyPart(i);
            String contentID = mimeBodyPart.getContentID();
            if (contentID != null && contentID.equals(str)) {
                return mimeBodyPart;
            }
        }
        return null;
    }

    @Override // javax.mail.Multipart
    public boolean removeBodyPart(BodyPart bodyPart) throws MessagingException {
        parse();
        return super.removeBodyPart(bodyPart);
    }

    @Override // javax.mail.Multipart
    public void removeBodyPart(int i) throws MessagingException {
        parse();
        super.removeBodyPart(i);
    }

    @Override // javax.mail.Multipart
    public synchronized void addBodyPart(BodyPart bodyPart) throws MessagingException {
        parse();
        super.addBodyPart(bodyPart);
    }

    @Override // javax.mail.Multipart
    public synchronized void addBodyPart(BodyPart bodyPart, int i) throws MessagingException {
        parse();
        super.addBodyPart(bodyPart, i);
    }

    public synchronized boolean isComplete() throws MessagingException {
        parse();
        return this.complete;
    }

    public synchronized String getPreamble() throws MessagingException {
        parse();
        return this.preamble;
    }

    public synchronized void setPreamble(String str) throws MessagingException {
        this.preamble = str;
    }

    /* access modifiers changed from: protected */
    public void updateHeaders() throws MessagingException {
        for (int i = 0; i < this.parts.size(); i++) {
            ((MimeBodyPart) this.parts.elementAt(i)).updateHeaders();
        }
    }

    @Override // javax.mail.Multipart
    public synchronized void writeTo(OutputStream outputStream) throws IOException, MessagingException {
        parse();
        String str = "--" + new ContentType(this.contentType).getParameter("boundary");
        LineOutputStream lineOutputStream = new LineOutputStream(outputStream);
        String str2 = this.preamble;
        if (str2 != null) {
            byte[] bytes = ASCIIUtility.getBytes(str2);
            lineOutputStream.write(bytes);
            if (!(bytes.length <= 0 || bytes[bytes.length - 1] == 13 || bytes[bytes.length - 1] == 10)) {
                lineOutputStream.writeln();
            }
        }
        for (int i = 0; i < this.parts.size(); i++) {
            lineOutputStream.writeln(str);
            ((MimeBodyPart) this.parts.elementAt(i)).writeTo(outputStream);
            lineOutputStream.writeln();
        }
        lineOutputStream.writeln(String.valueOf(str) + "--");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x0186 A[SYNTHETIC] */
    public synchronized void parse() throws MessagingException {
        String str;
        String readLine;
        boolean z;
        boolean z2;
        InternetHeaders internetHeaders;
        ByteArrayOutputStream byteArrayOutputStream;
        LineInputStream lineInputStream;
        MimeBodyPart mimeBodyPart;
        int read;
        int i;
        String readLine2;
        String str2;
        if (!this.parsed) {
            if (bmparse) {
                parsebm();
                return;
            }
            try {
                InputStream inputStream = this.ds.getInputStream();
                InputStream bufferedInputStream = ((inputStream instanceof ByteArrayInputStream) || (inputStream instanceof BufferedInputStream) || (inputStream instanceof SharedInputStream)) ? inputStream : new BufferedInputStream(inputStream);
                SharedInputStream sharedInputStream = bufferedInputStream instanceof SharedInputStream ? (SharedInputStream) bufferedInputStream : null;
                String parameter = new ContentType(this.contentType).getParameter("boundary");
                if (parameter != null) {
                    str = "--" + parameter;
                } else if (ignoreMissingBoundaryParameter) {
                    str = null;
                } else {
                    throw new MessagingException("Missing boundary parameter");
                }
                try {
                    LineInputStream lineInputStream2 = new LineInputStream(bufferedInputStream);
                    StringBuffer stringBuffer = null;
                    String str3 = null;
                    while (true) {
                        readLine = lineInputStream2.readLine();
                        char c = '\t';
                        char c2 = ' ';
                        z = false;
                        z = false;
                        z2 = true;
                        if (readLine == null) {
                            break;
                        }
                        int length = readLine.length() - 1;
                        while (true) {
                            if (length < 0) {
                                break;
                            }
                            char charAt = readLine.charAt(length);
                            if (charAt != c2 && charAt != c) {
                                break;
                            }
                            length--;
                            lineInputStream2 = lineInputStream2;
                            c = '\t';
                            c2 = ' ';
                            z = false;
                            z2 = true;
                        }
                        int i2 = z ? 1 : 0;
                        int i3 = z ? 1 : 0;
                        int i4 = z ? 1 : 0;
                        int i5 = z ? 1 : 0;
                        readLine = readLine.substring(i2, length + 1);
                        if (str == null) {
                            if (readLine.startsWith("--")) {
                                str = readLine;
                                break;
                            }
                        } else if (readLine.equals(str)) {
                            break;
                        }
                        if (readLine.length() > 0) {
                            if (str3 == null) {
                                try {
                                    str2 = System.getProperty("line.separator", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                                } catch (SecurityException unused) {
                                    str2 = ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
                                }
                                str3 = str2;
                            }
                            if (stringBuffer == null) {
                                stringBuffer = new StringBuffer(readLine.length() + 2);
                            }
                            stringBuffer.append(readLine);
                            stringBuffer.append(str3);
                        }
                        lineInputStream2 = lineInputStream2;
                    }
                    if (readLine != null) {
                        if (stringBuffer != null) {
                            this.preamble = stringBuffer.toString();
                        }
                        byte[] bytes = ASCIIUtility.getBytes(str);
                        int length2 = bytes.length;
                        long j = 0;
                        long j2 = 0;
                        boolean z3 = false;
                        while (true) {
                            if (z3) {
                                break;
                            }
                            if (sharedInputStream != null) {
                                j = sharedInputStream.getPosition();
                                do {
                                    readLine2 = lineInputStream2.readLine();
                                    if (readLine2 == null) {
                                        break;
                                    }
                                } while (readLine2.length() > 0);
                                if (readLine2 != null) {
                                    internetHeaders = null;
                                } else if (ignoreMissingEndBoundary) {
                                    this.complete = z;
                                } else {
                                    throw new MessagingException("missing multipart end boundary");
                                }
                            } else {
                                internetHeaders = createInternetHeaders(bufferedInputStream);
                            }
                            if (bufferedInputStream.markSupported()) {
                                if (sharedInputStream == null) {
                                    byteArrayOutputStream = new ByteArrayOutputStream();
                                } else {
                                    j2 = sharedInputStream.getPosition();
                                    byteArrayOutputStream = null;
                                }
                                int i6 = -1;
                                boolean z4 = true;
                                int i7 = -1;
                                while (true) {
                                    if (z4) {
                                        bufferedInputStream.mark(length2 + 4 + 1000);
                                        int i8 = 0;
                                        while (true) {
                                            if (i8 >= length2) {
                                                lineInputStream = lineInputStream2;
                                                break;
                                            }
                                            lineInputStream = lineInputStream2;
                                            if (bufferedInputStream.read() != (bytes[i8] & UByte.MAX_VALUE)) {
                                                break;
                                            }
                                            i8++;
                                            lineInputStream2 = lineInputStream;
                                        }
                                        if (i8 == length2) {
                                            int read2 = bufferedInputStream.read();
                                            if (read2 != 45 || bufferedInputStream.read() != 45) {
                                                while (true) {
                                                    if (read2 != 32 && read2 != 9) {
                                                        break;
                                                    }
                                                    read2 = bufferedInputStream.read();
                                                }
                                                if (read2 == 10) {
                                                    break;
                                                } else if (read2 == 13) {
                                                    bufferedInputStream.mark(1);
                                                    if (bufferedInputStream.read() != 10) {
                                                        bufferedInputStream.reset();
                                                    }
                                                }
                                            } else {
                                                this.complete = true;
                                                z3 = true;
                                                break;
                                            }
                                        }
                                        bufferedInputStream.reset();
                                        if (byteArrayOutputStream != null) {
                                            if (i6 != -1) {
                                                byteArrayOutputStream.write(i6);
                                                if (i7 != -1) {
                                                    byteArrayOutputStream.write(i7);
                                                }
                                                i6 = -1;
                                                i7 = -1;
                                                read = bufferedInputStream.read();
                                                if (read < 0) {
                                                    if (ignoreMissingEndBoundary) {
                                                        this.complete = false;
                                                        z3 = true;
                                                    } else {
                                                        throw new MessagingException("missing multipart end boundary");
                                                    }
                                                } else if (read == 13 || read == 10) {
                                                    if (sharedInputStream != null) {
                                                        j2 = sharedInputStream.getPosition() - 1;
                                                    }
                                                    if (read == 13) {
                                                        bufferedInputStream.mark(1);
                                                        int read3 = bufferedInputStream.read();
                                                        if (read3 == 10) {
                                                            i7 = read3;
                                                        } else {
                                                            bufferedInputStream.reset();
                                                        }
                                                    }
                                                    i6 = read;
                                                    lineInputStream2 = lineInputStream;
                                                    z4 = true;
                                                } else {
                                                    if (byteArrayOutputStream != null) {
                                                        byteArrayOutputStream.write(read);
                                                    }
                                                    lineInputStream2 = lineInputStream;
                                                    z4 = false;
                                                }
                                            } else {
                                                i = i7;
                                                i7 = i;
                                                read = bufferedInputStream.read();
                                                if (read < 0) {
                                                }
                                            }
                                        }
                                    } else {
                                        lineInputStream = lineInputStream2;
                                    }
                                    i = i7;
                                    i7 = i;
                                    read = bufferedInputStream.read();
                                    if (read < 0) {
                                    }
                                }
                                if (sharedInputStream != null) {
                                    mimeBodyPart = createMimeBodyPart(sharedInputStream.newStream(j, j2));
                                } else {
                                    mimeBodyPart = createMimeBodyPart(internetHeaders, byteArrayOutputStream.toByteArray());
                                }
                                super.addBodyPart(mimeBodyPart);
                                lineInputStream2 = lineInputStream;
                                z = false;
                                z2 = true;
                            } else {
                                throw new MessagingException("Stream doesn't support mark");
                            }
                        }
                        try {
                            bufferedInputStream.close();
                        } catch (IOException unused2) {
                        }
                        this.parsed = z2;
                        return;
                    }
                    throw new MessagingException("Missing start boundary");
                } catch (IOException e) {
                    throw new MessagingException("IO Error", e);
                } catch (Throwable th) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException unused3) {
                    }
                    throw th;
                }
            } catch (Exception e2) {
                throw new MessagingException("No inputstream from datasource", e2);
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r12v26 */
    /* JADX WARN: Type inference failed for: r12v27 */
    /* JADX WARN: Type inference failed for: r12v28 */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0153, code lost:
        if (r12 == 10) goto L_0x0158;
     */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0186  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x01c1 A[EDGE_INSN: B:237:0x01c1->B:135:0x01c1 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    private synchronized void parsebm() throws MessagingException {
        String str;
        String readLine;
        boolean z;
        boolean z2;
        long j;
        InternetHeaders internetHeaders;
        ByteArrayOutputStream byteArrayOutputStream;
        int readFully;
        LineInputStream lineInputStream;
        int[] iArr;
        int[] iArr2;
        long j2;
        int i;
        byte[] bArr;
        long j3;
        MimeBodyPart mimeBodyPart;
        byte[] bArr2;
        int i2;
        int read;
        String readLine2;
        String str2;
        if (!this.parsed) {
            try {
                InputStream inputStream = this.ds.getInputStream();
                InputStream bufferedInputStream = ((inputStream instanceof ByteArrayInputStream) || (inputStream instanceof BufferedInputStream) || (inputStream instanceof SharedInputStream)) ? inputStream : new BufferedInputStream(inputStream);
                SharedInputStream sharedInputStream = bufferedInputStream instanceof SharedInputStream ? (SharedInputStream) bufferedInputStream : null;
                String parameter = new ContentType(this.contentType).getParameter("boundary");
                if (parameter != null) {
                    str = "--" + parameter;
                } else if (ignoreMissingBoundaryParameter) {
                    str = null;
                } else {
                    throw new MessagingException("Missing boundary parameter");
                }
                try {
                    LineInputStream lineInputStream2 = new LineInputStream(bufferedInputStream);
                    StringBuffer stringBuffer = null;
                    String str3 = null;
                    while (true) {
                        readLine = lineInputStream2.readLine();
                        char c = '\t';
                        char c2 = ' ';
                        z = false;
                        ?? r12 = 0;
                        z2 = true;
                        z2 = true;
                        if (readLine == null) {
                            break;
                        }
                        int length = readLine.length() - 1;
                        while (true) {
                            if (length < 0) {
                                break;
                            }
                            char charAt = readLine.charAt(length);
                            if (charAt != c2 && charAt != c) {
                                break;
                            }
                            length--;
                            lineInputStream2 = lineInputStream2;
                            c = '\t';
                            c2 = ' ';
                            r12 = 0;
                            z2 = true;
                        }
                        readLine = readLine.substring(r12 == true ? 1 : 0, length + 1);
                        if (str == null) {
                            if (readLine.startsWith("--")) {
                                str = readLine;
                                z = r12;
                                break;
                            }
                        } else if (readLine.equals(str)) {
                            z = r12;
                            break;
                        }
                        if (readLine.length() > 0) {
                            if (str3 == null) {
                                try {
                                    str2 = System.getProperty("line.separator", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                                } catch (SecurityException unused) {
                                    str2 = ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
                                }
                                str3 = str2;
                            }
                            if (stringBuffer == null) {
                                stringBuffer = new StringBuffer(readLine.length() + 2);
                            }
                            stringBuffer.append(readLine);
                            stringBuffer.append(str3);
                        }
                        lineInputStream2 = lineInputStream2;
                    }
                    if (readLine != null) {
                        if (stringBuffer != null) {
                            this.preamble = stringBuffer.toString();
                        }
                        byte[] bytes = ASCIIUtility.getBytes(str);
                        int length2 = bytes.length;
                        int[] iArr3 = new int[256];
                        int i3 = 0;
                        boolean z3 = z;
                        while (i3 < length2) {
                            byte b = bytes[i3];
                            i3++;
                            iArr3[b] = i3;
                            iArr3 = iArr3;
                            lineInputStream2 = lineInputStream2;
                            bytes = bytes;
                            z3 = false;
                            z2 = true;
                        }
                        int[] iArr4 = new int[length2];
                        int i4 = length2;
                        boolean z4 = z3;
                        while (i4 > 0) {
                            int i5 = length2 - 1;
                            while (true) {
                                if (i5 >= i4) {
                                    if (bytes[i5] != bytes[i5 - i4]) {
                                        break;
                                    }
                                    iArr4[i5 - 1] = i4;
                                    i5--;
                                } else {
                                    while (i5 > 0) {
                                        i5--;
                                        iArr4[i5] = i4;
                                    }
                                }
                            }
                            i4--;
                            iArr3 = iArr3;
                            lineInputStream2 = lineInputStream2;
                            bytes = bytes;
                            iArr4 = iArr4;
                            z4 = false;
                            z2 = true;
                        }
                        int i6 = length2 - 1;
                        int i7 = z2 ? 1 : 0;
                        int i8 = z2 ? 1 : 0;
                        int i9 = z2 ? 1 : 0;
                        int i10 = z2 ? 1 : 0;
                        int i11 = z2 ? 1 : 0;
                        iArr4[i6] = i7;
                        long j4 = 0;
                        long j5 = 0;
                        boolean z5 = false;
                        boolean z6 = z4;
                        while (true) {
                            if (z5) {
                                break;
                            }
                            if (sharedInputStream != null) {
                                long position = sharedInputStream.getPosition();
                                do {
                                    readLine2 = lineInputStream2.readLine();
                                    if (readLine2 == null) {
                                        break;
                                    }
                                } while (readLine2.length() > 0);
                                if (readLine2 != null) {
                                    j = position;
                                    internetHeaders = null;
                                } else if (ignoreMissingEndBoundary) {
                                    this.complete = z6;
                                } else {
                                    throw new MessagingException("missing multipart end boundary");
                                }
                            } else {
                                j = j4;
                                internetHeaders = createInternetHeaders(bufferedInputStream);
                            }
                            if (bufferedInputStream.markSupported()) {
                                if (sharedInputStream == null) {
                                    byteArrayOutputStream = new ByteArrayOutputStream();
                                } else {
                                    j5 = sharedInputStream.getPosition();
                                    byteArrayOutputStream = null;
                                }
                                byte[] bArr3 = new byte[length2];
                                byte[] bArr4 = new byte[length2];
                                int i12 = 0;
                                boolean z7 = true;
                                boolean z8 = z6;
                                while (true) {
                                    bufferedInputStream.mark(length2 + 4 + 1000);
                                    int i13 = z8 ? 1 : 0;
                                    int i14 = z8 ? 1 : 0;
                                    int i15 = z8 ? 1 : 0;
                                    int i16 = z8 ? 1 : 0;
                                    readFully = readFully(bufferedInputStream, bArr3, i13, length2);
                                    if (readFully >= length2) {
                                        int i17 = i6;
                                        while (true) {
                                            if (i17 < 0) {
                                                lineInputStream = lineInputStream2;
                                                break;
                                            }
                                            lineInputStream = lineInputStream2;
                                            if (bArr3[i17] != bytes[i17]) {
                                                break;
                                            }
                                            i17--;
                                            lineInputStream2 = lineInputStream;
                                        }
                                        if (i17 < 0) {
                                            if (!z7) {
                                                byte b2 = bArr4[i12 - 1];
                                                byte b3 = b2 != 13 ? (byte) 10 : 10;
                                                i2 = (b2 == b3 && i12 >= 2 && bArr4[i12 + -2] == 13) ? 2 : 1;
                                                if (!z7 || i2 > 0) {
                                                    if (sharedInputStream == null) {
                                                        iArr = iArr3;
                                                        iArr2 = iArr4;
                                                        j5 = (sharedInputStream.getPosition() - ((long) length2)) - ((long) i2);
                                                    } else {
                                                        iArr = iArr3;
                                                        iArr2 = iArr4;
                                                    }
                                                    read = bufferedInputStream.read();
                                                    if (read != 45) {
                                                        i = i2;
                                                        if (bufferedInputStream.read() == 45) {
                                                            this.complete = true;
                                                            j2 = j5;
                                                            z5 = true;
                                                            break;
                                                        }
                                                    } else {
                                                        i = i2;
                                                    }
                                                    while (true) {
                                                        if (read == 32 && read != 9) {
                                                            break;
                                                        }
                                                        read = bufferedInputStream.read();
                                                        bytes = bytes;
                                                    }
                                                    if (read == 10) {
                                                        break;
                                                    } else if (read == 13) {
                                                        bufferedInputStream.mark(1);
                                                        if (bufferedInputStream.read() != 10) {
                                                            bufferedInputStream.reset();
                                                        }
                                                    } else {
                                                        bArr2 = bytes;
                                                    }
                                                } else {
                                                    bArr2 = bytes;
                                                    iArr = iArr3;
                                                    iArr2 = iArr4;
                                                }
                                                i17 = 0;
                                            }
                                            i2 = 0;
                                            if (!z7) {
                                            }
                                            if (sharedInputStream == null) {
                                            }
                                            read = bufferedInputStream.read();
                                            if (read != 45) {
                                            }
                                            while (true) {
                                                if (read == 32) {
                                                }
                                                read = bufferedInputStream.read();
                                                bytes = bytes;
                                            }
                                            if (read == 10) {
                                            }
                                        } else {
                                            bArr2 = bytes;
                                            iArr = iArr3;
                                            iArr2 = iArr4;
                                        }
                                        int max = Math.max((i17 + 1) - iArr[(bArr3[i17] & ByteCompanionObject.MAX_VALUE) == 1 ? 1 : 0], iArr2[i17]);
                                        if (max < 2) {
                                            if (sharedInputStream == null && i12 > 1) {
                                                byteArrayOutputStream.write(bArr4, 0, i12 - 1);
                                            }
                                            bufferedInputStream.reset();
                                            skipFully(bufferedInputStream, 1);
                                            if (i12 >= 1) {
                                                bArr4[0] = bArr4[i12 - 1];
                                                bArr4[1] = bArr3[0];
                                                i12 = 2;
                                            } else {
                                                bArr4[0] = bArr3[0];
                                                i12 = 1;
                                            }
                                        } else {
                                            if (i12 > 0 && sharedInputStream == null) {
                                                byteArrayOutputStream.write(bArr4, 0, i12);
                                            }
                                            bufferedInputStream.reset();
                                            skipFully(bufferedInputStream, (long) max);
                                            i12 = max;
                                            bArr4 = bArr3;
                                            bArr3 = bArr4;
                                        }
                                        iArr3 = iArr;
                                        lineInputStream2 = lineInputStream;
                                        bytes = bArr2;
                                        iArr4 = iArr2;
                                        z8 = false;
                                        z7 = false;
                                    } else if (ignoreMissingEndBoundary) {
                                        if (sharedInputStream != null) {
                                            j5 = sharedInputStream.getPosition();
                                        }
                                        this.complete = z8;
                                        lineInputStream = lineInputStream2;
                                        iArr = iArr3;
                                        iArr2 = iArr4;
                                        j2 = j5;
                                        z5 = true;
                                        i = 0;
                                    } else {
                                        throw new MessagingException("missing multipart end boundary");
                                    }
                                }
                                j2 = j5;
                                if (sharedInputStream != null) {
                                    bArr = bytes;
                                    mimeBodyPart = createMimeBodyPart(sharedInputStream.newStream(j, j2));
                                    j3 = j;
                                } else {
                                    bArr = bytes;
                                    int i18 = i12 - i;
                                    if (i18 > 0) {
                                        j3 = j;
                                        byteArrayOutputStream.write(bArr4, 0, i18);
                                    } else {
                                        j3 = j;
                                    }
                                    if (!this.complete && readFully > 0) {
                                        byteArrayOutputStream.write(bArr3, 0, readFully);
                                    }
                                    mimeBodyPart = createMimeBodyPart(internetHeaders, byteArrayOutputStream.toByteArray());
                                }
                                super.addBodyPart(mimeBodyPart);
                                j5 = j2;
                                iArr3 = iArr;
                                j4 = j3;
                                lineInputStream2 = lineInputStream;
                                bytes = bArr;
                                iArr4 = iArr2;
                                z6 = false;
                                z2 = true;
                            } else {
                                throw new MessagingException("Stream doesn't support mark");
                            }
                        }
                        try {
                            bufferedInputStream.close();
                        } catch (IOException unused2) {
                        }
                        this.parsed = z2;
                        return;
                    }
                    throw new MessagingException("Missing start boundary");
                } catch (IOException e) {
                    throw new MessagingException("IO Error", e);
                } catch (Throwable th) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException unused3) {
                    }
                    throw th;
                }
            } catch (Exception e2) {
                throw new MessagingException("No inputstream from datasource", e2);
            }
        }
    }

    private static int readFully(InputStream inputStream, byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        if (i2 == 0) {
            return 0;
        }
        while (i2 > 0) {
            int read = inputStream.read(bArr, i, i2);
            if (read <= 0) {
                break;
            }
            i += read;
            i3 += read;
            i2 -= read;
        }
        if (i3 > 0) {
            return i3;
        }
        return -1;
    }

    private void skipFully(InputStream inputStream, long j) throws IOException {
        while (j > 0) {
            long skip = inputStream.skip(j);
            if (skip > 0) {
                j -= skip;
            } else {
                throw new EOFException("can't skip");
            }
        }
    }

    /* access modifiers changed from: protected */
    public InternetHeaders createInternetHeaders(InputStream inputStream) throws MessagingException {
        return new InternetHeaders(inputStream);
    }

    /* access modifiers changed from: protected */
    public MimeBodyPart createMimeBodyPart(InternetHeaders internetHeaders, byte[] bArr) throws MessagingException {
        return new MimeBodyPart(internetHeaders, bArr);
    }

    /* access modifiers changed from: protected */
    public MimeBodyPart createMimeBodyPart(InputStream inputStream) throws MessagingException {
        return new MimeBodyPart(inputStream);
    }
}
