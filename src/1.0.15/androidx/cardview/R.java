package androidx.cardview;

public final class R {

    public static final class attr {
        public static final int cardBackgroundColor = 2130903151;
        public static final int cardCornerRadius = 2130903152;
        public static final int cardElevation = 2130903153;
        public static final int cardMaxElevation = 2130903155;
        public static final int cardPreventCornerOverlap = 2130903156;
        public static final int cardUseCompatPadding = 2130903157;
        public static final int cardViewStyle = 2130903158;
        public static final int contentPadding = 2130903231;
        public static final int contentPaddingBottom = 2130903232;
        public static final int contentPaddingLeft = 2130903233;
        public static final int contentPaddingRight = 2130903234;
        public static final int contentPaddingTop = 2130903235;

        private attr() {
        }
    }

    public static final class color {
        public static final int cardview_dark_background = 2131034153;
        public static final int cardview_light_background = 2131034154;
        public static final int cardview_shadow_end_color = 2131034155;
        public static final int cardview_shadow_start_color = 2131034156;

        private color() {
        }
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131099732;
        public static final int cardview_default_elevation = 2131099733;
        public static final int cardview_default_radius = 2131099734;

        private dimen() {
        }
    }

    public static final class style {
        public static final int Base_CardView = 2131755023;
        public static final int CardView = 2131755229;
        public static final int CardView_Dark = 2131755230;
        public static final int CardView_Light = 2131755231;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.naavote.R.attr.cardBackgroundColor, com.naavote.R.attr.cardCornerRadius, com.naavote.R.attr.cardElevation, com.naavote.R.attr.cardMaxElevation, com.naavote.R.attr.cardPreventCornerOverlap, com.naavote.R.attr.cardUseCompatPadding, com.naavote.R.attr.contentPadding, com.naavote.R.attr.contentPaddingBottom, com.naavote.R.attr.contentPaddingLeft, com.naavote.R.attr.contentPaddingRight, com.naavote.R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;

        private styleable() {
        }
    }

    private R() {
    }
}
