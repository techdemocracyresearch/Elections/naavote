package com.face.liveness.utils;

public enum LivenessTask {
    UNKNOWN,
    LIVENESS_PASSED,
    LIVENESS_FAILED,
    ROTATE_LEFT,
    ROTATE_RIGHT,
    ROTATE_CLOCLWISE,
    ROTATE_COUNTERCLOCLWISE,
    FACE_FORWARD
}
