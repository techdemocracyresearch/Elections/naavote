package com.face.liveness.utils;

import android.util.Base64;
import com.brentvatne.react.ReactVideoView;
import com.bumptech.glide.load.Key;
import com.face.liveness.BuildConfig;
import com.sdklicense.LivenessSDK;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.UUID;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONException;
import org.json.JSONObject;

public class AES256Cipher {
    public static String isValidKey(String str) {
        return decodeKey(str);
    }

    public static void encodeData() {
        String uuid = UUID.randomUUID().toString();
        JSONObject jSONObject = new JSONObject();
        Utils.showLogs("nonce", uuid);
        Utils.showLogs("iv", "Rw3xBgJ3Trj1JYUiXdL16w==");
        try {
            jSONObject.put("expireDate", "26-03-2019");
            jSONObject.put("createdDate", "26-12-2018");
            jSONObject.put(ReactVideoView.EVENT_PROP_METADATA_IDENTIFIER, BuildConfig.LIBRARY_PACKAGE_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String encodeToString = Base64.encodeToString(encrypt(Base64.decode("Rw3xBgJ3Trj1JYUiXdL16w==", 2), Base64.decode(uuid, 2), jSONObject.toString().getBytes()), 2);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("cn", Base64.encodeToString(uuid.getBytes(), 2));
            jSONObject2.put("gkey", encodeToString);
            Utils.showLogs("KEY string", jSONObject2.toString());
            Utils.showLogs("FinalKEY ", Base64.encodeToString(jSONObject2.toString().getBytes(), 2));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static byte[] encrypt(byte[] bArr, byte[] bArr2, byte[] bArr3) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(1, secretKeySpec, ivParameterSpec);
        return instance.doFinal(bArr3);
    }

    private static String decodeKey(String str) {
        Utils.showLogs("KEY", str);
        String stringFromJNI = LivenessSDK.stringFromJNI();
        try {
            JSONObject jSONObject = new JSONObject(new String(Base64.decode(str, 2), Key.STRING_CHARSET_NAME));
            String str2 = new String(decrypt(Base64.decode(stringFromJNI, 2), Base64.decode(jSONObject.getString("cn"), 2), Base64.decode(jSONObject.getString("gkey"), 2)), Key.STRING_CHARSET_NAME);
            Utils.showLogs("response", str2);
            Utils.showLogs("package from cpp", LivenessSDK.getPackageName());
            JSONObject jSONObject2 = new JSONObject(str2);
            if (jSONObject2.getString(ReactVideoView.EVENT_PROP_METADATA_IDENTIFIER).equals(LivenessSDK.getPackageName())) {
                String string = jSONObject2.getString("expireDate");
                if (System.currentTimeMillis() - new SimpleDateFormat("dd-MM-yyyy").parse(string).getTime() < 0) {
                    return "Success";
                }
                return ErrorUtils.licenseKeyExpiredMessage + string;
            }
            return ErrorUtils.invalidIdentifierKeyMessage + LivenessSDK.getPackageName();
        } catch (Exception e) {
            Utils.showLogs("Exception", e.getMessage());
            return ErrorUtils.invalidKeyMessage;
        }
    }

    private static byte[] decrypt(byte[] bArr, byte[] bArr2, byte[] bArr3) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, secretKeySpec, ivParameterSpec);
        return instance.doFinal(bArr3);
    }
}
