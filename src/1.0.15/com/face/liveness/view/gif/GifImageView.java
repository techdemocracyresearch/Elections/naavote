package com.face.liveness.view.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.ImageView;

public class GifImageView extends ImageView implements Runnable {
    private static final String TAG = "GifDecoderView";
    private boolean animating;
    private OnAnimationStart animationStartCallback = null;
    private OnAnimationStop animationStopCallback = null;
    private Thread animationThread;
    private final Runnable cleanupRunnable = new Runnable() {
        /* class com.face.liveness.view.gif.GifImageView.AnonymousClass2 */

        public void run() {
            GifImageView.this.tmpBitmap = null;
            GifImageView.this.gifDecoder = null;
            GifImageView.this.animationThread = null;
            GifImageView.this.shouldClear = false;
        }
    };
    private OnFrameAvailable frameCallback = null;
    private long framesDisplayDuration = -1;
    private GifDecoder gifDecoder;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private boolean renderFrame;
    private boolean shouldClear;
    private Bitmap tmpBitmap;
    private final Runnable updateResults = new Runnable() {
        /* class com.face.liveness.view.gif.GifImageView.AnonymousClass1 */

        public void run() {
            if (GifImageView.this.tmpBitmap != null && !GifImageView.this.tmpBitmap.isRecycled()) {
                GifImageView gifImageView = GifImageView.this;
                gifImageView.setImageBitmap(gifImageView.tmpBitmap);
            }
        }
    };

    public interface OnAnimationStart {
        void onAnimationStart();
    }

    public interface OnAnimationStop {
        void onAnimationStop();
    }

    public interface OnFrameAvailable {
        Bitmap onFrameAvailable(Bitmap bitmap);
    }

    public GifImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public GifImageView(Context context) {
        super(context);
    }

    public void setBytes(byte[] bArr) {
        GifDecoder gifDecoder2 = new GifDecoder();
        this.gifDecoder = gifDecoder2;
        try {
            gifDecoder2.read(bArr);
            if (this.animating) {
                startAnimationThread();
            } else {
                gotoFrame(0);
            }
        } catch (Exception unused) {
            this.gifDecoder = null;
        }
    }

    public long getFramesDisplayDuration() {
        return this.framesDisplayDuration;
    }

    public void setFramesDisplayDuration(long j) {
        this.framesDisplayDuration = j;
    }

    public void startAnimation() {
        this.animating = true;
        startAnimationThread();
    }

    public boolean isAnimating() {
        return this.animating;
    }

    public void stopAnimation() {
        this.animating = false;
        Thread thread = this.animationThread;
        if (thread != null) {
            thread.interrupt();
            this.animationThread = null;
        }
    }

    public void gotoFrame(int i) {
        if (this.gifDecoder.getCurrentFrameIndex() != i && this.gifDecoder.setFrameIndex(i - 1) && !this.animating) {
            this.renderFrame = true;
            startAnimationThread();
        }
    }

    public void resetAnimation() {
        GifDecoder gifDecoder2 = this.gifDecoder;
        if (gifDecoder2 != null) {
            gifDecoder2.resetLoopIndex();
            gotoFrame(0);
        }
    }

    public void clear() {
        this.animating = false;
        this.renderFrame = false;
        this.shouldClear = true;
        stopAnimation();
        this.handler.post(this.cleanupRunnable);
    }

    private boolean canStart() {
        return (this.animating || this.renderFrame) && this.gifDecoder != null && this.animationThread == null;
    }

    public int getGifWidth() {
        return this.gifDecoder.getWidth();
    }

    public int getFrameCount() {
        return this.gifDecoder.getFrameCount();
    }

    public int getGifHeight() {
        return this.gifDecoder.getHeight();
    }

    public void run() {
        long j;
        OnAnimationStart onAnimationStart = this.animationStartCallback;
        if (onAnimationStart != null) {
            onAnimationStart.onAnimationStart();
        }
        while (true) {
            if (!this.animating && !this.renderFrame) {
                break;
            }
            boolean advance = this.gifDecoder.advance();
            try {
                long nanoTime = System.nanoTime();
                Bitmap nextFrame = this.gifDecoder.getNextFrame();
                this.tmpBitmap = nextFrame;
                OnFrameAvailable onFrameAvailable = this.frameCallback;
                if (onFrameAvailable != null) {
                    this.tmpBitmap = onFrameAvailable.onFrameAvailable(nextFrame);
                }
                j = (System.nanoTime() - nanoTime) / 1000000;
                try {
                    this.handler.post(this.updateResults);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException unused) {
                }
            } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException unused2) {
                j = 0;
            }
            this.renderFrame = false;
            if (this.animating && advance) {
                try {
                    int nextDelay = (int) (((long) this.gifDecoder.getNextDelay()) - j);
                    if (nextDelay > 0) {
                        long j2 = this.framesDisplayDuration;
                        if (j2 <= 0) {
                            j2 = (long) nextDelay;
                        }
                        Thread.sleep(j2);
                    }
                } catch (Exception unused3) {
                }
                if (!this.animating) {
                    break;
                }
            } else {
                this.animating = false;
            }
        }
        if (this.shouldClear) {
            this.handler.post(this.cleanupRunnable);
        }
        this.animationThread = null;
        OnAnimationStop onAnimationStop = this.animationStopCallback;
        if (onAnimationStop != null) {
            onAnimationStop.onAnimationStop();
        }
    }

    public OnFrameAvailable getOnFrameAvailable() {
        return this.frameCallback;
    }

    public void setOnFrameAvailable(OnFrameAvailable onFrameAvailable) {
        this.frameCallback = onFrameAvailable;
    }

    public OnAnimationStop getOnAnimationStop() {
        return this.animationStopCallback;
    }

    public void setOnAnimationStop(OnAnimationStop onAnimationStop) {
        this.animationStopCallback = onAnimationStop;
    }

    public void setOnAnimationStart(OnAnimationStart onAnimationStart) {
        this.animationStartCallback = onAnimationStart;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clear();
    }

    private void startAnimationThread() {
        if (canStart()) {
            Thread thread = new Thread(this);
            this.animationThread = thread;
            thread.start();
        }
    }
}
