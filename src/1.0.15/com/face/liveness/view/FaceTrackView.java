package com.face.liveness.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import androidx.core.internal.view.SupportMenu;
import com.face.liveness.R;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;

public class FaceTrackView extends View {
    Bitmap bitmap;
    RectF dest;
    Bitmap eye;
    private FirebaseVisionFace face;
    private float mHeightScaleFactor = 1.0f;
    private float mPreviewHeight;
    private float mPreviewWidth;
    private float mWidthScaleFactor = 1.0f;
    Paint myPointPaint = new Paint();
    Paint myRectPaint = new Paint();

    public FaceTrackView(Context context) {
        super(context);
    }

    public FaceTrackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void drawUpdatedFace(Context context, FirebaseVisionFace firebaseVisionFace, float f, float f2) {
        this.face = firebaseVisionFace;
        this.mPreviewWidth = f;
        this.mPreviewHeight = f2;
        if (this.bitmap == null) {
            this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.head);
        }
        if (this.eye == null) {
            Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), R.drawable.eye);
            this.eye = decodeResource;
            this.eye = Bitmap.createScaledBitmap(decodeResource, 50, 30, false);
        }
        this.mWidthScaleFactor = ((float) getWidth()) / this.mPreviewWidth;
        this.mHeightScaleFactor = ((float) getHeight()) / this.mPreviewHeight;
        this.myPointPaint.setStrokeWidth(5.0f);
        this.myPointPaint.setColor(SupportMenu.CATEGORY_MASK);
        this.myPointPaint.setStyle(Paint.Style.STROKE);
        this.myRectPaint.setColor(Color.parseColor("#5000ff00"));
        this.myRectPaint.setStyle(Paint.Style.FILL);
        invalidate();
    }

    public float scaleX(float f) {
        return f * this.mWidthScaleFactor;
    }

    public float scaleY(float f) {
        return f * this.mHeightScaleFactor;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        FirebaseVisionFace firebaseVisionFace = this.face;
        if (firebaseVisionFace != null) {
            for (FirebaseVisionPoint firebaseVisionPoint : firebaseVisionFace.getContour(1).getPoints()) {
                canvas.drawCircle(firebaseVisionPoint.getX().floatValue(), firebaseVisionPoint.getY().floatValue(), 2.0f, this.myPointPaint);
            }
            float scaleX = scaleX(((float) this.face.getBoundingBox().width()) / 2.0f);
            scaleY(((float) this.face.getBoundingBox().height()) / 2.0f);
            canvas.drawRect(((float) this.face.getBoundingBox().left) + scaleX, (float) this.face.getBoundingBox().top, ((float) this.face.getBoundingBox().right) + scaleX, (float) this.face.getBoundingBox().bottom, this.myRectPaint);
        }
    }

    public Bitmap rotate_bitmap(Bitmap bitmap2, float f) {
        Matrix matrix = new Matrix();
        matrix.postRotate(f);
        return Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), bitmap2.getHeight(), matrix, false);
    }
}
