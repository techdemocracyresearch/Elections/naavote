package com.google.firebase.ml.vision;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.firebase.FirebaseApp;
import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;
import com.google.firebase.ml.vision.automl.internal.zzb;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zza implements ComponentFactory {
    static final ComponentFactory zzbil = new zza();

    private zza() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        Preconditions.checkArgument(((zzqf) componentContainer.get(zzqf.class)).zzoh().getName().equals(FirebaseApp.DEFAULT_APP_NAME), "FirebaseAutoMLModelManager doesn't support Nondefault FirebaseApp");
        return new zzb((zzqf) componentContainer.get(zzqf.class), (zzqg) ((zzqg.zza) componentContainer.get(zzqg.zza.class)).get(5));
    }
}
