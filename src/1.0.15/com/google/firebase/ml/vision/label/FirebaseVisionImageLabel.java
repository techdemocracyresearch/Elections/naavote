package com.google.firebase.ml.vision.label;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzkv;
import com.google.android.gms.internal.firebase_ml.zzms;
import com.google.android.gms.internal.firebase_ml.zzrq;
import com.google.android.gms.vision.label.ImageLabel;
import com.google.firebase.ml.vision.automl.internal.zzl;
import javax.annotation.Nullable;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionImageLabel {
    private final String text;
    private final String zzbpu;
    private final float zzbpv;

    public FirebaseVisionImageLabel(ImageLabel imageLabel) {
        this(imageLabel.getLabel(), imageLabel.getConfidence(), imageLabel.getMid());
    }

    @Nullable
    public static FirebaseVisionImageLabel zza(@Nullable zzkv zzkv) {
        if (zzkv == null) {
            return null;
        }
        return new FirebaseVisionImageLabel(zzkv.getDescription(), zzrq.zza(zzkv.zzir()), zzkv.getMid());
    }

    public static FirebaseVisionImageLabel zza(zzl zzl) {
        Preconditions.checkNotNull(zzl, "Returned image label parcel can not be null");
        return new FirebaseVisionImageLabel(zzl.text, zzl.zzbpv, zzl.zzbpu);
    }

    private FirebaseVisionImageLabel(@Nullable String str, float f, @Nullable String str2) {
        this.text = zzms.zzbb(str);
        this.zzbpu = str2;
        if (Float.compare(f, 0.0f) < 0) {
            f = 0.0f;
        } else if (Float.compare(f, 1.0f) > 0) {
            f = 1.0f;
        }
        this.zzbpv = f;
    }

    @Nullable
    public String getEntityId() {
        return this.zzbpu;
    }

    public String getText() {
        return this.text;
    }

    public float getConfidence() {
        return this.zzbpv;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionImageLabel)) {
            return false;
        }
        FirebaseVisionImageLabel firebaseVisionImageLabel = (FirebaseVisionImageLabel) obj;
        return Objects.equal(this.zzbpu, firebaseVisionImageLabel.getEntityId()) && Objects.equal(this.text, firebaseVisionImageLabel.getText()) && Float.compare(this.zzbpv, firebaseVisionImageLabel.getConfidence()) == 0;
    }

    public int hashCode() {
        return Objects.hashCode(this.zzbpu, this.text, Float.valueOf(this.zzbpv));
    }
}
