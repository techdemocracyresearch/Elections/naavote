package com.google.firebase.ml.vision.objects.internal;

import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.internal.firebase_ml.zzmw;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzoc;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzpu;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqm;
import com.google.android.gms.internal.firebase_ml.zzqp;
import com.google.android.gms.internal.firebase_ml.zzsa;
import com.google.android.gms.internal.firebase_ml.zzsb;
import com.google.android.gms.internal.firebase_ml.zzsf;
import com.google.android.gms.internal.firebase_ml.zzwz;
import com.google.android.gms.vision.Frame;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.objects.FirebaseVisionObject;
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetectorOptions;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zze implements zzpu<List<FirebaseVisionObject>, zzsf>, zzqp {
    private static final AtomicBoolean zzbob = new AtomicBoolean(true);
    private final zzqf zzbkb;
    private final zzqg zzbmd;
    private final FirebaseVisionObjectDetectorOptions zzbuj;
    private long zzbuk = -1;
    private final zzns.zzas zzbul;
    private IObjectDetector zzbum;

    public zze(zzqf zzqf, FirebaseVisionObjectDetectorOptions firebaseVisionObjectDetectorOptions) {
        zzns.zzas.zza zza;
        Preconditions.checkNotNull(zzqf, "Context can not be null");
        Preconditions.checkNotNull(firebaseVisionObjectDetectorOptions, "FirebaseVisionObjectDetectorOptions can not be null");
        this.zzbuj = firebaseVisionObjectDetectorOptions;
        this.zzbkb = zzqf;
        zzqg zza2 = zzqg.zza(zzqf, 1);
        this.zzbmd = zza2;
        zzns.zzas.zzb zznh = zzns.zzas.zznh();
        int zzqw = firebaseVisionObjectDetectorOptions.zzqw();
        if (zzqw == 1) {
            zza = zzns.zzas.zza.STREAM;
        } else if (zzqw != 2) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Unexpected detector mode: ");
            sb.append(zzqw);
            Log.e("ObjectDetectorTask", sb.toString());
            zza = zzns.zzas.zza.MODE_UNSPECIFIED;
        } else {
            zza = zzns.zzas.zza.SINGLE_IMAGE;
        }
        zzns.zzas zzas = (zzns.zzas) ((zzwz) zznh.zzb(zza).zzao(firebaseVisionObjectDetectorOptions.zzqy()).zzap(firebaseVisionObjectDetectorOptions.zzqx()).zzvb());
        this.zzbul = zzas;
        zza2.zza(zzns.zzad.zzma().zza(zzns.zzar.zznf().zzc(zzas).zzm(zzoc.NO_ERROR)), zzod.ON_DEVICE_OBJECT_CREATE);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return this;
    }

    /* access modifiers changed from: private */
    public final synchronized List<FirebaseVisionObject> zza(zzsf zzsf) throws FirebaseMLException {
        Preconditions.checkNotNull(zzsf, "Mobile vision input can not bu null");
        Preconditions.checkNotNull(zzsf.zzbsk, "Input image can not be null");
        Preconditions.checkNotNull(zzsf.zzbrv, "Input frame can not be null");
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            if (this.zzbum == null) {
                Log.e("ObjectDetectorTask", "Object detector is not initialized.");
                return zzmw.zzji();
            }
            boolean z = true;
            if (this.zzbuj.zzqw() == 1) {
                long j = this.zzbuk;
                if (j > 0) {
                    if (elapsedRealtime - j <= 300) {
                        z = false;
                    }
                    if (z) {
                        Log.w("ObjectDetectorTask", "Object detector pipeline is reset.");
                        zzra();
                    }
                }
            }
            this.zzbuk = elapsedRealtime;
            IObjectWrapper wrap = ObjectWrapper.wrap(zzsf.zzbrv);
            IObjectDetector iObjectDetector = this.zzbum;
            Frame.Metadata metadata = zzsf.zzbrv.getMetadata();
            zzj[] zzc = iObjectDetector.zzc(wrap, new zzsb(metadata.getWidth(), metadata.getHeight(), metadata.getId(), metadata.getTimestampMillis(), metadata.getRotation()));
            ArrayList arrayList = new ArrayList();
            for (zzj zzj : zzc) {
                arrayList.add(new FirebaseVisionObject(zzj));
            }
            zza(zzoc.NO_ERROR, zzsf, arrayList, elapsedRealtime);
            zzbob.set(false);
            return arrayList;
        } catch (RemoteException e) {
            Log.e("ObjectDetectorTask", "Error calling object detector inference", e);
            zza(zzoc.UNKNOWN_ERROR, zzsf, zzmw.zzji(), elapsedRealtime);
            throw new FirebaseMLException("Cannot run object detector.", 14);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void zzol() throws FirebaseMLException {
        try {
            if (this.zzbum == null) {
                zzc zzqz = zzqz();
                if (zzqz != null) {
                    IObjectWrapper wrap = ObjectWrapper.wrap(this.zzbkb.getApplicationContext());
                    FirebaseVisionObjectDetectorOptions firebaseVisionObjectDetectorOptions = this.zzbuj;
                    this.zzbum = zzqz.newObjectDetector(wrap, new ObjectDetectorOptionsParcel(firebaseVisionObjectDetectorOptions.zzqw(), firebaseVisionObjectDetectorOptions.zzqy(), firebaseVisionObjectDetectorOptions.zzqx()));
                    zzp(zzoc.NO_ERROR);
                } else {
                    Log.e("ObjectDetectorTask", "Error when creating object detector creator");
                    zzp(zzoc.UNKNOWN_ERROR);
                    throw new FirebaseMLException("Cannot load object detector module.", 14);
                }
            }
            this.zzbum.start();
        } catch (RemoteException e) {
            Log.e("ObjectDetectorTask", "Error when creating object detector");
            zzp(zzoc.UNKNOWN_ERROR);
            throw new FirebaseMLException("Can not create ObjectDetector", 14, e);
        }
    }

    private final zzc zzqz() throws FirebaseMLException {
        try {
            return zzb.asInterface(DynamiteModule.load(this.zzbkb.getApplicationContext(), DynamiteModule.PREFER_LOCAL, "com.google.firebase.ml.vision.dynamite.objects").instantiate("com.google.firebase.ml.vision.objects.ObjectDetectorCreator"));
        } catch (DynamiteModule.LoadingException e) {
            Log.e("ObjectDetectorTask", "Error when loading object detector module");
            throw new FirebaseMLException("Cannot load object detector module.", 14, e);
        }
    }

    private final synchronized void zzra() {
        try {
            IObjectDetector iObjectDetector = this.zzbum;
            if (iObjectDetector != null) {
                iObjectDetector.stop();
            }
        } catch (RemoteException e) {
            Log.e("ObjectDetectorTask", "Error calling object detector stop", e);
        }
        try {
            IObjectDetector iObjectDetector2 = this.zzbum;
            if (iObjectDetector2 != null) {
                iObjectDetector2.start();
            }
        } catch (RemoteException e2) {
            Log.e("ObjectDetectorTask", "Error calling object detector restart", e2);
        }
        zzbob.set(true);
        return;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final synchronized void release() {
        try {
            IObjectDetector iObjectDetector = this.zzbum;
            if (iObjectDetector != null) {
                iObjectDetector.stop();
            }
            zzbob.set(true);
            this.zzbmd.zza(zzns.zzad.zzma(), zzod.ON_DEVICE_OBJECT_CLOSE);
        } catch (RemoteException e) {
            Log.e("ObjectDetectorTask", "Error calling object detector stop", e);
        }
    }

    private final void zzp(zzoc zzoc) {
        this.zzbmd.zza(zzns.zzad.zzma().zza(zzns.zzau.zznl().zze(this.zzbul).zzn(zzoc)), zzod.ON_DEVICE_OBJECT_LOAD);
    }

    private final void zza(zzoc zzoc, zzsf zzsf, List<FirebaseVisionObject> list, long j) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        this.zzbmd.zza(new zzh(this, list, elapsedRealtime, zzoc, zzsf), zzod.ON_DEVICE_OBJECT_INFERENCE);
        zzqm zzqm = zzg.zzboj;
        this.zzbmd.zza((zzns.zzf.zzb) ((zzwz) zzns.zzf.zzb.zzkm().zza(this.zzbul).zzg(zzoc).zzt(zzbob.get()).zzf(zzsa.zzc(zzsf)).zzs(!list.isEmpty()).zzvb()), elapsedRealtime, zzod.AGGREGATED_ON_DEVICE_OBJECT_INFERENCE, zzqm);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzns.zzad.zza zza(List list, long j, zzoc zzoc, zzsf zzsf) {
        zzns.zzal.zzb zzb;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            FirebaseVisionObject firebaseVisionObject = (FirebaseVisionObject) it.next();
            zzns.zzal.zza zzmr = zzns.zzal.zzmr();
            int classificationCategory = firebaseVisionObject.getClassificationCategory();
            if (classificationCategory == 0) {
                zzb = zzns.zzal.zzb.CATEGORY_UNKNOWN;
            } else if (classificationCategory == 1) {
                zzb = zzns.zzal.zzb.CATEGORY_HOME_GOOD;
            } else if (classificationCategory == 2) {
                zzb = zzns.zzal.zzb.CATEGORY_FASHION_GOOD;
            } else if (classificationCategory == 3) {
                zzb = zzns.zzal.zzb.CATEGORY_FOOD;
            } else if (classificationCategory == 4) {
                zzb = zzns.zzal.zzb.CATEGORY_PLACE;
            } else if (classificationCategory != 5) {
                StringBuilder sb = new StringBuilder(32);
                sb.append("Unexpected category: ");
                sb.append(classificationCategory);
                Log.e("ObjectDetectorTask", sb.toString());
                zzb = zzns.zzal.zzb.CATEGORY_UNKNOWN;
            } else {
                zzb = zzns.zzal.zzb.CATEGORY_PLANT;
            }
            zzns.zzal.zza zza = zzmr.zza(zzb);
            if (firebaseVisionObject.getClassificationConfidence() != null) {
                zza.zzm(firebaseVisionObject.getClassificationConfidence().floatValue());
            }
            if (firebaseVisionObject.getTrackingId() != null) {
                zza.zzbh(firebaseVisionObject.getTrackingId().intValue());
            }
            arrayList.add((zzns.zzal) ((zzwz) zza.zzvb()));
        }
        return zzns.zzad.zzma().zza(zzns.zzat.zznj().zzf(zzns.zzaf.zzme().zzk(j).zzk(zzoc).zzah(zzbob.get()).zzai(true).zzaj(true)).zzl(zzsa.zzc(zzsf)).zzd(this.zzbul).zzu(arrayList));
    }
}
