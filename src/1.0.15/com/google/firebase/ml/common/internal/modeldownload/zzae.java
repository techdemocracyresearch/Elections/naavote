package com.google.firebase.ml.common.internal.modeldownload;

import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.firebase.ml.common.FirebaseMLException;
import java.io.File;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzae implements zzk {
    private final zzqf zzbkb;
    private final String zzbkn;

    zzae(zzqf zzqf, String str) {
        this.zzbkb = zzqf;
        this.zzbkn = str;
    }

    @Override // com.google.firebase.ml.common.internal.modeldownload.zzk
    public final File zza(File file) throws FirebaseMLException {
        File zzc = new zzi(this.zzbkb).zzc(this.zzbkn, zzn.TRANSLATE);
        File file2 = new File(zzc, String.valueOf(zzi.zzb(zzc) + 1));
        if (file.renameTo(file2)) {
            zzz.zzbin.d("RemoteModelFileManager", "Rename to serving model successfully");
            file2.setExecutable(false);
            file2.setWritable(false);
            return file2;
        }
        zzz.zzbin.d("RemoteModelFileManager", "Rename to serving model failed, remove the temp file.");
        if (file.delete()) {
            return null;
        }
        GmsLogger gmsLogger = zzz.zzbin;
        String valueOf = String.valueOf(file.getAbsolutePath());
        gmsLogger.d("RemoteModelFileManager", valueOf.length() != 0 ? "Failed to delete the temp file: ".concat(valueOf) : new String("Failed to delete the temp file: "));
        return null;
    }
}
