package com.google.firebase.ml.common.internal.modeldownload;

import com.google.android.gms.internal.firebase_ml.zzoc;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzl {
    void zze(List<zzoc> list);
}
