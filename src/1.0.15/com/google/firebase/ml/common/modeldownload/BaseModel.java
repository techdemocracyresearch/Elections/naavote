package com.google.firebase.ml.common.modeldownload;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum BaseModel {
    FACE_DETECTION,
    SMART_REPLY,
    TRANSLATE
}
