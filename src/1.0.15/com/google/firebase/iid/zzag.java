package com.google.firebase.iid;

import java.util.concurrent.TimeUnit;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-iid@@20.1.5 */
public final /* synthetic */ class zzag implements Runnable {
    private final zzac zza;

    zzag(zzac zzac) {
        this.zza = zzac;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        if (android.util.Log.isLoggable("MessengerIpcClient", 3) == false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0042, code lost:
        r4 = java.lang.String.valueOf(r1);
        r6 = new java.lang.StringBuilder(java.lang.String.valueOf(r4).length() + 8);
        r6.append("Sending ");
        r6.append(r4);
        android.util.Log.d("MessengerIpcClient", r6.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0066, code lost:
        r3 = r0.zzf.zzb;
        r4 = r0.zzb;
        r5 = android.os.Message.obtain();
        r5.what = r1.zzc;
        r5.arg1 = r1.zza;
        r5.replyTo = r4;
        r4 = new android.os.Bundle();
        r4.putBoolean("oneWay", r1.zza());
        r4.putString("pkg", r3.getPackageName());
        r4.putBundle(com.facebook.common.util.UriUtil.DATA_SCHEME, r1.zzd);
        r5.setData(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r0.zzc.zza(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a4, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a5, code lost:
        r0.zza(2, r1.getMessage());
     */
    public final void run() {
        zzac zzac = this.zza;
        while (true) {
            synchronized (zzac) {
                if (zzac.zza == 2) {
                    if (zzac.zzd.isEmpty()) {
                        zzac.zzb();
                        return;
                    }
                    zzan<?> poll = zzac.zzd.poll();
                    zzac.zze.put(poll.zza, poll);
                    zzac.zzf.zzc.schedule(new zzai(zzac, poll), 30, TimeUnit.SECONDS);
                } else {
                    return;
                }
            }
        }
    }
}
