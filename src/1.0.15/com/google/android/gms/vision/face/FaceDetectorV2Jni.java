package com.google.android.gms.vision.face;

import android.content.res.AssetManager;
import android.os.RemoteException;
import com.google.android.gms.internal.vision.zzbq;
import com.google.android.gms.internal.vision.zzbx;
import com.google.android.gms.internal.vision.zzgd;
import com.google.android.gms.internal.vision.zzhc;
import com.google.android.gms.vision.L;
import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public class FaceDetectorV2Jni {
    private final zzgd zzcn;

    public FaceDetectorV2Jni() {
        zzgd zzfk = zzgd.zzfk();
        this.zzcn = zzfk;
        zzfk.zza(zzbx.zzir);
    }

    private native void closeDetectorJni(long j);

    private native byte[] detectFacesImageByteArrayJni(long j, byte[] bArr, byte[] bArr2);

    private native byte[] detectFacesImageByteArrayMultiPlanesJni(long j, byte[] bArr, byte[] bArr2, byte[] bArr3, int i, int i2, int i3, int i4, int i5, int i6, byte[] bArr4);

    private native byte[] detectFacesImageByteBufferJni(long j, ByteBuffer byteBuffer, byte[] bArr);

    private native byte[] detectFacesImageByteBufferMultiPlanesJni(long j, ByteBuffer byteBuffer, ByteBuffer byteBuffer2, ByteBuffer byteBuffer3, int i, int i2, int i3, int i4, int i5, int i6, byte[] bArr);

    private native long initDetectorJni(byte[] bArr, AssetManager assetManager);

    /* access modifiers changed from: package-private */
    public final long zza(zzbx.zzd zzd, AssetManager assetManager) {
        L.v("%s initialize.start()", "FaceDetectorV2Jni");
        long initDetectorJni = initDetectorJni(zzd.toByteArray(), assetManager);
        L.v("%s initialize.end()", "FaceDetectorV2Jni");
        return initDetectorJni;
    }

    /* access modifiers changed from: package-private */
    public final zzbx.zzc zza(long j, ByteBuffer byteBuffer, zzbq zzbq) throws RemoteException {
        L.v("%s detectFacesImageByteBuffer.start()", "FaceDetectorV2Jni");
        zzbx.zzc zzc = null;
        try {
            byte[] detectFacesImageByteBufferJni = detectFacesImageByteBufferJni(j, byteBuffer, zzbq.toByteArray());
            if (detectFacesImageByteBufferJni != null && detectFacesImageByteBufferJni.length > 0) {
                zzc = zzbx.zzc.zza(detectFacesImageByteBufferJni, this.zzcn);
            }
        } catch (zzhc e) {
            L.e("%s detectFacesImageByteBuffer failed to parse result: %s", "FaceDetectorV2Jni", e.getMessage());
        }
        L.v("%s detectFacesImageByteBuffer.end()", "FaceDetectorV2Jni");
        return zzc;
    }

    /* access modifiers changed from: package-private */
    public final zzbx.zzc zza(long j, byte[] bArr, zzbq zzbq) throws RemoteException {
        L.v("%s detectFacesImageByteArray.start()", "FaceDetectorV2Jni");
        zzbx.zzc zzc = null;
        try {
            byte[] detectFacesImageByteArrayJni = detectFacesImageByteArrayJni(j, bArr, zzbq.toByteArray());
            if (detectFacesImageByteArrayJni != null && detectFacesImageByteArrayJni.length > 0) {
                zzc = zzbx.zzc.zza(detectFacesImageByteArrayJni, this.zzcn);
            }
        } catch (zzhc e) {
            L.e("%s detectFacesImageByteArray failed to parse result: %s", "FaceDetectorV2Jni", e.getMessage());
        }
        L.v("%s detectFacesImageByteArray.end()", "FaceDetectorV2Jni");
        return zzc;
    }

    /* access modifiers changed from: package-private */
    public final zzbx.zzc zza(long j, ByteBuffer byteBuffer, ByteBuffer byteBuffer2, ByteBuffer byteBuffer3, int i, int i2, int i3, int i4, int i5, int i6, zzbq zzbq) {
        zzhc e;
        L.v("%s detectFacesImageByteBufferMultiPlanes.start()", "FaceDetectorV2Jni");
        zzbx.zzc zzc = null;
        try {
            byte[] detectFacesImageByteBufferMultiPlanesJni = detectFacesImageByteBufferMultiPlanesJni(j, byteBuffer, byteBuffer2, byteBuffer3, i, i2, i3, i4, i5, i6, zzbq.toByteArray());
            if (detectFacesImageByteBufferMultiPlanesJni == null || detectFacesImageByteBufferMultiPlanesJni.length <= 0) {
                L.v("%s detectFacesImageByteBuffer.end()", "FaceDetectorV2Jni");
                return zzc;
            }
            try {
                zzc = zzbx.zzc.zza(detectFacesImageByteBufferMultiPlanesJni, this.zzcn);
            } catch (zzhc e2) {
                e = e2;
                L.e("%s detectFacesImageByteBufferMultiPlanes failed to parse result: %s", "FaceDetectorV2Jni", e.getMessage());
                L.v("%s detectFacesImageByteBuffer.end()", "FaceDetectorV2Jni");
                return zzc;
            }
            L.v("%s detectFacesImageByteBuffer.end()", "FaceDetectorV2Jni");
            return zzc;
        } catch (zzhc e3) {
            e = e3;
            L.e("%s detectFacesImageByteBufferMultiPlanes failed to parse result: %s", "FaceDetectorV2Jni", e.getMessage());
            L.v("%s detectFacesImageByteBuffer.end()", "FaceDetectorV2Jni");
            return zzc;
        }
    }

    /* access modifiers changed from: package-private */
    public final zzbx.zzc zza(long j, byte[] bArr, byte[] bArr2, byte[] bArr3, int i, int i2, int i3, int i4, int i5, int i6, zzbq zzbq) {
        zzhc e;
        L.v("%s detectFacesImageByteArrayMultiPlanes.start()", "FaceDetectorV2Jni");
        zzbx.zzc zzc = null;
        try {
            byte[] detectFacesImageByteArrayMultiPlanesJni = detectFacesImageByteArrayMultiPlanesJni(j, bArr, bArr2, bArr3, i, i2, i3, i4, i5, i6, zzbq.toByteArray());
            if (detectFacesImageByteArrayMultiPlanesJni == null || detectFacesImageByteArrayMultiPlanesJni.length <= 0) {
                L.v("%s detectFacesImageByteArrayMultiPlanes.end()", "FaceDetectorV2Jni");
                return zzc;
            }
            try {
                zzc = zzbx.zzc.zza(detectFacesImageByteArrayMultiPlanesJni, this.zzcn);
            } catch (zzhc e2) {
                e = e2;
                L.e("%s detectFacesImageByteArrayMultiPlanes failed to parse result: %s", "FaceDetectorV2Jni", e.getMessage());
                L.v("%s detectFacesImageByteArrayMultiPlanes.end()", "FaceDetectorV2Jni");
                return zzc;
            }
            L.v("%s detectFacesImageByteArrayMultiPlanes.end()", "FaceDetectorV2Jni");
            return zzc;
        } catch (zzhc e3) {
            e = e3;
            L.e("%s detectFacesImageByteArrayMultiPlanes failed to parse result: %s", "FaceDetectorV2Jni", e.getMessage());
            L.v("%s detectFacesImageByteArrayMultiPlanes.end()", "FaceDetectorV2Jni");
            return zzc;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(long j) throws RemoteException {
        L.v("%s closeDetector.start()", "FaceDetectorV2Jni");
        closeDetectorJni(j);
        L.v("%s closeDetector.end()", "FaceDetectorV2Jni");
    }
}
