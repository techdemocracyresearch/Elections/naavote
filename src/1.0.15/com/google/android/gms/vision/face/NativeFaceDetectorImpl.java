package com.google.android.gms.vision.face;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.vision.zzp;
import com.google.android.gms.vision.face.internal.client.FaceParcel;
import com.google.android.gms.vision.face.internal.client.zzg;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
public class NativeFaceDetectorImpl extends zzg {
    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final void zzm() throws RemoteException {
        throw new NoSuchMethodError();
    }

    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final FaceParcel[] zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3, int i, int i2, int i3, int i4, int i5, int i6, zzp zzp) throws RemoteException {
        throw new NoSuchMethodError();
    }

    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final FaceParcel[] zzc(IObjectWrapper iObjectWrapper, zzp zzp) throws RemoteException {
        throw new NoSuchMethodError();
    }

    @Override // com.google.android.gms.vision.face.internal.client.zzh
    public final boolean zzd(int i) throws RemoteException {
        throw new NoSuchMethodError();
    }
}
