package com.google.android.gms.vision.text;

import android.graphics.Point;
import android.graphics.Rect;
import com.google.android.gms.internal.vision.zzaj;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision@@20.0.0 */
public class Element implements Text {
    private zzaj zzdy;

    Element(zzaj zzaj) {
        this.zzdy = zzaj;
    }

    @Override // com.google.android.gms.vision.text.Text
    public String getLanguage() {
        return this.zzdy.zzed;
    }

    @Override // com.google.android.gms.vision.text.Text
    public String getValue() {
        return this.zzdy.zzem;
    }

    @Override // com.google.android.gms.vision.text.Text
    public Rect getBoundingBox() {
        return zzc.zza(this);
    }

    @Override // com.google.android.gms.vision.text.Text
    public Point[] getCornerPoints() {
        return zzc.zza(this.zzdy.zzej);
    }

    @Override // com.google.android.gms.vision.text.Text
    public List<? extends Text> getComponents() {
        return new ArrayList();
    }
}
