package com.google.android.gms.internal.firebase_ml;

import java.util.ListIterator;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzaaa implements ListIterator<String> {
    private ListIterator<String> zzcqd;
    private final /* synthetic */ int zzcqe;
    private final /* synthetic */ zzaab zzcqf;

    zzaaa(zzaab zzaab, int i) {
        this.zzcqf = zzaab;
        this.zzcqe = i;
        this.zzcqd = zzaab.zzcqg.listIterator(i);
    }

    public final boolean hasNext() {
        return this.zzcqd.hasNext();
    }

    public final boolean hasPrevious() {
        return this.zzcqd.hasPrevious();
    }

    public final int nextIndex() {
        return this.zzcqd.nextIndex();
    }

    public final int previousIndex() {
        return this.zzcqd.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void add(String str) {
        throw new UnsupportedOperationException();
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void set(String str) {
        throw new UnsupportedOperationException();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.ListIterator
    public final /* synthetic */ String previous() {
        return this.zzcqd.previous();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator, java.util.ListIterator
    public final /* synthetic */ String next() {
        return this.zzcqd.next();
    }
}
