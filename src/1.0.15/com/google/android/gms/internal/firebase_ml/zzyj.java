package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzyj {
    private static final zzyh zzcnw = zzwc();
    private static final zzyh zzcnx = new zzyg();

    static zzyh zzwa() {
        return zzcnw;
    }

    static zzyh zzwb() {
        return zzcnx;
    }

    private static zzyh zzwc() {
        try {
            return (zzyh) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
