package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzlj extends zzhy {
    @zzjg
    private String adult;
    @zzjg
    private String medical;
    @zzjg
    private String racy;
    @zzjg
    private String spoof;
    @zzjg
    private String violence;

    @Override // com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzhy zza(String str, Object obj) {
        return (zzlj) zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzhy zzfc() {
        return (zzlj) clone();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzjf zzfd() {
        return (zzlj) clone();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ zzjf zzb(String str, Object obj) {
        return (zzlj) super.zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf, java.util.AbstractMap, java.lang.Object, com.google.android.gms.internal.firebase_ml.zzhy
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        return (zzlj) super.clone();
    }
}
