package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjv implements Iterable<T> {
    final /* synthetic */ Object zzaii;

    zzjv(Object obj) {
        this.zzaii = obj;
    }

    @Override // java.lang.Iterable
    public final Iterator<T> iterator() {
        return new zzju(this);
    }
}
