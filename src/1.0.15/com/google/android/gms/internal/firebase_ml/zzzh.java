package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzzh {
    <T> zzze<T> zzk(Class<T> cls);
}
