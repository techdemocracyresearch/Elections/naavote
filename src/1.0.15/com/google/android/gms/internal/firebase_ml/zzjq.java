package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.OutputStream;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzjq {
    void writeTo(OutputStream outputStream) throws IOException;
}
