package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzoq implements zzxe {
    static final zzxe zzan = new zzoq();

    private zzoq() {
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxe
    public final boolean zzb(int i) {
        return zzns.zzae.zzb.zzbd(i) != null;
    }
}
