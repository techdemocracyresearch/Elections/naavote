package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwt;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzwr<T extends zzwt<T>> {
    private static final zzwr zzcio = new zzwr(true);
    final zzzj<T, Object> zzcil;
    private boolean zzcim;
    private boolean zzcin;

    private zzwr() {
        this.zzcil = zzzj.zzeb(16);
    }

    private zzwr(boolean z) {
        this(zzzj.zzeb(0));
        zztm();
    }

    private zzwr(zzzj<T, Object> zzzj) {
        this.zzcil = zzzj;
        zztm();
    }

    public static <T extends zzwt<T>> zzwr<T> zzud() {
        return zzcio;
    }

    public final void zztm() {
        if (!this.zzcim) {
            this.zzcil.zztm();
            this.zzcim = true;
        }
    }

    public final boolean isImmutable() {
        return this.zzcim;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzwr)) {
            return false;
        }
        return this.zzcil.equals(((zzwr) obj).zzcil);
    }

    public final int hashCode() {
        return this.zzcil.hashCode();
    }

    public final Iterator<Map.Entry<T, Object>> iterator() {
        if (this.zzcin) {
            return new zzxq(this.zzcil.entrySet().iterator());
        }
        return this.zzcil.entrySet().iterator();
    }

    /* access modifiers changed from: package-private */
    public final Iterator<Map.Entry<T, Object>> descendingIterator() {
        if (this.zzcin) {
            return new zzxq(this.zzcil.zzws().iterator());
        }
        return this.zzcil.zzws().iterator();
    }

    private final Object zza(T t) {
        Object obj = this.zzcil.get(t);
        if (!(obj instanceof zzxp)) {
            return obj;
        }
        zzxp zzxp = (zzxp) obj;
        return zzxp.zzvl();
    }

    private final void zza(T t, Object obj) {
        if (!t.zzuk()) {
            zza(t.zzui(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList2.get(i);
                i++;
                zza(t.zzui(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof zzxp) {
            this.zzcin = true;
        }
        this.zzcil.put(t, obj);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        if ((r3 instanceof com.google.android.gms.internal.firebase_ml.zzxc) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001c, code lost:
        if ((r3 instanceof com.google.android.gms.internal.firebase_ml.zzxp) == false) goto L_0x0014;
     */
    private static void zza(zzaan zzaan, Object obj) {
        zzxd.checkNotNull(obj);
        boolean z = true;
        switch (zzwu.zzcir[zzaan.zzxi().ordinal()]) {
            case 1:
                z = obj instanceof Integer;
                break;
            case 2:
                z = obj instanceof Long;
                break;
            case 3:
                z = obj instanceof Float;
                break;
            case 4:
                z = obj instanceof Double;
                break;
            case 5:
                z = obj instanceof Boolean;
                break;
            case 6:
                z = obj instanceof String;
                break;
            case 7:
                if (!(obj instanceof zzvv)) {
                    break;
                }
                break;
            case 8:
                if (!(obj instanceof Integer)) {
                    break;
                }
                break;
            case 9:
                if (!(obj instanceof zzyk)) {
                    break;
                }
                break;
            default:
                z = false;
                break;
        }
        if (!z) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    public final boolean isInitialized() {
        for (int i = 0; i < this.zzcil.zzwq(); i++) {
            if (!zzb(this.zzcil.zzec(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.zzcil.zzwr()) {
            if (!zzb(entry)) {
                return false;
            }
        }
        return true;
    }

    private static <T extends zzwt<T>> boolean zzb(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.zzuj() == zzaaq.MESSAGE) {
            if (key.zzuk()) {
                for (zzyk zzyk : (List) entry.getValue()) {
                    if (!zzyk.isInitialized()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof zzyk) {
                    if (!((zzyk) value).isInitialized()) {
                        return false;
                    }
                } else if (value instanceof zzxp) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public final void zza(zzwr<T> zzwr) {
        for (int i = 0; i < zzwr.zzcil.zzwq(); i++) {
            zzc(zzwr.zzcil.zzec(i));
        }
        for (Map.Entry<T, Object> entry : zzwr.zzcil.zzwr()) {
            zzc(entry);
        }
    }

    private static Object zzr(Object obj) {
        if (obj instanceof zzyt) {
            return ((zzyt) obj).zzti();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    private final void zzc(Map.Entry<T, Object> entry) {
        zzyk zzyk;
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof zzxp) {
            zzxp zzxp = (zzxp) value;
            value = zzxp.zzvl();
        }
        if (key.zzuk()) {
            Object zza = zza(key);
            if (zza == null) {
                zza = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) zza).add(zzr(obj));
            }
            this.zzcil.put(key, zza);
        } else if (key.zzuj() == zzaaq.MESSAGE) {
            Object zza2 = zza(key);
            if (zza2 == null) {
                this.zzcil.put(key, zzr(value));
                return;
            }
            if (zza2 instanceof zzyt) {
                zzyk = key.zza((zzyt) zza2, (zzyt) value);
            } else {
                zzyk = key.zza(((zzyk) zza2).zzut(), (zzyk) value).zzvb();
            }
            this.zzcil.put(key, zzyk);
        } else {
            this.zzcil.put(key, zzr(value));
        }
    }

    static void zza(zzwi zzwi, zzaan zzaan, int i, Object obj) throws IOException {
        if (zzaan == zzaan.GROUP) {
            zzyk zzyk = (zzyk) obj;
            zzxd.zzf(zzyk);
            zzwi.writeTag(i, 3);
            zzyk.zzb(zzwi);
            zzwi.writeTag(i, 4);
            return;
        }
        zzwi.writeTag(i, zzaan.zzxj());
        switch (zzwu.zzcis[zzaan.ordinal()]) {
            case 1:
                zzwi.zzc(((Double) obj).doubleValue());
                return;
            case 2:
                zzwi.zzq(((Float) obj).floatValue());
                return;
            case 3:
                zzwi.zzw(((Long) obj).longValue());
                return;
            case 4:
                zzwi.zzw(((Long) obj).longValue());
                return;
            case 5:
                zzwi.zzdb(((Integer) obj).intValue());
                return;
            case 6:
                zzwi.zzy(((Long) obj).longValue());
                return;
            case 7:
                zzwi.zzde(((Integer) obj).intValue());
                return;
            case 8:
                zzwi.zzax(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((zzyk) obj).zzb(zzwi);
                return;
            case 10:
                zzwi.zzb((zzyk) obj);
                return;
            case 11:
                if (obj instanceof zzvv) {
                    zzwi.zzc((zzvv) obj);
                    return;
                } else {
                    zzwi.zzck((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof zzvv) {
                    zzwi.zzc((zzvv) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                zzwi.zze(bArr, 0, bArr.length);
                return;
            case 13:
                zzwi.zzdc(((Integer) obj).intValue());
                return;
            case 14:
                zzwi.zzde(((Integer) obj).intValue());
                return;
            case 15:
                zzwi.zzy(((Long) obj).longValue());
                return;
            case 16:
                zzwi.zzdd(((Integer) obj).intValue());
                return;
            case 17:
                zzwi.zzx(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof zzxc) {
                    zzwi.zzdb(((zzxc) obj).zzd());
                    return;
                } else {
                    zzwi.zzdb(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    public final int zzue() {
        int i = 0;
        for (int i2 = 0; i2 < this.zzcil.zzwq(); i2++) {
            i += zzd(this.zzcil.zzec(i2));
        }
        for (Map.Entry<T, Object> entry : this.zzcil.zzwr()) {
            i += zzd(entry);
        }
        return i;
    }

    private static int zzd(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (key.zzuj() != zzaaq.MESSAGE || key.zzuk() || key.zzul()) {
            return zzb((zzwt<?>) key, value);
        }
        if (value instanceof zzxp) {
            return zzwi.zzb(entry.getKey().zzd(), (zzxp) value);
        }
        return zzwi.zzb(entry.getKey().zzd(), (zzyk) value);
    }

    static int zza(zzaan zzaan, int i, Object obj) {
        int zzdf = zzwi.zzdf(i);
        if (zzaan == zzaan.GROUP) {
            zzxd.zzf((zzyk) obj);
            zzdf <<= 1;
        }
        return zzdf + zzb(zzaan, obj);
    }

    private static int zzb(zzaan zzaan, Object obj) {
        switch (zzwu.zzcis[zzaan.ordinal()]) {
            case 1:
                return zzwi.zzd(((Double) obj).doubleValue());
            case 2:
                return zzwi.zzr(((Float) obj).floatValue());
            case 3:
                return zzwi.zzz(((Long) obj).longValue());
            case 4:
                return zzwi.zzaa(((Long) obj).longValue());
            case 5:
                return zzwi.zzdg(((Integer) obj).intValue());
            case 6:
                return zzwi.zzac(((Long) obj).longValue());
            case 7:
                return zzwi.zzdj(((Integer) obj).intValue());
            case 8:
                return zzwi.zzay(((Boolean) obj).booleanValue());
            case 9:
                return zzwi.zzd((zzyk) obj);
            case 10:
                if (obj instanceof zzxp) {
                    return zzwi.zza((zzxp) obj);
                }
                return zzwi.zzc((zzyk) obj);
            case 11:
                if (obj instanceof zzvv) {
                    return zzwi.zzd((zzvv) obj);
                }
                return zzwi.zzcl((String) obj);
            case 12:
                if (obj instanceof zzvv) {
                    return zzwi.zzd((zzvv) obj);
                }
                return zzwi.zzh((byte[]) obj);
            case 13:
                return zzwi.zzdh(((Integer) obj).intValue());
            case 14:
                return zzwi.zzdk(((Integer) obj).intValue());
            case 15:
                return zzwi.zzad(((Long) obj).longValue());
            case 16:
                return zzwi.zzdi(((Integer) obj).intValue());
            case 17:
                return zzwi.zzab(((Long) obj).longValue());
            case 18:
                if (obj instanceof zzxc) {
                    return zzwi.zzdl(((zzxc) obj).zzd());
                }
                return zzwi.zzdl(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int zzb(zzwt<?> zzwt, Object obj) {
        zzaan zzui = zzwt.zzui();
        int zzd = zzwt.zzd();
        if (!zzwt.zzuk()) {
            return zza(zzui, zzd, obj);
        }
        int i = 0;
        if (zzwt.zzul()) {
            for (Object obj2 : (List) obj) {
                i += zzb(zzui, obj2);
            }
            return zzwi.zzdf(zzd) + i + zzwi.zzdo(i);
        }
        for (Object obj3 : (List) obj) {
            i += zza(zzui, zzd, obj3);
        }
        return i;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzwr zzwr = new zzwr();
        for (int i = 0; i < this.zzcil.zzwq(); i++) {
            Map.Entry<T, Object> zzec = this.zzcil.zzec(i);
            zzwr.zza(zzec.getKey(), zzec.getValue());
        }
        for (Map.Entry<T, Object> entry : this.zzcil.zzwr()) {
            zzwr.zza(entry.getKey(), entry.getValue());
        }
        zzwr.zzcin = this.zzcin;
        return zzwr;
    }
}
