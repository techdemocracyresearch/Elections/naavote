package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzza extends zzvv {
    static final int[] zzcox = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170, 1836311903, Integer.MAX_VALUE};
    private final int zzcoy;
    private final zzvv zzcoz;
    private final zzvv zzcpa;
    private final int zzcpb;
    private final int zzcpc;

    private zzza(zzvv zzvv, zzvv zzvv2) {
        this.zzcoz = zzvv;
        this.zzcpa = zzvv2;
        int size = zzvv.size();
        this.zzcpb = size;
        this.zzcoy = size + zzvv2.size();
        this.zzcpc = Math.max(zzvv.zztr(), zzvv2.zztr()) + 1;
    }

    static zzvv zza(zzvv zzvv, zzvv zzvv2) {
        if (zzvv2.size() == 0) {
            return zzvv;
        }
        if (zzvv.size() == 0) {
            return zzvv2;
        }
        int size = zzvv.size() + zzvv2.size();
        if (size < 128) {
            return zzb(zzvv, zzvv2);
        }
        if (zzvv instanceof zzza) {
            zzza zzza = (zzza) zzvv;
            if (zzza.zzcpa.size() + zzvv2.size() < 128) {
                return new zzza(zzza.zzcoz, zzb(zzza.zzcpa, zzvv2));
            } else if (zzza.zzcoz.zztr() > zzza.zzcpa.zztr() && zzza.zztr() > zzvv2.zztr()) {
                return new zzza(zzza.zzcoz, new zzza(zzza.zzcpa, zzvv2));
            }
        }
        if (size >= zzdz(Math.max(zzvv.zztr(), zzvv2.zztr()) + 1)) {
            return new zzza(zzvv, zzvv2);
        }
        return zzzc.zza(new zzzc(null), zzvv, zzvv2);
    }

    private static zzvv zzb(zzvv zzvv, zzvv zzvv2) {
        int size = zzvv.size();
        int size2 = zzvv2.size();
        byte[] bArr = new byte[(size + size2)];
        zzvv.zza(bArr, 0, 0, size);
        zzvv2.zza(bArr, 0, size, size2);
        return new zzwf(bArr);
    }

    static int zzdz(int i) {
        int[] iArr = zzcox;
        if (i >= iArr.length) {
            return Integer.MAX_VALUE;
        }
        return iArr[i];
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final byte zzcw(int i) {
        zzg(i, this.zzcoy);
        return zzcx(i);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final byte zzcx(int i) {
        int i2 = this.zzcpb;
        if (i < i2) {
            return this.zzcoz.zzcx(i);
        }
        return this.zzcpa.zzcx(i - i2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final int size() {
        return this.zzcoy;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final zzwa zzto() {
        return new zzzd(this);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final int zztr() {
        return this.zzcpc;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final boolean zzts() {
        return this.zzcoy >= zzdz(this.zzcpc);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final zzvv zzf(int i, int i2) {
        int zzd = zzd(i, i2, this.zzcoy);
        if (zzd == 0) {
            return zzvv.zzchp;
        }
        if (zzd == this.zzcoy) {
            return this;
        }
        int i3 = this.zzcpb;
        if (i2 <= i3) {
            return this.zzcoz.zzf(i, i2);
        }
        if (i >= i3) {
            return this.zzcpa.zzf(i - i3, i2 - i3);
        }
        zzvv zzvv = this.zzcoz;
        return new zzza(zzvv.zzf(i, zzvv.size()), this.zzcpa.zzf(0, i2 - this.zzcpb));
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final void zzb(byte[] bArr, int i, int i2, int i3) {
        int i4 = i + i3;
        int i5 = this.zzcpb;
        if (i4 <= i5) {
            this.zzcoz.zzb(bArr, i, i2, i3);
        } else if (i >= i5) {
            this.zzcpa.zzb(bArr, i - i5, i2, i3);
        } else {
            int i6 = i5 - i;
            this.zzcoz.zzb(bArr, i, i2, i6);
            this.zzcpa.zzb(bArr, 0, i2 + i6, i3 - i6);
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final void zza(zzvs zzvs) throws IOException {
        this.zzcoz.zza(zzvs);
        this.zzcpa.zza(zzvs);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final String zzb(Charset charset) {
        byte[] bArr;
        int size = size();
        if (size == 0) {
            bArr = zzxd.zzcmh;
        } else {
            byte[] bArr2 = new byte[size];
            zzb(bArr2, 0, 0, size);
            bArr = bArr2;
        }
        return new String(bArr, charset);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final boolean zztq() {
        int zzb = this.zzcoz.zzb(0, 0, this.zzcpb);
        zzvv zzvv = this.zzcpa;
        if (zzvv.zzb(zzb, 0, zzvv.size()) == 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final int zzb(int i, int i2, int i3) {
        int i4 = i2 + i3;
        int i5 = this.zzcpb;
        if (i4 <= i5) {
            return this.zzcoz.zzb(i, i2, i3);
        }
        if (i2 >= i5) {
            return this.zzcpa.zzb(i, i2 - i5, i3);
        }
        int i6 = i5 - i2;
        return this.zzcpa.zzb(this.zzcoz.zzb(i, i2, i6), 0, i3 - i6);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzvv)) {
            return false;
        }
        zzvv zzvv = (zzvv) obj;
        if (this.zzcoy != zzvv.size()) {
            return false;
        }
        if (this.zzcoy == 0) {
            return true;
        }
        int zztt = zztt();
        int zztt2 = zzvv.zztt();
        if (zztt != 0 && zztt2 != 0 && zztt != zztt2) {
            return false;
        }
        zzzf zzzf = new zzzf(this, null);
        zzwc zzwc = (zzwc) zzzf.next();
        zzzf zzzf2 = new zzzf(zzvv, null);
        zzwc zzwc2 = (zzwc) zzzf2.next();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int size = zzwc.size() - i;
            int size2 = zzwc2.size() - i2;
            int min = Math.min(size, size2);
            if (i == 0) {
                z = zzwc.zza(zzwc2, i2, min);
            } else {
                z = zzwc2.zza(zzwc, i, min);
            }
            if (!z) {
                return false;
            }
            i3 += min;
            int i4 = this.zzcoy;
            if (i3 < i4) {
                if (min == size) {
                    zzwc = (zzwc) zzzf.next();
                    i = 0;
                } else {
                    i += min;
                }
                if (min == size2) {
                    zzwc2 = (zzwc) zzzf2.next();
                    i2 = 0;
                } else {
                    i2 += min;
                }
            } else if (i3 == i4) {
                return true;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv
    public final int zzc(int i, int i2, int i3) {
        int i4 = i2 + i3;
        int i5 = this.zzcpb;
        if (i4 <= i5) {
            return this.zzcoz.zzc(i, i2, i3);
        }
        if (i2 >= i5) {
            return this.zzcpa.zzc(i, i2 - i5, i3);
        }
        int i6 = i5 - i2;
        return this.zzcpa.zzc(this.zzcoz.zzc(i, i2, i6), 0, i3 - i6);
    }

    /* Return type fixed from 'java.util.Iterator' to match base method */
    @Override // com.google.android.gms.internal.firebase_ml.zzvv, java.lang.Iterable
    public final /* synthetic */ Iterator<Byte> iterator() {
        return iterator();
    }

    /* synthetic */ zzza(zzvv zzvv, zzvv zzvv2, zzzd zzzd) {
        this(zzvv, zzvv2);
    }
}
