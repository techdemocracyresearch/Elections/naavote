package com.google.android.gms.internal.firebase_ml;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public class zziq<K, V> extends AbstractMap<K, V> implements Cloneable {
    int size;
    private Object[] zzagb;

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    final class zza implements Iterator<Map.Entry<K, V>> {
        private boolean zzagc;
        private int zzagd;

        zza() {
        }

        public final boolean hasNext() {
            return this.zzagd < zziq.this.size;
        }

        public final void remove() {
            int i = this.zzagd - 1;
            if (this.zzagc || i < 0) {
                throw new IllegalArgumentException();
            }
            zziq.this.remove(i);
            this.zzagd--;
            this.zzagc = true;
        }

        @Override // java.util.Iterator
        public final /* synthetic */ Object next() {
            int i = this.zzagd;
            if (i != zziq.this.size) {
                this.zzagd++;
                this.zzagc = false;
                return new zzb(i);
            }
            throw new NoSuchElementException();
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    final class zzc extends AbstractSet<Map.Entry<K, V>> {
        zzc() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public final Iterator<Map.Entry<K, V>> iterator() {
            return new zza();
        }

        public final int size() {
            return zziq.this.size;
        }
    }

    public final int size() {
        return this.size;
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    final class zzb implements Map.Entry<K, V> {
        private int index;

        zzb(int i) {
            this.index = i;
        }

        @Override // java.util.Map.Entry
        public final K getKey() {
            return (K) zziq.this.zzai(this.index);
        }

        @Override // java.util.Map.Entry
        public final V getValue() {
            return (V) zziq.this.zzaj(this.index);
        }

        @Override // java.util.Map.Entry
        public final V setValue(V v) {
            return (V) zziq.this.set(this.index, v);
        }

        public final int hashCode() {
            Object key = getKey();
            Object value = getValue();
            int i = 0;
            int hashCode = key != null ? key.hashCode() : 0;
            if (value != null) {
                i = value.hashCode();
            }
            return hashCode ^ i;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return zzmf.equal(getKey(), entry.getKey()) && zzmf.equal(getValue(), entry.getValue());
        }
    }

    public final K zzai(int i) {
        if (i < 0 || i >= this.size) {
            return null;
        }
        return (K) this.zzagb[i << 1];
    }

    public final V zzaj(int i) {
        if (i < 0 || i >= this.size) {
            return null;
        }
        return zzak((i << 1) + 1);
    }

    public final V set(int i, V v) {
        int i2 = this.size;
        if (i < 0 || i >= i2) {
            throw new IndexOutOfBoundsException();
        }
        int i3 = (i << 1) + 1;
        V zzak = zzak(i3);
        this.zzagb[i3] = v;
        return zzak;
    }

    public final V remove(int i) {
        return zzal(i << 1);
    }

    public final boolean containsKey(Object obj) {
        return -2 != zze(obj);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final V get(Object obj) {
        return zzak(zze(obj) + 1);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final V put(K k, V v) {
        int i;
        int zze = zze(k) >> 1;
        if (zze == -1) {
            zze = this.size;
        }
        if (zze >= 0) {
            int i2 = zze + 1;
            if (i2 >= 0) {
                Object[] objArr = this.zzagb;
                int i3 = i2 << 1;
                if (objArr == null) {
                    i = 0;
                } else {
                    i = objArr.length;
                }
                if (i3 > i) {
                    int i4 = ((i / 2) * 3) + 1;
                    if (i4 % 2 != 0) {
                        i4++;
                    }
                    if (i4 >= i3) {
                        i3 = i4;
                    }
                    if (i3 == 0) {
                        this.zzagb = null;
                    } else {
                        int i5 = this.size;
                        if (i5 == 0 || i3 != objArr.length) {
                            Object[] objArr2 = new Object[i3];
                            this.zzagb = objArr2;
                            if (i5 != 0) {
                                System.arraycopy(objArr, 0, objArr2, 0, i5 << 1);
                            }
                        }
                    }
                }
                int i6 = zze << 1;
                V zzak = zzak(i6 + 1);
                zzb(i6, k, v);
                if (i2 > this.size) {
                    this.size = i2;
                }
                return zzak;
            }
            throw new IndexOutOfBoundsException();
        }
        throw new IndexOutOfBoundsException();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final V remove(Object obj) {
        return zzal(zze(obj));
    }

    private final void zzb(int i, K k, V v) {
        Object[] objArr = this.zzagb;
        objArr[i] = k;
        objArr[i + 1] = v;
    }

    private final V zzak(int i) {
        if (i < 0) {
            return null;
        }
        return (V) this.zzagb[i];
    }

    private final int zze(Object obj) {
        int i = this.size << 1;
        Object[] objArr = this.zzagb;
        for (int i2 = 0; i2 < i; i2 += 2) {
            Object obj2 = objArr[i2];
            if (obj == null) {
                if (obj2 != null) {
                }
            } else if (!obj.equals(obj2)) {
            }
            return i2;
        }
        return -2;
    }

    private final V zzal(int i) {
        int i2 = this.size << 1;
        if (i < 0 || i >= i2) {
            return null;
        }
        V zzak = zzak(i + 1);
        Object[] objArr = this.zzagb;
        int i3 = (i2 - i) - 2;
        if (i3 != 0) {
            System.arraycopy(objArr, i + 2, objArr, i, i3);
        }
        this.size--;
        zzb(i2 - 2, null, null);
        return zzak;
    }

    public void clear() {
        this.size = 0;
        this.zzagb = null;
    }

    public final boolean containsValue(Object obj) {
        int i = this.size << 1;
        Object[] objArr = this.zzagb;
        for (int i2 = 1; i2 < i; i2 += 2) {
            Object obj2 = objArr[i2];
            if (obj == null) {
                if (obj2 != null) {
                }
            } else if (!obj.equals(obj2)) {
            }
            return true;
        }
        return false;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final Set<Map.Entry<K, V>> entrySet() {
        return new zzc();
    }

    /* access modifiers changed from: private */
    /* renamed from: zzhv */
    public final zziq<K, V> clone() {
        try {
            zziq<K, V> zziq = (zziq) super.clone();
            Object[] objArr = this.zzagb;
            if (objArr != null) {
                int length = objArr.length;
                Object[] objArr2 = new Object[length];
                zziq.zzagb = objArr2;
                System.arraycopy(objArr, 0, objArr2, 0, length);
            }
            return zziq;
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }
}
