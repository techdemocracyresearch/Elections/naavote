package com.google.android.gms.internal.firebase_ml;

import com.bumptech.glide.load.Key;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public abstract class zzhx {
    public abstract zzia zza(OutputStream outputStream, Charset charset) throws IOException;

    public abstract zzib zza(InputStream inputStream) throws IOException;

    public abstract zzib zza(InputStream inputStream, Charset charset) throws IOException;

    public abstract zzib zzam(String str) throws IOException;

    public final String toString(Object obj) throws IOException {
        return zza(obj, false);
    }

    public final String zzc(Object obj) throws IOException {
        return zza(obj, true);
    }

    private final String zza(Object obj, boolean z) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        zzia zza = zza(byteArrayOutputStream, zziw.UTF_8);
        if (z) {
            zza.zzha();
        }
        zza.zzd(obj);
        zza.flush();
        return byteArrayOutputStream.toString(Key.STRING_CHARSET_NAME);
    }
}
