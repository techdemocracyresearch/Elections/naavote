package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.auth.api.AuthProxy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.common.FirebaseMLException;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzru implements zzpu<ResultType, zzrs>, zzqp {
    private final GoogleApiClient zzbrj;
    private final zzrt zzbrl;
    private final /* synthetic */ zzrr zzbrm;

    zzru(zzrr zzrr, FirebaseApp firebaseApp, boolean z) {
        this.zzbrm = zzrr;
        if (z) {
            GoogleApiClient build = new GoogleApiClient.Builder(firebaseApp.getApplicationContext()).addApi(AuthProxy.API).build();
            this.zzbrj = build;
            build.connect();
        } else {
            this.zzbrj = null;
        }
        this.zzbrl = zzrt.zza(firebaseApp, z, this.zzbrj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final zzqp zzoc() {
        return this;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final void zzol() throws FirebaseMLException {
        GoogleApiClient googleApiClient = this.zzbrj;
        if (googleApiClient != null && !zzmf.equal(googleApiClient.blockingConnect(3, TimeUnit.SECONDS), ConnectionResult.RESULT_SUCCESS)) {
            throw new FirebaseMLException("Failed to contact Google Play services", 14);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqp
    public final void release() {
        GoogleApiClient googleApiClient = this.zzbrj;
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.google.android.gms.internal.firebase_ml.zzpy] */
    @Override // com.google.android.gms.internal.firebase_ml.zzpu
    public final /* synthetic */ Object zza(zzrs zzrs) throws FirebaseMLException {
        zzrs zzrs2 = zzrs;
        return this.zzbrm.zza(this.zzbrl.zza(zzrs2), zzrs2.zzbre);
    }
}
