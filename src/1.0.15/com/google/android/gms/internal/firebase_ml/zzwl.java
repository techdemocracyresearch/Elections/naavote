package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzwl implements zzaat {
    private final zzwi zzchu;

    public static zzwl zza(zzwi zzwi) {
        if (zzwi.zzchz != null) {
            return zzwi.zzchz;
        }
        return new zzwl(zzwi);
    }

    private zzwl(zzwi zzwi) {
        zzwi zzwi2 = (zzwi) zzxd.checkNotNull(zzwi, "output");
        this.zzchu = zzwi2;
        zzwi2.zzchz = this;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final int zzub() {
        return zzwz.zzg.zzcme;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzr(int i, int i2) throws IOException {
        this.zzchu.zzk(i, i2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzi(int i, long j) throws IOException {
        this.zzchu.zza(i, j);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzj(int i, long j) throws IOException {
        this.zzchu.zzc(i, j);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, float f) throws IOException {
        this.zzchu.zza(i, f);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, double d) throws IOException {
        this.zzchu.zza(i, d);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzs(int i, int i2) throws IOException {
        this.zzchu.zzh(i, i2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, long j) throws IOException {
        this.zzchu.zza(i, j);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzh(int i, int i2) throws IOException {
        this.zzchu.zzh(i, i2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzc(int i, long j) throws IOException {
        this.zzchu.zzc(i, j);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzk(int i, int i2) throws IOException {
        this.zzchu.zzk(i, i2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, boolean z) throws IOException {
        this.zzchu.zza(i, z);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzb(int i, String str) throws IOException {
        this.zzchu.zzb(i, str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, zzvv zzvv) throws IOException {
        this.zzchu.zza(i, zzvv);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzi(int i, int i2) throws IOException {
        this.zzchu.zzi(i, i2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzj(int i, int i2) throws IOException {
        this.zzchu.zzj(i, i2);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzb(int i, long j) throws IOException {
        this.zzchu.zzb(i, j);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, Object obj, zzze zzze) throws IOException {
        this.zzchu.zza(i, (zzyk) obj, zzze);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzb(int i, Object obj, zzze zzze) throws IOException {
        zzwi zzwi = this.zzchu;
        zzwi.writeTag(i, 3);
        zzze.zza((zzyk) obj, zzwi.zzchz);
        zzwi.writeTag(i, 4);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzdp(int i) throws IOException {
        this.zzchu.writeTag(i, 3);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzdq(int i) throws IOException {
        this.zzchu.writeTag(i, 4);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof zzvv) {
            this.zzchu.zzb(i, (zzvv) obj);
        } else {
            this.zzchu.zza(i, (zzyk) obj);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzdg(list.get(i4).intValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzdb(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzh(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzdj(list.get(i4).intValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzde(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzk(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzz(list.get(i4).longValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzw(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zza(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzaa(list.get(i4).longValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzw(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zza(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzac(list.get(i4).longValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzy(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzc(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzr(list.get(i4).floatValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzq(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zza(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzd(list.get(i4).doubleValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzc(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zza(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzdl(list.get(i4).intValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzdb(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzh(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzay(list.get(i4).booleanValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzax(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zza(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof zzxv) {
            zzxv zzxv = (zzxv) list;
            while (i2 < list.size()) {
                Object raw = zzxv.getRaw(i2);
                if (raw instanceof String) {
                    this.zzchu.zzb(i, (String) raw);
                } else {
                    this.zzchu.zza(i, (zzvv) raw);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzb(i, list.get(i2));
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzb(int i, List<zzvv> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.zzchu.zza(i, list.get(i2));
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzdh(list.get(i4).intValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzdc(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzi(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzdk(list.get(i4).intValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzde(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzk(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzad(list.get(i4).longValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzy(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzc(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzdi(list.get(i4).intValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzdd(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzj(i, list.get(i2).intValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzchu.writeTag(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzwi.zzab(list.get(i4).longValue());
            }
            this.zzchu.zzdc(i3);
            while (i2 < list.size()) {
                this.zzchu.zzx(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzchu.zzb(i, list.get(i2).longValue());
            i2++;
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zza(int i, List<?> list, zzze zzze) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zza(i, list.get(i2), zzze);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final void zzb(int i, List<?> list, zzze zzze) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zzb(i, list.get(i2), zzze);
        }
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzaat
    public final <K, V> void zza(int i, zzyf<K, V> zzyf, Map<K, V> map) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.zzchu.writeTag(i, 2);
            this.zzchu.zzdc(zzyc.zza(zzyf, entry.getKey(), entry.getValue()));
            zzyc.zza(this.zzchu, zzyf, entry.getKey(), entry.getValue());
        }
    }
}
