package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import java.util.RandomAccess;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzyy<E> extends zzvp<E> implements RandomAccess {
    private static final zzyy<Object> zzcot;
    private int size;
    private E[] zzalu;

    public static <E> zzyy<E> zzwg() {
        return (zzyy<E>) zzcot;
    }

    zzyy() {
        this(new Object[10], 0);
    }

    private zzyy(E[] eArr, int i) {
        this.zzalu = eArr;
        this.size = i;
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection, java.util.AbstractList
    public final boolean add(E e) {
        zztn();
        int i = this.size;
        E[] eArr = this.zzalu;
        if (i == eArr.length) {
            this.zzalu = (E[]) Arrays.copyOf(eArr, ((i * 3) / 2) + 1);
        }
        E[] eArr2 = this.zzalu;
        int i2 = this.size;
        this.size = i2 + 1;
        eArr2[i2] = e;
        this.modCount++;
        return true;
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final void add(int i, E e) {
        int i2;
        zztn();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
        E[] eArr = this.zzalu;
        if (i2 < eArr.length) {
            System.arraycopy(eArr, i, eArr, i + 1, i2 - i);
        } else {
            E[] eArr2 = (E[]) new Object[(((i2 * 3) / 2) + 1)];
            System.arraycopy(eArr, 0, eArr2, 0, i);
            System.arraycopy(this.zzalu, i, eArr2, i + 1, this.size - i);
            this.zzalu = eArr2;
        }
        this.zzalu[i] = e;
        this.size++;
        this.modCount++;
    }

    @Override // java.util.List, java.util.AbstractList
    public final E get(int i) {
        zzct(i);
        return this.zzalu[i];
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final E remove(int i) {
        zztn();
        zzct(i);
        E[] eArr = this.zzalu;
        E e = eArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(eArr, i + 1, eArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return e;
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final E set(int i, E e) {
        zztn();
        zzct(i);
        E[] eArr = this.zzalu;
        E e2 = eArr[i];
        eArr[i] = e;
        this.modCount++;
        return e2;
    }

    public final int size() {
        return this.size;
    }

    private final void zzct(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
    }

    private final String zzcu(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzxl
    public final /* synthetic */ zzxl zzcv(int i) {
        if (i >= this.size) {
            return new zzyy(Arrays.copyOf(this.zzalu, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    static {
        zzyy<Object> zzyy = new zzyy<>(new Object[0], 0);
        zzcot = zzyy;
        zzyy.zztm();
    }
}
