package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzhy extends zzjf implements Cloneable {
    private zzhx zzaeg;

    public final void zza(zzhx zzhx) {
        this.zzaeg = zzhx;
    }

    public String toString() {
        zzhx zzhx = this.zzaeg;
        if (zzhx == null) {
            return super.toString();
        }
        try {
            return zzhx.toString(this);
        } catch (IOException e) {
            throw zzmv.zza(e);
        }
    }

    public final String zzgt() throws IOException {
        zzhx zzhx = this.zzaeg;
        if (zzhx != null) {
            return zzhx.zzc(this);
        }
        return super.toString();
    }

    /* renamed from: zzfc */
    public zzhy clone() {
        return (zzhy) super.clone();
    }

    /* renamed from: zza */
    public zzhy zzb(String str, Object obj) {
        return (zzhy) super.zzb(str, obj);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzjf
    public /* synthetic */ zzjf zzfd() {
        return (zzhy) clone();
    }
}
