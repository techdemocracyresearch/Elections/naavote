package com.google.android.gms.internal.firebase_ml;

import androidx.core.app.FrameMetricsAggregator;
import com.facebook.common.statfs.StatFsHelper;
import com.google.android.gms.internal.firebase_ml.zzabh;
import com.google.android.gms.internal.firebase_ml.zztg;
import com.google.android.gms.internal.firebase_ml.zzwz;
import kotlin.text.Typography;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzns {

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zza extends zzwz<zza, C0011zza> implements zzym {
        private static final zza zzamx;
        private static volatile zzyx<zza> zzh;
        private zzb zzamu;
        private int zzamv;
        private zzab zzamw;
        private int zzj;

        private zza() {
        }

        /* renamed from: com.google.android.gms.internal.firebase_ml.zzns$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class C0011zza extends zzwz.zzb<zza, C0011zza> implements zzym {
            private C0011zza() {
                super(zza.zzamx);
            }

            /* synthetic */ C0011zza(zznr zznr) {
                this();
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, C0012zza> implements zzym {
            private static final zzb zzanc;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private zzak zzanb;
            private int zzj;

            private zzb() {
            }

            /* renamed from: com.google.android.gms.internal.firebase_ml.zzns$zza$zzb$zza  reason: collision with other inner class name */
            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class C0012zza extends zzwz.zzb<zzb, C0012zza> implements zzym {
                private C0012zza() {
                    super(zzb.zzanc);
                }

                public final C0012zza zza(zzoc zzoc) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzoc);
                    return this;
                }

                public final C0012zza zzm(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzn(z);
                    return this;
                }

                public final C0012zza zza(zzae zzae) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzae);
                    return this;
                }

                /* synthetic */ C0012zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzoc zzoc) {
                this.zzamy = zzoc.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzn(boolean z) {
                this.zzj |= 2;
                this.zzamz = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzae zzae) {
                zzae.getClass();
                this.zzana = zzae;
                this.zzj |= 4;
            }

            public static C0012zza zzjx() {
                return (C0012zza) zzanc.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new C0012zza(null);
                    case 3:
                        return zza(zzanc, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဉ\u0002\u0004ဉ\u0003", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzamz", "zzana", "zzanb"});
                    case 4:
                        return zzanc;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzanc);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzanc = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0011zza(null);
                case 3:
                    return zza(zzamx, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzamu", "zzamv", "zzamw"});
                case 4:
                    return zzamx;
                case 5:
                    zzyx<zza> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zza.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzamx);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zza zza = new zza();
            zzamx = zza;
            zzwz.zza(zza.class, zza);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzab extends zzwz<zzab, zza> implements zzym {
        private static final zzab zzaqk;
        private static volatile zzyx<zzab> zzh;
        private long zzaqe;
        private long zzaqf;
        private long zzaqg;
        private long zzaqh;
        private long zzaqi;
        private long zzaqj;
        private int zzj;

        private zzab() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzab, zza> implements zzym {
            private zza() {
                super(zzab.zzaqk);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzab();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaqk, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဃ\u0000\u0002ဃ\u0001\u0003ဃ\u0002\u0004ဃ\u0003\u0005ဃ\u0004\u0006ဃ\u0005", new Object[]{"zzj", "zzaqe", "zzaqf", "zzaqg", "zzaqh", "zzaqi", "zzaqj"});
                case 4:
                    return zzaqk;
                case 5:
                    zzyx<zzab> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzab.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaqk);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzab zzab = new zzab();
            zzaqk = zzab;
            zzwz.zza(zzab.class, zzab);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzah extends zzwz<zzah, zza> implements zzym {
        private static final zzah zzazr;
        private static volatile zzyx<zzah> zzh;
        private float zzat;
        private float zzazp;
        private float zzazq;
        private int zzj;

        private zzah() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzah, zza> implements zzym {
            private zza() {
                super(zzah.zzazr);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzah();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzazr, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ခ\u0000\u0002ခ\u0001\u0003ခ\u0002", new Object[]{"zzj", "zzazp", "zzazq", "zzat"});
                case 4:
                    return zzazr;
                case 5:
                    zzyx<zzah> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzah.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzazr);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzah zzah = new zzah();
            zzazr = zzah;
            zzwz.zza(zzah.class, zzah);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzaq extends zzwz<zzaq, zzb> implements zzym {
        private static final zzaq zzbdr;
        private static volatile zzyx<zzaq> zzh;
        private zzaf zzaoq;
        private zzah zzbdo;
        private zzc zzbdp;
        private zzd zzbdq;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz<zza, C0013zza> implements zzym {
            private static final zza zzbdt;
            private static volatile zzyx<zza> zzh;
            private float zzar;
            private String zzbds = "";
            private int zzj;

            private zza() {
            }

            /* renamed from: com.google.android.gms.internal.firebase_ml.zzns$zzaq$zza$zza  reason: collision with other inner class name */
            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class C0013zza extends zzwz.zzb<zza, C0013zza> implements zzym {
                private C0013zza() {
                    super(zza.zzbdt);
                }

                /* synthetic */ C0013zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zza();
                    case 2:
                        return new C0013zza(null);
                    case 3:
                        return zza(zzbdt, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ခ\u0000\u0002ဈ\u0001", new Object[]{"zzj", "zzar", "zzbds"});
                    case 4:
                        return zzbdt;
                    case 5:
                        zzyx<zza> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zza.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzbdt);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zza zza = new zza();
                zzbdt = zza;
                zzwz.zza(zza.class, zza);
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzc extends zzwz<zzc, zza> implements zzym {
            private static final zzc zzbdv;
            private static volatile zzyx<zzc> zzh;
            private zza zzbdu;
            private int zzj;

            private zzc() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzc, zza> implements zzym {
                private zza() {
                    super(zzc.zzbdv);
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzc();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzbdv, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ဉ\u0000", new Object[]{"zzj", "zzbdu"});
                    case 4:
                        return zzbdv;
                    case 5:
                        zzyx<zzc> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzc.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzbdv);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzc zzc = new zzc();
                zzbdv = zzc;
                zzwz.zza(zzc.class, zzc);
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzd extends zzwz<zzd, zza> implements zzym {
            private static final zzd zzbdx;
            private static volatile zzyx<zzd> zzh;
            private zzxl<zza> zzbdw = zzus();

            private zzd() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzd, zza> implements zzym {
                private zza() {
                    super(zzd.zzbdx);
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzd();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzbdx, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzbdw", zza.class});
                    case 4:
                        return zzbdx;
                    case 5:
                        zzyx<zzd> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzd.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzbdx);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzd zzd = new zzd();
                zzbdx = zzd;
                zzwz.zza(zzd.class, zzd);
            }
        }

        private zzaq() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zzaq, zzb> implements zzym {
            private zzb() {
                super(zzaq.zzbdr);
            }

            /* synthetic */ zzb(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzaq();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzbdr, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဉ\u0003", new Object[]{"zzj", "zzaoq", "zzbdo", "zzbdp", "zzbdq"});
                case 4:
                    return zzbdr;
                case 5:
                    zzyx<zzaq> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzaq.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbdr);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzaq zzaq = new zzaq();
            zzbdr = zzaq;
            zzwz.zza(zzaq.class, zzaq);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzav extends zzwz<zzav, zza> implements zzym {
        private static final zzav zzbeo;
        private static volatile zzyx<zzav> zzh;
        private zzae zzana;
        private zzaw zzaoh;
        private zzaf zzaoq;
        private int zzj;

        private zzav() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzav, zza> implements zzym {
            private zza() {
                super(zzav.zzbeo);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzav();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbeo, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzana", "zzaoh"});
                case 4:
                    return zzbeo;
                case 5:
                    zzyx<zzav> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzav.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbeo);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzav zzav = new zzav();
            zzbeo = zzav;
            zzwz.zza(zzav.class, zzav);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzaw extends zzwz<zzaw, zza> implements zzym {
        private static final zzaw zzbep;
        private static volatile zzyx<zzaw> zzh;
        private int zzbdz;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            INVALID_MODE(0),
            STREAM(1),
            SINGLE_IMAGE(2);
            
            private static final zzxf<zzb> zzac = new zzph();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzbp(int i) {
                if (i == 0) {
                    return INVALID_MODE;
                }
                if (i == 1) {
                    return STREAM;
                }
                if (i != 2) {
                    return null;
                }
                return SINGLE_IMAGE;
            }

            public static zzxe zzf() {
                return zzpi.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzaw() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzaw, zza> implements zzym {
            private zza() {
                super(zzaw.zzbep);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzaw();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbep, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ဌ\u0000", new Object[]{"zzj", "zzbdz", zzb.zzf()});
                case 4:
                    return zzbep;
                case 5:
                    zzyx<zzaw> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzaw.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbep);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzaw zzaw = new zzaw();
            zzbep = zzaw;
            zzwz.zza(zzaw.class, zzaw);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzax extends zzwz<zzax, zza> implements zzym {
        private static final zzax zzbeu;
        private static volatile zzyx<zzax> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private int zzj;

        private zzax() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzax, zza> implements zzym {
            private zza() {
                super(zzax.zzbeu);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzax();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbeu, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001", new Object[]{"zzj", "zzaoq", "zzana"});
                case 4:
                    return zzbeu;
                case 5:
                    zzyx<zzax> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzax.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbeu);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzax zzax = new zzax();
            zzbeu = zzax;
            zzwz.zza(zzax.class, zzax);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzay extends zzwz<zzay, zzb> implements zzym {
        private static final zzay zzbez;
        private static volatile zzyx<zzay> zzh;
        private zzaf zzaoq;
        private zzxl<zzc> zzbev = zzus();
        private int zzbew;
        private int zzbex;
        private int zzbey;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            NO_ERROR(0),
            STATUS_SENSITIVE_TOPIC(1),
            STATUS_QUALITY_THRESHOLDED(2),
            STATUS_INTERNAL_ERROR(3),
            STATUS_NOT_SUPPORTED_LANGUAGE(101),
            STATUS_32_BIT_CPU(1001),
            STATUS_32_BIT_APP(1002);
            
            private static final zzxf<zza> zzac = new zzpk();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzbq(int i) {
                if (i == 0) {
                    return NO_ERROR;
                }
                if (i == 1) {
                    return STATUS_SENSITIVE_TOPIC;
                }
                if (i == 2) {
                    return STATUS_QUALITY_THRESHOLDED;
                }
                if (i == 3) {
                    return STATUS_INTERNAL_ERROR;
                }
                if (i == 101) {
                    return STATUS_NOT_SUPPORTED_LANGUAGE;
                }
                if (i == 1001) {
                    return STATUS_32_BIT_CPU;
                }
                if (i != 1002) {
                    return null;
                }
                return STATUS_32_BIT_APP;
            }

            public static zzxe zzf() {
                return zzpj.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzc extends zzwz<zzc, zza> implements zzym {
            private static final zzc zzbfi;
            private static volatile zzyx<zzc> zzh;
            private float zzar;
            private int zzj;

            private zzc() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzc, zza> implements zzym {
                private zza() {
                    super(zzc.zzbfi);
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzc();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzbfi, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ခ\u0000", new Object[]{"zzj", "zzar"});
                    case 4:
                        return zzbfi;
                    case 5:
                        zzyx<zzc> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzc.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzbfi);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzc zzc = new zzc();
                zzbfi = zzc;
                zzwz.zza(zzc.class, zzc);
            }
        }

        private zzay() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zzay, zzb> implements zzym {
            private zzb() {
                super(zzay.zzbez);
            }

            /* synthetic */ zzb(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzay();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzbez, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001ဉ\u0000\u0002\u001b\u0003ဌ\u0001\u0004င\u0002\u0005င\u0003", new Object[]{"zzj", "zzaoq", "zzbev", zzc.class, "zzbew", zza.zzf(), "zzbex", "zzbey"});
                case 4:
                    return zzbez;
                case 5:
                    zzyx<zzay> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzay.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbez);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzay zzay = new zzay();
            zzbez = zzay;
            zzwz.zza(zzay.class, zzay);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzb extends zzwz<zzb, zza> implements zzym {
        private static final zzb zzane;
        private static volatile zzyx<zzb> zzh;
        private int zzamv;
        private zzab zzamw;
        private C0014zzb zzand;
        private int zzj;

        private zzb() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
            private zza() {
                super(zzb.zzane);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* renamed from: com.google.android.gms.internal.firebase_ml.zzns$zzb$zzb  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class C0014zzb extends zzwz<C0014zzb, zza> implements zzym {
            private static final C0014zzb zzanh;
            private static volatile zzyx<C0014zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzak zzanb;
            private zzxl<zzy.zzb> zzanf = zzus();
            private zzxl<zzy.zzb> zzang = zzus();
            private int zzj;

            private C0014zzb() {
            }

            /* renamed from: com.google.android.gms.internal.firebase_ml.zzns$zzb$zzb$zza */
            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<C0014zzb, zza> implements zzym {
                private zza() {
                    super(C0014zzb.zzanh);
                }

                public final zza zzc(zzoc zzoc) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((C0014zzb) this.zzclo).zzb((C0014zzb) zzoc);
                    return this;
                }

                public final zza zzo(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((C0014zzb) this.zzclo).zzn(z);
                    return this;
                }

                public final zza zzd(Iterable<? extends zzy.zzb> iterable) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((C0014zzb) this.zzclo).zzf(iterable);
                    return this;
                }

                public final zza zze(Iterable<? extends zzy.zzb> iterable) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((C0014zzb) this.zzclo).zzg(iterable);
                    return this;
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzoc zzoc) {
                this.zzamy = zzoc.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzn(boolean z) {
                this.zzj |= 2;
                this.zzamz = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzf(Iterable<? extends zzy.zzb> iterable) {
                zzxl<zzy.zzb> zzxl = this.zzanf;
                if (!zzxl.zztl()) {
                    this.zzanf = zzwz.zza(zzxl);
                }
                zzvl.zza(iterable, this.zzanf);
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzg(Iterable<? extends zzy.zzb> iterable) {
                zzxl<zzy.zzb> zzxl = this.zzang;
                if (!zzxl.zztl()) {
                    this.zzang = zzwz.zza(zzxl);
                }
                zzvl.zza(iterable, this.zzang);
            }

            public static zza zzka() {
                return (zza) zzanh.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new C0014zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzanh, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003\u001b\u0004\u001b\u0005ဉ\u0002", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzamz", "zzanf", zzy.zzb.class, "zzang", zzy.zzb.class, "zzanb"});
                    case 4:
                        return zzanh;
                    case 5:
                        zzyx<C0014zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (C0014zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzanh);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                C0014zzb zzb = new C0014zzb();
                zzanh = zzb;
                zzwz.zza(C0014zzb.class, zzb);
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzane, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzand", "zzamv", "zzamw"});
                case 4:
                    return zzane;
                case 5:
                    zzyx<zzb> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzb.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzane);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzb zzb = new zzb();
            zzane = zzb;
            zzwz.zza(zzb.class, zzb);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzba extends zzwz<zzba, zza> implements zzym {
        private static final zzba zzbfr;
        private static volatile zzyx<zzba> zzh;
        private zzaf zzaoq;
        private zzbd zzbfk;
        private int zzbfl;
        private int zzbfm;
        private int zzbfn;
        private int zzbfo;
        private int zzbfp;
        private int zzbfq;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            NO_ERROR(0),
            METADATA_FILE_UNAVAILABLE(1),
            METADATA_ENTRY_NOT_FOUND(2),
            METADATA_JSON_INVALID(3),
            METADATA_HASH_NOT_FOUND(4),
            DOWNLOAD_MANAGER_SERVICE_MISSING(5),
            DOWNLOAD_MANAGER_HTTP_UNKNOWN_STATUS(6),
            DOWNLOAD_MANAGER_HTTP_BAD_REQUEST(StatFsHelper.DEFAULT_DISK_YELLOW_LEVEL_IN_MB),
            DOWNLOAD_MANAGER_HTTP_UNAUTHORIZED(401),
            DOWNLOAD_MANAGER_HTTP_FORBIDDEN(403),
            DOWNLOAD_MANAGER_HTTP_NOT_FOUND(404),
            DOWNLOAD_MANAGER_HTTP_REQUEST_TIMEOUT(408),
            DOWNLOAD_MANAGER_HTTP_ABORTED(409),
            DOWNLOAD_MANAGER_HTTP_TOO_MANY_REQUESTS(429),
            DOWNLOAD_MANAGER_HTTP_CANCELLED(499),
            DOWNLOAD_MANAGER_HTTP_UNIMPLEMENTED(501),
            DOWNLOAD_MANAGER_HTTP_INTERNAL_SERVICE_ERROR(500),
            DOWNLOAD_MANAGER_HTTP_SERVICE_UNAVAILABLE(503),
            DOWNLOAD_MANAGER_HTTP_DEADLINE_EXCEEDED(504),
            DOWNLOAD_MANAGER_HTTP_NETWORK_AUTHENTICATION_REQUIRED(FrameMetricsAggregator.EVERY_DURATION),
            DOWNLOAD_MANAGER_FILE_ERROR(7),
            DOWNLOAD_MANAGER_UNHANDLED_HTTP_CODE(8),
            DOWNLOAD_MANAGER_HTTP_DATA_ERROR(9),
            DOWNLOAD_MANAGER_TOO_MANY_REDIRECTS(10),
            DOWNLOAD_MANAGER_INSUFFICIENT_SPACE(11),
            DOWNLOAD_MANAGER_DEVICE_NOT_FOUND(12),
            DOWNLOAD_MANAGER_CANNOT_RESUME(13),
            DOWNLOAD_MANAGER_FILE_ALREADY_EXISTS(14),
            DOWNLOAD_MANAGER_UNKNOWN_ERROR(15),
            POST_DOWNLOAD_FILE_NOT_FOUND(16),
            POST_DOWNLOAD_MOVE_FILE_FAILED(17),
            POST_DOWNLOAD_UNZIP_FAILED(18),
            RAPID_RESPONSE_COULD_NOT_BE_WRITTEN(19),
            DRIVER_OBJECT_DEALLOCATED(20);
            
            private static final zzxf<zzb> zzac = new zzpl();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzbr(int i) {
                if (i == 400) {
                    return DOWNLOAD_MANAGER_HTTP_BAD_REQUEST;
                }
                if (i == 401) {
                    return DOWNLOAD_MANAGER_HTTP_UNAUTHORIZED;
                }
                if (i == 403) {
                    return DOWNLOAD_MANAGER_HTTP_FORBIDDEN;
                }
                if (i == 404) {
                    return DOWNLOAD_MANAGER_HTTP_NOT_FOUND;
                }
                if (i == 408) {
                    return DOWNLOAD_MANAGER_HTTP_REQUEST_TIMEOUT;
                }
                if (i == 409) {
                    return DOWNLOAD_MANAGER_HTTP_ABORTED;
                }
                if (i == 429) {
                    return DOWNLOAD_MANAGER_HTTP_TOO_MANY_REQUESTS;
                }
                if (i == 511) {
                    return DOWNLOAD_MANAGER_HTTP_NETWORK_AUTHENTICATION_REQUIRED;
                }
                if (i == 503) {
                    return DOWNLOAD_MANAGER_HTTP_SERVICE_UNAVAILABLE;
                }
                if (i == 504) {
                    return DOWNLOAD_MANAGER_HTTP_DEADLINE_EXCEEDED;
                }
                switch (i) {
                    case 0:
                        return NO_ERROR;
                    case 1:
                        return METADATA_FILE_UNAVAILABLE;
                    case 2:
                        return METADATA_ENTRY_NOT_FOUND;
                    case 3:
                        return METADATA_JSON_INVALID;
                    case 4:
                        return METADATA_HASH_NOT_FOUND;
                    case 5:
                        return DOWNLOAD_MANAGER_SERVICE_MISSING;
                    case 6:
                        return DOWNLOAD_MANAGER_HTTP_UNKNOWN_STATUS;
                    case 7:
                        return DOWNLOAD_MANAGER_FILE_ERROR;
                    case 8:
                        return DOWNLOAD_MANAGER_UNHANDLED_HTTP_CODE;
                    case 9:
                        return DOWNLOAD_MANAGER_HTTP_DATA_ERROR;
                    case 10:
                        return DOWNLOAD_MANAGER_TOO_MANY_REDIRECTS;
                    case 11:
                        return DOWNLOAD_MANAGER_INSUFFICIENT_SPACE;
                    case 12:
                        return DOWNLOAD_MANAGER_DEVICE_NOT_FOUND;
                    case 13:
                        return DOWNLOAD_MANAGER_CANNOT_RESUME;
                    case 14:
                        return DOWNLOAD_MANAGER_FILE_ALREADY_EXISTS;
                    case 15:
                        return DOWNLOAD_MANAGER_UNKNOWN_ERROR;
                    case 16:
                        return POST_DOWNLOAD_FILE_NOT_FOUND;
                    case 17:
                        return POST_DOWNLOAD_MOVE_FILE_FAILED;
                    case 18:
                        return POST_DOWNLOAD_UNZIP_FAILED;
                    case 19:
                        return RAPID_RESPONSE_COULD_NOT_BE_WRITTEN;
                    case 20:
                        return DRIVER_OBJECT_DEALLOCATED;
                    default:
                        switch (i) {
                            case 499:
                                return DOWNLOAD_MANAGER_HTTP_CANCELLED;
                            case 500:
                                return DOWNLOAD_MANAGER_HTTP_INTERNAL_SERVICE_ERROR;
                            case 501:
                                return DOWNLOAD_MANAGER_HTTP_UNIMPLEMENTED;
                            default:
                                return null;
                        }
                }
            }

            public static zzxe zzf() {
                return zzpm.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzba() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzba, zza> implements zzym {
            private zza() {
                super(zzba.zzbfr);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzba();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbfr, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003င\u0002\u0004င\u0003\u0005င\u0004\u0006င\u0005\u0007ဌ\u0006\bင\u0007", new Object[]{"zzj", "zzaoq", "zzbfk", "zzbfl", "zzbfm", "zzbfn", "zzbfo", "zzbfp", zzb.zzf(), "zzbfq"});
                case 4:
                    return zzbfr;
                case 5:
                    zzyx<zzba> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzba.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbfr);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzba zzba = new zzba();
            zzbfr = zzba;
            zzwz.zza(zzba.class, zzba);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzbb extends zzwz<zzbb, zza> implements zzym {
        private static final zzxj<Integer, zzb> zzbhc = new zzpo();
        private static final zzxj<Integer, zzb> zzbhe = new zzpn();
        private static final zzxj<Integer, zzb> zzbhg = new zzpp();
        private static final zzbb zzbhi;
        private static volatile zzyx<zzbb> zzh;
        private long zzazi;
        private zzxg zzbhb = zzup();
        private zzxg zzbhd = zzup();
        private zzxg zzbhf = zzup();
        private int zzbhh;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            UNKNOWN_ERROR(0),
            NO_CONNECTION(1),
            RPC_ERROR(2),
            RPC_RETURNED_INVALID_RESULT(3),
            RPC_RETURNED_MALFORMED_RESULT(4),
            RPC_EXPONENTIAL_BACKOFF_FAILED(5),
            DIRECTORY_CREATION_FAILED(10),
            FILE_WRITE_FAILED_DISK_FULL(11),
            FILE_WRITE_FAILED(12),
            FILE_READ_FAILED(13),
            FILE_READ_RETURNED_INVALID_DATA(14),
            FILE_READ_RETURNED_MALFORMED_DATA(15);
            
            private static final zzxf<zzb> zzac = new zzpq();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzbs(int i) {
                if (i == 0) {
                    return UNKNOWN_ERROR;
                }
                if (i == 1) {
                    return NO_CONNECTION;
                }
                if (i == 2) {
                    return RPC_ERROR;
                }
                if (i == 3) {
                    return RPC_RETURNED_INVALID_RESULT;
                }
                if (i == 4) {
                    return RPC_RETURNED_MALFORMED_RESULT;
                }
                if (i == 5) {
                    return RPC_EXPONENTIAL_BACKOFF_FAILED;
                }
                switch (i) {
                    case 10:
                        return DIRECTORY_CREATION_FAILED;
                    case 11:
                        return FILE_WRITE_FAILED_DISK_FULL;
                    case 12:
                        return FILE_WRITE_FAILED;
                    case 13:
                        return FILE_READ_FAILED;
                    case 14:
                        return FILE_READ_RETURNED_INVALID_DATA;
                    case 15:
                        return FILE_READ_RETURNED_MALFORMED_DATA;
                    default:
                        return null;
                }
            }

            public static zzxe zzf() {
                return zzpr.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzbb() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzbb, zza> implements zzym {
            private zza() {
                super(zzbb.zzbhi);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzbb();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbhi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0003\u0000\u0001ဃ\u0000\u0002\u001e\u0003\u001e\u0004\u001e\u0005င\u0001", new Object[]{"zzj", "zzazi", "zzbhb", zzb.zzf(), "zzbhd", zzb.zzf(), "zzbhf", zzb.zzf(), "zzbhh"});
                case 4:
                    return zzbhi;
                case 5:
                    zzyx<zzbb> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzbb.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbhi);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzbb zzbb = new zzbb();
            zzbhi = zzbb;
            zzwz.zza(zzbb.class, zzbb);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzbd extends zzwz<zzbd, zza> implements zzym {
        private static final zzbd zzbik;
        private static volatile zzyx<zzbd> zzh;
        private String zzbii = "";
        private String zzbij = "";
        private int zzj;

        private zzbd() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzbd, zza> implements zzym {
            private zza() {
                super(zzbd.zzbik);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzbd();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbik, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001", new Object[]{"zzj", "zzbii", "zzbij"});
                case 4:
                    return zzbik;
                case 5:
                    zzyx<zzbd> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzbd.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbik);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzbd zzbd = new zzbd();
            zzbik = zzbd;
            zzwz.zza(zzbd.class, zzbd);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzc extends zzwz<zzc, zza> implements zzym {
        private static final zzc zzanj;
        private static volatile zzyx<zzc> zzh;
        private int zzamv;
        private zzab zzamw;
        private zzb zzani;
        private int zzj;

        private zzc() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzc, zza> implements zzym {
            private zza() {
                super(zzc.zzanj);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzxj<Integer, zzam.zza> zzann = new zznt();
            private static final zzxj<Integer, zzam.zzb> zzanp = new zznu();
            private static final zzb zzanq;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private boolean zzank;
            private zztg.zza zzanl;
            private zzxg zzanm = zzup();
            private zzxg zzano = zzup();
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzanq);
                }

                public final zza zzd(zzoc zzoc) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzoc);
                    return this;
                }

                public final zza zzp(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzn(z);
                    return this;
                }

                public final zza zzc(zzae zzae) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzae);
                    return this;
                }

                public final zza zzb(zztg.zza zza) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zza((zzb) zza);
                    return this;
                }

                public final zza zzj(Iterable<? extends zzam.zza> iterable) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzh((zzb) iterable);
                    return this;
                }

                public final zza zzk(Iterable<? extends zzam.zzb> iterable) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzi(iterable);
                    return this;
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzoc zzoc) {
                this.zzamy = zzoc.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzn(boolean z) {
                this.zzj |= 4;
                this.zzamz = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzae zzae) {
                zzae.getClass();
                this.zzana = zzae;
                this.zzj |= 8;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zza(zztg.zza zza2) {
                zza2.getClass();
                this.zzanl = zza2;
                this.zzj |= 16;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzh(Iterable<? extends zzam.zza> iterable) {
                if (!this.zzanm.zztl()) {
                    this.zzanm = zzwz.zza(this.zzanm);
                }
                for (zzam.zza zza2 : iterable) {
                    this.zzanm.zzds(zza2.zzd());
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzi(Iterable<? extends zzam.zzb> iterable) {
                if (!this.zzano.zztl()) {
                    this.zzano = zzwz.zza(this.zzano);
                }
                for (zzam.zzb zzb : iterable) {
                    this.zzano.zzds(zzb.zzd());
                }
            }

            public static zza zzkd() {
                return (zza) zzanq.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzanq, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0002\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဇ\u0002\u0004ဉ\u0003\u0005ဉ\u0004\u0006\u001e\u0007\u001e", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzank", "zzamz", "zzana", "zzanl", "zzanm", zzam.zza.zzf(), "zzano", zzam.zzb.zzf()});
                    case 4:
                        return zzanq;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzanq);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzanq = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzanj, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzani", "zzamv", "zzamw"});
                case 4:
                    return zzanj;
                case 5:
                    zzyx<zzc> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzc.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzanj);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzc zzc = new zzc();
            zzanj = zzc;
            zzwz.zza(zzc.class, zzc);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzd extends zzwz<zzd, zza> implements zzym {
        private static final zzd zzans;
        private static volatile zzyx<zzd> zzh;
        private int zzamv;
        private zzab zzamw;
        private zzb zzanr;
        private int zzj;

        private zzd() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzd, zza> implements zzym {
            private zza() {
                super(zzd.zzans);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzanw;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private zzac zzant;
            private int zzanu;
            private int zzanv;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzanw);
                }

                public final zza zze(zzoc zzoc) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzoc);
                    return this;
                }

                public final zza zzq(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzn(z);
                    return this;
                }

                public final zza zzd(zzae zzae) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzae);
                    return this;
                }

                public final zza zza(zzac zzac) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzac);
                    return this;
                }

                public final zza zzap(int i) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzar(i);
                    return this;
                }

                public final zza zzaq(int i) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzas(i);
                    return this;
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzoc zzoc) {
                this.zzamy = zzoc.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzn(boolean z) {
                this.zzj |= 2;
                this.zzamz = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzae zzae) {
                zzae.getClass();
                this.zzana = zzae;
                this.zzj |= 4;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzac zzac) {
                zzac.getClass();
                this.zzant = zzac;
                this.zzj |= 8;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzar(int i) {
                this.zzj |= 16;
                this.zzanu = i;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzas(int i) {
                this.zzj |= 32;
                this.zzanv = i;
            }

            public static zza zzkg() {
                return (zza) zzanw.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzanw, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဉ\u0002\u0004ဉ\u0003\u0005ဋ\u0004\u0006ဋ\u0005", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzamz", "zzana", "zzant", "zzanu", "zzanv"});
                    case 4:
                        return zzanw;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzanw);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzanw = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzd();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzans, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzanr", "zzamv", "zzamw"});
                case 4:
                    return zzans;
                case 5:
                    zzyx<zzd> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzd.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzans);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzd zzd = new zzd();
            zzans = zzd;
            zzwz.zza(zzd.class, zzd);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zze extends zzwz<zze, zza> implements zzym {
        private static final zze zzany;
        private static volatile zzyx<zze> zzh;
        private int zzamv;
        private zzab zzamw;
        private zzb zzanx;
        private int zzj;

        private zze() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zze, zza> implements zzym {
            private zza() {
                super(zze.zzany);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzaoa;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private zzap zzanz;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzaoa);
                }

                public final zza zzf(zzoc zzoc) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzoc);
                    return this;
                }

                public final zza zzr(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzn(z);
                    return this;
                }

                public final zza zze(zzae zzae) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzae);
                    return this;
                }

                public final zza zza(zzap zzap) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzap);
                    return this;
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzoc zzoc) {
                this.zzamy = zzoc.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzn(boolean z) {
                this.zzj |= 2;
                this.zzamz = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzae zzae) {
                zzae.getClass();
                this.zzana = zzae;
                this.zzj |= 4;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzap zzap) {
                zzap.getClass();
                this.zzanz = zzap;
                this.zzj |= 8;
            }

            public static zza zzkj() {
                return (zza) zzaoa.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzaoa, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဉ\u0002\u0004ဉ\u0003", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzamz", "zzana", "zzanz"});
                    case 4:
                        return zzaoa;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzaoa);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzaoa = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zze();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzany, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzanx", "zzamv", "zzamw"});
                case 4:
                    return zzany;
                case 5:
                    zzyx<zze> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zze.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzany);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zze zze = new zze();
            zzany = zze;
            zzwz.zza(zze.class, zze);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzf extends zzwz<zzf, zza> implements zzym {
        private static final zzf zzaoc;
        private static volatile zzyx<zzf> zzh;
        private int zzamv;
        private zzab zzamw;
        private zzb zzaob;
        private int zzj;

        private zzf() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzf, zza> implements zzym {
            private zza() {
                super(zzf.zzaoc);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzaoe;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private boolean zzank;
            private zzas zzaod;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzaoe);
                }

                public final zza zzg(zzoc zzoc) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzoc);
                    return this;
                }

                public final zza zzs(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzu(z);
                    return this;
                }

                public final zza zzt(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzn(z);
                    return this;
                }

                public final zza zzf(zzae zzae) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzae);
                    return this;
                }

                public final zza zza(zzas zzas) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzas);
                    return this;
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzoc zzoc) {
                this.zzamy = zzoc.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzu(boolean z) {
                this.zzj |= 2;
                this.zzank = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzn(boolean z) {
                this.zzj |= 4;
                this.zzamz = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzae zzae) {
                zzae.getClass();
                this.zzana = zzae;
                this.zzj |= 8;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzas zzas) {
                zzas.getClass();
                this.zzaod = zzas;
                this.zzj |= 16;
            }

            public static zza zzkm() {
                return (zza) zzaoe.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzaoe, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဇ\u0002\u0004ဉ\u0003\u0005ဉ\u0004", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzank", "zzamz", "zzana", "zzaod"});
                    case 4:
                        return zzaoe;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzaoe);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzaoe = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzf();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaoc, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaob", "zzamv", "zzamw"});
                case 4:
                    return zzaoc;
                case 5:
                    zzyx<zzf> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzf.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaoc);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzf zzf = new zzf();
            zzaoc = zzf;
            zzwz.zza(zzf.class, zzf);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzg extends zzwz<zzg, zza> implements zzym {
        private static final zzg zzaog;
        private static volatile zzyx<zzg> zzh;
        private int zzamv;
        private zzab zzamw;
        private zzb zzaof;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzaoi;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private zzaw zzaoh;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzaoi);
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzaoi, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဉ\u0002\u0004ဉ\u0003", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzamz", "zzana", "zzaoh"});
                    case 4:
                        return zzaoi;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzaoi);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzaoi = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        private zzg() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzg, zza> implements zzym {
            private zza() {
                super(zzg.zzaog);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzg();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaog, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaof", "zzamv", "zzamw"});
                case 4:
                    return zzaog;
                case 5:
                    zzyx<zzg> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzg.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaog);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzg zzg = new zzg();
            zzaog = zzg;
            zzwz.zza(zzg.class, zzg);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzh extends zzwz<zzh, zza> implements zzym {
        private static final zzh zzaok;
        private static volatile zzyx<zzh> zzh;
        private int zzamv;
        private zzab zzamw;
        private zzb zzaoj;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzaol;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzaol);
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzaol, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzamz", "zzana"});
                    case 4:
                        return zzaol;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzaol);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzaol = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        private zzh() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzh, zza> implements zzym {
            private zza() {
                super(zzh.zzaok);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzh();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaok, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoj", "zzamv", "zzamw"});
                case 4:
                    return zzaok;
                case 5:
                    zzyx<zzh> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzh.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaok);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzh zzh2 = new zzh();
            zzaok = zzh2;
            zzwz.zza(zzh.class, zzh2);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzi extends zzwz<zzi, zza> implements zzym {
        private static final zzi zzaon;
        private static volatile zzyx<zzi> zzh;
        private int zzamv;
        private zzab zzamw;
        private zzb zzaom;
        private int zzj;

        private zzi() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzi, zza> implements zzym {
            private zza() {
                super(zzi.zzaon);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzaoo;
            private static volatile zzyx<zzb> zzh;
            private int zzamy;
            private boolean zzamz;
            private zzae zzana;
            private boolean zzank;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzaoo);
                }

                public final zza zzh(zzoc zzoc) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzoc);
                    return this;
                }

                public final zza zzv(boolean z) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzn(z);
                    return this;
                }

                public final zza zzg(zzae zzae) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzae);
                    return this;
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzoc zzoc) {
                this.zzamy = zzoc.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzn(boolean z) {
                this.zzj |= 4;
                this.zzamz = z;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(zzae zzae) {
                zzae.getClass();
                this.zzana = zzae;
                this.zzj |= 8;
            }

            public static zza zzkt() {
                return (zza) zzaoo.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzaoo, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဇ\u0002\u0004ဉ\u0003", new Object[]{"zzj", "zzamy", zzoc.zzf(), "zzank", "zzamz", "zzana"});
                    case 4:
                        return zzaoo;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzaoo);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzaoo = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzi();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaon, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaom", "zzamv", "zzamw"});
                case 4:
                    return zzaon;
                case 5:
                    zzyx<zzi> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzi.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaon);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzi zzi = new zzi();
            zzaon = zzi;
            zzwz.zza(zzi.class, zzi);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzm extends zzwz<zzm, zza> implements zzym {
        private static final zzm zzaoz;
        private static volatile zzyx<zzm> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzm() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzm, zza> implements zzym {
            private zza() {
                super(zzm.zzaoz);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzm();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaoz, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzaoz;
                case 5:
                    zzyx<zzm> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzm.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaoz);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzm zzm = new zzm();
            zzaoz = zzm;
            zzwz.zza(zzm.class, zzm);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzn extends zzwz<zzn, zzb> implements zzym {
        private static final zzn zzapb;
        private static volatile zzyx<zzn> zzh;
        private int zzapa;
        private int zzer;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            UNKNOWN_MODEL_TYPE(0),
            STABLE_MODEL(1),
            LATEST_MODEL(2);
            
            private static final zzxf<zza> zzac = new zznx();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzat(int i) {
                if (i == 0) {
                    return UNKNOWN_MODEL_TYPE;
                }
                if (i == 1) {
                    return STABLE_MODEL;
                }
                if (i != 2) {
                    return null;
                }
                return LATEST_MODEL;
            }

            public static zzxe zzf() {
                return zznw.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        private zzn() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zzn, zzb> implements zzym {
            private zzb() {
                super(zzn.zzapb);
            }

            /* synthetic */ zzb(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzn();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzapb, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001င\u0000\u0002ဌ\u0001", new Object[]{"zzj", "zzer", "zzapa", zza.zzf()});
                case 4:
                    return zzapb;
                case 5:
                    zzyx<zzn> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzn.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapb);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzn zzn = new zzn();
            zzapb = zzn;
            zzwz.zza(zzn.class, zzn);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzo extends zzwz<zzo, zza> implements zzym {
        private static final zzo zzapg;
        private static volatile zzyx<zzo> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzo() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzo, zza> implements zzym {
            private zza() {
                super(zzo.zzapg);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzo();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapg, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapg;
                case 5:
                    zzyx<zzo> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzo.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapg);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzo zzo = new zzo();
            zzapg = zzo;
            zzwz.zza(zzo.class, zzo);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzp extends zzwz<zzp, zza> implements zzym {
        private static final zzp zzaph;
        private static volatile zzyx<zzp> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzp() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzp, zza> implements zzym {
            private zza() {
                super(zzp.zzaph);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzp();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaph, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzaph;
                case 5:
                    zzyx<zzp> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzp.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaph);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzp zzp = new zzp();
            zzaph = zzp;
            zzwz.zza(zzp.class, zzp);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzq extends zzwz<zzq, zza> implements zzym {
        private static final zzq zzapi;
        private static volatile zzyx<zzq> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzq() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzq, zza> implements zzym {
            private zza() {
                super(zzq.zzapi);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzq();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapi, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapi;
                case 5:
                    zzyx<zzq> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzq.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapi);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzq zzq = new zzq();
            zzapi = zzq;
            zzwz.zza(zzq.class, zzq);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzr extends zzwz<zzr, zza> implements zzym {
        private static final zzr zzapj;
        private static volatile zzyx<zzr> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzr() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzr, zza> implements zzym {
            private zza() {
                super(zzr.zzapj);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzr();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapj, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapj;
                case 5:
                    zzyx<zzr> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzr.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapj);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzr zzr = new zzr();
            zzapj = zzr;
            zzwz.zza(zzr.class, zzr);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzs extends zzwz<zzs, zza> implements zzym {
        private static final zzs zzapk;
        private static volatile zzyx<zzs> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzs() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzs, zza> implements zzym {
            private zza() {
                super(zzs.zzapk);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzs();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapk, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapk;
                case 5:
                    zzyx<zzs> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzs.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapk);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzs zzs = new zzs();
            zzapk = zzs;
            zzwz.zza(zzs.class, zzs);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzt extends zzwz<zzt, zza> implements zzym {
        private static final zzt zzapl;
        private static volatile zzyx<zzt> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzt() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzt, zza> implements zzym {
            private zza() {
                super(zzt.zzapl);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzt();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapl, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapl;
                case 5:
                    zzyx<zzt> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzt.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapl);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzt zzt = new zzt();
            zzapl = zzt;
            zzwz.zza(zzt.class, zzt);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzu extends zzwz<zzu, zza> implements zzym {
        private static final zzu zzapm;
        private static volatile zzyx<zzu> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzu() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzu, zza> implements zzym {
            private zza() {
                super(zzu.zzapm);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzu();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapm, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapm;
                case 5:
                    zzyx<zzu> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzu.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapm);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzu zzu = new zzu();
            zzapm = zzu;
            zzwz.zza(zzu.class, zzu);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzv extends zzwz<zzv, zza> implements zzym {
        private static final zzv zzapn;
        private static volatile zzyx<zzv> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzv() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzv, zza> implements zzym {
            private zza() {
                super(zzv.zzapn);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzv();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapn, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapn;
                case 5:
                    zzyx<zzv> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzv.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapn);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzv zzv = new zzv();
            zzapn = zzv;
            zzwz.zza(zzv.class, zzv);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzw extends zzwz<zzw, zza> implements zzym {
        private static final zzw zzapo;
        private static volatile zzyx<zzw> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private zzn zzaoy;
        private int zzj;

        private zzw() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzw, zza> implements zzym {
            private zza() {
                super(zzw.zzapo);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzw();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapo, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzaoy", "zzana"});
                case 4:
                    return zzapo;
                case 5:
                    zzyx<zzw> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzw.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapo);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzw zzw = new zzw();
            zzapo = zzw;
            zzwz.zza(zzw.class, zzw);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzy extends zzwz<zzy, zza> implements zzym {
        private static final zzy zzapq;
        private static volatile zzyx<zzy> zzh;
        private zzak zzanb;
        private zzxl<zzb> zzanf = zzus();
        private zzxl<zzb> zzang = zzus();
        private zzaf zzaoq;
        private long zzaor;
        private int zzj;

        private zzy() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzapt;
            private static volatile zzyx<zzb> zzh;
            private int zzapr;
            private zzxg zzaps = zzup();
            private int zzj;

            /* renamed from: com.google.android.gms.internal.firebase_ml.zzns$zzy$zzb$zzb  reason: collision with other inner class name */
            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public enum EnumC0015zzb implements zzxc {
                UNKNOWN_DATA_TYPE(0),
                TYPE_FLOAT32(1),
                TYPE_INT32(2),
                TYPE_BYTE(3),
                TYPE_LONG(4);
                
                private static final zzxf<EnumC0015zzb> zzac = new zzny();
                private final int value;

                @Override // com.google.android.gms.internal.firebase_ml.zzxc
                public final int zzd() {
                    return this.value;
                }

                public static EnumC0015zzb zzau(int i) {
                    if (i == 0) {
                        return UNKNOWN_DATA_TYPE;
                    }
                    if (i == 1) {
                        return TYPE_FLOAT32;
                    }
                    if (i == 2) {
                        return TYPE_INT32;
                    }
                    if (i == 3) {
                        return TYPE_BYTE;
                    }
                    if (i != 4) {
                        return null;
                    }
                    return TYPE_LONG;
                }

                public static zzxe zzf() {
                    return zznz.zzan;
                }

                public final String toString() {
                    return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
                }

                private EnumC0015zzb(int i) {
                    this.value = i;
                }
            }

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzapt);
                }

                public final zza zza(EnumC0015zzb zzb) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzb((zzb) zzb);
                    return this;
                }

                public final zza zzp(Iterable<? extends Integer> iterable) {
                    if (this.zzclp) {
                        zzux();
                        this.zzclp = false;
                    }
                    ((zzb) this.zzclo).zzq(iterable);
                    return this;
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzb(EnumC0015zzb zzb) {
                this.zzapr = zzb.zzd();
                this.zzj |= 1;
            }

            /* access modifiers changed from: private */
            /* access modifiers changed from: public */
            private final void zzq(Iterable<? extends Integer> iterable) {
                if (!this.zzaps.zztl()) {
                    this.zzaps = zzwz.zza(this.zzaps);
                }
                zzvl.zza(iterable, this.zzaps);
            }

            public static zza zzlq() {
                return (zza) zzapt.zzun();
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzapt, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001ဌ\u0000\u0002\u0016", new Object[]{"zzj", "zzapr", EnumC0015zzb.zzf(), "zzaps"});
                    case 4:
                        return zzapt;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzapt);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzapt = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzy, zza> implements zzym {
            private zza() {
                super(zzy.zzapq);
            }

            public final zza zzb(zzaf.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzy) this.zzclo).zza((zzy) ((zzaf) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zzh(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzy) this.zzclo).zzb((zzy) zzak);
                return this;
            }

            public final zza zzn(Iterable<? extends zzb> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzy) this.zzclo).zzf((zzy) iterable);
                return this;
            }

            public final zza zzo(Iterable<? extends zzb> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzy) this.zzclo).zzg(iterable);
                return this;
            }

            public final zza zzh(long j) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzy) this.zzclo).zzf((zzy) j);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaf zzaf) {
            zzaf.getClass();
            this.zzaoq = zzaf;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzak zzak) {
            zzak.getClass();
            this.zzanb = zzak;
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzf(Iterable<? extends zzb> iterable) {
            zzxl<zzb> zzxl = this.zzanf;
            if (!zzxl.zztl()) {
                this.zzanf = zzwz.zza(zzxl);
            }
            zzvl.zza(iterable, this.zzanf);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzg(Iterable<? extends zzb> iterable) {
            zzxl<zzb> zzxl = this.zzang;
            if (!zzxl.zztl()) {
                this.zzang = zzwz.zza(zzxl);
            }
            zzvl.zza(iterable, this.zzang);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzf(long j) {
            this.zzj |= 4;
            this.zzaor = j;
        }

        public static zza zzlo() {
            return (zza) zzapq.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzy();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapq, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001b\u0004\u001b\u0005ဃ\u0002", new Object[]{"zzj", "zzaoq", "zzanb", "zzanf", zzb.class, "zzang", zzb.class, "zzaor"});
                case 4:
                    return zzapq;
                case 5:
                    zzyx<zzy> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzy.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapq);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzy zzy = new zzy();
            zzapq = zzy;
            zzwz.zza(zzy.class, zzy);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzaa extends zzwz<zzaa, zza> implements zzym {
        private static final zzaa zzaqd;
        private static volatile zzyx<zzaa> zzh;
        private int zzapa;
        private boolean zzaqc;
        private int zzj;

        private zzaa() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzaa, zza> implements zzym {
            private zza() {
                super(zzaa.zzaqd);
            }

            public final zza zza(zzaj.zzb zzb) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaa) this.zzclo).zzb((zzaa) zzb);
                return this;
            }

            public final zza zzy(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaa) this.zzclo).zzz(z);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzaj.zzb zzb) {
            this.zzapa = zzb.zzd();
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzz(boolean z) {
            this.zzj |= 2;
            this.zzaqc = z;
        }

        public static zza zzlu() {
            return (zza) zzaqd.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzaa();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaqd, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001", new Object[]{"zzj", "zzapa", zzaj.zzb.zzf(), "zzaqc"});
                case 4:
                    return zzaqd;
                case 5:
                    zzyx<zzaa> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzaa.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaqd);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzaa zzaa = new zzaa();
            zzaqd = zzaa;
            zzwz.zza(zzaa.class, zzaa);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzac extends zzwz<zzac, zza> implements zzym {
        private static final zzac zzawj;
        private static volatile zzyx<zzac> zzh;
        private int zzawd;
        private int zzawe;
        private int zzawf;
        private int zzawg;
        private boolean zzawh;
        private float zzawi;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            UNKNOWN_CLASSIFICATIONS(0),
            NO_CLASSIFICATIONS(1),
            ALL_CLASSIFICATIONS(2);
            
            private static final zzxf<zzb> zzac = new zzoh();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzax(int i) {
                if (i == 0) {
                    return UNKNOWN_CLASSIFICATIONS;
                }
                if (i == 1) {
                    return NO_CLASSIFICATIONS;
                }
                if (i != 2) {
                    return null;
                }
                return ALL_CLASSIFICATIONS;
            }

            public static zzxe zzf() {
                return zzoi.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzc implements zzxc {
            UNKNOWN_CONTOURS(0),
            NO_CONTOURS(1),
            ALL_CONTOURS(2);
            
            private static final zzxf<zzc> zzac = new zzok();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzc zzay(int i) {
                if (i == 0) {
                    return UNKNOWN_CONTOURS;
                }
                if (i == 1) {
                    return NO_CONTOURS;
                }
                if (i != 2) {
                    return null;
                }
                return ALL_CONTOURS;
            }

            public static zzxe zzf() {
                return zzoj.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzc(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzd implements zzxc {
            UNKNOWN_LANDMARKS(0),
            NO_LANDMARKS(1),
            ALL_LANDMARKS(2);
            
            private static final zzxf<zzd> zzac = new zzol();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzd zzaz(int i) {
                if (i == 0) {
                    return UNKNOWN_LANDMARKS;
                }
                if (i == 1) {
                    return NO_LANDMARKS;
                }
                if (i != 2) {
                    return null;
                }
                return ALL_LANDMARKS;
            }

            public static zzxe zzf() {
                return zzom.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzd(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zze implements zzxc {
            UNKNOWN_PERFORMANCE(0),
            FAST(1),
            ACCURATE(2);
            
            private static final zzxf<zze> zzac = new zzoo();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zze zzba(int i) {
                if (i == 0) {
                    return UNKNOWN_PERFORMANCE;
                }
                if (i == 1) {
                    return FAST;
                }
                if (i != 2) {
                    return null;
                }
                return ACCURATE;
            }

            public static zzxe zzf() {
                return zzon.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zze(int i) {
                this.value = i;
            }
        }

        private zzac() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzac, zza> implements zzym {
            private zza() {
                super(zzac.zzawj);
            }

            public final zza zza(zzd zzd) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzac) this.zzclo).zzb((zzac) zzd);
                return this;
            }

            public final zza zza(zzb zzb) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzac) this.zzclo).zzb((zzac) zzb);
                return this;
            }

            public final zza zza(zze zze) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzac) this.zzclo).zzb((zzac) zze);
                return this;
            }

            public final zza zza(zzc zzc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzac) this.zzclo).zzb((zzac) zzc);
                return this;
            }

            public final zza zzaa(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzac) this.zzclo).zzab(z);
                return this;
            }

            public final zza zzk(float f) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzac) this.zzclo).zzl(f);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzd zzd2) {
            this.zzawd = zzd2.zzd();
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzb zzb2) {
            this.zzawe = zzb2.zzd();
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zze zze2) {
            this.zzawf = zze2.zzd();
            this.zzj |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzc zzc2) {
            this.zzawg = zzc2.zzd();
            this.zzj |= 8;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzab(boolean z) {
            this.zzj |= 16;
            this.zzawh = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzl(float f) {
            this.zzj |= 32;
            this.zzawi = f;
        }

        public static zza zzlx() {
            return (zza) zzawj.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzac();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzawj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဌ\u0002\u0004ဌ\u0003\u0005ဇ\u0004\u0006ခ\u0005", new Object[]{"zzj", "zzawd", zzd.zzf(), "zzawe", zzb.zzf(), "zzawf", zze.zzf(), "zzawg", zzc.zzf(), "zzawh", "zzawi"});
                case 4:
                    return zzawj;
                case 5:
                    zzyx<zzac> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzac.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzawj);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzac zzac = new zzac();
            zzawj = zzac;
            zzwz.zza(zzac.class, zzac);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzae extends zzwz<zzae, zza> implements zzym {
        private static final zzae zzayy;
        private static volatile zzyx<zzae> zzh;
        private int zzayv;
        private int zzayw;
        private int zzayx;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            UNKNOWN_FORMAT(0),
            NV16(1),
            NV21(2),
            YV12(3),
            YUV_420_888(7),
            BITMAP(4),
            CM_SAMPLE_BUFFER_REF(5),
            UI_IMAGE(6);
            
            private static final zzxf<zzb> zzac = new zzop();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzbd(int i) {
                switch (i) {
                    case 0:
                        return UNKNOWN_FORMAT;
                    case 1:
                        return NV16;
                    case 2:
                        return NV21;
                    case 3:
                        return YV12;
                    case 4:
                        return BITMAP;
                    case 5:
                        return CM_SAMPLE_BUFFER_REF;
                    case 6:
                        return UI_IMAGE;
                    case 7:
                        return YUV_420_888;
                    default:
                        return null;
                }
            }

            public static zzxe zzf() {
                return zzoq.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzae() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzae, zza> implements zzym {
            private zza() {
                super(zzae.zzayy);
            }

            public final zza zza(zzb zzb) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzae) this.zzclo).zzb((zzae) zzb);
                return this;
            }

            public final zza zzbb(int i) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzae) this.zzclo).zzbc(i);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzb zzb2) {
            this.zzayv = zzb2.zzd();
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbc(int i) {
            this.zzj |= 2;
            this.zzayw = i;
        }

        public static zza zzmc() {
            return (zza) zzayy.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzae();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzayy, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဋ\u0001\u0003ဋ\u0002", new Object[]{"zzj", "zzayv", zzb.zzf(), "zzayw", "zzayx"});
                case 4:
                    return zzayy;
                case 5:
                    zzyx<zzae> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzae.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzayy);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzae zzae = new zzae();
            zzayy = zzae;
            zzwz.zza(zzae.class, zzae);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzaf extends zzwz<zzaf, zza> implements zzym {
        private static final zzaf zzazm;
        private static volatile zzyx<zzaf> zzh;
        private int zzamy;
        private boolean zzamz;
        private boolean zzaqa;
        private long zzazi;
        private boolean zzazj;
        private boolean zzazk;
        private int zzazl;
        private int zzj;

        private zzaf() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzaf, zza> implements zzym {
            private zza() {
                super(zzaf.zzazm);
            }

            public final zza zzk(long j) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaf) this.zzclo).zzj(j);
                return this;
            }

            public final zza zzk(zzoc zzoc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaf) this.zzclo).zzb((zzaf) zzoc);
                return this;
            }

            public final zza zzah(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaf) this.zzclo).zzn(z);
                return this;
            }

            public final zza zzai(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaf) this.zzclo).zzae(true);
                return this;
            }

            public final zza zzaj(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaf) this.zzclo).zzaf(true);
                return this;
            }

            public final zza zzak(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaf) this.zzclo).zzw(z);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzj(long j) {
            this.zzj |= 1;
            this.zzazi = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzoc zzoc) {
            this.zzamy = zzoc.zzd();
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzn(boolean z) {
            this.zzj |= 4;
            this.zzamz = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzae(boolean z) {
            this.zzj |= 8;
            this.zzazj = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzaf(boolean z) {
            this.zzj |= 16;
            this.zzazk = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzw(boolean z) {
            this.zzj |= 32;
            this.zzaqa = z;
        }

        public static zza zzme() {
            return (zza) zzazm.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzaf();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzazm, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0000\u0000\u0001ဃ\u0000\u0002ဌ\u0001\u0003ဇ\u0002\u0004ဇ\u0003\u0005ဇ\u0004\u0006ဇ\u0005\u0007ဋ\u0006", new Object[]{"zzj", "zzazi", "zzamy", zzoc.zzf(), "zzamz", "zzazj", "zzazk", "zzaqa", "zzazl"});
                case 4:
                    return zzazm;
                case 5:
                    zzyx<zzaf> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzaf.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzazm);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzaf zzaf = new zzaf();
            zzazm = zzaf;
            zzwz.zza(zzaf.class, zzaf);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzag extends zzwz<zzag, zza> implements zzym {
        private static final zzag zzazo;
        private static volatile zzyx<zzag> zzh;
        private int zzapa;
        private boolean zzazn;
        private int zzj;

        private zzag() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzag, zza> implements zzym {
            private zza() {
                super(zzag.zzazo);
            }

            public final zza zzc(zzaj.zzb zzb) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzag) this.zzclo).zzb((zzag) zzb);
                return this;
            }

            public final zza zzal(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzag) this.zzclo).zzag(z);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzaj.zzb zzb) {
            this.zzapa = zzb.zzd();
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzag(boolean z) {
            this.zzj |= 2;
            this.zzazn = z;
        }

        public static zza zzmg() {
            return (zza) zzazo.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzag();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzazo, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001", new Object[]{"zzj", "zzapa", zzaj.zzb.zzf(), "zzazn"});
                case 4:
                    return zzazo;
                case 5:
                    zzyx<zzag> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzag.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzazo);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzag zzag = new zzag();
            zzazo = zzag;
            zzwz.zza(zzag.class, zzag);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzai extends zzwz<zzai, zzb> implements zzym {
        private static final zzai zzazw;
        private static volatile zzyx<zzai> zzh;
        private int zzamy;
        private zzak zzanb;
        private long zzazs;
        private long zzazt;
        private int zzazu;
        private long zzazv;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            UNKNOWN_STATUS(0),
            EXPLICITLY_REQUESTED(1),
            IMPLICITLY_REQUESTED(2),
            MODEL_INFO_RETRIEVAL_SUCCEEDED(3),
            MODEL_INFO_RETRIEVAL_FAILED(4),
            SCHEDULED(5),
            DOWNLOADING(6),
            SUCCEEDED(7),
            FAILED(8),
            LIVE(9),
            UPDATE_AVAILABLE(10),
            DOWNLOADED(11);
            
            private static final zzxf<zza> zzac = new zzos();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzbe(int i) {
                switch (i) {
                    case 0:
                        return UNKNOWN_STATUS;
                    case 1:
                        return EXPLICITLY_REQUESTED;
                    case 2:
                        return IMPLICITLY_REQUESTED;
                    case 3:
                        return MODEL_INFO_RETRIEVAL_SUCCEEDED;
                    case 4:
                        return MODEL_INFO_RETRIEVAL_FAILED;
                    case 5:
                        return SCHEDULED;
                    case 6:
                        return DOWNLOADING;
                    case 7:
                        return SUCCEEDED;
                    case 8:
                        return FAILED;
                    case 9:
                        return LIVE;
                    case 10:
                        return UPDATE_AVAILABLE;
                    case 11:
                        return DOWNLOADED;
                    default:
                        return null;
                }
            }

            public static zzxe zzf() {
                return zzor.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        private zzai() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zzai, zzb> implements zzym {
            private zzb() {
                super(zzai.zzazw);
            }

            public final zzb zzk(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzai) this.zzclo).zzb((zzai) zzak);
                return this;
            }

            public final zzb zzo(long j) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzai) this.zzclo).zzl(j);
                return this;
            }

            public final zzb zzl(zzoc zzoc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzai) this.zzclo).zzb((zzai) zzoc);
                return this;
            }

            public final zzb zzp(long j) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzai) this.zzclo).zzm(j);
                return this;
            }

            public final zzb zzb(zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzai) this.zzclo).zza((zzai) zza);
                return this;
            }

            public final zzb zzq(long j) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzai) this.zzclo).zzn(j);
                return this;
            }

            /* synthetic */ zzb(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzak zzak) {
            zzak.getClass();
            this.zzanb = zzak;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzl(long j) {
            this.zzj |= 2;
            this.zzazs = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzoc zzoc) {
            this.zzamy = zzoc.zzd();
            this.zzj |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzm(long j) {
            this.zzj |= 8;
            this.zzazt = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zza zza2) {
            this.zzazu = zza2.zzd();
            this.zzj |= 16;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzn(long j) {
            this.zzj |= 32;
            this.zzazv = j;
        }

        public static zzb zzmj() {
            return (zzb) zzazw.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzai();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzazw, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဃ\u0001\u0003ဌ\u0002\u0004ဃ\u0003\u0005ဌ\u0004\u0006ဂ\u0005", new Object[]{"zzj", "zzanb", "zzazs", "zzamy", zzoc.zzf(), "zzazt", "zzazu", zza.zzf(), "zzazv"});
                case 4:
                    return zzazw;
                case 5:
                    zzyx<zzai> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzai.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzazw);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzai zzai = new zzai();
            zzazw = zzai;
            zzwz.zza(zzai.class, zzai);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzaj extends zzwz<zzaj, zza> implements zzym {
        private static final zzaj zzbar;
        private static volatile zzyx<zzaj> zzh;
        private int zzapa;
        private String zzbak = "";
        private String zzbal = "";
        private int zzbam;
        private String zzban = "";
        private String zzbao = "";
        private long zzbap;
        private boolean zzbaq;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            TYPE_UNKNOWN(0),
            CUSTOM(1),
            AUTOML_IMAGE_LABELING(2),
            BASE_TRANSLATE(3),
            CUSTOM_OBJECT_DETECTION(4),
            CUSTOM_IMAGE_LABELING(5);
            
            private static final zzxf<zzb> zzac = new zzot();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzbf(int i) {
                if (i == 0) {
                    return TYPE_UNKNOWN;
                }
                if (i == 1) {
                    return CUSTOM;
                }
                if (i == 2) {
                    return AUTOML_IMAGE_LABELING;
                }
                if (i == 3) {
                    return BASE_TRANSLATE;
                }
                if (i == 4) {
                    return CUSTOM_OBJECT_DETECTION;
                }
                if (i != 5) {
                    return null;
                }
                return CUSTOM_IMAGE_LABELING;
            }

            public static zzxe zzf() {
                return zzou.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzc implements zzxc {
            SOURCE_UNKNOWN(0),
            APP_ASSET(1),
            LOCAL(2),
            CLOUD(3),
            SDK_BUILT_IN(4);
            
            private static final zzxf<zzc> zzac = new zzow();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzc zzbg(int i) {
                if (i == 0) {
                    return SOURCE_UNKNOWN;
                }
                if (i == 1) {
                    return APP_ASSET;
                }
                if (i == 2) {
                    return LOCAL;
                }
                if (i == 3) {
                    return CLOUD;
                }
                if (i != 4) {
                    return null;
                }
                return SDK_BUILT_IN;
            }

            public static zzxe zzf() {
                return zzov.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzc(int i) {
                this.value = i;
            }
        }

        private zzaj() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzaj, zza> implements zzym {
            private zza() {
                super(zzaj.zzbar);
            }

            public final zza zzbd(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaj) this.zzclo).setName(str);
                return this;
            }

            public final zza zza(zzc zzc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaj) this.zzclo).zzb((zzaj) zzc);
                return this;
            }

            public final zza zzbe(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaj) this.zzclo).zzbg(str);
                return this;
            }

            public final zza zzbf(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaj) this.zzclo).zzbh(str);
                return this;
            }

            public final zza zzd(zzb zzb) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaj) this.zzclo).zzb((zzaj) zzb);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void setName(String str) {
            str.getClass();
            this.zzj |= 1;
            this.zzbak = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzc zzc2) {
            this.zzbam = zzc2.zzd();
            this.zzj |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbg(String str) {
            str.getClass();
            this.zzj |= 8;
            this.zzban = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbh(String str) {
            str.getClass();
            this.zzj |= 16;
            this.zzbao = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzb zzb2) {
            this.zzapa = zzb2.zzd();
            this.zzj |= 32;
        }

        public static zza zzml() {
            return (zza) zzbar.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzaj();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbar, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဌ\u0002\u0004ဈ\u0003\u0005ဈ\u0004\u0006ဌ\u0005\u0007ဃ\u0006\bဇ\u0007", new Object[]{"zzj", "zzbak", "zzbal", "zzbam", zzc.zzf(), "zzban", "zzbao", "zzapa", zzb.zzf(), "zzbap", "zzbaq"});
                case 4:
                    return zzbar;
                case 5:
                    zzyx<zzaj> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzaj.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbar);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzaj zzaj = new zzaj();
            zzbar = zzaj;
            zzwz.zza(zzaj.class, zzaj);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzak extends zzwz<zzak, zza> implements zzym {
        private static final zzak zzbbj;
        private static volatile zzyx<zzak> zzh;
        private zzaj zzbbf;
        private zzb zzbbg;
        private zzb zzbbh;
        private boolean zzbbi;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz<zzb, zza> implements zzym {
            private static final zzb zzbbo;
            private static volatile zzyx<zzb> zzh;
            private boolean zzbbk;
            private boolean zzbbl;
            private boolean zzbbm;
            private boolean zzbbn;
            private int zzj;

            private zzb() {
            }

            /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
            public static final class zza extends zzwz.zzb<zzb, zza> implements zzym {
                private zza() {
                    super(zzb.zzbbo);
                }

                /* synthetic */ zza(zznr zznr) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.firebase_ml.zzwz
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zznr.zzi[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzbbo, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဇ\u0000\u0002ဇ\u0001\u0003ဇ\u0002\u0004ဇ\u0003", new Object[]{"zzj", "zzbbk", "zzbbl", "zzbbm", "zzbbn"});
                    case 4:
                        return zzbbo;
                    case 5:
                        zzyx<zzb> zzyx = zzh;
                        if (zzyx == null) {
                            synchronized (zzb.class) {
                                zzyx = zzh;
                                if (zzyx == null) {
                                    zzyx = new zzwz.zza<>(zzbbo);
                                    zzh = zzyx;
                                }
                            }
                        }
                        return zzyx;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzb zzb = new zzb();
                zzbbo = zzb;
                zzwz.zza(zzb.class, zzb);
            }
        }

        private zzak() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzak, zza> implements zzym {
            private zza() {
                super(zzak.zzbbj);
            }

            public final zza zza(zzaj.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzak) this.zzclo).zza((zzak) ((zzaj) ((zzwz) zza.zzvb())));
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaj zzaj) {
            zzaj.getClass();
            this.zzbbf = zzaj;
            this.zzj |= 1;
        }

        public static zza zzmn() {
            return (zza) zzbbj.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzak();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbbj, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဇ\u0003", new Object[]{"zzj", "zzbbf", "zzbbg", "zzbbh", "zzbbi"});
                case 4:
                    return zzbbj;
                case 5:
                    zzyx<zzak> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzak.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbbj);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public static zzak zzmo() {
            return zzbbj;
        }

        static {
            zzak zzak = new zzak();
            zzbbj = zzak;
            zzwz.zza(zzak.class, zzak);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzal extends zzwz<zzal, zza> implements zzym {
        private static final zzal zzbbt;
        private static volatile zzyx<zzal> zzh;
        private int zzbbp;
        private float zzbbq;
        private int zzbbr;
        private int zzbbs;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            CATEGORY_UNKNOWN(0),
            CATEGORY_HOME_GOOD(1),
            CATEGORY_FASHION_GOOD(2),
            CATEGORY_ANIMAL(3),
            CATEGORY_FOOD(4),
            CATEGORY_PLACE(5),
            CATEGORY_PLANT(6);
            
            private static final zzxf<zzb> zzac = new zzox();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzbj(int i) {
                switch (i) {
                    case 0:
                        return CATEGORY_UNKNOWN;
                    case 1:
                        return CATEGORY_HOME_GOOD;
                    case 2:
                        return CATEGORY_FASHION_GOOD;
                    case 3:
                        return CATEGORY_ANIMAL;
                    case 4:
                        return CATEGORY_FOOD;
                    case 5:
                        return CATEGORY_PLACE;
                    case 6:
                        return CATEGORY_PLANT;
                    default:
                        return null;
                }
            }

            public static zzxe zzf() {
                return zzoy.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzal() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzal, zza> implements zzym {
            private zza() {
                super(zzal.zzbbt);
            }

            public final zza zza(zzb zzb) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzal) this.zzclo).zzb((zzal) zzb);
                return this;
            }

            public final zza zzm(float f) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzal) this.zzclo).zzn(f);
                return this;
            }

            public final zza zzbh(int i) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzal) this.zzclo).zzbi(i);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzb zzb2) {
            this.zzbbp = zzb2.zzd();
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzn(float f) {
            this.zzj |= 2;
            this.zzbbq = f;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbi(int i) {
            this.zzj |= 4;
            this.zzbbr = i;
        }

        public static zza zzmr() {
            return (zza) zzbbt.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzal();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbbt, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဌ\u0000\u0002ခ\u0001\u0003င\u0002\u0004ဋ\u0003", new Object[]{"zzj", "zzbbp", zzb.zzf(), "zzbbq", "zzbbr", "zzbbs"});
                case 4:
                    return zzbbt;
                case 5:
                    zzyx<zzal> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzal.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbbt);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzal zzal = new zzal();
            zzbbt = zzal;
            zzwz.zza(zzal.class, zzal);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzam extends zzwz<zzam, zzc> implements zzym {
        private static final zzxj<Integer, zza> zzann = new zzpa();
        private static final zzxj<Integer, zzb> zzanp = new zzoz();
        private static final zzam zzbcc;
        private static volatile zzyx<zzam> zzh;
        private zzae zzana;
        private zztg.zza zzanl;
        private zzxg zzanm = zzup();
        private zzxg zzano = zzup();
        private zzaf zzaoq;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            FORMAT_UNKNOWN(0),
            FORMAT_CODE_128(1),
            FORMAT_CODE_39(2),
            FORMAT_CODE_93(4),
            FORMAT_CODABAR(8),
            FORMAT_DATA_MATRIX(16),
            FORMAT_EAN_13(32),
            FORMAT_EAN_8(64),
            FORMAT_ITF(128),
            FORMAT_QR_CODE(256),
            FORMAT_UPC_A(512),
            FORMAT_UPC_E(1024),
            FORMAT_PDF417(2048),
            FORMAT_AZTEC(4096);
            
            private static final zzxf<zza> zzac = new zzpb();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzbk(int i) {
                if (i == 0) {
                    return FORMAT_UNKNOWN;
                }
                if (i == 1) {
                    return FORMAT_CODE_128;
                }
                if (i == 2) {
                    return FORMAT_CODE_39;
                }
                switch (i) {
                    case 4:
                        return FORMAT_CODE_93;
                    case 8:
                        return FORMAT_CODABAR;
                    case 16:
                        return FORMAT_DATA_MATRIX;
                    case 32:
                        return FORMAT_EAN_13;
                    case 64:
                        return FORMAT_EAN_8;
                    case 128:
                        return FORMAT_ITF;
                    case 256:
                        return FORMAT_QR_CODE;
                    case 512:
                        return FORMAT_UPC_A;
                    case 1024:
                        return FORMAT_UPC_E;
                    case 2048:
                        return FORMAT_PDF417;
                    case 4096:
                        return FORMAT_AZTEC;
                    default:
                        return null;
                }
            }

            public static zzxe zzf() {
                return zzpc.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zzb implements zzxc {
            TYPE_UNKNOWN(0),
            TYPE_CONTACT_INFO(1),
            TYPE_EMAIL(2),
            TYPE_ISBN(3),
            TYPE_PHONE(4),
            TYPE_PRODUCT(5),
            TYPE_SMS(6),
            TYPE_TEXT(7),
            TYPE_URL(8),
            TYPE_WIFI(9),
            TYPE_GEO(10),
            TYPE_CALENDAR_EVENT(11),
            TYPE_DRIVER_LICENSE(12);
            
            private static final zzxf<zzb> zzac = new zzpe();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zzb zzbl(int i) {
                switch (i) {
                    case 0:
                        return TYPE_UNKNOWN;
                    case 1:
                        return TYPE_CONTACT_INFO;
                    case 2:
                        return TYPE_EMAIL;
                    case 3:
                        return TYPE_ISBN;
                    case 4:
                        return TYPE_PHONE;
                    case 5:
                        return TYPE_PRODUCT;
                    case 6:
                        return TYPE_SMS;
                    case 7:
                        return TYPE_TEXT;
                    case 8:
                        return TYPE_URL;
                    case 9:
                        return TYPE_WIFI;
                    case 10:
                        return TYPE_GEO;
                    case 11:
                        return TYPE_CALENDAR_EVENT;
                    case 12:
                        return TYPE_DRIVER_LICENSE;
                    default:
                        return null;
                }
            }

            public static zzxe zzf() {
                return zzpd.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        private zzam() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzc extends zzwz.zzb<zzam, zzc> implements zzym {
            private zzc() {
                super(zzam.zzbcc);
            }

            public final zzc zzc(zzaf.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzam) this.zzclo).zza((zzam) ((zzaf) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zzc zzc(zztg.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzam) this.zzclo).zza((zzam) zza);
                return this;
            }

            public final zzc zzs(Iterable<? extends zza> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzam) this.zzclo).zzh((zzam) iterable);
                return this;
            }

            public final zzc zzt(Iterable<? extends zzb> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzam) this.zzclo).zzi(iterable);
                return this;
            }

            public final zzc zzi(zzae zzae) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzam) this.zzclo).zzb((zzam) zzae);
                return this;
            }

            /* synthetic */ zzc(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaf zzaf) {
            zzaf.getClass();
            this.zzaoq = zzaf;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zztg.zza zza2) {
            zza2.getClass();
            this.zzanl = zza2;
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzh(Iterable<? extends zza> iterable) {
            if (!this.zzanm.zztl()) {
                this.zzanm = zzwz.zza(this.zzanm);
            }
            for (zza zza2 : iterable) {
                this.zzanm.zzds(zza2.zzd());
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzi(Iterable<? extends zzb> iterable) {
            if (!this.zzano.zztl()) {
                this.zzano = zzwz.zza(this.zzano);
            }
            for (zzb zzb2 : iterable) {
                this.zzano.zzds(zzb2.zzd());
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzae zzae) {
            zzae.getClass();
            this.zzana = zzae;
            this.zzj |= 4;
        }

        public static zzc zzmt() {
            return (zzc) zzbcc.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzam();
                case 2:
                    return new zzc(null);
                case 3:
                    return zza(zzbcc, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001e\u0004\u001e\u0005ဉ\u0002", new Object[]{"zzj", "zzaoq", "zzanl", "zzanm", zza.zzf(), "zzano", zzb.zzf(), "zzana"});
                case 4:
                    return zzbcc;
                case 5:
                    zzyx<zzam> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzam.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbcc);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzam zzam = new zzam();
            zzbcc = zzam;
            zzwz.zza(zzam.class, zzam);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzan extends zzwz<zzan, zza> implements zzym {
        private static final zzan zzbdh;
        private static volatile zzyx<zzan> zzh;
        private zzae zzana;
        private zzac zzant;
        private int zzanu;
        private int zzanv;
        private zzaf zzaoq;
        private zztg.zzb zzbdg;
        private int zzj;

        private zzan() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzan, zza> implements zzym {
            private zza() {
                super(zzan.zzbdh);
            }

            public final zza zzd(zzaf.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzan) this.zzclo).zza((zzan) ((zzaf) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zzj(zzae zzae) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzan) this.zzclo).zzb((zzan) zzae);
                return this;
            }

            public final zza zzc(zzac zzac) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzan) this.zzclo).zzb((zzan) zzac);
                return this;
            }

            public final zza zzbm(int i) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzan) this.zzclo).zzar(i);
                return this;
            }

            public final zza zzbn(int i) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzan) this.zzclo).zzas(i);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaf zzaf) {
            zzaf.getClass();
            this.zzaoq = zzaf;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzae zzae) {
            zzae.getClass();
            this.zzana = zzae;
            this.zzj |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzac zzac) {
            zzac.getClass();
            this.zzant = zzac;
            this.zzj |= 8;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzar(int i) {
            this.zzj |= 16;
            this.zzanu = i;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzas(int i) {
            this.zzj |= 32;
            this.zzanv = i;
        }

        public static zza zzmv() {
            return (zza) zzbdh.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzan();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbdh, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဉ\u0003\u0005ဋ\u0004\u0006ဋ\u0005", new Object[]{"zzj", "zzaoq", "zzbdg", "zzana", "zzant", "zzanu", "zzanv"});
                case 4:
                    return zzbdh;
                case 5:
                    zzyx<zzan> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzan.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbdh);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzan zzan = new zzan();
            zzbdh = zzan;
            zzwz.zza(zzan.class, zzan);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzao extends zzwz<zzao, zza> implements zzym {
        private static final zzao zzbdk;
        private static volatile zzyx<zzao> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private int zzbbs;
        private zzap zzbdi;
        private float zzbdj;
        private int zzj;

        private zzao() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzao, zza> implements zzym {
            private zza() {
                super(zzao.zzbdk);
            }

            public final zza zze(zzaf.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzao) this.zzclo).zza((zzao) ((zzaf) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zzd(zzap zzap) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzao) this.zzclo).zzc(zzap);
                return this;
            }

            public final zza zzk(zzae zzae) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzao) this.zzclo).zzb((zzao) zzae);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaf zzaf) {
            zzaf.getClass();
            this.zzaoq = zzaf;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzc(zzap zzap) {
            zzap.getClass();
            this.zzbdi = zzap;
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzae zzae) {
            zzae.getClass();
            this.zzana = zzae;
            this.zzj |= 4;
        }

        public static zza zzmx() {
            return (zza) zzbdk.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzao();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbdk, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဋ\u0003\u0005ခ\u0004", new Object[]{"zzj", "zzaoq", "zzbdi", "zzana", "zzbbs", "zzbdj"});
                case 4:
                    return zzbdk;
                case 5:
                    zzyx<zzao> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzao.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbdk);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzao zzao = new zzao();
            zzbdk = zzao;
            zzwz.zza(zzao.class, zzao);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzap extends zzwz<zzap, zza> implements zzym {
        private static final zzap zzbdn;
        private static volatile zzyx<zzap> zzh;
        private float zzat;
        private int zzbdl;
        private zzak zzbdm;
        private int zzj;

        private zzap() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzap, zza> implements zzym {
            private zza() {
                super(zzap.zzbdn);
            }

            public final zza zzp(float f) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzap) this.zzclo).zzo(f);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzo(float f) {
            this.zzj |= 2;
            this.zzat = f;
        }

        public static zza zzmz() {
            return (zza) zzbdn.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzap();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbdn, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဋ\u0000\u0002ခ\u0001\u0003ဉ\u0002", new Object[]{"zzj", "zzbdl", "zzat", "zzbdm"});
                case 4:
                    return zzbdn;
                case 5:
                    zzyx<zzap> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzap.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbdn);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzap zzap = new zzap();
            zzbdn = zzap;
            zzwz.zza(zzap.class, zzap);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzar extends zzwz<zzar, zza> implements zzym {
        private static final zzar zzbdy;
        private static volatile zzyx<zzar> zzh;
        private int zzamy;
        private zzas zzaod;
        private int zzj;

        private zzar() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzar, zza> implements zzym {
            private zza() {
                super(zzar.zzbdy);
            }

            public final zza zzc(zzas zzas) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzar) this.zzclo).zzb((zzar) zzas);
                return this;
            }

            public final zza zzm(zzoc zzoc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzar) this.zzclo).zzb((zzar) zzoc);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzas zzas) {
            zzas.getClass();
            this.zzaod = zzas;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzoc zzoc) {
            this.zzamy = zzoc.zzd();
            this.zzj |= 2;
        }

        public static zza zznf() {
            return (zza) zzbdy.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzar();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbdy, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဌ\u0001", new Object[]{"zzj", "zzaod", "zzamy", zzoc.zzf()});
                case 4:
                    return zzbdy;
                case 5:
                    zzyx<zzar> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzar.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbdy);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzar zzar = new zzar();
            zzbdy = zzar;
            zzwz.zza(zzar.class, zzar);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzas extends zzwz<zzas, zzb> implements zzym {
        private static final zzas zzbee;
        private static volatile zzyx<zzas> zzh;
        private zzak zzbdm;
        private int zzbdz;
        private boolean zzbea;
        private boolean zzbeb;
        private int zzbec;
        private float zzbed;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public enum zza implements zzxc {
            MODE_UNSPECIFIED(0),
            STREAM(1),
            SINGLE_IMAGE(2);
            
            private static final zzxf<zza> zzac = new zzpg();
            private final int value;

            @Override // com.google.android.gms.internal.firebase_ml.zzxc
            public final int zzd() {
                return this.value;
            }

            public static zza zzbo(int i) {
                if (i == 0) {
                    return MODE_UNSPECIFIED;
                }
                if (i == 1) {
                    return STREAM;
                }
                if (i != 2) {
                    return null;
                }
                return SINGLE_IMAGE;
            }

            public static zzxe zzf() {
                return zzpf.zzan;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + Typography.greater;
            }

            private zza(int i) {
                this.value = i;
            }
        }

        private zzas() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zzb extends zzwz.zzb<zzas, zzb> implements zzym {
            private zzb() {
                super(zzas.zzbee);
            }

            public final zzb zzb(zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzas) this.zzclo).zza((zzas) zza);
                return this;
            }

            public final zzb zzao(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzas) this.zzclo).zzam(z);
                return this;
            }

            public final zzb zzap(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzas) this.zzclo).zzan(z);
                return this;
            }

            /* synthetic */ zzb(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zza zza2) {
            this.zzbdz = zza2.zzd();
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzam(boolean z) {
            this.zzj |= 2;
            this.zzbea = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzan(boolean z) {
            this.zzj |= 4;
            this.zzbeb = z;
        }

        public static zzb zznh() {
            return (zzb) zzbee.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzas();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzbee, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003ဇ\u0002\u0004ဋ\u0003\u0005ခ\u0004\u0006ဉ\u0005", new Object[]{"zzj", "zzbdz", zza.zzf(), "zzbea", "zzbeb", "zzbec", "zzbed", "zzbdm"});
                case 4:
                    return zzbee;
                case 5:
                    zzyx<zzas> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzas.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbee);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzas zzas = new zzas();
            zzbee = zzas;
            zzwz.zza(zzas.class, zzas);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzat extends zzwz<zzat, zza> implements zzym {
        private static final zzat zzbek;
        private static volatile zzyx<zzat> zzh;
        private zzae zzana;
        private zzas zzaod;
        private zzaf zzaoq;
        private zzxl<zzal> zzbej = zzus();
        private int zzj;

        private zzat() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzat, zza> implements zzym {
            private zza() {
                super(zzat.zzbek);
            }

            public final zza zzf(zzaf.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzat) this.zzclo).zza((zzat) ((zzaf) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zzl(zzae zzae) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzat) this.zzclo).zzb((zzat) zzae);
                return this;
            }

            public final zza zzd(zzas zzas) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzat) this.zzclo).zzb((zzat) zzas);
                return this;
            }

            public final zza zzu(Iterable<? extends zzal> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzat) this.zzclo).zzv(iterable);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaf zzaf) {
            zzaf.getClass();
            this.zzaoq = zzaf;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzae zzae) {
            zzae.getClass();
            this.zzana = zzae;
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzas zzas) {
            zzas.getClass();
            this.zzaod = zzas;
            this.zzj |= 4;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzv(Iterable<? extends zzal> iterable) {
            zzxl<zzal> zzxl = this.zzbej;
            if (!zzxl.zztl()) {
                this.zzbej = zzwz.zza(zzxl);
            }
            zzvl.zza(iterable, this.zzbej);
        }

        public static zza zznj() {
            return (zza) zzbek.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzat();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbek, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004\u001b", new Object[]{"zzj", "zzaoq", "zzana", "zzaod", "zzbej", zzal.class});
                case 4:
                    return zzbek;
                case 5:
                    zzyx<zzat> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzat.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbek);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzat zzat = new zzat();
            zzbek = zzat;
            zzwz.zza(zzat.class, zzat);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzau extends zzwz<zzau, zza> implements zzym {
        private static final zzau zzben;
        private static volatile zzyx<zzau> zzh;
        private int zzamy;
        private zzas zzaod;
        private long zzbel;
        private long zzbem;
        private int zzj;

        private zzau() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzau, zza> implements zzym {
            private zza() {
                super(zzau.zzben);
            }

            public final zza zze(zzas zzas) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzau) this.zzclo).zzb((zzau) zzas);
                return this;
            }

            public final zza zzn(zzoc zzoc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzau) this.zzclo).zzb((zzau) zzoc);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzas zzas) {
            zzas.getClass();
            this.zzaod = zzas;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzoc zzoc) {
            this.zzamy = zzoc.zzd();
            this.zzj |= 2;
        }

        public static zza zznl() {
            return (zza) zzben.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzau();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzben, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဌ\u0001\u0003ဃ\u0002\u0004ဃ\u0003", new Object[]{"zzj", "zzaod", "zzamy", zzoc.zzf(), "zzbel", "zzbem"});
                case 4:
                    return zzben;
                case 5:
                    zzyx<zzau> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzau.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzben);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzau zzau = new zzau();
            zzben = zzau;
            zzwz.zza(zzau.class, zzau);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzaz extends zzwz<zzaz, zza> implements zzym {
        private static final zzaz zzbfj;
        private static volatile zzyx<zzaz> zzh;
        private zzae zzana;
        private zzaf zzaoq;
        private int zzj;

        private zzaz() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzaz, zza> implements zzym {
            private zza() {
                super(zzaz.zzbfj);
            }

            public final zza zzg(zzaf.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaz) this.zzclo).zza((zzaz) ((zzaf) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zzm(zzae zzae) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzaz) this.zzclo).zzb((zzaz) zzae);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaf zzaf) {
            zzaf.getClass();
            this.zzaoq = zzaf;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzae zzae) {
            zzae.getClass();
            this.zzana = zzae;
            this.zzj |= 2;
        }

        public static zza zzns() {
            return (zza) zzbfj.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzaz();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbfj, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001", new Object[]{"zzj", "zzaoq", "zzana"});
                case 4:
                    return zzbfj;
                case 5:
                    zzyx<zzaz> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzaz.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbfj);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public static zzaz zznt() {
            return zzbfj;
        }

        static {
            zzaz zzaz = new zzaz();
            zzbfj = zzaz;
            zzwz.zza(zzaz.class, zzaz);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzbc extends zzwz<zzbc, zza> implements zzym {
        private static final zzbc zzbih;
        private static volatile zzyx<zzbc> zzh;
        private String zzbhw = "";
        private String zzbhx = "";
        private String zzbhy = "";
        private String zzbhz = "";
        private String zzbia = "";
        private String zzbib = "";
        private String zzbic = "";
        private zzxl<String> zzbid = zzwz.zzus();
        private String zzbie = "";
        private boolean zzbif;
        private boolean zzbig;
        private int zzj;

        private zzbc() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzbc, zza> implements zzym {
            private zza() {
                super(zzbc.zzbih);
            }

            public final zza zzbp(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzbi(str);
                return this;
            }

            public final zza zzbq(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzbj(str);
                return this;
            }

            public final zza zzbr(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzbk(str);
                return this;
            }

            public final zza zzbs(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzbl(str);
                return this;
            }

            public final zza zzbt(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzbm(str);
                return this;
            }

            public final zza zzbu(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzbn(str);
                return this;
            }

            public final zza zzbv(String str) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzbo(str);
                return this;
            }

            public final zza zzx(Iterable<String> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzbc) this.zzclo).zzw(iterable);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbi(String str) {
            str.getClass();
            this.zzj |= 1;
            this.zzbhw = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbj(String str) {
            str.getClass();
            this.zzj |= 2;
            this.zzbhx = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbk(String str) {
            str.getClass();
            this.zzj |= 4;
            this.zzbhy = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbl(String str) {
            str.getClass();
            this.zzj |= 8;
            this.zzbhz = str;
        }

        public final String zznx() {
            return this.zzbia;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbm(String str) {
            str.getClass();
            this.zzj |= 16;
            this.zzbia = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbn(String str) {
            str.getClass();
            this.zzj |= 32;
            this.zzbib = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzbo(String str) {
            str.getClass();
            this.zzj |= 64;
            this.zzbic = str;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzw(Iterable<String> iterable) {
            if (!this.zzbid.zztl()) {
                this.zzbid = zzwz.zza(this.zzbid);
            }
            zzvl.zza(iterable, this.zzbid);
        }

        public static zza zzny() {
            return (zza) zzbih.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzbc();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzbih, "\u0001\u000b\u0000\u0001\u0001\u000b\u000b\u0000\u0001\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဈ\u0002\u0004ဈ\u0003\u0005ဈ\u0004\u0006ဈ\u0005\u0007ဈ\u0006\b\u001a\tဈ\u0007\nဇ\b\u000bဇ\t", new Object[]{"zzj", "zzbhw", "zzbhx", "zzbhy", "zzbhz", "zzbia", "zzbib", "zzbic", "zzbid", "zzbie", "zzbif", "zzbig"});
                case 4:
                    return zzbih;
                case 5:
                    zzyx<zzbc> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzbc.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzbih);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public static zzbc zznz() {
            return zzbih;
        }

        static {
            zzbc zzbc = new zzbc();
            zzbih = zzbc;
            zzwz.zza(zzbc.class, zzbc);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzj extends zzwz<zzj, zza> implements zzym {
        private static final zzj zzaop;
        private static volatile zzyx<zzj> zzh;
        private int zzamy;
        private int zzj;

        private zzj() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzj, zza> implements zzym {
            private zza() {
                super(zzj.zzaop);
            }

            public final zza zzi(zzoc zzoc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzj) this.zzclo).zzb((zzj) zzoc);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzoc zzoc) {
            this.zzamy = zzoc.zzd();
            this.zzj |= 1;
        }

        public static zza zzkv() {
            return (zza) zzaop.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzj();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaop, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ဌ\u0000", new Object[]{"zzj", "zzamy", zzoc.zzf()});
                case 4:
                    return zzaop;
                case 5:
                    zzyx<zzj> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzj.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaop);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzj zzj2 = new zzj();
            zzaop = zzj2;
            zzwz.zza(zzj.class, zzj2);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzk extends zzwz<zzk, zza> implements zzym {
        private static final zzk zzaos;
        private static volatile zzyx<zzk> zzh;
        private zzae zzana;
        private zzak zzanb;
        private zzaf zzaoq;
        private long zzaor;
        private float zzat;
        private int zzj;

        private zzk() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzk, zza> implements zzym {
            private zza() {
                super(zzk.zzaos);
            }

            public final zza zza(zzaf.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzk) this.zzclo).zza((zzk) ((zzaf) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzk) this.zzclo).zzb((zzk) zzak);
                return this;
            }

            public final zza zzh(zzae zzae) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzk) this.zzclo).zzb((zzk) zzae);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzaf zzaf) {
            zzaf.getClass();
            this.zzaoq = zzaf;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzak zzak) {
            zzak.getClass();
            this.zzanb = zzak;
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzae zzae) {
            zzae.getClass();
            this.zzana = zzae;
            this.zzj |= 16;
        }

        public static zza zzkx() {
            return (zza) zzaos.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzk();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaos, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဃ\u0002\u0004ခ\u0003\u0005ဉ\u0004", new Object[]{"zzj", "zzaoq", "zzanb", "zzaor", "zzat", "zzana"});
                case 4:
                    return zzaos;
                case 5:
                    zzyx<zzk> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzk.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaos);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzk zzk = new zzk();
            zzaos = zzk;
            zzwz.zza(zzk.class, zzk);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzl extends zzwz<zzl, zza> implements zzym {
        private static final zzxj<Integer, zzoc> zzaow = new zznv();
        private static final zzl zzaox;
        private static volatile zzyx<zzl> zzh;
        private long zzaor;
        private zzak zzaot;
        private zzak zzaou;
        private zzxg zzaov = zzup();
        private int zzj;

        private zzl() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzl, zza> implements zzym {
            private zza() {
                super(zzl.zzaox);
            }

            public final zza zze(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzl) this.zzclo).zzc(zzak);
                return this;
            }

            public final zza zzf(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzl) this.zzclo).zzd(zzak);
                return this;
            }

            public final zza zzm(Iterable<? extends zzoc> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzl) this.zzclo).zzl(iterable);
                return this;
            }

            public final zza zzg(long j) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzl) this.zzclo).zzf(j);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzc(zzak zzak) {
            zzak.getClass();
            this.zzaot = zzak;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzd(zzak zzak) {
            zzak.getClass();
            this.zzaou = zzak;
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzl(Iterable<? extends zzoc> iterable) {
            if (!this.zzaov.zztl()) {
                this.zzaov = zzwz.zza(this.zzaov);
            }
            for (zzoc zzoc : iterable) {
                this.zzaov.zzds(zzoc.zzd());
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzf(long j) {
            this.zzj |= 4;
            this.zzaor = j;
        }

        public static zza zzkz() {
            return (zza) zzaox.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzl();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaox, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001e\u0004ဃ\u0002", new Object[]{"zzj", "zzaot", "zzaou", "zzaov", zzoc.zzf(), "zzaor"});
                case 4:
                    return zzaox;
                case 5:
                    zzyx<zzl> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzl.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaox);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzl zzl = new zzl();
            zzaox = zzl;
            zzwz.zza(zzl.class, zzl);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzx extends zzwz<zzx, zza> implements zzym {
        private static final zzx zzapp;
        private static volatile zzyx<zzx> zzh;
        private int zzamy;
        private zzak zzanb;
        private int zzj;

        private zzx() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzx, zza> implements zzym {
            private zza() {
                super(zzx.zzapp);
            }

            public final zza zzg(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzx) this.zzclo).zzb((zzx) zzak);
                return this;
            }

            public final zza zzj(zzoc zzoc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzx) this.zzclo).zzb((zzx) zzoc);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzak zzak) {
            zzak.getClass();
            this.zzanb = zzak;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzoc zzoc) {
            this.zzamy = zzoc.zzd();
            this.zzj |= 2;
        }

        public static zza zzlm() {
            return (zza) zzapp.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzx();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzapp, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဌ\u0001", new Object[]{"zzj", "zzanb", "zzamy", zzoc.zzf()});
                case 4:
                    return zzapp;
                case 5:
                    zzyx<zzx> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzx.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzapp);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzx zzx = new zzx();
            zzapp = zzx;
            zzwz.zza(zzx.class, zzx);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzz extends zzwz<zzz, zza> implements zzym {
        private static final zzxj<Integer, zzoc> zzaow = new zzoa();
        private static final zzz zzaqb;
        private static volatile zzyx<zzz> zzh;
        private long zzaor;
        private zzak zzaot;
        private zzak zzaou;
        private zzxg zzaov = zzup();
        private boolean zzaqa;
        private int zzj;

        private zzz() {
        }

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzb<zzz, zza> implements zzym {
            private zza() {
                super(zzz.zzaqb);
            }

            public final zza zzi(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzz) this.zzclo).zzc(zzak);
                return this;
            }

            public final zza zzj(zzak zzak) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzz) this.zzclo).zzd(zzak);
                return this;
            }

            public final zza zzr(Iterable<? extends zzoc> iterable) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzz) this.zzclo).zzl(iterable);
                return this;
            }

            public final zza zzi(long j) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzz) this.zzclo).zzf(j);
                return this;
            }

            public final zza zzx(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzz) this.zzclo).zzw(z);
                return this;
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzc(zzak zzak) {
            zzak.getClass();
            this.zzaot = zzak;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzd(zzak zzak) {
            zzak.getClass();
            this.zzaou = zzak;
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzl(Iterable<? extends zzoc> iterable) {
            if (!this.zzaov.zztl()) {
                this.zzaov = zzwz.zza(this.zzaov);
            }
            for (zzoc zzoc : iterable) {
                this.zzaov.zzds(zzoc.zzd());
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzf(long j) {
            this.zzj |= 4;
            this.zzaor = j;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzw(boolean z) {
            this.zzj |= 8;
            this.zzaqa = z;
        }

        public static zza zzls() {
            return (zza) zzaqb.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzz();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzaqb, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001e\u0004ဃ\u0002\u0005ဇ\u0003", new Object[]{"zzj", "zzaot", "zzaou", "zzaov", zzoc.zzf(), "zzaor", "zzaqa"});
                case 4:
                    return zzaqb;
                case 5:
                    zzyx<zzz> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzz.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzaqb);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzz zzz = new zzz();
            zzaqb = zzz;
            zzwz.zza(zzz.class, zzz);
        }
    }

    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public static final class zzad extends zzwz.zzc<zzad, zza> implements zzym {
        private static final zzad zzayu;
        private static volatile zzyx<zzad> zzh;
        private int zzaxa;
        private zzbc zzaxb;
        private int zzaxc;
        private boolean zzaxd;
        private zzai zzaxe;
        private zzz zzaxf;
        private zzy zzaxg;
        private zzx zzaxh;
        private zzan zzaxi;
        private zzaz zzaxj;
        private zzam zzaxk;
        private zzao zzaxl;
        private zzar zzaxm;
        private zzau zzaxn;
        private zzat zzaxo;
        private zzav zzaxp;
        private zzax zzaxq;
        private zzay zzaxr;
        private zzaq zzaxs;
        private zzba zzaxt;
        private zzp zzaxu;
        private zzm zzaxv;
        private zzo zzaxw;
        private zzr zzaxx;
        private zzq zzaxy;
        private zzs zzaxz;
        private zzt zzaya;
        private zzu zzayb;
        private zzv zzayc;
        private zzw zzayd;
        private zzj zzaye;
        private zzl zzayf;
        private zzk zzayg;
        private zzag zzayh;
        private zzaa zzayi;
        private zza zzayj;
        private zzb zzayk;
        private zzd zzayl;
        private zzc zzaym;
        private zze zzayn;
        private zzf zzayo;
        private zzi zzayp;
        private zzg zzayq;
        private zzh zzayr;
        private zzxl<zzabh.zze> zzays = zzus();
        private zzbb zzayt;
        private byte zzch = 2;
        private int zzj;

        /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
        public static final class zza extends zzwz.zzd<zzad, zza> implements zzym {
            private zza() {
                super(zzad.zzayu);
            }

            /* synthetic */ zza(zznr zznr) {
                this();
            }

            public final zzbc zzlz() {
                return ((zzad) this.zzclo).zzlz();
            }

            public final zza zza(zzbc.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzbc) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzod zzod) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzod);
                return this;
            }

            public final zza zzac(boolean z) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzad(z);
                return this;
            }

            public final zza zza(zzai.zzb zzb) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzai) ((zzwz) zzb.zzvb())));
                return this;
            }

            public final zza zza(zzz.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzz) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzy zzy) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzy);
                return this;
            }

            public final zza zza(zzx zzx) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzx);
                return this;
            }

            public final zza zza(zzan zzan) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzan);
                return this;
            }

            public final zza zza(zzaz zzaz) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzaz);
                return this;
            }

            public final zza zza(zzam zzam) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzam);
                return this;
            }

            public final zza zza(zzam.zzc zzc) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) ((zzam) ((zzwz) zzc.zzvb())));
                return this;
            }

            public final zza zza(zzao zzao) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzao);
                return this;
            }

            public final zza zza(zzar.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzar) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzau.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzau) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzat.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzat) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzj zzj) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzj);
                return this;
            }

            public final zza zza(zzl.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzl) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzk.zza zza) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zza((zzad) ((zzk) ((zzwz) zza.zzvb())));
                return this;
            }

            public final zza zza(zzag zzag) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzag);
                return this;
            }

            public final zza zza(zzaa zzaa) {
                if (this.zzclp) {
                    zzux();
                    this.zzclp = false;
                }
                ((zzad) this.zzclo).zzb((zzad) zzaa);
                return this;
            }
        }

        static {
            zzad zzad = new zzad();
            zzayu = zzad;
            zzwz.zza(zzad.class, zzad);
        }

        private zzad() {
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzk zzk) {
            zzk.getClass();
            this.zzayg = zzk;
            this.zzj |= Integer.MIN_VALUE;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzl zzl) {
            zzl.getClass();
            this.zzayf = zzl;
            this.zzj |= 1073741824;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzaa zzaa) {
            zzaa.getClass();
            this.zzayi = zzaa;
            this.zzaxa |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzag zzag) {
            zzag.getClass();
            this.zzayh = zzag;
            this.zzaxa |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzj zzj2) {
            zzj2.getClass();
            this.zzaye = zzj2;
            this.zzj |= 536870912;
        }

        public static zza zzma() {
            return (zza) zzayu.zzun();
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.firebase_ml.zzwz
        public final Object zza(int i, Object obj, Object obj2) {
            int i2 = 1;
            switch (zznr.zzi[i - 1]) {
                case 1:
                    return new zzad();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzayu, "\u0001-\u0000\u0002\u0001/-\u0000\u0001\u0001\u0001ဉ\u0000\u0002ဌ\u0001\u0003ဉ\u0003\u0004ဉ\u0005\u0005ဉ\u0007\u0006ဉ\b\u0007ဉ\t\bဉ\u0013\tဉ\u0014\nဉ\u0015\u000bဉ\u0016\fဉ\u0017\rဉ\u0018\u000eဉ\u0019\u000fဉ\u001a\u0010ဉ\u001b\u0011ဉ\u001c\u0012ဉ\n\u0013ဉ\u0010\u0014ဉ\u0004\u0015ဉ\u0011\u0016ဉ\u0012\u0017ဉ\u001d\u0018ဉ\u001e\u0019ဉ\u001f\u001aဉ\u000b\u001bဉ\f\u001cဉ\r\u001dဉ\u0006\u001eဉ\"\u001fဉ# ဉ$!ဉ%\"ဉ&#ဉ'$ဉ(%ဇ\u0002'ဉ (ဉ!)Л*ဉ+,ဉ\u000e-ဉ\u000f.ဉ)/ဉ*", new Object[]{"zzj", "zzaxa", "zzaxb", "zzaxc", zzod.zzf(), "zzaxe", "zzaxg", "zzaxi", "zzaxj", "zzaxk", "zzaxu", "zzaxv", "zzaxw", "zzaxx", "zzaxy", "zzaxz", "zzaya", "zzayb", "zzayc", "zzayd", "zzaxl", "zzaxr", "zzaxf", "zzaxs", "zzaxt", "zzaye", "zzayf", "zzayg", "zzaxm", "zzaxn", "zzaxo", "zzaxh", "zzayj", "zzayk", "zzayl", "zzaym", "zzayn", "zzayo", "zzayp", "zzaxd", "zzayh", "zzayi", "zzays", zzabh.zze.class, "zzayt", "zzaxp", "zzaxq", "zzayq", "zzayr"});
                case 4:
                    return zzayu;
                case 5:
                    zzyx<zzad> zzyx = zzh;
                    if (zzyx == null) {
                        synchronized (zzad.class) {
                            zzyx = zzh;
                            if (zzyx == null) {
                                zzyx = new zzwz.zza<>(zzayu);
                                zzh = zzyx;
                            }
                        }
                    }
                    return zzyx;
                case 6:
                    return Byte.valueOf(this.zzch);
                case 7:
                    if (obj == null) {
                        i2 = 0;
                    }
                    this.zzch = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final zzbc zzlz() {
            zzbc zzbc = this.zzaxb;
            return zzbc == null ? zzbc.zznz() : zzbc;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzbc zzbc) {
            zzbc.getClass();
            this.zzaxb = zzbc;
            this.zzj |= 1;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzod zzod) {
            this.zzaxc = zzod.zzd();
            this.zzj |= 2;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzad(boolean z) {
            this.zzj |= 4;
            this.zzaxd = z;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzai zzai) {
            zzai.getClass();
            this.zzaxe = zzai;
            this.zzj |= 8;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzz zzz) {
            zzz.getClass();
            this.zzaxf = zzz;
            this.zzj |= 16;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzy zzy) {
            zzy.getClass();
            this.zzaxg = zzy;
            this.zzj |= 32;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzx zzx) {
            zzx.getClass();
            this.zzaxh = zzx;
            this.zzj |= 64;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzan zzan) {
            zzan.getClass();
            this.zzaxi = zzan;
            this.zzj |= 128;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzaz zzaz) {
            zzaz.getClass();
            this.zzaxj = zzaz;
            this.zzj |= 256;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzam zzam) {
            zzam.getClass();
            this.zzaxk = zzam;
            this.zzj |= 512;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zzb(zzao zzao) {
            zzao.getClass();
            this.zzaxl = zzao;
            this.zzj |= 1024;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzar zzar) {
            zzar.getClass();
            this.zzaxm = zzar;
            this.zzj |= 2048;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzau zzau) {
            zzau.getClass();
            this.zzaxn = zzau;
            this.zzj |= 4096;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private final void zza(zzat zzat) {
            zzat.getClass();
            this.zzaxo = zzat;
            this.zzj |= 8192;
        }
    }
}
