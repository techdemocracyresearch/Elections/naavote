package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzke {
    final /* synthetic */ zzkc zzait;

    public zzke(zzkc zzkc) {
        this.zzait = zzkc;
    }

    public final zzkh zza(zzkk zzkk) throws IOException {
        zzkh zzkh = new zzkh(this, zzkk);
        this.zzait.zza(zzkh);
        return zzkh;
    }
}
