package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class zzgn extends zzgk {
    public zzgn() {
    }

    /* access modifiers changed from: protected */
    public void zza(zzgo<?> zzgo) throws IOException {
    }

    public zzgn(String str) {
        super(str);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzgk, com.google.android.gms.internal.firebase_ml.zzgj
    public final void zza(zzgg<?> zzgg) throws IOException {
        super.zza(zzgg);
        zza((zzgo) zzgg);
    }
}
