package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzsg implements zzqo {
    private final long zzbok;
    private final zzoc zzbol;
    private final zzsf zzbqr;
    private final zzsh zzbte;
    private final int zzbtf;
    private final int zzbtg;

    zzsg(zzsh zzsh, long j, zzoc zzoc, int i, int i2, zzsf zzsf) {
        this.zzbte = zzsh;
        this.zzbok = j;
        this.zzbol = zzoc;
        this.zzbtf = i;
        this.zzbtg = i2;
        this.zzbqr = zzsf;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqo
    public final zzns.zzad.zza zzok() {
        return this.zzbte.zza(this.zzbok, this.zzbol, this.zzbtf, this.zzbtg, this.zzbqr);
    }
}
