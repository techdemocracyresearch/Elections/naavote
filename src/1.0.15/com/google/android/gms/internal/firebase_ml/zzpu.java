package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzpy;
import com.google.firebase.ml.common.FirebaseMLException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzpu<T, S extends zzpy> {
    T zza(S s) throws FirebaseMLException;

    zzqp zzoc();
}
