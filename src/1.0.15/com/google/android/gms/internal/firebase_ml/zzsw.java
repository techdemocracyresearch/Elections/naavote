package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzsw implements zzqo {
    private final long zzbvc;
    private final zzoc zzbvd;
    private final zzsf zzbve;

    zzsw(long j, zzoc zzoc, zzsf zzsf) {
        this.zzbvc = j;
        this.zzbvd = zzoc;
        this.zzbve = zzsf;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqo
    public final zzns.zzad.zza zzok() {
        long j = this.zzbvc;
        zzoc zzoc = this.zzbvd;
        zzsf zzsf = this.zzbve;
        return zzns.zzad.zzma().zza((zzns.zzaz) ((zzwz) zzns.zzaz.zzns().zzg(zzns.zzaf.zzme().zzk(j).zzk(zzoc).zzah(zzst.zzbqs).zzai(true).zzaj(true)).zzm(zzsa.zzc(zzsf)).zzvb()));
    }
}
