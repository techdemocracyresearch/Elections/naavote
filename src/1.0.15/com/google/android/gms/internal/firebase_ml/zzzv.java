package com.google.android.gms.internal.firebase_ml;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzv implements zzzu {
    private final /* synthetic */ zzvv zzcpz;

    zzzv(zzvv zzvv) {
        this.zzcpz = zzvv;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzzu
    public final int size() {
        return this.zzcpz.size();
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzzu
    public final byte zzcw(int i) {
        return this.zzcpz.zzcw(i);
    }
}
