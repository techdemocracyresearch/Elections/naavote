package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzyk extends zzym {
    void zzb(zzwi zzwi) throws IOException;

    zzvv zztg();

    int zzuo();

    zzyn zzut();

    zzyn zzuu();
}
