package com.google.android.gms.internal.firebase_ml;

import android.content.Context;
import com.google.firebase.FirebaseApp;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final class zzrv extends zzkj {
    private final /* synthetic */ FirebaseApp zzbrn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzrv(zzrt zzrt, String str, FirebaseApp firebaseApp) {
        super(str);
        this.zzbrn = firebaseApp;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzkj
    public final void zza(zzkg<?> zzkg) throws IOException {
        super.zza(zzkg);
        Context applicationContext = this.zzbrn.getApplicationContext();
        String packageName = applicationContext.getPackageName();
        zzkg.zzfl().put("X-Android-Package", packageName);
        zzkg.zzfl().put("X-Android-Cert", zzrt.zzc(applicationContext, packageName));
    }
}
