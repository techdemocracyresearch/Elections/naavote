package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
final /* synthetic */ class zzsn implements zzqo {
    private final long zzbok;
    private final zzoc zzbol;
    private final zzsk zzbtx;
    private final zzsf zzbty;

    zzsn(zzsk zzsk, long j, zzoc zzoc, zzsf zzsf) {
        this.zzbtx = zzsk;
        this.zzbok = j;
        this.zzbol = zzoc;
        this.zzbty = zzsf;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzqo
    public final zzns.zzad.zza zzok() {
        return this.zzbtx.zza(this.zzbok, this.zzbol, this.zzbty);
    }
}
