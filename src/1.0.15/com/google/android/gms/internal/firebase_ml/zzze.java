package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
interface zzze<T> {
    boolean equals(T t, T t2);

    int hashCode(T t);

    T newInstance();

    void zza(T t, zzaat zzaat) throws IOException;

    void zza(T t, byte[] bArr, int i, int i2, zzvq zzvq) throws IOException;

    int zzaa(T t);

    boolean zzac(T t);

    void zze(T t, T t2);

    void zzq(T t);
}
