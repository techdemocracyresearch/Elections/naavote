package com.google.android.gms.internal.firebase_ml;

import java.util.concurrent.Callable;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final /* synthetic */ class zzqi implements Callable {
    private final zzqu zzbjw;

    private zzqi(zzqu zzqu) {
        this.zzbjw = zzqu;
    }

    static Callable zza(zzqu zzqu) {
        return new zzqi(zzqu);
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        return this.zzbjw.zzos();
    }
}
