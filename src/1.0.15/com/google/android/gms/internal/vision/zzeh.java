package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzea;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzeh implements zzgv<zzea.zzg.zza> {
    zzeh() {
    }

    /* Return type fixed from 'com.google.android.gms.internal.vision.zzgw' to match base method */
    @Override // com.google.android.gms.internal.vision.zzgv
    public final /* synthetic */ zzea.zzg.zza zzh(int i) {
        return zzea.zzg.zza.zzv(i);
    }
}
