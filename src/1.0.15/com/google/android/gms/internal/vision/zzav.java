package com.google.android.gms.internal.vision;

import android.content.Context;
import android.database.ContentObserver;
import android.util.Log;
import androidx.core.content.PermissionChecker;
import javax.annotation.Nullable;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzav implements zzau {
    private static zzav zzfs;
    @Nullable
    private final Context zze;
    @Nullable
    private final ContentObserver zzfm;

    static zzav zze(Context context) {
        zzav zzav;
        synchronized (zzav.class) {
            if (zzfs == null) {
                zzfs = PermissionChecker.checkSelfPermission(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new zzav(context) : new zzav();
            }
            zzav = zzfs;
        }
        return zzav;
    }

    private zzav(Context context) {
        this.zze = context;
        zzax zzax = new zzax(this, null);
        this.zzfm = zzax;
        context.getContentResolver().registerContentObserver(zzal.CONTENT_URI, true, zzax);
    }

    private zzav() {
        this.zze = null;
        this.zzfm = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzc */
    public final String zzb(String str) {
        if (this.zze == null) {
            return null;
        }
        try {
            return (String) zzat.zza(new zzay(this, str));
        } catch (IllegalStateException | SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    static synchronized void zzaa() {
        Context context;
        synchronized (zzav.class) {
            zzav zzav = zzfs;
            if (!(zzav == null || (context = zzav.zze) == null || zzav.zzfm == null)) {
                context.getContentResolver().unregisterContentObserver(zzfs.zzfm);
            }
            zzfs = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzd(String str) {
        return zzal.zza(this.zze.getContentResolver(), str, (String) null);
    }
}
