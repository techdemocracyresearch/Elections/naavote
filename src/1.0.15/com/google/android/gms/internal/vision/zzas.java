package com.google.android.gms.internal.vision;

import android.database.ContentObserver;
import android.os.Handler;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzas extends ContentObserver {
    private final /* synthetic */ zzaq zzfr;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzas(zzaq zzaq, Handler handler) {
        super(null);
        this.zzfr = zzaq;
    }

    public final void onChange(boolean z) {
        this.zzfr.zzv();
    }
}
