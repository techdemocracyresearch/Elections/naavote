package com.google.android.gms.internal.vision;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzdi<E> extends zzdf<E> {
    static final zzdf<Object> zzlx = new zzdi(new Object[0], 0);
    private final transient int size;
    private final transient Object[] zzly;

    zzdi(Object[] objArr, int i) {
        this.zzly = objArr;
        this.size = i;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdc
    public final int zzca() {
        return 0;
    }

    public final int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdc
    public final Object[] zzbz() {
        return this.zzly;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdc
    public final int zzcb() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdc, com.google.android.gms.internal.vision.zzdf
    public final int zza(Object[] objArr, int i) {
        System.arraycopy(this.zzly, 0, objArr, i, this.size);
        return i + this.size;
    }

    @Override // java.util.List
    public final E get(int i) {
        zzct.zzc(i, this.size);
        return (E) this.zzly[i];
    }
}
