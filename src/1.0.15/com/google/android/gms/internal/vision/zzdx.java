package com.google.android.gms.internal.vision;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzdx extends WeakReference<Throwable> {
    private final int zzmm;

    public zzdx(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        Objects.requireNonNull(th, "The referent cannot be null");
        this.zzmm = System.identityHashCode(th);
    }

    public final int hashCode() {
        return this.zzmm;
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (this == obj) {
                return true;
            }
            zzdx zzdx = (zzdx) obj;
            return this.zzmm == zzdx.zzmm && get() == zzdx.get();
        }
    }
}
