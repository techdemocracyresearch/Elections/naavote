package com.google.android.gms.internal.p001authapi;

import com.google.android.gms.common.api.internal.RemoteCall;
import com.google.android.gms.tasks.TaskCompletionSource;

/* renamed from: com.google.android.gms.internal.auth-api.zban  reason: invalid package */
/* compiled from: com.google.android.gms:play-services-auth@@19.2.0 */
public final /* synthetic */ class zban implements RemoteCall {
    public final /* synthetic */ zbau zba;

    public /* synthetic */ zban(zbau zbau) {
        this.zba = zbau;
    }

    @Override // com.google.android.gms.common.api.internal.RemoteCall
    public final void accept(Object obj, Object obj2) {
        this.zba.zba((zbav) obj, (TaskCompletionSource) obj2);
    }
}
