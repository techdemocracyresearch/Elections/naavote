package com.faizal.OtpVerify;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.util.Log;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import java.util.ArrayList;
import java.util.Iterator;

public class RNOtpVerifyModule extends ReactContextBaseJavaModule implements LifecycleEventListener {
    private static final String TAG = "RNOtpVerifyModule";
    private boolean isReceiverRegistered = false;
    private BroadcastReceiver mReceiver;
    private final ReactApplicationContext reactContext;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return "RNOtpVerify";
    }

    public RNOtpVerifyModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        this.reactContext = reactApplicationContext;
        this.mReceiver = new OtpBroadcastReceiver(reactApplicationContext);
        getReactApplicationContext().addLifecycleEventListener(this);
        registerReceiverIfNecessary(this.mReceiver);
    }

    @ReactMethod
    public void getOtp(Promise promise) {
        requestOtp(promise);
    }

    @ReactMethod
    public void getHash(Promise promise) {
        try {
            ArrayList<String> appSignatures = new AppSignatureHelper(this.reactContext).getAppSignatures();
            WritableArray createArray = Arguments.createArray();
            Iterator<String> it = appSignatures.iterator();
            while (it.hasNext()) {
                createArray.pushString(it.next());
            }
            promise.resolve(createArray);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    private void registerReceiverIfNecessary(BroadcastReceiver broadcastReceiver) {
        if (getCurrentActivity() != null) {
            try {
                getCurrentActivity().registerReceiver(broadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
                Log.d(TAG, "Receiver Registered");
                this.isReceiverRegistered = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void requestOtp(final Promise promise) {
        Task<Void> startSmsRetriever = SmsRetriever.getClient(this.reactContext).startSmsRetriever();
        startSmsRetriever.addOnSuccessListener(new OnSuccessListener<Void>() {
            /* class com.faizal.OtpVerify.RNOtpVerifyModule.AnonymousClass1 */

            public void onSuccess(Void r2) {
                Log.e(RNOtpVerifyModule.TAG, "started sms listener");
                promise.resolve(true);
            }
        });
        startSmsRetriever.addOnFailureListener(new OnFailureListener() {
            /* class com.faizal.OtpVerify.RNOtpVerifyModule.AnonymousClass2 */

            @Override // com.google.android.gms.tasks.OnFailureListener
            public void onFailure(Exception exc) {
                Log.e(RNOtpVerifyModule.TAG, "Could not start sms listener", exc);
                promise.reject(exc);
            }
        });
    }

    private void unregisterReceiver(BroadcastReceiver broadcastReceiver) {
        if (this.isReceiverRegistered && getCurrentActivity() != null && broadcastReceiver != null) {
            try {
                getCurrentActivity().unregisterReceiver(broadcastReceiver);
                Log.d(TAG, "Receiver UnRegistered");
                this.isReceiverRegistered = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override // com.facebook.react.bridge.LifecycleEventListener
    public void onHostResume() {
        registerReceiverIfNecessary(this.mReceiver);
    }

    @Override // com.facebook.react.bridge.LifecycleEventListener
    public void onHostPause() {
        unregisterReceiver(this.mReceiver);
    }

    @Override // com.facebook.react.bridge.LifecycleEventListener
    public void onHostDestroy() {
        unregisterReceiver(this.mReceiver);
    }
}
