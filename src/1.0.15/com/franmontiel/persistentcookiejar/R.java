package com.franmontiel.persistentcookiejar;

public final class R {

    public static final class string {
        public static final int app_name = 2131689500;

        private string() {
        }
    }

    private R() {
    }
}
