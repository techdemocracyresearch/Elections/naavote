package com.gantix.JailMonkey.MockLocation;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import java.util.Iterator;

public class MockLocationCheck {
    public static boolean isMockLocationOn(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            return "0".equals(Settings.Secure.getString(context.getContentResolver(), "mock_location"));
        }
        PackageManager packageManager = context.getPackageManager();
        Iterator<ApplicationInfo> it = packageManager.getInstalledApplications(128).iterator();
        while (true) {
            if (!it.hasNext()) {
                return false;
            }
            ApplicationInfo next = it.next();
            try {
                String[] strArr = packageManager.getPackageInfo(next.packageName, 4096).requestedPermissions;
                if (strArr != null) {
                    for (String str : strArr) {
                        if (str.equals("android.permission.ACCESS_MOCK_LOCATION") && !next.packageName.equals(context.getPackageName())) {
                            return true;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("Mock location check error ", e.getMessage());
            }
        }
    }
}
