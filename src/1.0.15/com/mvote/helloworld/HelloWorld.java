package com.mvote.helloworld;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class HelloWorld extends ReactContextBaseJavaModule {
    MvoteUid mvote_id = null;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return "HelloWorld";
    }

    public HelloWorld(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
    }

    @ReactMethod
    public void getImeiAndDeviceId(Callback callback, Callback callback2) {
        callback2.invoke(new BasicHelper().getDeviceInfo());
    }

    @ReactMethod
    public void cryptoLib(String str, String str2, Callback callback, Callback callback2) {
        callback2.invoke(new VoteEncryptBridge().encryptVote(str, str2));
    }

    @ReactMethod
    public void getStrFrom64(String str, Callback callback, Callback callback2) {
        callback2.invoke(new VoteEncryptBridge().getStrFrom64(str));
    }

    @ReactMethod
    public void getVVpatSlip(String str, String str2, Callback callback, Callback callback2) {
        callback2.invoke(new VoteEncryptBridge().encryptVVPAT(str, str2));
    }

    @ReactMethod
    public void getUIDAICaptcha(Callback callback, Callback callback2) {
        MvoteUid mvoteUid = new MvoteUid();
        this.mvote_id = mvoteUid;
        callback2.invoke(mvoteUid.get_captcha());
    }

    @ReactMethod
    public void sendCaptchaandAadhaar(String str, String str2, Callback callback, Callback callback2) {
        callback2.invoke(this.mvote_id.captcha_post_page(str, str2));
    }

    @ReactMethod
    public void downloadAadhaar(String str, String str2, String str3, Callback callback, Callback callback2) {
        callback2.invoke(this.mvote_id.send_otp(str, str2, str3));
    }

    @ReactMethod
    public void cameraOn(String str, Callback callback, Callback callback2) {
        LivelinessIntegration livelinessIntegration = new LivelinessIntegration(getReactApplicationContext());
        livelinessIntegration.check_liveliness(str);
        while (true) {
            if (livelinessIntegration.capture_done) {
                break;
            }
            try {
                if (livelinessIntegration.capture_error) {
                    break;
                }
                Thread.sleep(1000);
            } catch (Exception unused) {
            }
        }
        callback2.invoke(livelinessIntegration.getImage());
    }
}
