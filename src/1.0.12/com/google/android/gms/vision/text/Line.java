package com.google.android.gms.vision.text;

import android.graphics.Point;
import android.graphics.Rect;
import com.google.android.gms.internal.vision.zzac;
import com.google.android.gms.internal.vision.zzaj;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision@@20.0.0 */
public class Line implements Text {
    private zzac zzdz;
    private List<Element> zzea;

    Line(zzac zzac) {
        this.zzdz = zzac;
    }

    @Override // com.google.android.gms.vision.text.Text
    public String getLanguage() {
        return this.zzdz.zzed;
    }

    @Override // com.google.android.gms.vision.text.Text
    public String getValue() {
        return this.zzdz.zzem;
    }

    @Override // com.google.android.gms.vision.text.Text
    public Rect getBoundingBox() {
        return zzc.zza(this);
    }

    @Override // com.google.android.gms.vision.text.Text
    public Point[] getCornerPoints() {
        return zzc.zza(this.zzdz.zzej);
    }

    @Override // com.google.android.gms.vision.text.Text
    public List<? extends Text> getComponents() {
        if (this.zzdz.zzei.length == 0) {
            return new ArrayList(0);
        }
        if (this.zzea == null) {
            this.zzea = new ArrayList(this.zzdz.zzei.length);
            for (zzaj zzaj : this.zzdz.zzei) {
                this.zzea.add(new Element(zzaj));
            }
        }
        return this.zzea;
    }

    public float getAngle() {
        return this.zzdz.zzej.zzeh;
    }

    public boolean isVertical() {
        return this.zzdz.zzeo;
    }
}
