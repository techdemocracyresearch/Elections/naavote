package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public enum zzxm {
    VOID(Void.class, Void.class, null),
    INT(Integer.TYPE, Integer.class, 0),
    LONG(Long.TYPE, Long.class, 0L),
    FLOAT(Float.TYPE, Float.class, Float.valueOf(0.0f)),
    DOUBLE(Double.TYPE, Double.class, Double.valueOf(0.0d)),
    BOOLEAN(Boolean.TYPE, Boolean.class, false),
    STRING(String.class, String.class, ""),
    BYTE_STRING(zzvv.class, zzvv.class, zzvv.zzchp),
    ENUM(Integer.TYPE, Integer.class, null),
    MESSAGE(Object.class, Object.class, null);
    
    private final Class<?> zzcmv;
    private final Class<?> zzcmw;
    private final Object zzcmx;

    private zzxm(Class cls, Class cls2, Object obj) {
        this.zzcmv = cls;
        this.zzcmw = cls2;
        this.zzcmx = obj;
    }

    public final Class<?> zzvk() {
        return this.zzcmw;
    }
}
