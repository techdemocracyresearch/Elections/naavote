package com.google.android.gms.internal.firebase_ml;

import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzps<K, V> {
    private final Map<K, V> zzbim = new HashMap();

    /* access modifiers changed from: protected */
    public abstract V create(K k);

    public final V get(K k) {
        synchronized (this.zzbim) {
            if (this.zzbim.containsKey(k)) {
                return this.zzbim.get(k);
            }
            V create = create(k);
            this.zzbim.put(k, create);
            return create;
        }
    }
}
