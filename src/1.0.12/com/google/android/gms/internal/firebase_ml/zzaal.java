package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzaal extends zzaag {
    zzaal() {
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0035, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zza(r23, r8) > -65) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (com.google.android.gms.internal.firebase_ml.zzaac.zza(r23, r8) > -65) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x00f9, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0120, code lost:
        return -1;
     */
    @Override // com.google.android.gms.internal.firebase_ml.zzaag
    public final int zzb(int i, byte[] bArr, int i2, int i3) {
        int i4;
        byte b;
        long j;
        if ((i2 | i3 | (bArr.length - i3)) >= 0) {
            long j2 = (long) i2;
            long j3 = (long) i3;
            if (i != 0) {
                if (j2 >= j3) {
                    return i;
                }
                byte b2 = (byte) i;
                if (b2 < -32) {
                    if (b2 >= -62) {
                        j = j2 + 1;
                    }
                    return -1;
                } else if (b2 < -16) {
                    byte b3 = (byte) (~(i >> 8));
                    if (b3 == 0) {
                        long j4 = j2 + 1;
                        b3 = zzaac.zza(bArr, j2);
                        if (j4 >= j3) {
                            return zzaaf.zzv(b2, b3);
                        }
                        j2 = j4;
                    }
                    if (b3 <= -65 && ((b2 != -32 || b3 >= -96) && (b2 != -19 || b3 < -96))) {
                        j = j2 + 1;
                    }
                    return -1;
                } else {
                    byte b4 = (byte) (~(i >> 8));
                    if (b4 == 0) {
                        long j5 = j2 + 1;
                        b4 = zzaac.zza(bArr, j2);
                        if (j5 >= j3) {
                            return zzaaf.zzv(b2, b4);
                        }
                        j2 = j5;
                        b = 0;
                    } else {
                        b = (byte) (i >> 16);
                    }
                    if (b == 0) {
                        long j6 = j2 + 1;
                        b = zzaac.zza(bArr, j2);
                        if (j6 >= j3) {
                            return zzaaf.zze(b2, b4, b);
                        }
                        j2 = j6;
                    }
                    if (b4 <= -65 && (((b2 << 28) + (b4 + 112)) >> 30) == 0 && b <= -65) {
                        long j7 = j2 + 1;
                        if (zzaac.zza(bArr, j2) <= -65) {
                            j2 = j7;
                        }
                    }
                    return -1;
                }
                j2 = j;
            }
            int i5 = (int) (j3 - j2);
            if (i5 >= 16) {
                long j8 = j2;
                i4 = 0;
                while (true) {
                    if (i4 >= i5) {
                        i4 = i5;
                        break;
                    }
                    long j9 = j8 + 1;
                    if (zzaac.zza(bArr, j8) < 0) {
                        break;
                    }
                    i4++;
                    j8 = j9;
                }
            } else {
                i4 = 0;
            }
            int i6 = i5 - i4;
            long j10 = j2 + ((long) i4);
            while (true) {
                byte b5 = 0;
                while (true) {
                    if (i6 <= 0) {
                        break;
                    }
                    long j11 = j10 + 1;
                    byte zza = zzaac.zza(bArr, j10);
                    if (zza < 0) {
                        j10 = j11;
                        b5 = zza;
                        break;
                    }
                    i6--;
                    j10 = j11;
                    b5 = zza;
                }
                if (i6 != 0) {
                    int i7 = i6 - 1;
                    if (b5 >= -32) {
                        if (b5 >= -16) {
                            if (i7 >= 3) {
                                i6 = i7 - 3;
                                long j12 = j10 + 1;
                                byte zza2 = zzaac.zza(bArr, j10);
                                if (zza2 > -65 || (((b5 << 28) + (zza2 + 112)) >> 30) != 0) {
                                    break;
                                }
                                long j13 = j12 + 1;
                                if (zzaac.zza(bArr, j12) > -65) {
                                    break;
                                }
                                long j14 = j13 + 1;
                                if (zzaac.zza(bArr, j13) > -65) {
                                    break;
                                }
                                j10 = j14;
                            } else {
                                return zza(bArr, b5, j10, i7);
                            }
                        } else if (i7 >= 2) {
                            i6 = i7 - 2;
                            long j15 = j10 + 1;
                            byte zza3 = zzaac.zza(bArr, j10);
                            if (zza3 > -65 || ((b5 == -32 && zza3 < -96) || (b5 == -19 && zza3 >= -96))) {
                                break;
                            }
                            j10 = j15 + 1;
                            if (zzaac.zza(bArr, j15) > -65) {
                                break;
                            }
                        } else {
                            return zza(bArr, b5, j10, i7);
                        }
                    } else if (i7 != 0) {
                        i6 = i7 - 1;
                        if (b5 < -62) {
                            break;
                        }
                        long j16 = j10 + 1;
                        if (zzaac.zza(bArr, j10) > -65) {
                            break;
                        }
                        j10 = j16;
                    } else {
                        return b5;
                    }
                } else {
                    return 0;
                }
            }
            return -1;
        }
        throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", Integer.valueOf(bArr.length), Integer.valueOf(i2), Integer.valueOf(i3)));
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzaag
    public final String zzh(byte[] bArr, int i, int i2) throws zzxk {
        if ((i | i2 | ((bArr.length - i) - i2)) >= 0) {
            int i3 = i + i2;
            char[] cArr = new char[i2];
            int i4 = 0;
            while (i < i3) {
                byte zza = zzaac.zza(bArr, (long) i);
                if (!(zzaah.zze(zza))) {
                    break;
                }
                i++;
                zzaah.zza(zza, cArr, i4);
                i4++;
            }
            int i5 = i4;
            while (i < i3) {
                int i6 = i + 1;
                byte zza2 = zzaac.zza(bArr, (long) i);
                if (zzaah.zze(zza2)) {
                    int i7 = i5 + 1;
                    zzaah.zza(zza2, cArr, i5);
                    while (i6 < i3) {
                        byte zza3 = zzaac.zza(bArr, (long) i6);
                        if (!(zzaah.zze(zza3))) {
                            break;
                        }
                        i6++;
                        zzaah.zza(zza3, cArr, i7);
                        i7++;
                    }
                    i = i6;
                    i5 = i7;
                } else if (zzaah.zzf(zza2)) {
                    if (i6 < i3) {
                        int i8 = i6 + 1;
                        zzaah.zza(zza2, zzaac.zza(bArr, (long) i6), cArr, i5);
                        i = i8;
                        i5++;
                    } else {
                        throw zzxk.zzvj();
                    }
                } else if (zzaah.zzg(zza2)) {
                    if (i6 < i3 - 1) {
                        int i9 = i6 + 1;
                        int i10 = i9 + 1;
                        zzaah.zza(zza2, zzaac.zza(bArr, (long) i6), zzaac.zza(bArr, (long) i9), cArr, i5);
                        i = i10;
                        i5++;
                    } else {
                        throw zzxk.zzvj();
                    }
                } else if (i6 < i3 - 2) {
                    int i11 = i6 + 1;
                    int i12 = i11 + 1;
                    zzaah.zza(zza2, zzaac.zza(bArr, (long) i6), zzaac.zza(bArr, (long) i11), zzaac.zza(bArr, (long) i12), cArr, i5);
                    i = i12 + 1;
                    i5 = i5 + 1 + 1;
                } else {
                    throw zzxk.zzvj();
                }
            }
            return new String(cArr, 0, i5);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0033 A[LOOP:1: B:13:0x0033->B:38:0x00fb, LOOP_START, PHI: r2 r3 r4 r11 
      PHI: (r2v4 int) = (r2v3 int), (r2v6 int) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r3v3 char) = (r3v2 char), (r3v4 char) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r4v3 long) = (r4v2 long), (r4v5 long) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r11v3 long) = (r11v2 long), (r11v4 long) binds: [B:10:0x002f, B:38:0x00fb] A[DONT_GENERATE, DONT_INLINE]] */
    @Override // com.google.android.gms.internal.firebase_ml.zzaag
    public final int zzb(CharSequence charSequence, byte[] bArr, int i, int i2) {
        long j;
        long j2;
        int i3;
        char charAt;
        long j3 = (long) i;
        long j4 = ((long) i2) + j3;
        int length = charSequence.length();
        if (length > i2 || bArr.length - i2 < i) {
            char charAt2 = charSequence.charAt(length - 1);
            StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(charAt2);
            sb.append(" at index ");
            sb.append(i + i2);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i4 = 0;
        while (true) {
            char c = 128;
            long j5 = 1;
            if (i4 < length && (charAt = charSequence.charAt(i4)) < 128) {
                zzaac.zza(bArr, j3, (byte) charAt);
                i4++;
                j3 = 1 + j3;
            } else if (i4 != length) {
                return (int) j3;
            } else {
                while (i4 < length) {
                    char charAt3 = charSequence.charAt(i4);
                    if (charAt3 < c && j3 < j4) {
                        long j6 = j3 + j5;
                        zzaac.zza(bArr, j3, (byte) charAt3);
                        j2 = j5;
                        j = j6;
                    } else if (charAt3 < 2048 && j3 <= j4 - 2) {
                        long j7 = j3 + j5;
                        zzaac.zza(bArr, j3, (byte) ((charAt3 >>> 6) | 960));
                        zzaac.zza(bArr, j7, (byte) ((charAt3 & '?') | 128));
                        j = j7 + j5;
                        j2 = j5;
                        i4++;
                        c = 128;
                        j3 = j;
                        j5 = j2;
                    } else if ((charAt3 < 55296 || 57343 < charAt3) && j3 <= j4 - 3) {
                        long j8 = j3 + j5;
                        zzaac.zza(bArr, j3, (byte) ((charAt3 >>> '\f') | 480));
                        long j9 = j8 + j5;
                        zzaac.zza(bArr, j8, (byte) (((charAt3 >>> 6) & 63) | 128));
                        zzaac.zza(bArr, j9, (byte) ((charAt3 & '?') | 128));
                        j = j9 + 1;
                        j2 = 1;
                    } else if (j3 <= j4 - 4) {
                        int i5 = i4 + 1;
                        if (i5 != length) {
                            char charAt4 = charSequence.charAt(i5);
                            if (Character.isSurrogatePair(charAt3, charAt4)) {
                                int codePoint = Character.toCodePoint(charAt3, charAt4);
                                long j10 = j3 + 1;
                                zzaac.zza(bArr, j3, (byte) ((codePoint >>> 18) | 240));
                                long j11 = j10 + 1;
                                zzaac.zza(bArr, j10, (byte) (((codePoint >>> 12) & 63) | 128));
                                long j12 = j11 + 1;
                                zzaac.zza(bArr, j11, (byte) (((codePoint >>> 6) & 63) | 128));
                                j2 = 1;
                                j = j12 + 1;
                                zzaac.zza(bArr, j12, (byte) ((codePoint & 63) | 128));
                                i4 = i5;
                                i4++;
                                c = 128;
                                j3 = j;
                                j5 = j2;
                            } else {
                                i4 = i5;
                            }
                        }
                        throw new zzaai(i4 - 1, length);
                    } else if (55296 > charAt3 || charAt3 > 57343 || ((i3 = i4 + 1) != length && Character.isSurrogatePair(charAt3, charSequence.charAt(i3)))) {
                        StringBuilder sb2 = new StringBuilder(46);
                        sb2.append("Failed writing ");
                        sb2.append(charAt3);
                        sb2.append(" at index ");
                        sb2.append(j3);
                        throw new ArrayIndexOutOfBoundsException(sb2.toString());
                    } else {
                        throw new zzaai(i4, length);
                    }
                    i4++;
                    c = 128;
                    j3 = j;
                    j5 = j2;
                }
                return (int) j3;
            }
        }
        if (i4 != length) {
        }
    }

    private static int zza(byte[] bArr, int i, long j, int i2) {
        if (i2 == 0) {
            return zzaaf.zzee(i);
        }
        if (i2 == 1) {
            return zzaaf.zzv(i, zzaac.zza(bArr, j));
        }
        if (i2 == 2) {
            return zzaaf.zze(i, zzaac.zza(bArr, j), zzaac.zza(bArr, j + 1));
        }
        throw new AssertionError();
    }
}
