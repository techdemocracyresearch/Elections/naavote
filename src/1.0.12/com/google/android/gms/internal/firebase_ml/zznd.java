package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zznd<E> implements Iterator<E> {
    protected zznd() {
    }

    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
