package com.google.android.gms.internal.firebase_ml;

import java.util.NoSuchElementException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzvu extends zzvw {
    private final int limit;
    private int position = 0;
    private final /* synthetic */ zzvv zzcho;

    zzvu(zzvv zzvv) {
        this.zzcho = zzvv;
        this.limit = zzvv.size();
    }

    public final boolean hasNext() {
        return this.position < this.limit;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwa
    public final byte nextByte() {
        int i = this.position;
        if (i < this.limit) {
            this.position = i + 1;
            return this.zzcho.zzcx(i);
        }
        throw new NoSuchElementException();
    }
}
