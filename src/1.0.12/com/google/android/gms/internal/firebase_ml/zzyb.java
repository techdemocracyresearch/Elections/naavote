package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzyb implements zzzh {
    private static final zzyl zzcnn = new zzya();
    private final zzyl zzcnm;

    public zzyb() {
        this(new zzyd(zzxa.zzuw(), zzvs()));
    }

    private zzyb(zzyl zzyl) {
        this.zzcnm = (zzyl) zzxd.checkNotNull(zzyl, "messageInfoFactory");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzzh
    public final <T> zzze<T> zzk(Class<T> cls) {
        zzzg.zzm((Class<?>) cls);
        zzyi zzj = this.zzcnm.zzj(cls);
        if (zzj.zzvy()) {
            if (zzwz.class.isAssignableFrom(cls)) {
                return zzyq.zza(zzzg.zzwn(), zzws.zzug(), zzj.zzvz());
            }
            return zzyq.zza(zzzg.zzwl(), zzws.zzuh(), zzj.zzvz());
        } else if (zzwz.class.isAssignableFrom(cls)) {
            if (zza(zzj)) {
                return zzyo.zza(cls, zzj, zzyu.zzwe(), zzxu.zzvq(), zzzg.zzwn(), zzws.zzug(), zzyj.zzwb());
            }
            return zzyo.zza(cls, zzj, zzyu.zzwe(), zzxu.zzvq(), zzzg.zzwn(), (zzwq<?>) null, zzyj.zzwb());
        } else if (zza(zzj)) {
            return zzyo.zza(cls, zzj, zzyu.zzwd(), zzxu.zzvp(), zzzg.zzwl(), zzws.zzuh(), zzyj.zzwa());
        } else {
            return zzyo.zza(cls, zzj, zzyu.zzwd(), zzxu.zzvp(), zzzg.zzwm(), (zzwq<?>) null, zzyj.zzwa());
        }
    }

    private static boolean zza(zzyi zzyi) {
        return zzyi.zzvx() == zzwz.zzg.zzcmb;
    }

    private static zzyl zzvs() {
        try {
            return (zzyl) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return zzcnn;
        }
    }
}
