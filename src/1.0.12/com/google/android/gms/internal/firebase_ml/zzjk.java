package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzjk implements zzjq {
    private final Logger logger;
    private final zzjq zzabv;
    private final int zzacn;
    private final Level zzaie;

    public zzjk(zzjq zzjq, Logger logger2, Level level, int i) {
        this.zzabv = zzjq;
        this.logger = logger2;
        this.zzaie = level;
        this.zzacn = i;
    }

    /* JADX INFO: finally extract failed */
    @Override // com.google.android.gms.internal.firebase_ml.zzjq
    public final void writeTo(OutputStream outputStream) throws IOException {
        zzjl zzjl = new zzjl(outputStream, this.logger, this.zzaie, this.zzacn);
        try {
            this.zzabv.writeTo(zzjl);
            zzjl.zzif().close();
            outputStream.flush();
        } catch (Throwable th) {
            zzjl.zzif().close();
            throw th;
        }
    }
}
