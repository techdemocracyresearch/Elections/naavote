package com.google.android.gms.internal.firebase_ml;

import android.app.Application;
import android.content.Context;
import com.google.android.gms.common.api.internal.BackgroundDetector;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.Dependency;
import com.google.firebase.ml.common.FirebaseMLException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzqr {
    private static final GmsLogger zzbiy = new GmsLogger("ModelResourceManager", "");
    public static final Component<?> zzbja = Component.builder(zzqr.class).add(Dependency.required(Context.class)).factory(zzqs.zzbil).build();
    private final zzpx zzbke = zzpx.zzof();
    private final AtomicLong zzbkf;
    private final Set<zzqp> zzbkg;
    private final Set<zzqp> zzbkh;
    private final ConcurrentHashMap<zzqp, zza> zzbki;

    private zzqr(Context context) {
        AtomicLong atomicLong = new AtomicLong(300000);
        this.zzbkf = atomicLong;
        this.zzbkg = new HashSet();
        this.zzbkh = new HashSet();
        this.zzbki = new ConcurrentHashMap<>();
        if (context instanceof Application) {
            BackgroundDetector.initialize((Application) context);
        } else {
            zzbiy.e("ModelResourceManager", "No valid Application available and auto-manage cannot work");
        }
        BackgroundDetector.getInstance().addListener(new zzqq(this));
        if (BackgroundDetector.getInstance().readCurrentStateIfPossible(true)) {
            atomicLong.set(2000);
        }
    }

    /* access modifiers changed from: package-private */
    /* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
    public class zza implements Callable<Void> {
        private final zzqp zzbkj;
        private final String zzbkk;

        zza(zzqp zzqp, String str) {
            this.zzbkj = zzqp;
            this.zzbkk = str;
        }

        /* access modifiers changed from: private */
        /* renamed from: zzoo */
        public final Void call() {
            String str = this.zzbkk;
            str.hashCode();
            if (str.equals("OPERATION_RELEASE")) {
                zzqp zzqp = this.zzbkj;
                zzqr.zzbiy.v("ModelResourceManager", "Releasing modelResource");
                zzqp.release();
                zzqr.this.zzbkh.remove(zzqp);
                return null;
            } else if (!str.equals("OPERATION_LOAD")) {
                return null;
            } else {
                try {
                    zzqr.this.zzf(this.zzbkj);
                    return null;
                } catch (FirebaseMLException e) {
                    zzqr.zzbiy.e("ModelResourceManager", "Error preloading model resource", e);
                    return null;
                }
            }
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (!Objects.equal(this.zzbkj, zza.zzbkj) || !Objects.equal(this.zzbkk, zza.zzbkk)) {
                return false;
            }
            return true;
        }

        public final int hashCode() {
            return Objects.hashCode(this.zzbkj, this.zzbkk);
        }
    }

    public final synchronized void zza(zzqp zzqp) {
        Preconditions.checkNotNull(zzqp, "Model source can not be null");
        GmsLogger gmsLogger = zzbiy;
        gmsLogger.d("ModelResourceManager", "Add auto-managed model resource");
        if (this.zzbkg.contains(zzqp)) {
            gmsLogger.i("ModelResourceManager", "The model resource is already registered.");
            return;
        }
        this.zzbkg.add(zzqp);
        if (zzqp != null) {
            this.zzbke.zza(new zza(zzqp, "OPERATION_LOAD"));
            zzb(zzqp);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void zzb(zzqp zzqp) {
        if (this.zzbkg.contains(zzqp)) {
            zzc(zzqp);
        }
    }

    private final synchronized void zzom() {
        for (zzqp zzqp : this.zzbkg) {
            zzc(zzqp);
        }
    }

    private final void zzc(zzqp zzqp) {
        zza zze = zze(zzqp);
        this.zzbke.zzb(zze);
        long j = this.zzbkf.get();
        GmsLogger gmsLogger = zzbiy;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Rescheduling modelResource release after: ");
        sb.append(j);
        gmsLogger.v("ModelResourceManager", sb.toString());
        this.zzbke.zza(zze, j);
    }

    public final synchronized void zzd(zzqp zzqp) {
        if (zzqp != null) {
            zza zze = zze(zzqp);
            this.zzbke.zzb(zze);
            this.zzbke.zza(zze, 0);
        }
    }

    private final zza zze(zzqp zzqp) {
        this.zzbki.putIfAbsent(zzqp, new zza(zzqp, "OPERATION_RELEASE"));
        return this.zzbki.get(zzqp);
    }

    /* access modifiers changed from: package-private */
    public final void zzf(zzqp zzqp) throws FirebaseMLException {
        if (!this.zzbkh.contains(zzqp)) {
            try {
                zzqp.zzol();
                this.zzbkh.add(zzqp);
            } catch (RuntimeException e) {
                throw new FirebaseMLException("The load task failed", 13, e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaq(boolean z) {
        GmsLogger gmsLogger = zzbiy;
        StringBuilder sb = new StringBuilder(34);
        sb.append("Background state changed to: ");
        sb.append(z);
        gmsLogger.v("ModelResourceManager", sb.toString());
        this.zzbkf.set(z ? 2000 : 300000);
        zzom();
    }

    static final /* synthetic */ zzqr zzd(ComponentContainer componentContainer) {
        return new zzqr((Context) componentContainer.get(Context.class));
    }
}
