package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
abstract class zzlx extends zzlv {
    private final String description;

    zzlx(String str) {
        this.description = (String) zzml.checkNotNull(str);
    }

    public final String toString() {
        return this.description;
    }
}
