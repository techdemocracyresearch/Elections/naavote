package com.google.android.gms.internal.firebase_ml;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzyf<K, V> {
    public final zzaan zzcns;
    public final K zzcnt;
    public final zzaan zzcnu;
    public final V zzcnv;

    public zzyf(zzaan zzaan, K k, zzaan zzaan2, V v) {
        this.zzcns = zzaan;
        this.zzcnt = k;
        this.zzcnu = zzaan2;
        this.zzcnv = v;
    }
}
