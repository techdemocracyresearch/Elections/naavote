package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzmq {
    Iterator<String> zza(zzmm zzmm, CharSequence charSequence);
}
