package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzio {
    byte[] buffer;
    boolean eof;
    int pos;
    int zzafr;
    int zzafs;
    int zzaft;
    int zzafu;

    zzio() {
    }

    public final String toString() {
        return String.format("%s[buffer=%s, currentLinePos=%s, eof=%s, ibitWorkArea=%s, lbitWorkArea=%s, modulus=%s, pos=%s, readPos=%s]", getClass().getSimpleName(), Arrays.toString(this.buffer), Integer.valueOf(this.zzaft), Boolean.valueOf(this.eof), Integer.valueOf(this.zzafr), 0L, Integer.valueOf(this.zzafu), Integer.valueOf(this.pos), Integer.valueOf(this.zzafs));
    }
}
