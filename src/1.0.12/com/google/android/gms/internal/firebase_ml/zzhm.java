package com.google.android.gms.internal.firebase_ml;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzhm {
    static final Map<Character, zzhl> zzadx = new HashMap();

    private static Map<String, Object> zzb(Object obj) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<String, Object> entry : zzix.zzf(obj).entrySet()) {
            Object value = entry.getValue();
            if (value != null && !zzix.isNull(value)) {
                linkedHashMap.put(entry.getKey(), value);
            }
        }
        return linkedHashMap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0200 A[EDGE_INSN: B:85:0x0200->B:83:0x0200 ?: BREAK  , SYNTHETIC] */
    public static String zza(String str, String str2, Object obj, boolean z) {
        String str3;
        int length;
        int i;
        String str4;
        if (str2.startsWith("/")) {
            zzgu zzgu = new zzgu(str);
            zzgu.zzu(null);
            String valueOf = String.valueOf(zzgu.zzft());
            String valueOf2 = String.valueOf(str2);
            if (valueOf2.length() != 0) {
                str3 = valueOf.concat(valueOf2);
                Map<String, Object> zzb = zzb(obj);
                StringBuilder sb = new StringBuilder();
                length = str3.length();
                i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    int indexOf = str3.indexOf(123, i);
                    if (indexOf == -1) {
                        sb.append(str3.substring(i));
                        break;
                    }
                    sb.append(str3.substring(i, indexOf));
                    int indexOf2 = str3.indexOf(125, indexOf + 2);
                    int i2 = indexOf2 + 1;
                    String substring = str3.substring(indexOf + 1, indexOf2);
                    zzhl zzhl = zzadx.get(Character.valueOf(substring.charAt(0)));
                    if (zzhl == null) {
                        zzhl = zzhl.SIMPLE;
                    }
                    ListIterator<String> listIterator = zzmm.zza(zzlw.zza(',')).zza(substring).listIterator();
                    boolean z2 = true;
                    while (listIterator.hasNext()) {
                        String next = listIterator.next();
                        boolean endsWith = next.endsWith("*");
                        int zzgq = listIterator.nextIndex() == 1 ? zzhl.zzgq() : 0;
                        int length2 = next.length();
                        if (endsWith) {
                            length2--;
                        }
                        String substring2 = next.substring(zzgq, length2);
                        Object remove = zzb.remove(substring2);
                        if (remove != null) {
                            if (!z2) {
                                sb.append(zzhl.zzgo());
                            } else {
                                sb.append(zzhl.zzgn());
                                z2 = false;
                            }
                            if (remove instanceof Iterator) {
                                str4 = zza(substring2, (Iterator) remove, endsWith, zzhl);
                            } else if ((remove instanceof Iterable) || remove.getClass().isArray()) {
                                str4 = zza(substring2, zzjs.zzi(remove).iterator(), endsWith, zzhl);
                            } else if (remove.getClass().isEnum()) {
                                if (zzjd.zza((Enum) remove).getName() != null) {
                                    if (zzhl.zzgp()) {
                                        remove = String.format("%s=%s", substring2, remove);
                                    }
                                    remove = zzjw.zzas(remove.toString());
                                }
                                str4 = remove;
                            } else if (!zzix.zzg(remove)) {
                                Map<String, Object> zzb2 = zzb(remove);
                                if (zzb2.isEmpty()) {
                                    str4 = "";
                                } else {
                                    StringBuilder sb2 = new StringBuilder();
                                    String str5 = "=";
                                    String str6 = ",";
                                    if (endsWith) {
                                        str6 = zzhl.zzgo();
                                    } else {
                                        if (zzhl.zzgp()) {
                                            sb2.append(zzjw.zzas(substring2));
                                            sb2.append(str5);
                                        }
                                        str5 = str6;
                                    }
                                    Iterator<Map.Entry<String, Object>> it = zzb2.entrySet().iterator();
                                    while (it.hasNext()) {
                                        Map.Entry<String, Object> next2 = it.next();
                                        String zzak = zzhl.zzak(next2.getKey());
                                        String zzak2 = zzhl.zzak(next2.getValue().toString());
                                        sb2.append(zzak);
                                        sb2.append(str5);
                                        sb2.append(zzak2);
                                        if (it.hasNext()) {
                                            sb2.append(str6);
                                        }
                                    }
                                    str4 = sb2.toString();
                                }
                            } else {
                                if (zzhl.zzgp()) {
                                    remove = String.format("%s=%s", substring2, remove);
                                }
                                if (zzhl.zzgr()) {
                                    str4 = zzjw.zzat(remove.toString());
                                } else {
                                    str4 = zzjw.zzas(remove.toString());
                                }
                            }
                            sb.append((Object) str4);
                        }
                    }
                    i = i2;
                }
                zzgu.zza(zzb.entrySet(), sb);
                return sb.toString();
            }
            str2 = new String(valueOf);
        } else if (!str2.startsWith("http://") && !str2.startsWith("https://")) {
            String valueOf3 = String.valueOf(str);
            String valueOf4 = String.valueOf(str2);
            str2 = valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
        }
        str3 = str2;
        Map<String, Object> zzb3 = zzb(obj);
        StringBuilder sb3 = new StringBuilder();
        length = str3.length();
        i = 0;
        while (true) {
            if (i >= length) {
            }
            i = i2;
        }
        zzgu.zza(zzb3.entrySet(), sb3);
        return sb3.toString();
    }

    private static String zza(String str, Iterator<?> it, boolean z, zzhl zzhl) {
        String str2;
        if (!it.hasNext()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (z) {
            str2 = zzhl.zzgo();
        } else {
            if (zzhl.zzgp()) {
                sb.append(zzjw.zzas(str));
                sb.append("=");
            }
            str2 = ",";
        }
        while (it.hasNext()) {
            if (z && zzhl.zzgp()) {
                sb.append(zzjw.zzas(str));
                sb.append("=");
            }
            sb.append(zzhl.zzak(it.next().toString()));
            if (it.hasNext()) {
                sb.append(str2);
            }
        }
        return sb.toString();
    }

    static {
        zzhl.values();
    }
}
