package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;
import java.io.IOException;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzwp extends zzwq<zzwz.zzf> {
    zzwp() {
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwq
    public final boolean zze(zzyk zzyk) {
        return zzyk instanceof zzwz.zzc;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwq
    public final zzwr<zzwz.zzf> zzo(Object obj) {
        return ((zzwz.zzc) obj).zzclq;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwq
    public final zzwr<zzwz.zzf> zzp(Object obj) {
        return ((zzwz.zzc) obj).zzvc();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwq
    public final void zzq(Object obj) {
        zzo(obj).zztm();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwq
    public final int zza(Map.Entry<?, ?> entry) {
        zzwz.zzf zzf = (zzwz.zzf) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwq
    public final void zza(zzaat zzaat, Map.Entry<?, ?> entry) throws IOException {
        zzwz.zzf zzf = (zzwz.zzf) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.firebase_ml.zzwq
    public final Object zza(zzwo zzwo, zzyk zzyk, int i) {
        return zzwo.zza(zzyk, i);
    }
}
