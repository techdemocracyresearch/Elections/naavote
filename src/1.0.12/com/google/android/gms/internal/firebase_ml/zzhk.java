package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public abstract class zzhk {
    private String zzada;
    private String zzadb;
    private long zzadh = -1;
    private zzjq zzadi;

    public abstract void addHeader(String str, String str2) throws IOException;

    public void zza(int i, int i2) throws IOException {
    }

    public abstract zzhj zzgm() throws IOException;

    public final void setContentLength(long j) throws IOException {
        this.zzadh = j;
    }

    public final long getContentLength() {
        return this.zzadh;
    }

    public final void setContentEncoding(String str) throws IOException {
        this.zzada = str;
    }

    public final String getContentEncoding() {
        return this.zzada;
    }

    public final void setContentType(String str) throws IOException {
        this.zzadb = str;
    }

    public final String getContentType() {
        return this.zzadb;
    }

    public final void zza(zzjq zzjq) throws IOException {
        this.zzadi = zzjq;
    }

    public final zzjq zzgl() {
        return this.zzadi;
    }
}
