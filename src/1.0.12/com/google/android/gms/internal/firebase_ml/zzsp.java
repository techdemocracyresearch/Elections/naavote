package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions;
import java.io.Closeable;
import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsp extends zzrz<List<FirebaseVisionImageLabel>> implements Closeable {
    private final zzqg zzbmd;

    public zzsp(zzqf zzqf, FirebaseVisionOnDeviceAutoMLImageLabelerOptions firebaseVisionOnDeviceAutoMLImageLabelerOptions) throws FirebaseMLException {
        super(zzqf, new zzso(zzqf, firebaseVisionOnDeviceAutoMLImageLabelerOptions));
        zzqg zza = zzqg.zza(zzqf, 5);
        this.zzbmd = zza;
        zza.zza(zzns.zzad.zzma().zza((zzns.zzj) ((zzwz) zzns.zzj.zzkv().zzi(zzoc.NO_ERROR).zzvb())), zzod.AUTOML_IMAGE_LABELING_CREATE);
    }

    public final Task<List<FirebaseVisionImageLabel>> detectInImage(FirebaseVisionImage firebaseVisionImage) {
        return zza(firebaseVisionImage, true, false);
    }
}
