package com.google.android.gms.internal.firebase_ml;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzwx extends zzvp<Float> implements zzxl<Float>, zzyw, RandomAccess {
    private static final zzwx zzclb;
    private int size;
    private float[] zzclc;

    public static zzwx zzum() {
        return zzclb;
    }

    zzwx() {
        this(new float[10], 0);
    }

    private zzwx(float[] fArr, int i) {
        this.zzclc = fArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zztn();
        if (i2 >= i) {
            float[] fArr = this.zzclc;
            System.arraycopy(fArr, i2, fArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzwx)) {
            return super.equals(obj);
        }
        zzwx zzwx = (zzwx) obj;
        if (this.size != zzwx.size) {
            return false;
        }
        float[] fArr = zzwx.zzclc;
        for (int i = 0; i < this.size; i++) {
            if (Float.floatToIntBits(this.zzclc[i]) != Float.floatToIntBits(fArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzvp
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.zzclc[i2]);
        }
        return i;
    }

    public final int indexOf(Object obj) {
        if (!(obj instanceof Float)) {
            return -1;
        }
        float floatValue = ((Float) obj).floatValue();
        int size2 = size();
        for (int i = 0; i < size2; i++) {
            if (this.zzclc[i] == floatValue) {
                return i;
            }
        }
        return -1;
    }

    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final int size() {
        return this.size;
    }

    public final void zzs(float f) {
        zztn();
        int i = this.size;
        float[] fArr = this.zzclc;
        if (i == fArr.length) {
            float[] fArr2 = new float[(((i * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            this.zzclc = fArr2;
        }
        float[] fArr3 = this.zzclc;
        int i2 = this.size;
        this.size = i2 + 1;
        fArr3[i2] = f;
    }

    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection
    public final boolean addAll(Collection<? extends Float> collection) {
        zztn();
        zzxd.checkNotNull(collection);
        if (!(collection instanceof zzwx)) {
            return super.addAll(collection);
        }
        zzwx zzwx = (zzwx) collection;
        int i = zzwx.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.zzclc;
            if (i3 > fArr.length) {
                this.zzclc = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(zzwx.zzclc, 0, this.zzclc, this.size, zzwx.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp
    public final boolean remove(Object obj) {
        zztn();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Float.valueOf(this.zzclc[i]))) {
                float[] fArr = this.zzclc;
                System.arraycopy(fArr, i + 1, fArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzct(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
    }

    private final String zzcu(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Float set(int i, Float f) {
        float floatValue = f.floatValue();
        zztn();
        zzct(i);
        float[] fArr = this.zzclc;
        float f2 = fArr[i];
        fArr[i] = floatValue;
        return Float.valueOf(f2);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ Float remove(int i) {
        zztn();
        zzct(i);
        float[] fArr = this.zzclc;
        float f = fArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Float.valueOf(f);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [int, java.lang.Object] */
    @Override // java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.AbstractList
    public final /* synthetic */ void add(int i, Float f) {
        int i2;
        float floatValue = f.floatValue();
        zztn();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzcu(i));
        }
        float[] fArr = this.zzclc;
        if (i2 < fArr.length) {
            System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
        } else {
            float[] fArr2 = new float[(((i2 * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            System.arraycopy(this.zzclc, i, fArr2, i + 1, this.size - i);
            this.zzclc = fArr2;
        }
        this.zzclc[i] = floatValue;
        this.size++;
        this.modCount++;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, com.google.android.gms.internal.firebase_ml.zzvp, java.util.Collection, java.util.AbstractList
    public final /* synthetic */ boolean add(Float f) {
        zzs(f.floatValue());
        return true;
    }

    /* Return type fixed from 'com.google.android.gms.internal.firebase_ml.zzxl' to match base method */
    @Override // com.google.android.gms.internal.firebase_ml.zzxl
    public final /* synthetic */ zzxl<Float> zzcv(int i) {
        if (i >= this.size) {
            return new zzwx(Arrays.copyOf(this.zzclc, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        zzct(i);
        return Float.valueOf(this.zzclc[i]);
    }

    static {
        zzwx zzwx = new zzwx(new float[0], 0);
        zzclb = zzwx;
        zzwx.zztm();
    }
}
