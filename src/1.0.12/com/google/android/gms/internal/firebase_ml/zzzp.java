package com.google.android.gms.internal.firebase_ml;

import java.util.Iterator;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzzp implements Iterable<Object> {
    zzzp() {
    }

    @Override // java.lang.Iterable
    public final Iterator<Object> iterator() {
        return zzzn.zzcpv;
    }
}
