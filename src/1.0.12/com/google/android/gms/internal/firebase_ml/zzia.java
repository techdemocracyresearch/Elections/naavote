package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public abstract class zzia {
    public abstract void flush() throws IOException;

    public abstract void writeBoolean(boolean z) throws IOException;

    public abstract void writeString(String str) throws IOException;

    public abstract void zza(double d) throws IOException;

    public abstract void zza(BigDecimal bigDecimal) throws IOException;

    public abstract void zza(BigInteger bigInteger) throws IOException;

    public abstract void zzah(int i) throws IOException;

    public abstract void zzan(String str) throws IOException;

    public abstract void zze(long j) throws IOException;

    public abstract void zzgv() throws IOException;

    public abstract void zzgw() throws IOException;

    public abstract void zzgx() throws IOException;

    public abstract void zzgy() throws IOException;

    public abstract void zzgz() throws IOException;

    public void zzha() throws IOException {
    }

    public abstract void zzj(float f) throws IOException;

    public final void zzd(Object obj) throws IOException {
        zza(false, obj);
    }

    private final void zza(boolean z, Object obj) throws IOException {
        boolean z2;
        if (obj != null) {
            Class<?> cls = obj.getClass();
            if (zzix.isNull(obj)) {
                zzgz();
            } else if (obj instanceof String) {
                writeString((String) obj);
            } else {
                boolean z3 = true;
                if (obj instanceof Number) {
                    if (z) {
                        writeString(obj.toString());
                    } else if (obj instanceof BigDecimal) {
                        zza((BigDecimal) obj);
                    } else if (obj instanceof BigInteger) {
                        zza((BigInteger) obj);
                    } else if (obj instanceof Long) {
                        zze(((Long) obj).longValue());
                    } else if (obj instanceof Float) {
                        float floatValue = ((Number) obj).floatValue();
                        if (Float.isInfinite(floatValue) || Float.isNaN(floatValue)) {
                            z3 = false;
                        }
                        zzml.checkArgument(z3);
                        zzj(floatValue);
                    } else if ((obj instanceof Integer) || (obj instanceof Short) || (obj instanceof Byte)) {
                        zzah(((Number) obj).intValue());
                    } else {
                        double doubleValue = ((Number) obj).doubleValue();
                        if (Double.isInfinite(doubleValue) || Double.isNaN(doubleValue)) {
                            z3 = false;
                        }
                        zzml.checkArgument(z3);
                        zza(doubleValue);
                    }
                } else if (obj instanceof Boolean) {
                    writeBoolean(((Boolean) obj).booleanValue());
                } else if (obj instanceof zzje) {
                    writeString(((zzje) obj).zzid());
                } else if ((obj instanceof Iterable) || cls.isArray()) {
                    zzgv();
                    for (Object obj2 : zzjs.zzi(obj)) {
                        zza(z, obj2);
                    }
                    zzgw();
                } else if (cls.isEnum()) {
                    String name = zzjd.zza((Enum) obj).getName();
                    if (name == null) {
                        zzgz();
                    } else {
                        writeString(name);
                    }
                } else {
                    zzgx();
                    boolean z4 = (obj instanceof Map) && !(obj instanceof zzjf);
                    zziv zzc = z4 ? null : zziv.zzc(cls);
                    for (Map.Entry<String, Object> entry : zzix.zzf(obj).entrySet()) {
                        Object value = entry.getValue();
                        if (value != null) {
                            String key = entry.getKey();
                            if (z4) {
                                z2 = z;
                            } else {
                                zzjd zzao = zzc.zzao(key);
                                Field zzia = zzao == null ? null : zzao.zzia();
                                z2 = (zzia == null || zzia.getAnnotation(zzif.class) == null) ? false : true;
                            }
                            zzan(key);
                            zza(z2, value);
                        }
                    }
                    zzgy();
                }
            }
        }
    }
}
