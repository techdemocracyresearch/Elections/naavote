package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.common.api.internal.BackgroundDetector;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final /* synthetic */ class zzqq implements BackgroundDetector.BackgroundStateChangeListener {
    private final zzqr zzbkd;

    zzqq(zzqr zzqr) {
        this.zzbkd = zzqr;
    }

    @Override // com.google.android.gms.common.api.internal.BackgroundDetector.BackgroundStateChangeListener
    public final void onBackgroundStateChanged(boolean z) {
        this.zzbkd.zzaq(z);
    }
}
