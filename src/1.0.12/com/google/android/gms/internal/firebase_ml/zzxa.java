package com.google.android.gms.internal.firebase_ml;

import com.google.android.gms.internal.firebase_ml.zzwz;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
final class zzxa implements zzyl {
    private static final zzxa zzclm = new zzxa();

    private zzxa() {
    }

    public static zzxa zzuw() {
        return zzclm;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyl
    public final boolean zzi(Class<?> cls) {
        return zzwz.class.isAssignableFrom(cls);
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzyl
    public final zzyi zzj(Class<?> cls) {
        if (!zzwz.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (zzyi) zzwz.zzh(cls.asSubclass(zzwz.class)).zza(zzwz.zzg.zzclv, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
