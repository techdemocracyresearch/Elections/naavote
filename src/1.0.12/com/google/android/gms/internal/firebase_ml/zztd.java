package com.google.android.gms.internal.firebase_ml;

import java.io.IOException;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zztd extends IOException {
    public zztd(String str) {
        super(str);
    }
}
