package com.google.android.gms.internal.firebase_ml;

import java.util.List;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzx extends RuntimeException {
    private final List<String> zzcqa = null;

    public zzzx(zzyk zzyk) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
