package com.google.android.gms.internal.firebase_ml;

import java.util.NoSuchElementException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final class zzzd extends zzvw {
    private final zzzf zzcpe;
    private zzwa zzcpf = zzwk();
    private final /* synthetic */ zzza zzcpg;

    zzzd(zzza zzza) {
        this.zzcpg = zzza;
        this.zzcpe = new zzzf(zzza, null);
    }

    private final zzwa zzwk() {
        if (this.zzcpe.hasNext()) {
            return (zzwa) ((zzwc) this.zzcpe.next()).iterator();
        }
        return null;
    }

    public final boolean hasNext() {
        return this.zzcpf != null;
    }

    @Override // com.google.android.gms.internal.firebase_ml.zzwa
    public final byte nextByte() {
        zzwa zzwa = this.zzcpf;
        if (zzwa != null) {
            byte nextByte = zzwa.nextByte();
            if (!this.zzcpf.hasNext()) {
                this.zzcpf = zzwk();
            }
            return nextByte;
        }
        throw new NoSuchElementException();
    }
}
