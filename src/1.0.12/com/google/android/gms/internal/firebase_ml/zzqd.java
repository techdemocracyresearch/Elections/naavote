package com.google.android.gms.internal.firebase_ml;

import java.util.concurrent.Callable;

/* access modifiers changed from: package-private */
/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public final /* synthetic */ class zzqd implements Callable {
    private final zzqb zzbiv;
    private final zzqp zzbiw;
    private final zzpu zzbjb;
    private final zzpy zzbjc;

    zzqd(zzqb zzqb, zzqp zzqp, zzpu zzpu, zzpy zzpy) {
        this.zzbiv = zzqb;
        this.zzbiw = zzqp;
        this.zzbjb = zzpu;
        this.zzbjc = zzpy;
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        return this.zzbiv.zza(this.zzbiw, this.zzbjb, this.zzbjc);
    }
}
