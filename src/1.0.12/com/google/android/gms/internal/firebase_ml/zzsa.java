package com.google.android.gms.internal.firebase_ml;

import android.os.Build;
import com.google.android.gms.internal.firebase_ml.zzns;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class zzsa {
    public static int zzbw(int i) {
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        StringBuilder sb = new StringBuilder(34);
        sb.append("Invalid landmark type: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public static int zzbx(int i) {
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        StringBuilder sb = new StringBuilder(30);
        sb.append("Invalid mode type: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public static int zzby(int i) {
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        StringBuilder sb = new StringBuilder(40);
        sb.append("Invalid classification type: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public static zzns.zzae zzc(zzsf zzsf) {
        int i;
        zzns.zzae.zzb zzb;
        if (zzsf.zzbrv.getBitmap() != null) {
            zzb = zzns.zzae.zzb.BITMAP;
            if (Build.VERSION.SDK_INT >= 19) {
                i = zzsf.zzbrv.getBitmap().getAllocationByteCount();
            } else {
                i = zzsf.zzbrv.getBitmap().getByteCount();
            }
        } else {
            int format = zzsf.zzbrv.getMetadata().getFormat();
            if (format == 16) {
                zzb = zzns.zzae.zzb.NV16;
            } else if (format == 17) {
                zzb = zzns.zzae.zzb.NV21;
            } else if (format != 842094169) {
                zzb = zzns.zzae.zzb.UNKNOWN_FORMAT;
            } else {
                zzb = zzns.zzae.zzb.YV12;
            }
            i = zzsf.zzbrv.getGrayscaleImageData().capacity();
        }
        return (zzns.zzae) ((zzwz) zzns.zzae.zzmc().zza(zzb).zzbb(i).zzvb());
    }
}
