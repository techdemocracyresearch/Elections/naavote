package com.google.android.gms.internal.vision;

import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzfj implements Comparator<zzfh> {
    zzfj() {
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(zzfh zzfh, zzfh zzfh2) {
        zzfh zzfh3 = zzfh;
        zzfh zzfh4 = zzfh2;
        zzfq zzfq = (zzfq) zzfh3.iterator();
        zzfq zzfq2 = (zzfq) zzfh4.iterator();
        while (zzfq.hasNext() && zzfq2.hasNext()) {
            int compare = Integer.compare(zzfh.zza(zzfq.nextByte()), zzfh.zza(zzfq2.nextByte()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzfh3.size(), zzfh4.size());
    }
}
