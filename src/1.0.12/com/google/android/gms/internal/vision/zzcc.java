package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzbx;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
final class zzcc implements zzgv<zzbx.zzb.zzc> {
    zzcc() {
    }

    /* Return type fixed from 'com.google.android.gms.internal.vision.zzgw' to match base method */
    @Override // com.google.android.gms.internal.vision.zzgv
    public final /* synthetic */ zzbx.zzb.zzc zzh(int i) {
        return zzbx.zzb.zzc.zzp(i);
    }
}
