package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzhs implements zzhz {
    zzhs() {
    }

    @Override // com.google.android.gms.internal.vision.zzhz
    public final boolean zza(Class<?> cls) {
        return false;
    }

    @Override // com.google.android.gms.internal.vision.zzhz
    public final zzia zzb(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
