package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzhr implements zzhz {
    private zzhz[] zzyr;

    zzhr(zzhz... zzhzArr) {
        this.zzyr = zzhzArr;
    }

    @Override // com.google.android.gms.internal.vision.zzhz
    public final boolean zza(Class<?> cls) {
        for (zzhz zzhz : this.zzyr) {
            if (zzhz.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @Override // com.google.android.gms.internal.vision.zzhz
    public final zzia zzb(Class<?> cls) {
        zzhz[] zzhzArr = this.zzyr;
        for (zzhz zzhz : zzhzArr) {
            if (zzhz.zza(cls)) {
                return zzhz.zzb(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
