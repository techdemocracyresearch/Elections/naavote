package com.google.android.gms.internal.vision;

import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.os.UserManager;
import android.util.Log;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public class zzan {
    private static UserManager zzfg;
    private static volatile boolean zzfh = (!zzs());
    private static boolean zzfi = false;

    private zzan() {
    }

    public static boolean zzs() {
        return Build.VERSION.SDK_INT >= 24;
    }

    public static boolean isUserUnlocked(Context context) {
        return !zzs() || zzd(context);
    }

    private static boolean zzc(Context context) {
        boolean z;
        boolean z2 = true;
        int i = 1;
        while (true) {
            z = false;
            if (i > 2) {
                break;
            }
            if (zzfg == null) {
                zzfg = (UserManager) context.getSystemService(UserManager.class);
            }
            UserManager userManager = zzfg;
            if (userManager == null) {
                return true;
            }
            try {
                if (!userManager.isUserUnlocked() && userManager.isUserRunning(Process.myUserHandle())) {
                    z2 = false;
                }
                z = z2;
            } catch (NullPointerException e) {
                Log.w("DirectBootUtils", "Failed to check if user is unlocked.", e);
                zzfg = null;
                i++;
            }
        }
        if (z) {
            zzfg = null;
        }
        return z;
    }

    private static boolean zzd(Context context) {
        if (zzfh) {
            return true;
        }
        synchronized (zzan.class) {
            if (zzfh) {
                return true;
            }
            boolean zzc = zzc(context);
            if (zzc) {
                zzfh = zzc;
            }
            return zzc;
        }
    }
}
