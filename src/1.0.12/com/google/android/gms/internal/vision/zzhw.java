package com.google.android.gms.internal.vision;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzhw<K, V> extends LinkedHashMap<K, V> {
    private static final zzhw zzyv;
    private boolean zzrp = true;

    private zzhw() {
    }

    private zzhw(Map<K, V> map) {
        super(map);
    }

    public static <K, V> zzhw<K, V> zzhc() {
        return zzyv;
    }

    public final void zza(zzhw<K, V> zzhw) {
        zzhe();
        if (!zzhw.isEmpty()) {
            putAll(zzhw);
        }
    }

    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Set<Map.Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    public final void clear() {
        zzhe();
        super.clear();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V put(K k, V v) {
        zzhe();
        zzgt.checkNotNull(k);
        zzgt.checkNotNull(v);
        return (V) super.put(k, v);
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void putAll(Map<? extends K, ? extends V> map) {
        zzhe();
        for (Object obj : map.keySet()) {
            zzgt.checkNotNull(obj);
            zzgt.checkNotNull(map.get(obj));
        }
        super.putAll(map);
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V remove(Object obj) {
        zzhe();
        return (V) super.remove(obj);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[RETURN] */
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this != map) {
                if (size() == map.size()) {
                    Iterator<Map.Entry<K, V>> it = entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Map.Entry<K, V> next = it.next();
                        if (map.containsKey(next.getKey())) {
                            V value = next.getValue();
                            Object obj2 = map.get(next.getKey());
                            if (!(value instanceof byte[]) || !(obj2 instanceof byte[])) {
                                z2 = value.equals(obj2);
                                continue;
                            } else {
                                z2 = Arrays.equals((byte[]) value, (byte[]) obj2);
                                continue;
                            }
                            if (!z2) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                z = false;
                if (!z) {
                    return true;
                }
            }
            z = true;
            if (!z) {
                return false;
            }
        }
        return false;
    }

    private static int zzr(Object obj) {
        if (obj instanceof byte[]) {
            return zzgt.hashCode((byte[]) obj);
        }
        if (!(obj instanceof zzgw)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    public final int hashCode() {
        int i = 0;
        for (Map.Entry<K, V> entry : entrySet()) {
            i += zzr(entry.getValue()) ^ zzr(entry.getKey());
        }
        return i;
    }

    public final zzhw<K, V> zzhd() {
        return isEmpty() ? new zzhw<>() : new zzhw<>(this);
    }

    public final void zzdp() {
        this.zzrp = false;
    }

    public final boolean isMutable() {
        return this.zzrp;
    }

    private final void zzhe() {
        if (!this.zzrp) {
            throw new UnsupportedOperationException();
        }
    }

    static {
        zzhw zzhw = new zzhw();
        zzyv = zzhw;
        zzhw.zzrp = false;
    }
}
