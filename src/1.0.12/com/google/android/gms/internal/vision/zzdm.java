package com.google.android.gms.internal.vision;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
final class zzdm<K> extends zzdj<K> {
    private final transient zzdf<K> zzlq;
    private final transient zzdg<K, ?> zzma;

    zzdm(zzdg<K, ?> zzdg, zzdf<K> zzdf) {
        this.zzma = zzdg;
        this.zzlq = zzdf;
    }

    @Override // com.google.android.gms.internal.vision.zzdc
    public final zzdr<K> zzby() {
        return (zzdr) zzcc().iterator();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzdc
    public final int zza(Object[] objArr, int i) {
        return zzcc().zza(objArr, i);
    }

    @Override // com.google.android.gms.internal.vision.zzdc, com.google.android.gms.internal.vision.zzdj
    public final zzdf<K> zzcc() {
        return this.zzlq;
    }

    @Override // com.google.android.gms.internal.vision.zzdc
    public final boolean contains(@NullableDecl Object obj) {
        return this.zzma.get(obj) != null;
    }

    public final int size() {
        return this.zzma.size();
    }

    @Override // com.google.android.gms.internal.vision.zzdc, java.util.AbstractCollection, java.util.Collection, java.util.Set, com.google.android.gms.internal.vision.zzdj, java.lang.Iterable
    public final /* synthetic */ Iterator iterator() {
        return iterator();
    }
}
