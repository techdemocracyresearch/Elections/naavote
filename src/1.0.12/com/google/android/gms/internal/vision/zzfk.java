package com.google.android.gms.internal.vision;

import java.util.NoSuchElementException;

/* access modifiers changed from: package-private */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.0.2 */
public final class zzfk extends zzfm {
    private final int limit;
    private int position = 0;
    private final /* synthetic */ zzfh zzsg;

    zzfk(zzfh zzfh) {
        this.zzsg = zzfh;
        this.limit = zzfh.size();
    }

    public final boolean hasNext() {
        return this.position < this.limit;
    }

    @Override // com.google.android.gms.internal.vision.zzfq
    public final byte nextByte() {
        int i = this.position;
        if (i < this.limit) {
            this.position = i + 1;
            return this.zzsg.zzap(i);
        }
        throw new NoSuchElementException();
    }
}
