package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-face-contour-internal@@16.0.0 */
final class zzbn implements zzgy {
    static final zzgy zzhf = new zzbn();

    private zzbn() {
    }

    @Override // com.google.android.gms.internal.vision.zzgy
    public final boolean zzg(int i) {
        return zzbl.zzf(i) != null;
    }
}
