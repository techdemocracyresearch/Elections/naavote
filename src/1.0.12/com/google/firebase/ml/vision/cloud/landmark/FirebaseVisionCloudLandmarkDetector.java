package com.google.firebase.ml.vision.cloud.landmark;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzkl;
import com.google.android.gms.internal.firebase_ml.zzkv;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzod;
import com.google.android.gms.internal.firebase_ml.zzqf;
import com.google.android.gms.internal.firebase_ml.zzqg;
import com.google.android.gms.internal.firebase_ml.zzqh;
import com.google.android.gms.internal.firebase_ml.zzrr;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionCloudLandmarkDetector extends zzrr<List<FirebaseVisionCloudLandmark>> {
    private static final Map<zzqh<FirebaseVisionCloudDetectorOptions>, FirebaseVisionCloudLandmarkDetector> zzbim = new HashMap();

    public static synchronized FirebaseVisionCloudLandmarkDetector zza(zzqf zzqf, FirebaseVisionCloudDetectorOptions firebaseVisionCloudDetectorOptions) {
        FirebaseVisionCloudLandmarkDetector firebaseVisionCloudLandmarkDetector;
        synchronized (FirebaseVisionCloudLandmarkDetector.class) {
            Preconditions.checkNotNull(zzqf, "MlKitContext must not be null");
            Preconditions.checkNotNull(zzqf.getPersistenceKey(), "Firebase app name must not be null");
            Preconditions.checkNotNull(firebaseVisionCloudDetectorOptions, "Options must not be null");
            zzqh<FirebaseVisionCloudDetectorOptions> zzj = zzqh.zzj(zzqf.getPersistenceKey(), firebaseVisionCloudDetectorOptions);
            Map<zzqh<FirebaseVisionCloudDetectorOptions>, FirebaseVisionCloudLandmarkDetector> map = zzbim;
            firebaseVisionCloudLandmarkDetector = map.get(zzj);
            if (firebaseVisionCloudLandmarkDetector == null) {
                firebaseVisionCloudLandmarkDetector = new FirebaseVisionCloudLandmarkDetector(zzqf, firebaseVisionCloudDetectorOptions);
                map.put(zzj, firebaseVisionCloudLandmarkDetector);
            }
        }
        return firebaseVisionCloudLandmarkDetector;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzqk() {
        return 640;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final int zzql() {
        return 480;
    }

    private FirebaseVisionCloudLandmarkDetector(zzqf zzqf, FirebaseVisionCloudDetectorOptions firebaseVisionCloudDetectorOptions) {
        super(zzqf, "LANDMARK_DETECTION", firebaseVisionCloudDetectorOptions);
        zzqg.zza(zzqf, 1).zza(zzns.zzad.zzma(), zzod.CLOUD_LANDMARK_CREATE);
    }

    public Task<List<FirebaseVisionCloudLandmark>> detectInImage(FirebaseVisionImage firebaseVisionImage) {
        zzqg.zza(this.zzbkb, 1).zza(zzns.zzad.zzma(), zzod.CLOUD_LANDMARK_DETECT);
        return super.zza(firebaseVisionImage);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.firebase_ml.zzrr
    public final /* synthetic */ List<FirebaseVisionCloudLandmark> zza(zzkl zzkl, float f) {
        if (zzkl.zzik() == null) {
            return new ArrayList();
        }
        float f2 = 1.0f / f;
        List<zzkv> zzik = zzkl.zzik();
        ArrayList arrayList = new ArrayList();
        for (zzkv zzkv : zzik) {
            FirebaseVisionCloudLandmark zza = FirebaseVisionCloudLandmark.zza(zzkv, f2);
            if (zza != null) {
                arrayList.add(zza);
            }
        }
        return arrayList;
    }
}
