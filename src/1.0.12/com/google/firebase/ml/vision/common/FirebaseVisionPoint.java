package com.google.firebase.ml.vision.common;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.internal.firebase_ml.zzmb;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public final class FirebaseVisionPoint {
    private final Float zzbsc;
    private final Float zzbsd;
    private final Float zzbse = null;

    public final Float getX() {
        return this.zzbsc;
    }

    public final Float getZ() {
        return null;
    }

    public final Float getY() {
        return this.zzbsd;
    }

    public FirebaseVisionPoint(Float f, Float f2, Float f3) {
        this.zzbsc = f;
        this.zzbsd = f2;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof FirebaseVisionPoint)) {
            return false;
        }
        FirebaseVisionPoint firebaseVisionPoint = (FirebaseVisionPoint) obj;
        return Objects.equal(this.zzbsc, firebaseVisionPoint.zzbsc) && Objects.equal(this.zzbsd, firebaseVisionPoint.zzbsd) && Objects.equal(null, null);
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzbsc, this.zzbsd, null);
    }

    public final String toString() {
        return zzmb.zzaz("FirebaseVisionPoint").zzh("x", this.zzbsc).zzh("y", this.zzbsd).zzh("z", null).toString();
    }
}
