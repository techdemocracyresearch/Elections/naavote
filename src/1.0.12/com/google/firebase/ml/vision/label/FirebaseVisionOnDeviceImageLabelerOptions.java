package com.google.firebase.ml.vision.label;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_ml.zzns;
import com.google.android.gms.internal.firebase_ml.zzwz;

/* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
public class FirebaseVisionOnDeviceImageLabelerOptions {
    private final float confidenceThreshold;

    public float getConfidenceThreshold() {
        return this.confidenceThreshold;
    }

    private FirebaseVisionOnDeviceImageLabelerOptions(float f) {
        this.confidenceThreshold = f;
    }

    /* compiled from: com.google.firebase:firebase-ml-vision@@24.0.3 */
    public static class Builder {
        private float confidenceThreshold = 0.5f;

        public Builder setConfidenceThreshold(float f) {
            Preconditions.checkArgument(Float.compare(f, 0.0f) >= 0 && Float.compare(f, 1.0f) <= 0, "Confidence Threshold should be in range [0.0f, 1.0f].");
            this.confidenceThreshold = f;
            return this;
        }

        public FirebaseVisionOnDeviceImageLabelerOptions build() {
            return new FirebaseVisionOnDeviceImageLabelerOptions(this.confidenceThreshold);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof FirebaseVisionOnDeviceImageLabelerOptions) && this.confidenceThreshold == ((FirebaseVisionOnDeviceImageLabelerOptions) obj).confidenceThreshold;
    }

    public int hashCode() {
        return Objects.hashCode(Float.valueOf(this.confidenceThreshold));
    }

    public final zzns.zzap zzqv() {
        return (zzns.zzap) ((zzwz) zzns.zzap.zzmz().zzp(this.confidenceThreshold).zzvb());
    }
}
