package com.google.firebase.ml.common.internal.modeldownload;

import com.google.firebase.ml.common.FirebaseMLException;
import java.nio.MappedByteBuffer;

/* compiled from: com.google.firebase:firebase-ml-common@@22.1.1 */
public interface zzaf {
    MappedByteBuffer zzbz(String str) throws FirebaseMLException;
}
