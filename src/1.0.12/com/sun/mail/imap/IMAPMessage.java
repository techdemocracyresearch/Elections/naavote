package com.sun.mail.imap;

import com.sun.mail.iap.CommandFailedException;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.Utility;
import com.sun.mail.imap.protocol.BODY;
import com.sun.mail.imap.protocol.BODYSTRUCTURE;
import com.sun.mail.imap.protocol.ENVELOPE;
import com.sun.mail.imap.protocol.FetchResponse;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.INTERNALDATE;
import com.sun.mail.imap.protocol.Item;
import com.sun.mail.imap.protocol.MessageSet;
import com.sun.mail.imap.protocol.RFC822DATA;
import com.sun.mail.imap.protocol.RFC822SIZE;
import com.sun.mail.imap.protocol.UID;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;
import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.FolderClosedException;
import javax.mail.Header;
import javax.mail.IllegalWriteException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.UIDFolder;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import okhttp3.internal.cache.DiskLruCache;

public class IMAPMessage extends MimeMessage {
    private static String EnvelopeCmd = "ENVELOPE INTERNALDATE RFC822.SIZE";
    protected BODYSTRUCTURE bs;
    private String description;
    protected ENVELOPE envelope;
    private boolean headersLoaded = false;
    private Hashtable loadedHeaders;
    private boolean peek;
    private Date receivedDate;
    protected String sectionId;
    private int seqnum;
    private int size = -1;
    private String subject;
    private String type;
    private long uid = -1;

    protected IMAPMessage(IMAPFolder iMAPFolder, int i, int i2) {
        super(iMAPFolder, i);
        this.seqnum = i2;
        this.flags = null;
    }

    protected IMAPMessage(Session session) {
        super(session);
    }

    /* access modifiers changed from: protected */
    public IMAPProtocol getProtocol() throws ProtocolException, FolderClosedException {
        ((IMAPFolder) this.folder).waitIfIdle();
        IMAPProtocol iMAPProtocol = ((IMAPFolder) this.folder).protocol;
        if (iMAPProtocol != null) {
            return iMAPProtocol;
        }
        throw new FolderClosedException(this.folder);
    }

    /* access modifiers changed from: protected */
    public boolean isREV1() throws FolderClosedException {
        IMAPProtocol iMAPProtocol = ((IMAPFolder) this.folder).protocol;
        if (iMAPProtocol != null) {
            return iMAPProtocol.isREV1();
        }
        throw new FolderClosedException(this.folder);
    }

    /* access modifiers changed from: protected */
    public Object getMessageCacheLock() {
        return ((IMAPFolder) this.folder).messageCacheLock;
    }

    /* access modifiers changed from: protected */
    public int getSequenceNumber() {
        return this.seqnum;
    }

    /* access modifiers changed from: protected */
    public void setSequenceNumber(int i) {
        this.seqnum = i;
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Message
    public void setMessageNumber(int i) {
        super.setMessageNumber(i);
    }

    /* access modifiers changed from: protected */
    public long getUID() {
        return this.uid;
    }

    /* access modifiers changed from: protected */
    public void setUID(long j) {
        this.uid = j;
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Message
    public void setExpunged(boolean z) {
        super.setExpunged(z);
        this.seqnum = -1;
    }

    /* access modifiers changed from: protected */
    public void checkExpunged() throws MessageRemovedException {
        if (this.expunged) {
            throw new MessageRemovedException();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x000f */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0015  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0014 A[RETURN] */
    public void forceCheckExpunged() throws MessageRemovedException, FolderClosedException {
        synchronized (getMessageCacheLock()) {
            try {
                getProtocol().noop();
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException unknown) {
                if (!this.expunged) {
                }
            }
        }
        if (!this.expunged) {
            throw new MessageRemovedException();
        }
    }

    /* access modifiers changed from: protected */
    public int getFetchBlockSize() {
        return ((IMAPStore) this.folder.getStore()).getFetchBlockSize();
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public Address[] getFrom() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return aaclone(this.envelope.from);
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public void setFrom(Address address) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public void addFrom(Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.internet.MimeMessage
    public Address getSender() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (this.envelope.sender != null) {
            return this.envelope.sender[0];
        }
        return null;
    }

    @Override // javax.mail.internet.MimeMessage
    public void setSender(Address address) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public Address[] getRecipients(Message.RecipientType recipientType) throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (recipientType == Message.RecipientType.TO) {
            return aaclone(this.envelope.to);
        }
        if (recipientType == Message.RecipientType.CC) {
            return aaclone(this.envelope.cc);
        }
        if (recipientType == Message.RecipientType.BCC) {
            return aaclone(this.envelope.bcc);
        }
        return super.getRecipients(recipientType);
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public void setRecipients(Message.RecipientType recipientType, Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public void addRecipients(Message.RecipientType recipientType, Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public Address[] getReplyTo() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return aaclone(this.envelope.replyTo);
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public void setReplyTo(Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public String getSubject() throws MessagingException {
        checkExpunged();
        String str = this.subject;
        if (str != null) {
            return str;
        }
        loadEnvelope();
        if (this.envelope.subject == null) {
            return null;
        }
        try {
            this.subject = MimeUtility.decodeText(this.envelope.subject);
        } catch (UnsupportedEncodingException unused) {
            this.subject = this.envelope.subject;
        }
        return this.subject;
    }

    @Override // javax.mail.internet.MimeMessage
    public void setSubject(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public Date getSentDate() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (this.envelope.date == null) {
            return null;
        }
        return new Date(this.envelope.date.getTime());
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public void setSentDate(Date date) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public Date getReceivedDate() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (this.receivedDate == null) {
            return null;
        }
        return new Date(this.receivedDate.getTime());
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public int getSize() throws MessagingException {
        checkExpunged();
        if (this.size == -1) {
            loadEnvelope();
        }
        return this.size;
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public int getLineCount() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.lines;
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public String[] getContentLanguage() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        if (this.bs.language != null) {
            return (String[]) this.bs.language.clone();
        }
        return null;
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public void setContentLanguage(String[] strArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public String getInReplyTo() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return this.envelope.inReplyTo;
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public String getContentType() throws MessagingException {
        checkExpunged();
        if (this.type == null) {
            loadBODYSTRUCTURE();
            this.type = new ContentType(this.bs.type, this.bs.subtype, this.bs.cParams).toString();
        }
        return this.type;
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public String getDisposition() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.disposition;
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public void setDisposition(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public String getEncoding() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.encoding;
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public String getContentID() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.id;
    }

    @Override // javax.mail.internet.MimeMessage
    public void setContentID(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public String getContentMD5() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.md5;
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public void setContentMD5(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public String getDescription() throws MessagingException {
        checkExpunged();
        String str = this.description;
        if (str != null) {
            return str;
        }
        loadBODYSTRUCTURE();
        if (this.bs.description == null) {
            return null;
        }
        try {
            this.description = MimeUtility.decodeText(this.bs.description);
        } catch (UnsupportedEncodingException unused) {
            this.description = this.bs.description;
        }
        return this.description;
    }

    @Override // javax.mail.internet.MimeMessage
    public void setDescription(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.internet.MimeMessage
    public String getMessageID() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return this.envelope.messageId;
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public String getFileName() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        String str = this.bs.dParams != null ? this.bs.dParams.get("filename") : null;
        return (str != null || this.bs.cParams == null) ? str : this.bs.cParams.get("name");
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public void setFileName(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006f, code lost:
        if (r0 == null) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0071, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0079, code lost:
        throw new javax.mail.MessagingException("No content");
     */
    @Override // javax.mail.internet.MimeMessage
    public InputStream getContentStream() throws MessagingException {
        ByteArrayInputStream byteArrayInputStream;
        BODY body;
        boolean peek2 = getPeek();
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                if (protocol.isREV1()) {
                    int i = -1;
                    if (getFetchBlockSize() != -1) {
                        String section = toSection("TEXT");
                        BODYSTRUCTURE bodystructure = this.bs;
                        if (bodystructure != null) {
                            i = bodystructure.size;
                        }
                        return new IMAPInputStream(this, section, i, peek2);
                    }
                }
                if (protocol.isREV1()) {
                    if (peek2) {
                        body = protocol.peekBody(getSequenceNumber(), toSection("TEXT"));
                    } else {
                        body = protocol.fetchBody(getSequenceNumber(), toSection("TEXT"));
                    }
                    if (body != null) {
                        byteArrayInputStream = body.getByteArrayInputStream();
                    }
                } else {
                    RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), "TEXT");
                    if (fetchRFC822 != null) {
                        byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                    }
                }
                byteArrayInputStream = null;
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                forceCheckExpunged();
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public synchronized DataHandler getDataHandler() throws MessagingException {
        String str;
        checkExpunged();
        if (this.dh == null) {
            loadBODYSTRUCTURE();
            if (this.type == null) {
                this.type = new ContentType(this.bs.type, this.bs.subtype, this.bs.cParams).toString();
            }
            if (this.bs.isMulti()) {
                this.dh = new DataHandler(new IMAPMultipartDataSource(this, this.bs.bodies, this.sectionId, this));
            } else if (this.bs.isNested() && isREV1()) {
                BODYSTRUCTURE bodystructure = this.bs.bodies[0];
                ENVELOPE envelope2 = this.bs.envelope;
                String str2 = this.sectionId;
                if (str2 == null) {
                    str = DiskLruCache.VERSION_1;
                } else {
                    str = String.valueOf(str2) + ".1";
                }
                this.dh = new DataHandler(new IMAPNestedMessage(this, bodystructure, envelope2, str), this.type);
            }
        }
        return super.getDataHandler();
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public void setDataHandler(DataHandler dataHandler) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public void writeTo(OutputStream outputStream) throws IOException, MessagingException {
        ByteArrayInputStream byteArrayInputStream;
        BODY body;
        boolean peek2 = getPeek();
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                byteArrayInputStream = null;
                if (protocol.isREV1()) {
                    if (peek2) {
                        body = protocol.peekBody(getSequenceNumber(), this.sectionId);
                    } else {
                        body = protocol.fetchBody(getSequenceNumber(), this.sectionId);
                    }
                    if (body != null) {
                        byteArrayInputStream = body.getByteArrayInputStream();
                    }
                } else {
                    RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), null);
                    if (fetchRFC822 != null) {
                        byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                    }
                }
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                forceCheckExpunged();
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
        if (byteArrayInputStream != null) {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = byteArrayInputStream.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } else {
            throw new MessagingException("No content");
        }
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public String[] getHeader(String str) throws MessagingException {
        ByteArrayInputStream byteArrayInputStream;
        checkExpunged();
        if (isHeaderLoaded(str)) {
            return this.headers.getHeader(str);
        }
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                if (protocol.isREV1()) {
                    BODY peekBody = protocol.peekBody(getSequenceNumber(), toSection("HEADER.FIELDS (" + str + ")"));
                    if (peekBody != null) {
                        byteArrayInputStream = peekBody.getByteArrayInputStream();
                    }
                } else {
                    RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), "HEADER.LINES (" + str + ")");
                    if (fetchRFC822 != null) {
                        byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                    }
                }
                byteArrayInputStream = null;
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                forceCheckExpunged();
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
        if (byteArrayInputStream == null) {
            return null;
        }
        if (this.headers == null) {
            this.headers = new InternetHeaders();
        }
        this.headers.load(byteArrayInputStream);
        setHeaderLoaded(str);
        return this.headers.getHeader(str);
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public String getHeader(String str, String str2) throws MessagingException {
        checkExpunged();
        if (getHeader(str) == null) {
            return null;
        }
        return this.headers.getHeader(str, str2);
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public void setHeader(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public void addHeader(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public void removeHeader(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public Enumeration getAllHeaders() throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getAllHeaders();
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public Enumeration getMatchingHeaders(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getMatchingHeaders(strArr);
    }

    @Override // javax.mail.Part, javax.mail.internet.MimeMessage
    public Enumeration getNonMatchingHeaders(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getNonMatchingHeaders(strArr);
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public void addHeaderLine(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public Enumeration getAllHeaderLines() throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getAllHeaderLines();
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public Enumeration getMatchingHeaderLines(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getMatchingHeaderLines(strArr);
    }

    @Override // javax.mail.internet.MimePart, javax.mail.internet.MimeMessage
    public Enumeration getNonMatchingHeaderLines(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getNonMatchingHeaderLines(strArr);
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public synchronized Flags getFlags() throws MessagingException {
        checkExpunged();
        loadFlags();
        return super.getFlags();
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public synchronized boolean isSet(Flags.Flag flag) throws MessagingException {
        checkExpunged();
        loadFlags();
        return super.isSet(flag);
    }

    @Override // javax.mail.Message, javax.mail.internet.MimeMessage
    public synchronized void setFlags(Flags flags, boolean z) throws MessagingException {
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                protocol.storeFlags(getSequenceNumber(), flags, z);
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
    }

    public synchronized void setPeek(boolean z) {
        this.peek = z;
    }

    public synchronized boolean getPeek() {
        return this.peek;
    }

    public synchronized void invalidateHeaders() {
        this.headersLoaded = false;
        this.loadedHeaders = null;
        this.envelope = null;
        this.bs = null;
        this.receivedDate = null;
        this.size = -1;
        this.type = null;
        this.subject = null;
        this.description = null;
    }

    static void fetch(IMAPFolder iMAPFolder, Message[] messageArr, FetchProfile fetchProfile) throws MessagingException {
        boolean z;
        boolean z2;
        int i;
        FetchResponse fetchResponse;
        ByteArrayInputStream byteArrayInputStream;
        StringBuffer stringBuffer = new StringBuffer();
        if (fetchProfile.contains(FetchProfile.Item.ENVELOPE)) {
            stringBuffer.append(EnvelopeCmd);
            z = false;
        } else {
            z = true;
        }
        if (fetchProfile.contains(FetchProfile.Item.FLAGS)) {
            stringBuffer.append(z ? "FLAGS" : " FLAGS");
            z = false;
        }
        if (fetchProfile.contains(FetchProfile.Item.CONTENT_INFO)) {
            stringBuffer.append(z ? "BODYSTRUCTURE" : " BODYSTRUCTURE");
            z = false;
        }
        if (fetchProfile.contains(UIDFolder.FetchProfileItem.UID)) {
            stringBuffer.append(z ? "UID" : " UID");
            z = false;
        }
        if (fetchProfile.contains(IMAPFolder.FetchProfileItem.HEADERS)) {
            if (iMAPFolder.protocol.isREV1()) {
                stringBuffer.append(z ? "BODY.PEEK[HEADER]" : " BODY.PEEK[HEADER]");
            } else {
                stringBuffer.append(z ? "RFC822.HEADER" : " RFC822.HEADER");
            }
            z = false;
            z2 = true;
        } else {
            z2 = false;
        }
        if (fetchProfile.contains(IMAPFolder.FetchProfileItem.SIZE)) {
            stringBuffer.append(z ? "RFC822.SIZE" : " RFC822.SIZE");
            z = false;
        }
        String[] strArr = null;
        if (!z2) {
            strArr = fetchProfile.getHeaderNames();
            if (strArr.length > 0) {
                if (!z) {
                    stringBuffer.append(" ");
                }
                stringBuffer.append(craftHeaderCmd(iMAPFolder.protocol, strArr));
            }
        }
        AnonymousClass1FetchProfileCondition r3 = new Utility.Condition(fetchProfile) {
            /* class com.sun.mail.imap.IMAPMessage.AnonymousClass1FetchProfileCondition */
            private String[] hdrs = null;
            private boolean needBodyStructure = false;
            private boolean needEnvelope = false;
            private boolean needFlags = false;
            private boolean needHeaders = false;
            private boolean needSize = false;
            private boolean needUID = false;

            {
                if (r3.contains(FetchProfile.Item.ENVELOPE)) {
                    this.needEnvelope = true;
                }
                if (r3.contains(FetchProfile.Item.FLAGS)) {
                    this.needFlags = true;
                }
                if (r3.contains(FetchProfile.Item.CONTENT_INFO)) {
                    this.needBodyStructure = true;
                }
                if (r3.contains(UIDFolder.FetchProfileItem.UID)) {
                    this.needUID = true;
                }
                if (r3.contains(IMAPFolder.FetchProfileItem.HEADERS)) {
                    this.needHeaders = true;
                }
                if (r3.contains(IMAPFolder.FetchProfileItem.SIZE)) {
                    this.needSize = true;
                }
                this.hdrs = r3.getHeaderNames();
            }

            @Override // com.sun.mail.imap.Utility.Condition
            public boolean test(IMAPMessage iMAPMessage) {
                if (this.needEnvelope && iMAPMessage._getEnvelope() == null) {
                    return true;
                }
                if (this.needFlags && iMAPMessage._getFlags() == null) {
                    return true;
                }
                if (this.needBodyStructure && iMAPMessage._getBodyStructure() == null) {
                    return true;
                }
                if (this.needUID && iMAPMessage.getUID() == -1) {
                    return true;
                }
                if (this.needHeaders && !iMAPMessage.areHeadersLoaded()) {
                    return true;
                }
                if (this.needSize && iMAPMessage.size == -1) {
                    return true;
                }
                int i = 0;
                while (true) {
                    String[] strArr = this.hdrs;
                    if (i >= strArr.length) {
                        return false;
                    }
                    if (!iMAPMessage.isHeaderLoaded(strArr[i])) {
                        return true;
                    }
                    i++;
                }
            }
        };
        synchronized (iMAPFolder.messageCacheLock) {
            MessageSet[] messageSet = Utility.toMessageSet(messageArr, r3);
            if (messageSet != null) {
                Response[] responseArr = null;
                Vector vector = new Vector();
                try {
                    responseArr = iMAPFolder.protocol.fetch(messageSet, stringBuffer.toString());
                } catch (ConnectionException e) {
                    throw new FolderClosedException(iMAPFolder, e.getMessage());
                } catch (CommandFailedException unused) {
                } catch (ProtocolException e2) {
                    throw new MessagingException(e2.getMessage(), e2);
                }
                if (responseArr != null) {
                    int i2 = 0;
                    while (i2 < responseArr.length) {
                        if (responseArr[i2] != null) {
                            if (!(responseArr[i2] instanceof FetchResponse)) {
                                vector.addElement(responseArr[i2]);
                            } else {
                                FetchResponse fetchResponse2 = (FetchResponse) responseArr[i2];
                                IMAPMessage messageBySeqNumber = iMAPFolder.getMessageBySeqNumber(fetchResponse2.getNumber());
                                int itemCount = fetchResponse2.getItemCount();
                                int i3 = 0;
                                boolean z3 = false;
                                while (i3 < itemCount) {
                                    Item item = fetchResponse2.getItem(i3);
                                    if (item instanceof Flags) {
                                        if (!fetchProfile.contains(FetchProfile.Item.FLAGS) || messageBySeqNumber == null) {
                                            i = i2;
                                            fetchResponse = fetchResponse2;
                                            z3 = true;
                                            i3++;
                                            i2 = i;
                                            fetchResponse2 = fetchResponse;
                                        } else {
                                            messageBySeqNumber.flags = (Flags) item;
                                        }
                                    } else if (item instanceof ENVELOPE) {
                                        messageBySeqNumber.envelope = (ENVELOPE) item;
                                    } else if (item instanceof INTERNALDATE) {
                                        messageBySeqNumber.receivedDate = ((INTERNALDATE) item).getDate();
                                    } else if (item instanceof RFC822SIZE) {
                                        messageBySeqNumber.size = ((RFC822SIZE) item).size;
                                    } else if (item instanceof BODYSTRUCTURE) {
                                        messageBySeqNumber.bs = (BODYSTRUCTURE) item;
                                    } else {
                                        if (item instanceof UID) {
                                            UID uid2 = (UID) item;
                                            messageBySeqNumber.uid = uid2.uid;
                                            if (iMAPFolder.uidTable == null) {
                                                iMAPFolder.uidTable = new Hashtable();
                                            }
                                            i = i2;
                                            fetchResponse = fetchResponse2;
                                            iMAPFolder.uidTable.put(new Long(uid2.uid), messageBySeqNumber);
                                        } else {
                                            i = i2;
                                            fetchResponse = fetchResponse2;
                                            if ((item instanceof RFC822DATA) || (item instanceof BODY)) {
                                                if (item instanceof RFC822DATA) {
                                                    byteArrayInputStream = ((RFC822DATA) item).getByteArrayInputStream();
                                                } else {
                                                    byteArrayInputStream = ((BODY) item).getByteArrayInputStream();
                                                }
                                                InternetHeaders internetHeaders = new InternetHeaders();
                                                internetHeaders.load(byteArrayInputStream);
                                                if (messageBySeqNumber.headers == null || z2) {
                                                    messageBySeqNumber.headers = internetHeaders;
                                                } else {
                                                    Enumeration allHeaders = internetHeaders.getAllHeaders();
                                                    while (allHeaders.hasMoreElements()) {
                                                        Header header = (Header) allHeaders.nextElement();
                                                        if (!messageBySeqNumber.isHeaderLoaded(header.getName())) {
                                                            messageBySeqNumber.headers.addHeader(header.getName(), header.getValue());
                                                        }
                                                    }
                                                }
                                                if (z2) {
                                                    messageBySeqNumber.setHeadersLoaded(true);
                                                } else {
                                                    for (String str : strArr) {
                                                        messageBySeqNumber.setHeaderLoaded(str);
                                                    }
                                                }
                                                i3++;
                                                i2 = i;
                                                fetchResponse2 = fetchResponse;
                                            }
                                        }
                                        i3++;
                                        i2 = i;
                                        fetchResponse2 = fetchResponse;
                                    }
                                    i = i2;
                                    fetchResponse = fetchResponse2;
                                    i3++;
                                    i2 = i;
                                    fetchResponse2 = fetchResponse;
                                }
                                if (z3) {
                                    vector.addElement(fetchResponse2);
                                }
                            }
                        }
                        i2++;
                    }
                    int size2 = vector.size();
                    if (size2 != 0) {
                        Response[] responseArr2 = new Response[size2];
                        vector.copyInto(responseArr2);
                        iMAPFolder.handleResponses(responseArr2);
                    }
                }
            }
        }
    }

    private synchronized void loadEnvelope() throws MessagingException {
        if (this.envelope == null) {
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    int sequenceNumber = getSequenceNumber();
                    Response[] fetch = protocol.fetch(sequenceNumber, EnvelopeCmd);
                    for (int i = 0; i < fetch.length; i++) {
                        if (fetch[i] != null && (fetch[i] instanceof FetchResponse) && ((FetchResponse) fetch[i]).getNumber() == sequenceNumber) {
                            FetchResponse fetchResponse = (FetchResponse) fetch[i];
                            int itemCount = fetchResponse.getItemCount();
                            for (int i2 = 0; i2 < itemCount; i2++) {
                                Item item = fetchResponse.getItem(i2);
                                if (item instanceof ENVELOPE) {
                                    this.envelope = (ENVELOPE) item;
                                } else if (item instanceof INTERNALDATE) {
                                    this.receivedDate = ((INTERNALDATE) item).getDate();
                                } else if (item instanceof RFC822SIZE) {
                                    this.size = ((RFC822SIZE) item).size;
                                }
                            }
                        }
                    }
                    protocol.notifyResponseHandlers(fetch);
                    protocol.handleResult(fetch[fetch.length - 1]);
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
            if (this.envelope == null) {
                throw new MessagingException("Failed to load IMAP envelope");
            }
        }
    }

    private static String craftHeaderCmd(IMAPProtocol iMAPProtocol, String[] strArr) {
        StringBuffer stringBuffer;
        if (iMAPProtocol.isREV1()) {
            stringBuffer = new StringBuffer("BODY.PEEK[HEADER.FIELDS (");
        } else {
            stringBuffer = new StringBuffer("RFC822.HEADER.LINES (");
        }
        for (int i = 0; i < strArr.length; i++) {
            if (i > 0) {
                stringBuffer.append(" ");
            }
            stringBuffer.append(strArr[i]);
        }
        if (iMAPProtocol.isREV1()) {
            stringBuffer.append(")]");
        } else {
            stringBuffer.append(")");
        }
        return stringBuffer.toString();
    }

    private synchronized void loadBODYSTRUCTURE() throws MessagingException {
        if (this.bs == null) {
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    BODYSTRUCTURE fetchBodyStructure = protocol.fetchBodyStructure(getSequenceNumber());
                    this.bs = fetchBodyStructure;
                    if (fetchBodyStructure == null) {
                        forceCheckExpunged();
                        throw new MessagingException("Unable to load BODYSTRUCTURE");
                    }
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
        }
    }

    private synchronized void loadHeaders() throws MessagingException {
        if (!this.headersLoaded) {
            ByteArrayInputStream byteArrayInputStream = null;
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    if (protocol.isREV1()) {
                        BODY peekBody = protocol.peekBody(getSequenceNumber(), toSection("HEADER"));
                        if (peekBody != null) {
                            byteArrayInputStream = peekBody.getByteArrayInputStream();
                        }
                    } else {
                        RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), "HEADER");
                        if (fetchRFC822 != null) {
                            byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                        }
                    }
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
            if (byteArrayInputStream != null) {
                this.headers = new InternetHeaders(byteArrayInputStream);
                this.headersLoaded = true;
                return;
            }
            throw new MessagingException("Cannot load header");
        }
    }

    private synchronized void loadFlags() throws MessagingException {
        if (this.flags == null) {
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    this.flags = protocol.fetchFlags(getSequenceNumber());
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private synchronized boolean areHeadersLoaded() {
        return this.headersLoaded;
    }

    private synchronized void setHeadersLoaded(boolean z) {
        this.headersLoaded = z;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private synchronized boolean isHeaderLoaded(String str) {
        if (this.headersLoaded) {
            return true;
        }
        Hashtable hashtable = this.loadedHeaders;
        return hashtable != null ? hashtable.containsKey(str.toUpperCase(Locale.ENGLISH)) : false;
    }

    private synchronized void setHeaderLoaded(String str) {
        if (this.loadedHeaders == null) {
            this.loadedHeaders = new Hashtable(1);
        }
        this.loadedHeaders.put(str.toUpperCase(Locale.ENGLISH), str);
    }

    private String toSection(String str) {
        String str2 = this.sectionId;
        if (str2 == null) {
            return str;
        }
        return String.valueOf(str2) + "." + str;
    }

    private InternetAddress[] aaclone(InternetAddress[] internetAddressArr) {
        if (internetAddressArr == null) {
            return null;
        }
        return (InternetAddress[]) internetAddressArr.clone();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private Flags _getFlags() {
        return this.flags;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private ENVELOPE _getEnvelope() {
        return this.envelope;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private BODYSTRUCTURE _getBodyStructure() {
        return this.bs;
    }

    /* access modifiers changed from: package-private */
    public void _setFlags(Flags flags) {
        this.flags = flags;
    }

    /* access modifiers changed from: package-private */
    public Session _getSession() {
        return this.session;
    }
}
