package com.sun.mail.pop3;

import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.util.Properties;
import javax.mail.AuthenticationFailedException;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;

public class POP3Store extends Store {
    private int defaultPort;
    boolean disableTop;
    boolean forgetTopHeaders;
    private String host;
    private boolean isSSL;
    Constructor messageConstructor;
    private String name;
    private String passwd;
    private Protocol port;
    private int portNum;
    private POP3Folder portOwner;
    boolean rsetBeforeQuit;
    private String user;

    public POP3Store(Session session, URLName uRLName) {
        this(session, uRLName, "pop3", 110, false);
    }

    public POP3Store(Session session, URLName uRLName, String str, int i, boolean z) {
        super(session, uRLName);
        Class<?> cls;
        this.name = "pop3";
        this.defaultPort = 110;
        this.isSSL = false;
        this.port = null;
        this.portOwner = null;
        this.host = null;
        this.portNum = -1;
        this.user = null;
        this.passwd = null;
        this.rsetBeforeQuit = false;
        this.disableTop = false;
        this.forgetTopHeaders = false;
        this.messageConstructor = null;
        str = uRLName != null ? uRLName.getProtocol() : str;
        this.name = str;
        this.defaultPort = i;
        this.isSSL = z;
        String property = session.getProperty("mail." + str + ".rsetbeforequit");
        if (property != null && property.equalsIgnoreCase("true")) {
            this.rsetBeforeQuit = true;
        }
        String property2 = session.getProperty("mail." + str + ".disabletop");
        if (property2 != null && property2.equalsIgnoreCase("true")) {
            this.disableTop = true;
        }
        String property3 = session.getProperty("mail." + str + ".forgettopheaders");
        if (property3 != null && property3.equalsIgnoreCase("true")) {
            this.forgetTopHeaders = true;
        }
        String property4 = session.getProperty("mail." + str + ".message.class");
        if (property4 != null) {
            if (session.getDebug()) {
                PrintStream debugOut = session.getDebugOut();
                debugOut.println("DEBUG: POP3 message class: " + property4);
            }
            try {
                try {
                    cls = getClass().getClassLoader().loadClass(property4);
                } catch (ClassNotFoundException unused) {
                    cls = Class.forName(property4);
                }
                this.messageConstructor = cls.getConstructor(Folder.class, Integer.TYPE);
            } catch (Exception e) {
                if (session.getDebug()) {
                    PrintStream debugOut2 = session.getDebugOut();
                    debugOut2.println("DEBUG: failed to load POP3 message class: " + e);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Service
    public synchronized boolean protocolConnect(String str, int i, String str2, String str3) throws MessagingException {
        if (str == null || str3 == null || str2 == null) {
            return false;
        }
        if (i == -1) {
            try {
                Session session = this.session;
                String property = session.getProperty("mail." + this.name + ".port");
                if (property != null) {
                    i = Integer.parseInt(property);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        if (i == -1) {
            i = this.defaultPort;
        }
        this.host = str;
        this.portNum = i;
        this.user = str2;
        this.passwd = str3;
        try {
            this.port = getPort(null);
            return true;
        } catch (EOFException e) {
            throw new AuthenticationFailedException(e.getMessage());
        } catch (IOException e2) {
            throw new MessagingException("Connect failed", e2);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:19|20|21|23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        super.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0025, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0020 */
    @Override // javax.mail.Service
    public synchronized boolean isConnected() {
        if (!super.isConnected()) {
            return false;
        }
        synchronized (this) {
            Protocol protocol = this.port;
            if (protocol == null) {
                this.port = getPort(null);
            } else {
                protocol.noop();
            }
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized Protocol getPort(POP3Folder pOP3Folder) throws IOException {
        Protocol protocol = this.port;
        if (protocol == null || this.portOwner != null) {
            String str = this.host;
            int i = this.portNum;
            boolean debug = this.session.getDebug();
            PrintStream debugOut = this.session.getDebugOut();
            Properties properties = this.session.getProperties();
            Protocol protocol2 = new Protocol(str, i, debug, debugOut, properties, "mail." + this.name, this.isSSL);
            String login = protocol2.login(this.user, this.passwd);
            if (login == null) {
                if (this.port == null && pOP3Folder != null) {
                    this.port = protocol2;
                    this.portOwner = pOP3Folder;
                }
                if (this.portOwner == null) {
                    this.portOwner = pOP3Folder;
                }
                return protocol2;
            }
            try {
                protocol2.quit();
            } catch (IOException unused) {
            }
            throw new EOFException(login);
        }
        this.portOwner = pOP3Folder;
        return protocol;
    }

    /* access modifiers changed from: package-private */
    public synchronized void closePort(POP3Folder pOP3Folder) {
        if (this.portOwner == pOP3Folder) {
            this.port = null;
            this.portOwner = null;
        }
    }

    @Override // javax.mail.Service
    public synchronized void close() throws MessagingException {
        try {
            Protocol protocol = this.port;
            if (protocol != null) {
                protocol.quit();
            }
            this.port = null;
        } catch (IOException unused) {
            this.port = null;
        } catch (Throwable th) {
            throw th;
        }
        super.close();
    }

    @Override // javax.mail.Store
    public Folder getDefaultFolder() throws MessagingException {
        checkConnected();
        return new DefaultFolder(this);
    }

    @Override // javax.mail.Store
    public Folder getFolder(String str) throws MessagingException {
        checkConnected();
        return new POP3Folder(this, str);
    }

    @Override // javax.mail.Store
    public Folder getFolder(URLName uRLName) throws MessagingException {
        checkConnected();
        return new POP3Folder(this, uRLName.getFile());
    }

    /* access modifiers changed from: protected */
    @Override // javax.mail.Service
    public void finalize() throws Throwable {
        super.finalize();
        if (this.port != null) {
            close();
        }
    }

    private void checkConnected() throws MessagingException {
        if (!super.isConnected()) {
            throw new MessagingException("Not connected");
        }
    }
}
