package com.swmansion.rnscreens;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import com.facebook.react.ReactApplication;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.views.text.ReactFontManager;
import com.swmansion.rnscreens.ScreenStackHeaderSubview;
import java.util.ArrayList;

public class ScreenStackHeaderConfig extends ViewGroup {
    private boolean mBackButtonInCustomView;
    private View.OnClickListener mBackClickListener = new View.OnClickListener() {
        /* class com.swmansion.rnscreens.ScreenStackHeaderConfig.AnonymousClass1 */

        public void onClick(View view) {
            ScreenStackFragment screenFragment = ScreenStackHeaderConfig.this.getScreenFragment();
            if (screenFragment != null) {
                ScreenStack screenStack = ScreenStackHeaderConfig.this.getScreenStack();
                if (screenStack == null || screenStack.getRootScreen() != screenFragment.getScreen()) {
                    screenFragment.dismiss();
                    return;
                }
                Fragment parentFragment = screenFragment.getParentFragment();
                if (parentFragment instanceof ScreenStackFragment) {
                    ((ScreenStackFragment) parentFragment).dismiss();
                }
            }
        }
    };
    private Integer mBackgroundColor;
    private final ArrayList<ScreenStackHeaderSubview> mConfigSubviews = new ArrayList<>(3);
    private int mDefaultStartInset;
    private int mDefaultStartInsetWithNavigation;
    private boolean mDestroyed;
    private String mDirection;
    private boolean mIsAttachedToWindow = false;
    private boolean mIsBackButtonHidden;
    private boolean mIsHidden;
    private boolean mIsShadowHidden;
    private boolean mIsTopInsetEnabled = true;
    private boolean mIsTranslucent;
    private int mScreenOrientation = -1;
    private int mTintColor;
    private String mTitle;
    private int mTitleColor;
    private String mTitleFontFamily;
    private float mTitleFontSize;
    private final Toolbar mToolbar;

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    private static class DebugMenuToolbar extends Toolbar {
        public DebugMenuToolbar(Context context) {
            super(context);
        }

        @Override // androidx.appcompat.widget.Toolbar
        public boolean showOverflowMenu() {
            ((ReactApplication) getContext().getApplicationContext()).getReactNativeHost().getReactInstanceManager().showDevOptionsDialog();
            return true;
        }
    }

    public ScreenStackHeaderConfig(Context context) {
        super(context);
        setVisibility(8);
        Toolbar toolbar = new Toolbar(context);
        this.mToolbar = toolbar;
        this.mDefaultStartInset = toolbar.getContentInsetStart();
        this.mDefaultStartInsetWithNavigation = toolbar.getContentInsetStartWithNavigation();
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(16843827, typedValue, true)) {
            toolbar.setBackgroundColor(typedValue.data);
        }
        toolbar.setClipChildren(false);
    }

    public void destroy() {
        this.mDestroyed = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mIsAttachedToWindow = true;
        onUpdate();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mIsAttachedToWindow = false;
    }

    private Screen getScreen() {
        ViewParent parent = getParent();
        if (parent instanceof Screen) {
            return (Screen) parent;
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private ScreenStack getScreenStack() {
        Screen screen = getScreen();
        if (screen == null) {
            return null;
        }
        ScreenContainer container = screen.getContainer();
        if (container instanceof ScreenStack) {
            return (ScreenStack) container;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public ScreenStackFragment getScreenFragment() {
        ViewParent parent = getParent();
        if (!(parent instanceof Screen)) {
            return null;
        }
        ScreenFragment fragment = ((Screen) parent).getFragment();
        if (fragment instanceof ScreenStackFragment) {
            return (ScreenStackFragment) fragment;
        }
        return null;
    }

    public void onUpdate() {
        boolean z;
        AppCompatActivity appCompatActivity;
        Drawable navigationIcon;
        String str;
        Screen screen = (Screen) getParent();
        ScreenStack screenStack = getScreenStack();
        if (screenStack == null || screenStack.getTopScreen() == screen) {
            z = true;
        } else {
            z = false;
        }
        if (this.mIsAttachedToWindow && z && !this.mDestroyed && (appCompatActivity = (AppCompatActivity) getScreenFragment().getActivity()) != null) {
            if (Build.VERSION.SDK_INT >= 17 && (str = this.mDirection) != null) {
                if (str.equals("rtl")) {
                    this.mToolbar.setLayoutDirection(1);
                } else if (this.mDirection.equals("ltr")) {
                    this.mToolbar.setLayoutDirection(0);
                }
            }
            if (getScreenFragment() == null || !getScreenFragment().hasChildScreenWithConfig(getScreen())) {
                appCompatActivity.setRequestedOrientation(this.mScreenOrientation);
            }
            if (!this.mIsHidden) {
                if (this.mToolbar.getParent() == null) {
                    getScreenFragment().setToolbar(this.mToolbar);
                }
                if (this.mIsTopInsetEnabled) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        this.mToolbar.setPadding(0, getRootWindowInsets().getSystemWindowInsetTop(), 0, 0);
                    } else {
                        this.mToolbar.setPadding(0, (int) (getResources().getDisplayMetrics().density * 25.0f), 0, 0);
                    }
                } else if (this.mToolbar.getPaddingTop() > 0) {
                    this.mToolbar.setPadding(0, 0, 0, 0);
                }
                appCompatActivity.setSupportActionBar(this.mToolbar);
                ActionBar supportActionBar = appCompatActivity.getSupportActionBar();
                this.mToolbar.setContentInsetStartWithNavigation(this.mDefaultStartInsetWithNavigation);
                Toolbar toolbar = this.mToolbar;
                int i = this.mDefaultStartInset;
                toolbar.setContentInsetsRelative(i, i);
                supportActionBar.setDisplayHomeAsUpEnabled(getScreenFragment().canNavigateBack() && !this.mIsBackButtonHidden);
                this.mToolbar.setNavigationOnClickListener(this.mBackClickListener);
                getScreenFragment().setToolbarShadowHidden(this.mIsShadowHidden);
                getScreenFragment().setToolbarTranslucent(this.mIsTranslucent);
                supportActionBar.setTitle(this.mTitle);
                if (TextUtils.isEmpty(this.mTitle)) {
                    this.mToolbar.setContentInsetStartWithNavigation(0);
                }
                TextView titleTextView = getTitleTextView();
                int i2 = this.mTitleColor;
                if (i2 != 0) {
                    this.mToolbar.setTitleTextColor(i2);
                }
                if (titleTextView != null) {
                    if (this.mTitleFontFamily != null) {
                        titleTextView.setTypeface(ReactFontManager.getInstance().getTypeface(this.mTitleFontFamily, 0, getContext().getAssets()));
                    }
                    float f = this.mTitleFontSize;
                    if (f > 0.0f) {
                        titleTextView.setTextSize(f);
                    }
                }
                Integer num = this.mBackgroundColor;
                if (num != null) {
                    this.mToolbar.setBackgroundColor(num.intValue());
                }
                if (!(this.mTintColor == 0 || (navigationIcon = this.mToolbar.getNavigationIcon()) == null)) {
                    navigationIcon.setColorFilter(this.mTintColor, PorterDuff.Mode.SRC_ATOP);
                }
                for (int childCount = this.mToolbar.getChildCount() - 1; childCount >= 0; childCount--) {
                    if (this.mToolbar.getChildAt(childCount) instanceof ScreenStackHeaderSubview) {
                        this.mToolbar.removeViewAt(childCount);
                    }
                }
                int size = this.mConfigSubviews.size();
                for (int i3 = 0; i3 < size; i3++) {
                    ScreenStackHeaderSubview screenStackHeaderSubview = this.mConfigSubviews.get(i3);
                    ScreenStackHeaderSubview.Type type = screenStackHeaderSubview.getType();
                    if (type == ScreenStackHeaderSubview.Type.BACK) {
                        View childAt = screenStackHeaderSubview.getChildAt(0);
                        if (childAt instanceof ImageView) {
                            supportActionBar.setHomeAsUpIndicator(((ImageView) childAt).getDrawable());
                        } else {
                            throw new JSApplicationIllegalArgumentException("Back button header config view should have Image as first child");
                        }
                    } else {
                        Toolbar.LayoutParams layoutParams = new Toolbar.LayoutParams(-2, -1);
                        int i4 = AnonymousClass2.$SwitchMap$com$swmansion$rnscreens$ScreenStackHeaderSubview$Type[type.ordinal()];
                        if (i4 == 1) {
                            if (!this.mBackButtonInCustomView) {
                                this.mToolbar.setNavigationIcon((Drawable) null);
                            }
                            this.mToolbar.setTitle((CharSequence) null);
                            layoutParams.gravity = GravityCompat.START;
                        } else if (i4 == 2) {
                            layoutParams.gravity = GravityCompat.END;
                        } else if (i4 == 3) {
                            layoutParams.width = -1;
                            layoutParams.gravity = 1;
                            this.mToolbar.setTitle((CharSequence) null);
                        }
                        screenStackHeaderSubview.setLayoutParams(layoutParams);
                        this.mToolbar.addView(screenStackHeaderSubview);
                    }
                }
            } else if (this.mToolbar.getParent() != null) {
                getScreenFragment().removeToolbar();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.swmansion.rnscreens.ScreenStackHeaderConfig$2  reason: invalid class name */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$swmansion$rnscreens$ScreenStackHeaderSubview$Type;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|(3:5|6|8)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        static {
            int[] iArr = new int[ScreenStackHeaderSubview.Type.values().length];
            $SwitchMap$com$swmansion$rnscreens$ScreenStackHeaderSubview$Type = iArr;
            iArr[ScreenStackHeaderSubview.Type.LEFT.ordinal()] = 1;
            $SwitchMap$com$swmansion$rnscreens$ScreenStackHeaderSubview$Type[ScreenStackHeaderSubview.Type.RIGHT.ordinal()] = 2;
            try {
                $SwitchMap$com$swmansion$rnscreens$ScreenStackHeaderSubview$Type[ScreenStackHeaderSubview.Type.CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    private void maybeUpdate() {
        if (getParent() != null && !this.mDestroyed) {
            onUpdate();
        }
    }

    public Toolbar getToolbar() {
        return this.mToolbar;
    }

    public ScreenStackHeaderSubview getConfigSubview(int i) {
        return this.mConfigSubviews.get(i);
    }

    public int getConfigSubviewsCount() {
        return this.mConfigSubviews.size();
    }

    public void removeConfigSubview(int i) {
        this.mConfigSubviews.remove(i);
        maybeUpdate();
    }

    public void removeAllConfigSubviews() {
        this.mConfigSubviews.clear();
        maybeUpdate();
    }

    public void addConfigSubview(ScreenStackHeaderSubview screenStackHeaderSubview, int i) {
        this.mConfigSubviews.add(i, screenStackHeaderSubview);
        maybeUpdate();
    }

    private TextView getTitleTextView() {
        int childCount = this.mToolbar.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.mToolbar.getChildAt(i);
            if (childAt instanceof TextView) {
                TextView textView = (TextView) childAt;
                if (textView.getText().equals(this.mToolbar.getTitle())) {
                    return textView;
                }
            }
        }
        return null;
    }

    public int getScreenOrientation() {
        return this.mScreenOrientation;
    }

    public void setTitle(String str) {
        this.mTitle = str;
    }

    public void setTitleFontFamily(String str) {
        this.mTitleFontFamily = str;
    }

    public void setTitleFontSize(float f) {
        this.mTitleFontSize = f;
    }

    public void setTitleColor(int i) {
        this.mTitleColor = i;
    }

    public void setTintColor(int i) {
        this.mTintColor = i;
    }

    public void setTopInsetEnabled(boolean z) {
        this.mIsTopInsetEnabled = z;
    }

    public void setBackgroundColor(Integer num) {
        this.mBackgroundColor = num;
    }

    public void setHideShadow(boolean z) {
        this.mIsShadowHidden = z;
    }

    public void setHideBackButton(boolean z) {
        this.mIsBackButtonHidden = z;
    }

    public void setHidden(boolean z) {
        this.mIsHidden = z;
    }

    public void setTranslucent(boolean z) {
        this.mIsTranslucent = z;
    }

    public void setBackButtonInCustomView(boolean z) {
        this.mBackButtonInCustomView = z;
    }

    public void setDirection(String str) {
        this.mDirection = str;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void setScreenOrientation(String str) {
        char c;
        if (str == null) {
            this.mScreenOrientation = -1;
            return;
        }
        str.hashCode();
        switch (str.hashCode()) {
            case -1894896954:
                if (str.equals("portrait_down")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 96673:
                if (str.equals("all")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 729267099:
                if (str.equals("portrait")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 1430647483:
                if (str.equals("landscape")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1651658175:
                if (str.equals("portrait_up")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case 1730732811:
                if (str.equals("landscape_left")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case 2118770584:
                if (str.equals("landscape_right")) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                this.mScreenOrientation = 9;
                return;
            case 1:
                this.mScreenOrientation = 10;
                return;
            case 2:
                this.mScreenOrientation = 7;
                return;
            case 3:
                this.mScreenOrientation = 6;
                return;
            case 4:
                this.mScreenOrientation = 1;
                return;
            case 5:
                this.mScreenOrientation = 8;
                return;
            case 6:
                this.mScreenOrientation = 0;
                return;
            default:
                this.mScreenOrientation = -1;
                return;
        }
    }
}
