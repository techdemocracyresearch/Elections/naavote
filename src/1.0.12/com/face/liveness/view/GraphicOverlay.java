package com.face.liveness.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import java.util.HashSet;
import java.util.Set;

public class GraphicOverlay extends View {
    private int facing = 0;
    private Set<Graphic> graphics = new HashSet();
    private float heightScaleFactor = 1.0f;
    private final Object lock = new Object();
    private int previewHeight;
    private int previewWidth;
    private float widthScaleFactor = 1.0f;

    public static abstract class Graphic {
        private GraphicOverlay overlay;

        public abstract void draw(Canvas canvas);

        public Graphic(GraphicOverlay graphicOverlay) {
            this.overlay = graphicOverlay;
        }

        public float scaleX(float f) {
            return f * this.overlay.widthScaleFactor;
        }

        public float scaleY(float f) {
            return f * this.overlay.heightScaleFactor;
        }

        public Context getApplicationContext() {
            return this.overlay.getContext().getApplicationContext();
        }

        public float translateX(float f) {
            if (this.overlay.facing == 1) {
                return ((float) this.overlay.getWidth()) - scaleX(f);
            }
            return scaleX(f);
        }

        public float translateY(float f) {
            return scaleY(f);
        }

        public void postInvalidate() {
            this.overlay.postInvalidate();
        }
    }

    public GraphicOverlay(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void clear() {
        synchronized (this.lock) {
            this.graphics.clear();
        }
        postInvalidate();
    }

    public void add(Graphic graphic) {
        synchronized (this.lock) {
            this.graphics.add(graphic);
        }
        postInvalidate();
    }

    public void remove(Graphic graphic) {
        synchronized (this.lock) {
            this.graphics.remove(graphic);
        }
        postInvalidate();
    }

    public void setCameraInfo(int i, int i2, int i3) {
        synchronized (this.lock) {
            this.previewWidth = i;
            this.previewHeight = i2;
            this.facing = i3;
        }
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        synchronized (this.lock) {
            if (!(this.previewWidth == 0 || this.previewHeight == 0)) {
                this.widthScaleFactor = ((float) canvas.getWidth()) / ((float) this.previewWidth);
                this.heightScaleFactor = ((float) canvas.getHeight()) / ((float) this.previewHeight);
            }
            for (Graphic graphic : this.graphics) {
                graphic.draw(canvas);
            }
        }
    }
}
