package com.face.liveness.directApi;

public enum T5ImageOrientation {
    ORIENTATION_0,
    ORIENTATION_90,
    ORIENTATION_180,
    ORIENTATION_270
}
