package com.face.liveness.live;

import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import java.util.concurrent.ThreadLocalRandom;

public class Task {
    public Point destination;
    private final double maxY = 17.0d;
    private final double maxZ = 17.0d;
    private final double minY = -17.0d;
    private final double minZ = -17.0d;
    public Status status = Status.UNKNOWN;
    private final double step = 7.0d;

    public enum Status {
        UNKNOWN,
        LIVENESS_PASSED,
        LIVENESS_FAILED,
        PROCESSING
    }

    public Task() {
        defaultDestination();
    }

    public void init() {
        this.status = Status.PROCESSING;
        next();
    }

    public void next() {
        nextSimple();
    }

    private int randomSide(double d) {
        int nextInt = ThreadLocalRandom.current().nextInt(-1, 2);
        int nextInt2 = ThreadLocalRandom.current().nextInt(0, 2);
        if (nextInt != 0 || Math.abs(d) >= 0.001d) {
            if (((double) nextInt) * d <= 0.0d) {
                return nextInt;
            }
            if (nextInt2 == 0) {
                return ((int) Math.signum(d)) * -1;
            }
            return 0;
        } else if (nextInt2 == 0) {
            return -1;
        } else {
            return 1;
        }
    }

    public void nextSimple() {
        this.destination = new Point(Math.max(-17.0d, ((double) randomSide(this.destination.getY())) * 17.0d), Math.max(-17.0d, ((double) randomSide(this.destination.getZ())) * 17.0d));
    }

    public void nextDifficult() {
        double d;
        double d2;
        double d3 = -17.0d;
        double d4 = 17.0d;
        if (this.destination.getY() > 0.0d) {
            d = this.destination.getY() - 7.0d;
            d2 = -17.0d;
        } else {
            d2 = this.destination.getY() + 7.0d;
            d = 17.0d;
        }
        double nextDouble = ThreadLocalRandom.current().nextDouble(d2, d);
        if (this.destination.getZ() > 0.0d) {
            d4 = this.destination.getZ() - 7.0d;
        } else {
            d3 = this.destination.getZ() + 7.0d;
        }
        this.destination = new Point(nextDouble, ThreadLocalRandom.current().nextDouble(d3, d4));
    }

    public Boolean check(FirebaseVisionFace firebaseVisionFace, double d) {
        return Boolean.valueOf(this.destination.distanceLM(new Point((double) firebaseVisionFace.getHeadEulerAngleY(), (double) firebaseVisionFace.getHeadEulerAngleZ())) < d);
    }

    public void pass() {
        this.status = Status.LIVENESS_PASSED;
        defaultDestination();
    }

    public void fail() {
        this.status = Status.LIVENESS_FAILED;
        defaultDestination();
    }

    private void defaultDestination() {
        this.destination = new Point(24.0d, 24.0d);
    }
}
