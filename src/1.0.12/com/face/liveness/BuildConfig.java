package com.face.liveness;

public final class BuildConfig {
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String LIBRARY_PACKAGE_NAME = "com.face.liveness";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.6.17.1";
}
