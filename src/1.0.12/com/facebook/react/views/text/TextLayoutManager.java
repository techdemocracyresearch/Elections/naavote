package com.facebook.react.views.text;

import android.content.Context;
import android.os.Build;
import android.text.BoringLayout;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.LruCache;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableNativeMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.config.ReactFeatureFlags;
import com.facebook.react.uimanager.PixelUtil;
import com.facebook.react.uimanager.ReactAccessibilityDelegate;
import com.facebook.react.uimanager.ReactStylesDiffMap;
import com.facebook.react.uimanager.ViewProps;
import com.facebook.yoga.YogaConstants;
import com.facebook.yoga.YogaMeasureMode;
import com.facebook.yoga.YogaMeasureOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class TextLayoutManager {
    private static final boolean DEFAULT_INCLUDE_FONT_PADDING = true;
    private static final boolean ENABLE_MEASURE_LOGGING = false;
    private static final String INCLUDE_FONT_PADDING_KEY = "includeFontPadding";
    private static final String INLINE_VIEW_PLACEHOLDER = "0";
    private static final String MAXIMUM_NUMBER_OF_LINES_KEY = "maximumNumberOfLines";
    private static final String TAG = "TextLayoutManager";
    private static final String TEXT_BREAK_STRATEGY_KEY = "textBreakStrategy";
    private static final LruCache<String, Spannable> sSpannableCache = new LruCache<>(100);
    private static final Object sSpannableCacheLock = new Object();
    private static final LruCache<ReadableNativeMap, Spannable> sSpannableCacheV2 = new LruCache<>(100);
    private static final ConcurrentHashMap<Integer, Spannable> sTagToSpannableCache = new ConcurrentHashMap<>();
    private static final TextPaint sTextPaintInstance = new TextPaint(1);
    private static final int spannableCacheSize = 100;

    public static boolean isRTL(ReadableMap readableMap) {
        ReadableArray array = readableMap.getArray("fragments");
        if (array.size() <= 0 || new TextAttributeProps(new ReactStylesDiffMap(array.getMap(0).getMap("textAttributes"))).mLayoutDirection != 1) {
            return false;
        }
        return DEFAULT_INCLUDE_FONT_PADDING;
    }

    public static void setCachedSpannabledForTag(int i, Spannable spannable) {
        sTagToSpannableCache.put(Integer.valueOf(i), spannable);
    }

    public static void deleteCachedSpannableForTag(int i) {
        sTagToSpannableCache.remove(Integer.valueOf(i));
    }

    private static void buildSpannableFromFragment(Context context, ReadableArray readableArray, SpannableStringBuilder spannableStringBuilder, List<SetSpanOperation> list) {
        int i;
        int i2 = 0;
        for (int size = readableArray.size(); i2 < size; size = i) {
            ReadableMap map = readableArray.getMap(i2);
            int length = spannableStringBuilder.length();
            TextAttributeProps textAttributeProps = new TextAttributeProps(new ReactStylesDiffMap(map.getMap("textAttributes")));
            spannableStringBuilder.append((CharSequence) TextTransform.apply(map.getString("string"), textAttributeProps.mTextTransform));
            int length2 = spannableStringBuilder.length();
            int i3 = map.hasKey("reactTag") ? map.getInt("reactTag") : -1;
            if (map.hasKey(ViewProps.IS_ATTACHMENT) && map.getBoolean(ViewProps.IS_ATTACHMENT)) {
                list.add(new SetSpanOperation(spannableStringBuilder.length() - 1, spannableStringBuilder.length(), new TextInlineViewPlaceholderSpan(i3, (int) PixelUtil.toPixelFromSP(map.getDouble("width")), (int) PixelUtil.toPixelFromSP(map.getDouble("height")))));
            } else if (length2 >= length) {
                if (ReactAccessibilityDelegate.AccessibilityRole.LINK.equals(textAttributeProps.mAccessibilityRole)) {
                    list.add(new SetSpanOperation(length, length2, new ReactClickableSpan(i3, textAttributeProps.mColor)));
                } else if (textAttributeProps.mIsColorSet) {
                    list.add(new SetSpanOperation(length, length2, new ReactForegroundColorSpan(textAttributeProps.mColor)));
                }
                if (textAttributeProps.mIsBackgroundColorSet) {
                    list.add(new SetSpanOperation(length, length2, new ReactBackgroundColorSpan(textAttributeProps.mBackgroundColor)));
                }
                if (Build.VERSION.SDK_INT >= 21 && !Float.isNaN(textAttributeProps.getLetterSpacing())) {
                    list.add(new SetSpanOperation(length, length2, new CustomLetterSpacingSpan(textAttributeProps.getLetterSpacing())));
                }
                list.add(new SetSpanOperation(length, length2, new ReactAbsoluteSizeSpan(textAttributeProps.mFontSize)));
                if (textAttributeProps.mFontStyle == -1 && textAttributeProps.mFontWeight == -1 && textAttributeProps.mFontFamily == null) {
                    i = size;
                } else {
                    i = size;
                    list.add(new SetSpanOperation(length, length2, new CustomStyleSpan(textAttributeProps.mFontStyle, textAttributeProps.mFontWeight, textAttributeProps.mFontFeatureSettings, textAttributeProps.mFontFamily, context.getAssets())));
                }
                if (textAttributeProps.mIsUnderlineTextDecorationSet) {
                    list.add(new SetSpanOperation(length, length2, new ReactUnderlineSpan()));
                }
                if (textAttributeProps.mIsLineThroughTextDecorationSet) {
                    list.add(new SetSpanOperation(length, length2, new ReactStrikethroughSpan()));
                }
                if (!(textAttributeProps.mTextShadowOffsetDx == 0.0f && textAttributeProps.mTextShadowOffsetDy == 0.0f)) {
                    list.add(new SetSpanOperation(length, length2, new ShadowStyleSpan(textAttributeProps.mTextShadowOffsetDx, textAttributeProps.mTextShadowOffsetDy, textAttributeProps.mTextShadowRadius, textAttributeProps.mTextShadowColor)));
                }
                if (!Float.isNaN(textAttributeProps.getEffectiveLineHeight())) {
                    list.add(new SetSpanOperation(length, length2, new CustomLineHeightSpan(textAttributeProps.getEffectiveLineHeight())));
                }
                list.add(new SetSpanOperation(length, length2, new ReactTagSpan(i3)));
                i2++;
            }
            i = size;
            i2++;
        }
    }

    public static Spannable getOrCreateSpannableForText(Context context, ReadableMap readableMap, ReactTextViewManagerCallback reactTextViewManagerCallback) {
        String str = "";
        boolean z = ReactFeatureFlags.enableSpannableCacheByReadableNativeMapEquality;
        if (z) {
            synchronized (sSpannableCacheLock) {
                Spannable spannable = sSpannableCacheV2.get((ReadableNativeMap) readableMap);
                if (spannable != null) {
                    return spannable;
                }
            }
        } else {
            str = readableMap.toString();
            synchronized (sSpannableCacheLock) {
                Spannable spannable2 = sSpannableCache.get(str);
                if (spannable2 != null) {
                    return spannable2;
                }
            }
        }
        Spannable createSpannableFromAttributedString = createSpannableFromAttributedString(context, readableMap, reactTextViewManagerCallback);
        if (z) {
            synchronized (sSpannableCacheLock) {
                sSpannableCacheV2.put((ReadableNativeMap) readableMap, createSpannableFromAttributedString);
            }
        } else {
            synchronized (sSpannableCacheLock) {
                sSpannableCache.put(str, createSpannableFromAttributedString);
            }
        }
        return createSpannableFromAttributedString;
    }

    private static Spannable createSpannableFromAttributedString(Context context, ReadableMap readableMap, ReactTextViewManagerCallback reactTextViewManagerCallback) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        ArrayList<SetSpanOperation> arrayList = new ArrayList();
        buildSpannableFromFragment(context, readableMap.getArray("fragments"), spannableStringBuilder, arrayList);
        int i = 0;
        for (SetSpanOperation setSpanOperation : arrayList) {
            setSpanOperation.execute(spannableStringBuilder, i);
            i++;
        }
        if (reactTextViewManagerCallback != null) {
            reactTextViewManagerCallback.onPostProcessSpannable(spannableStringBuilder);
        }
        return spannableStringBuilder;
    }

    private static Layout createLayout(Spannable spannable, BoringLayout.Metrics metrics, float f, YogaMeasureMode yogaMeasureMode, boolean z, int i) {
        int length = spannable.length();
        boolean z2 = (yogaMeasureMode == YogaMeasureMode.UNDEFINED || f < 0.0f) ? DEFAULT_INCLUDE_FONT_PADDING : false;
        TextPaint textPaint = sTextPaintInstance;
        float desiredWidth = metrics == null ? Layout.getDesiredWidth(spannable, textPaint) : Float.NaN;
        if (metrics == null && (z2 || (!YogaConstants.isUndefined(desiredWidth) && desiredWidth <= f))) {
            int ceil = (int) Math.ceil((double) desiredWidth);
            if (Build.VERSION.SDK_INT < 23) {
                return new StaticLayout(spannable, textPaint, ceil, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, z);
            }
            return StaticLayout.Builder.obtain(spannable, 0, length, textPaint, ceil).setAlignment(Layout.Alignment.ALIGN_NORMAL).setLineSpacing(0.0f, 1.0f).setIncludePad(z).setBreakStrategy(i).setHyphenationFrequency(1).build();
        } else if (metrics != null && (z2 || ((float) metrics.width) <= f)) {
            return BoringLayout.make(spannable, textPaint, metrics.width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, metrics, z);
        } else {
            if (Build.VERSION.SDK_INT < 23) {
                return new StaticLayout(spannable, textPaint, (int) f, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, z);
            }
            StaticLayout.Builder hyphenationFrequency = StaticLayout.Builder.obtain(spannable, 0, length, textPaint, (int) f).setAlignment(Layout.Alignment.ALIGN_NORMAL).setLineSpacing(0.0f, 1.0f).setIncludePad(z).setBreakStrategy(i).setHyphenationFrequency(1);
            if (Build.VERSION.SDK_INT >= 28) {
                hyphenationFrequency.setUseLineSpacingFromFallbacks(DEFAULT_INCLUDE_FONT_PADDING);
            }
            return hyphenationFrequency.build();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a8, code lost:
        if (r11 > r21) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00bc, code lost:
        if (r1 > r23) goto L_0x00be;
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00c8  */
    public static long measureText(Context context, ReadableMap readableMap, ReadableMap readableMap2, float f, YogaMeasureMode yogaMeasureMode, float f2, YogaMeasureMode yogaMeasureMode2, ReactTextViewManagerCallback reactTextViewManagerCallback, float[] fArr) {
        Spannable spannable;
        int i;
        float f3;
        int i2;
        float f4;
        float f5;
        TextPaint textPaint = sTextPaintInstance;
        if (readableMap.hasKey("cacheId")) {
            int i3 = readableMap.getInt("cacheId");
            ConcurrentHashMap<Integer, Spannable> concurrentHashMap = sTagToSpannableCache;
            if (!concurrentHashMap.containsKey(Integer.valueOf(i3))) {
                return 0;
            }
            spannable = concurrentHashMap.get(Integer.valueOf(i3));
        } else {
            spannable = getOrCreateSpannableForText(context, readableMap, reactTextViewManagerCallback);
        }
        int textBreakStrategy = TextAttributeProps.getTextBreakStrategy(readableMap2.getString("textBreakStrategy"));
        boolean z = readableMap2.hasKey("includeFontPadding") ? readableMap2.getBoolean("includeFontPadding") : DEFAULT_INCLUDE_FONT_PADDING;
        if (spannable != null) {
            BoringLayout.Metrics isBoring = BoringLayout.isBoring(spannable, textPaint);
            if (isBoring == null) {
                Layout.getDesiredWidth(spannable, textPaint);
            }
            float f6 = 0.0f;
            if (yogaMeasureMode != YogaMeasureMode.UNDEFINED) {
                int i4 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
            }
            Layout createLayout = createLayout(spannable, isBoring, f, yogaMeasureMode, z, textBreakStrategy);
            int i5 = -1;
            int i6 = readableMap2.hasKey(MAXIMUM_NUMBER_OF_LINES_KEY) ? readableMap2.getInt(MAXIMUM_NUMBER_OF_LINES_KEY) : -1;
            if (i6 == -1 || i6 == 0) {
                i = createLayout.getLineCount();
            } else {
                i = Math.min(i6, createLayout.getLineCount());
            }
            if (yogaMeasureMode != YogaMeasureMode.EXACTLY) {
                for (int i7 = 0; i7 < i; i7++) {
                    float lineWidth = createLayout.getLineWidth(i7);
                    if (lineWidth > f6) {
                        f6 = lineWidth;
                    }
                }
                if (yogaMeasureMode == YogaMeasureMode.AT_MOST) {
                }
                if (yogaMeasureMode2 != YogaMeasureMode.EXACTLY) {
                    f3 = (float) createLayout.getLineBottom(i - 1);
                    if (yogaMeasureMode2 == YogaMeasureMode.AT_MOST) {
                    }
                    i2 = 0;
                    int i8 = 0;
                    while (i2 < spannable.length()) {
                        int nextSpanTransition = spannable.nextSpanTransition(i2, spannable.length(), TextInlineViewPlaceholderSpan.class);
                        TextInlineViewPlaceholderSpan[] textInlineViewPlaceholderSpanArr = (TextInlineViewPlaceholderSpan[]) spannable.getSpans(i2, nextSpanTransition, TextInlineViewPlaceholderSpan.class);
                        int length = textInlineViewPlaceholderSpanArr.length;
                        int i9 = 0;
                        while (i9 < length) {
                            TextInlineViewPlaceholderSpan textInlineViewPlaceholderSpan = textInlineViewPlaceholderSpanArr[i9];
                            int spanStart = spannable.getSpanStart(textInlineViewPlaceholderSpan);
                            int lineForOffset = createLayout.getLineForOffset(spanStart);
                            if (!(createLayout.getEllipsisCount(lineForOffset) > 0 ? DEFAULT_INCLUDE_FONT_PADDING : false) || spanStart < createLayout.getLineStart(lineForOffset) + createLayout.getEllipsisStart(lineForOffset) || spanStart >= createLayout.getLineEnd(lineForOffset)) {
                                float width = (float) textInlineViewPlaceholderSpan.getWidth();
                                float height = (float) textInlineViewPlaceholderSpan.getHeight();
                                boolean isRtlCharAt = createLayout.isRtlCharAt(spanStart);
                                boolean z2 = createLayout.getParagraphDirection(lineForOffset) == i5 ? DEFAULT_INCLUDE_FONT_PADDING : false;
                                if (spanStart == spannable.length() - 1) {
                                    f4 = z2 ? f6 - createLayout.getLineWidth(lineForOffset) : createLayout.getLineRight(lineForOffset) - width;
                                } else {
                                    if (z2 == isRtlCharAt ? DEFAULT_INCLUDE_FONT_PADDING : false) {
                                        f5 = createLayout.getPrimaryHorizontal(spanStart);
                                    } else {
                                        f5 = createLayout.getSecondaryHorizontal(spanStart);
                                    }
                                    if (z2) {
                                        f5 = f6 - (createLayout.getLineRight(lineForOffset) - f5);
                                    }
                                    f4 = isRtlCharAt ? f5 - width : f5;
                                }
                                int i10 = i8 * 2;
                                fArr[i10] = PixelUtil.toSPFromPixel(((float) createLayout.getLineBaseline(lineForOffset)) - height);
                                fArr[i10 + 1] = PixelUtil.toSPFromPixel(f4);
                                i8++;
                            }
                            i9++;
                            i5 = -1;
                        }
                        i2 = nextSpanTransition;
                    }
                    return YogaMeasureOutput.make(PixelUtil.toSPFromPixel(f6), PixelUtil.toSPFromPixel(f3));
                }
                f3 = f2;
                i2 = 0;
                int i82 = 0;
                while (i2 < spannable.length()) {
                }
                return YogaMeasureOutput.make(PixelUtil.toSPFromPixel(f6), PixelUtil.toSPFromPixel(f3));
            }
            f6 = f;
            if (yogaMeasureMode2 != YogaMeasureMode.EXACTLY) {
            }
            f3 = f2;
            i2 = 0;
            int i822 = 0;
            while (i2 < spannable.length()) {
            }
            return YogaMeasureOutput.make(PixelUtil.toSPFromPixel(f6), PixelUtil.toSPFromPixel(f3));
        }
        throw new IllegalStateException("Spannable element has not been prepared in onBeforeLayout");
    }

    public static WritableArray measureLines(Context context, ReadableMap readableMap, ReadableMap readableMap2, float f) {
        TextPaint textPaint = sTextPaintInstance;
        Spannable orCreateSpannableForText = getOrCreateSpannableForText(context, readableMap, null);
        return FontMetricsUtil.getFontMetrics(orCreateSpannableForText, createLayout(orCreateSpannableForText, BoringLayout.isBoring(orCreateSpannableForText, textPaint), f, YogaMeasureMode.EXACTLY, readableMap2.hasKey("includeFontPadding") ? readableMap2.getBoolean("includeFontPadding") : DEFAULT_INCLUDE_FONT_PADDING, TextAttributeProps.getTextBreakStrategy(readableMap2.getString("textBreakStrategy"))), textPaint, context);
    }

    public static class SetSpanOperation {
        protected int end;
        protected int start;
        protected ReactSpan what;

        public SetSpanOperation(int i, int i2, ReactSpan reactSpan) {
            this.start = i;
            this.end = i2;
            this.what = reactSpan;
        }

        public void execute(Spannable spannable, int i) {
            int i2 = this.start;
            spannable.setSpan(this.what, i2, this.end, ((i << 16) & 16711680) | ((i2 == 0 ? 18 : 34) & -16711681));
        }
    }
}
