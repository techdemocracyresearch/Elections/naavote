package com.toyberman;

import android.os.Build;
import android.util.Base64;
import androidx.core.app.NotificationCompat;
import com.bumptech.glide.load.Key;
import com.facebook.common.util.UriUtil;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.network.ForwardingCookieHandler;
import com.toyberman.Utils.OkHttpUtils;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.json.JSONException;

public class RNSslPinningModule extends ReactContextBaseJavaModule {
    private static final String DISABLE_ALL_SECURITY = "disableAllSecurity";
    private static final String KEY_NOT_ADDED_ERROR = "sslPinning key was not added";
    private static final String OPT_SSL_PINNING_KEY = "sslPinning";
    private static final String RESPONSE_TYPE = "responseType";
    private OkHttpClient client;
    private ForwardingCookieHandler cookieHandler;
    private CookieJar cookieJar = null;
    private final HashMap<String, List<Cookie>> cookieStore;
    private final ReactApplicationContext reactContext;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return "RNSslPinning";
    }

    public RNSslPinningModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        this.reactContext = reactApplicationContext;
        this.cookieStore = new HashMap<>();
        this.cookieHandler = new ForwardingCookieHandler(reactApplicationContext);
        this.cookieJar = new CookieJar() {
            /* class com.toyberman.RNSslPinningModule.AnonymousClass1 */

            @Override // okhttp3.CookieJar
            public synchronized void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                for (Cookie cookie : list) {
                    setCookie(httpUrl, cookie);
                }
            }

            @Override // okhttp3.CookieJar
            public synchronized List<Cookie> loadForRequest(HttpUrl httpUrl) {
                List<Cookie> list;
                list = (List) RNSslPinningModule.this.cookieStore.get(httpUrl.host());
                if (list == null) {
                    list = new ArrayList<>();
                }
                return list;
            }

            public void setCookie(HttpUrl httpUrl, Cookie cookie) {
                String host = httpUrl.host();
                List<Cookie> list = (List) RNSslPinningModule.this.cookieStore.get(host);
                if (list == null) {
                    list = new ArrayList<>();
                    RNSslPinningModule.this.cookieStore.put(host, list);
                }
                try {
                    putCookie(httpUrl, list, cookie);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            private void putCookie(HttpUrl httpUrl, List<Cookie> list, Cookie cookie) throws URISyntaxException, IOException {
                Cookie cookie2;
                HashMap hashMap = new HashMap();
                Iterator<Cookie> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        cookie2 = null;
                        break;
                    }
                    cookie2 = it.next();
                    if ((cookie2.name() + cookie2.path()).equals(cookie.name() + cookie.path())) {
                        break;
                    }
                }
                if (cookie2 != null) {
                    list.remove(cookie2);
                }
                list.add(cookie);
                hashMap.put("Set-cookie", Collections.singletonList(cookie.toString()));
                RNSslPinningModule.this.cookieHandler.put(httpUrl.uri(), hashMap);
            }
        };
    }

    public static String getDomainName(String str) throws URISyntaxException {
        String host = new URI(str).getHost();
        return host.startsWith("www.") ? host.substring(4) : host;
    }

    @ReactMethod
    public void getCookies(String str, Promise promise) {
        try {
            WritableNativeMap writableNativeMap = new WritableNativeMap();
            List<Cookie> list = this.cookieStore.get(getDomainName(str));
            if (list != null) {
                for (Cookie cookie : list) {
                    writableNativeMap.putString(cookie.name(), cookie.value());
                }
            }
            promise.resolve(writableNativeMap);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void removeCookieByName(String str, Promise promise) {
        for (String str2 : this.cookieStore.keySet()) {
            ArrayList arrayList = new ArrayList();
            List<Cookie> list = this.cookieStore.get(str2);
            if (list != null) {
                for (Cookie cookie : list) {
                    if (!cookie.name().equals(str)) {
                        arrayList.add(cookie);
                    }
                }
                this.cookieStore.put(str2, arrayList);
            }
        }
        promise.resolve(null);
    }

    @ReactMethod
    public void fetch(String str, final ReadableMap readableMap, final Callback callback) {
        String str2;
        final WritableMap createMap = Arguments.createMap();
        try {
            str2 = getDomainName(str);
        } catch (URISyntaxException unused) {
            str2 = str;
        }
        if (readableMap.hasKey(DISABLE_ALL_SECURITY) && readableMap.getBoolean(DISABLE_ALL_SECURITY)) {
            this.client = OkHttpUtils.buildDefaultOHttpClient(this.cookieJar, str2, readableMap);
        } else if (!readableMap.hasKey(OPT_SSL_PINNING_KEY)) {
            callback.invoke(new Throwable(KEY_NOT_ADDED_ERROR), null);
            return;
        } else if (readableMap.getMap(OPT_SSL_PINNING_KEY).hasKey("certs")) {
            ReadableArray array = readableMap.getMap(OPT_SSL_PINNING_KEY).getArray("certs");
            if (array == null || array.size() != 0) {
                this.client = OkHttpUtils.buildOkHttpClient(this.cookieJar, str2, array, readableMap);
            } else {
                throw new RuntimeException("certs array is empty");
            }
        } else {
            callback.invoke(new Throwable("key certs was not found"), null);
        }
        try {
            this.client.newCall(OkHttpUtils.buildRequest(this.reactContext, readableMap, str)).enqueue(new okhttp3.Callback() {
                /* class com.toyberman.RNSslPinningModule.AnonymousClass2 */

                @Override // okhttp3.Callback
                public void onFailure(Call call, IOException iOException) {
                    callback.invoke(iOException.getMessage());
                }

                @Override // okhttp3.Callback
                public void onResponse(Call call, Response response) throws IOException {
                    String str;
                    byte[] bytes = response.body().bytes();
                    String str2 = new String(bytes, Key.STRING_CHARSET_NAME);
                    WritableMap buildResponseHeaders = RNSslPinningModule.this.buildResponseHeaders(response);
                    createMap.putInt(NotificationCompat.CATEGORY_STATUS, response.code());
                    String string = readableMap.hasKey(RNSslPinningModule.RESPONSE_TYPE) ? readableMap.getString(RNSslPinningModule.RESPONSE_TYPE) : "";
                    string.hashCode();
                    if (!string.equals("base64")) {
                        createMap.putString("bodyString", str2);
                    } else {
                        if (Build.VERSION.SDK_INT < 26) {
                            str = Base64.encodeToString(bytes, 0);
                        } else {
                            str = java.util.Base64.getEncoder().encodeToString(bytes);
                        }
                        createMap.putString(UriUtil.DATA_SCHEME, str);
                    }
                    createMap.putMap("headers", buildResponseHeaders);
                    if (response.isSuccessful()) {
                        callback.invoke(null, createMap);
                    } else {
                        callback.invoke(createMap, null);
                    }
                }
            });
        } catch (JSONException e) {
            callback.invoke(e, null);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private WritableMap buildResponseHeaders(Response response) {
        Headers headers = response.headers();
        Set<String> names = headers.names();
        WritableMap createMap = Arguments.createMap();
        for (String str : names) {
            createMap.putString(str, headers.get(str));
        }
        return createMap;
    }
}
