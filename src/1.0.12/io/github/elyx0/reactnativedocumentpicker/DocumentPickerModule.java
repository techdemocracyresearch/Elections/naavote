package io.github.elyx0.reactnativedocumentpicker;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.brentvatne.react.ReactVideoViewManager;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;

public class DocumentPickerModule extends ReactContextBaseJavaModule {
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "ACTIVITY_DOES_NOT_EXIST";
    private static final String E_DOCUMENT_PICKER_CANCELED = "DOCUMENT_PICKER_CANCELED";
    private static final String E_FAILED_TO_SHOW_PICKER = "FAILED_TO_SHOW_PICKER";
    private static final String E_INVALID_DATA_RETURNED = "INVALID_DATA_RETURNED";
    private static final String E_UNABLE_TO_OPEN_FILE_TYPE = "UNABLE_TO_OPEN_FILE_TYPE";
    private static final String E_UNEXPECTED_EXCEPTION = "UNEXPECTED_EXCEPTION";
    private static final String E_UNKNOWN_ACTIVITY_RESULT = "UNKNOWN_ACTIVITY_RESULT";
    private static final String FIELD_FILE_COPY_URI = "fileCopyUri";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_SIZE = "size";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_URI = "uri";
    private static final String NAME = "RNDocumentPicker";
    private static final String OPTION_MULIPLE = "multiple";
    private static final String OPTION_TYPE = "type";
    private static final int READ_REQUEST_CODE = 41;
    private final ActivityEventListener activityEventListener;
    private Promise promise;

    @Override // com.facebook.react.bridge.NativeModule
    public String getName() {
        return NAME;
    }

    private String[] readableArrayToStringArray(ReadableArray readableArray) {
        int size = readableArray.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            strArr[i] = readableArray.getString(i);
        }
        return strArr;
    }

    public DocumentPickerModule(ReactApplicationContext reactApplicationContext) {
        super(reactApplicationContext);
        AnonymousClass1 r0 = new BaseActivityEventListener() {
            /* class io.github.elyx0.reactnativedocumentpicker.DocumentPickerModule.AnonymousClass1 */

            @Override // com.facebook.react.bridge.BaseActivityEventListener, com.facebook.react.bridge.ActivityEventListener
            public void onActivityResult(Activity activity, int i, int i2, Intent intent) {
                if (i == 41 && DocumentPickerModule.this.promise != null) {
                    DocumentPickerModule documentPickerModule = DocumentPickerModule.this;
                    documentPickerModule.onShowActivityResult(i2, intent, documentPickerModule.promise);
                    DocumentPickerModule.this.promise = null;
                }
            }
        };
        this.activityEventListener = r0;
        reactApplicationContext.addActivityEventListener(r0);
    }

    @Override // com.facebook.react.bridge.BaseJavaModule, com.facebook.react.bridge.NativeModule
    public void onCatalystInstanceDestroy() {
        super.onCatalystInstanceDestroy();
        getReactApplicationContext().removeActivityEventListener(this.activityEventListener);
    }

    @ReactMethod
    public void pick(ReadableMap readableMap, Promise promise2) {
        Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            promise2.reject(E_ACTIVITY_DOES_NOT_EXIST, "Current activity does not exist");
            return;
        }
        this.promise = promise2;
        try {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.addCategory("android.intent.category.OPENABLE");
            intent.setType("*/*");
            boolean z = false;
            if (!readableMap.isNull(ReactVideoViewManager.PROP_SRC_TYPE)) {
                ReadableArray array = readableMap.getArray(ReactVideoViewManager.PROP_SRC_TYPE);
                if (array == null || array.size() <= 1) {
                    if (array.size() == 1) {
                        intent.setType(array.getString(0));
                    }
                } else if (Build.VERSION.SDK_INT >= 19) {
                    intent.putExtra("android.intent.extra.MIME_TYPES", readableArrayToStringArray(array));
                } else {
                    Log.e(NAME, "Multiple type values not supported below API level 19");
                }
            }
            if (!readableMap.isNull(OPTION_MULIPLE) && readableMap.getBoolean(OPTION_MULIPLE)) {
                z = true;
            }
            if (Build.VERSION.SDK_INT >= 18) {
                intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", z);
            }
            if (Build.VERSION.SDK_INT < 19) {
                intent = Intent.createChooser(intent, null);
            }
            currentActivity.startActivityForResult(intent, 41, Bundle.EMPTY);
        } catch (ActivityNotFoundException e) {
            this.promise.reject(E_UNABLE_TO_OPEN_FILE_TYPE, e.getLocalizedMessage());
            this.promise = null;
        } catch (Exception e2) {
            e2.printStackTrace();
            this.promise.reject(E_FAILED_TO_SHOW_PICKER, e2.getLocalizedMessage());
            this.promise = null;
        }
    }

    public void onShowActivityResult(int i, Intent intent, Promise promise2) {
        ClipData clipData;
        if (i == 0) {
            promise2.reject(E_DOCUMENT_PICKER_CANCELED, "User canceled document picker");
        } else if (i == -1) {
            Uri uri = null;
            if (intent != null) {
                uri = intent.getData();
                clipData = intent.getClipData();
            } else {
                clipData = null;
            }
            try {
                WritableArray createArray = Arguments.createArray();
                if (uri != null) {
                    createArray.pushMap(getMetadata(uri));
                } else if (clipData == null || clipData.getItemCount() <= 0) {
                    promise2.reject(E_INVALID_DATA_RETURNED, "Invalid data returned by intent");
                    return;
                } else {
                    int itemCount = clipData.getItemCount();
                    for (int i2 = 0; i2 < itemCount; i2++) {
                        createArray.pushMap(getMetadata(clipData.getItemAt(i2).getUri()));
                    }
                }
                promise2.resolve(createArray);
            } catch (Exception e) {
                promise2.reject(E_UNEXPECTED_EXCEPTION, e.getLocalizedMessage(), e);
            }
        } else {
            promise2.reject(E_UNKNOWN_ACTIVITY_RESULT, "Unknown activity result: " + i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0080, code lost:
        if (r11 != null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0086, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0087, code lost:
        r0.addSuppressed(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008a, code lost:
        throw r1;
     */
    private WritableMap getMetadata(Uri uri) {
        WritableMap createMap = Arguments.createMap();
        createMap.putString("uri", uri.toString());
        createMap.putString(FIELD_FILE_COPY_URI, uri.toString());
        ContentResolver contentResolver = getReactApplicationContext().getContentResolver();
        createMap.putString(ReactVideoViewManager.PROP_SRC_TYPE, contentResolver.getType(uri));
        Cursor query = contentResolver.query(uri, null, null, null, null, null);
        if (query != null) {
            if (query.moveToFirst()) {
                int columnIndex = query.getColumnIndex("_display_name");
                if (!query.isNull(columnIndex)) {
                    createMap.putString(FIELD_NAME, query.getString(columnIndex));
                }
                if (Build.VERSION.SDK_INT >= 19) {
                    int columnIndex2 = query.getColumnIndex("mime_type");
                    if (!query.isNull(columnIndex2)) {
                        createMap.putString(ReactVideoViewManager.PROP_SRC_TYPE, query.getString(columnIndex2));
                    }
                }
                int columnIndex3 = query.getColumnIndex("_size");
                if (!query.isNull(columnIndex3)) {
                    createMap.putInt(FIELD_SIZE, query.getInt(columnIndex3));
                }
            }
        }
        if (query != null) {
            query.close();
        }
        return createMap;
    }
}
